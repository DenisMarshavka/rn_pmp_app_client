import React from 'react'
import Svg, {Image, Path, Defs, G, Circle} from 'react-native-svg'

export const HistorySvg = () => (
    <Svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="22px" height="22px" viewBox="0 0 64 64" enable-background="new 0 0 64 64">
        <Image id="image0" width="64" height="64" x="0" y="0"
               href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
                AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAACK1BMVEUAAACAgImAg4eBhIqA
                hYqAhYqBg4mBhYmAiIgA//+ChouAhIqAhIqAhIiAhImAg4mAg4mBhYmqqqqAhIiAhImAhImAg4mD
                g4mDg4qAhImAhImAhImAhYmCgoiAgICAg4mAhImAhImAhImAgIyChoqAhImAhImBhIiBhYmAhImA
                hImAhIiAhImAhImAg4mAhImAhImAgICAhIiBhYiAgI+AgICAg4eAg4mAhImAhImAgICAhIqAgICC
                goeAhImAg4mIiIiAhImAhImAhYqAh4eAhImAhIqAgICAhImAhImBhIqAhImAhYqAh4eAhImBg4qE
                hIyAhImAhImEhI6AgJKAhImAhImAhImAhIqAhImAgIqOjo6AhImAhImAhImAh4eGhoaBg4qAhImA
                g4mCgoeZmZmAhYuAhImAhImAhImAhIqBhYmSkpKAhImAhYmAhYiBhYmAhImAhYqAhImAhImAhImA
                hImBhYmAhImAhImAhYqAg4mBg4iAhIiAhYqAhImAhYiBhIiAhYuAhImBg4qAhImAhImAhImFhYWA
                hImAhImAhYqAhYiAhImBhYiAhIqHh4eAhImLi4uAg4iAg4mAhYiAg4uAhIqAhIqAhImAhImAg4mA
                hYmAhImDg4qBhYmAhYiAhImAhImAhoaAhIqAg4qCgouAg4mAhImAgI6JiYmAg4mAhImAhImAhIiA
                g4qDg4uAhImAhoaBhYmChoqAhYqEhI2AhIkAAACvAUSPAAAAt3RSTlMAGkBVYmZjQx4BOXy98fXE
                gkEDgd/mjCclnvr9qy8KgPj+ohQ74OpNd/vouaq3wuKNBJ1LEAxChNa0BrACNaTGD6bFMiSzvwj3
                alPuMCDPcR/v1BsOvvk0stwYCcPS7SITbdNQMwUu9uycensH2eNaRdvMm/zzbnW4lZiKabtk3shP
                LOtvzt3QF6D0ll7YR9cRkQuhVFxEiX5w6cDlNiNzWPCoKIdGN6eTEg2Gx7rkSiGZJnk9YB3w7uKD
                AAAAAWJLR0QAiAUdSAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAAd0SU1FB+QIAxAaHazJImkAAAM4
                SURBVFjD7Vf3Q9NAGI0MoSiWoTiKhDIrIEOGjFaQIUuwrCIoiiKKiAgoiCKiiFIUFBAF9xYnLjT/
                nul912Y0dz34Vd9Pyfvee7kvubskHPcfHrHGy9vHd623n79u5V5dwLr1gYITG/RBwSErsfuHbhTU
                2BS2mdW+Zes2QQuG8O0s9gg+UiDBGBXt0R8TK9AQF+/h3pl2yOUJ+sSknckpikGkplH8abskZWxY
                egamM7N2Z0uFnAiiPzfPdZ2cYLOyZtnjGlt+AcFv3uuUFBZplItLnOV9pdoBqbheVk6YeRWVWBGu
                KahwNr+f2GNVNdYc0Chaa6BWW0e5y/UNILI1utcOQqmpmaPh0GGQtbg9zHgoHDnK0dHaBMJjKl53
                HB5fm4JtPiGiXak8eQopOzKVdBvknlaynQ5O3dMZzfuoh9hohoCus0jbrdggio2I7OQYArhzMIQe
                OReKqMACpoCQXqSOknN9iDrPMQXgu9AhWy0ZFxDlxRhghYZlMzYdEf3tjAEcLO4BibiIiEGONeAS
                0l+WCNhHhpgDrsCqlohhRFxlDghG+hGJuIaI68wBo0h/QyJqEeHHHGBF+kjOpL11V7cqAorJAdoJ
                kh8CKsfcXotSCzzVDwGCcDNGFZCF6FuOQ7cxjMsXpH0C5pxxwq7xGBvQMU++vgON8ICE23yujL0j
                n0g8zS++LCbjoDR1VyLvKaYyT/OLmJ6BpSaUWDFTBeeul71Js38ZivKhHoTP16mXMx7DeCtHgG4y
                QaynzMJZGmzM92UCE+36CLO8TZjDxw/geg/ldd6DX8ToPD4ohR0tRfn65j35JSzAAB6pupxm9Vts
                yP84gtWgQvQIDKB8lf6IQvD3Eb877XlVFH/GE/Dbioj+bOHpM6L/eQuecmMUvxj/gjC+l6+w/7WO
                5nd0GKBRfDPonPINXaQBvHUtq8R3qs+894sGZ62wntij+YO0Mrtn4j9i+lPP5ympUEKdAQM2+fYQ
                9+Vr0qB+SU4Zh2ifuiIsvQINCd84Tyjly4h2A8PnvojvP7T/GAyLoyx2B+oW3BtZmv/JandAZ/k1
                XOMy9yfzy+aV2DHsy3O/fX28vf6swvuv4C+jm/6faSRnrAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAy
                MC0wOC0wM1QxNjoyNjoyOSswMDowMJPQegYAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjAtMDgtMDNU
                MTY6MjY6MjkrMDA6MDDijcK6AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAA
                AABJRU5ErkJggg=="
        />
</Svg>

);
