import React from 'react'
import Svg, { Path, G } from 'react-native-svg'

export const ChatSvg = () => (
    <Svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <G opacity="0.54">
            <Path d="M22 18.3636V5.63635L14.7273 12L22 18.3636Z" fill="#151C26"/>
            
            <Path d="M12.0001 14.726L9.98105 12.9091L2.90918 19.2727H21.091L14.0191 12.9091L12.0001 14.726Z" fill="#151C26"/>

            <Path d="M21.091 4.72726H2.90918L12.0001 12.9091L21.091 4.72726Z" fill="#151C26"/>

            <Path d="M2 5.63635V18.3636L9.27273 12L2 5.63635Z" fill="#151C26"/>
        </G>
    </Svg>
);
