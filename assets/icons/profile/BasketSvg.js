import React from "react";
import Svg, { Path, G, Circle } from "react-native-svg";

export const BasketSvg = ({ color = "#F46F22" }) => (
  <Svg
    width="26"
    height="24"
    viewBox="0 0 26 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <Path
      d="M1 1L4.83333 2.53333L9.81667 15.1833H20.9333L24.3833 5.98333H17.1H9.81667"
      stroke={color}
      stroke-width="2"
      stroke-linecap="round"
      stroke-linejoin="round"
    />

    <Path
      d="M9.81657 15.1833L7.8999 18.25H22.0832"
      stroke={color}
      stroke-width="2"
      stroke-linecap="round"
      stroke-linejoin="round"
    />

    <Circle cx="10.1999" cy="22.0834" r="1.91667" fill={color} />

    <Circle cx="20.1667" cy="22.0834" r="1.91667" fill={color} />
  </Svg>
);
