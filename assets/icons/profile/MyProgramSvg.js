import React from 'react'
import Svg, {Path, Circle, Defs} from 'react-native-svg'

export const MyProgramSvg = () => (
    <Svg
        xmlns="http://www.w3.org/2000/svg"
        id="svg8"
        version="1.1"
        fill="none"
        viewBox="0 0 24 24"
        height="24"
        width="24">
        <Defs
            id="defs12"/>
        <Circle
            id="circle2"
            strokeWidth="1.5"
            stroke="#81848A"
            r="8.25"
            cy="12"
            cx="12"/>
        <Path
            id="path4"
            strokeLinejoin="bevel"
            strokeLinecap="round"
            strokeWidth="5"
            stroke="white"
            d="M9 9L11.5 15L18 5.5"/>
        <Path
            id="path6"
            strokeLinejoin="round"
            strokeLinecap="round"
            strokeWidth="2.4"
            stroke="#81848A"
            d="M8 9.52174L11.8095 15L18 5"/>
    </Svg>

);
