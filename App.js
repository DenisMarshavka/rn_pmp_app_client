import React, { useState, useEffect, useRef } from "react";
import { enableScreens } from "react-native-screens";
import { useBackHandler } from "@react-native-community/hooks";
enableScreens();
import * as Sentry from "sentry-expo";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import { AppLoading, /*Notifications,*/ emitNotification } from "expo";
import { Asset } from "expo-asset";
import * as Notifications2 from "expo-notifications";
import axios from "axios";
import {
  Vibration,
  AsyncStorage,
  Platform,
  StatusBar,
  StyleSheet,
  View,
  Linking,
  YellowBox,
  Image,
  Dimensions,
} from "react-native";
import * as Font from "expo-font";

import ModalReview from "./components/ModalReview/index.js";
import configureStore from "./reducers";
import {
  setCurrentUser,
  getCurrentUser,
  setPushToken,
  // setNotificationOpened,
} from "./actions";
import AppNavigator from "./navigation/AppNavigator";
import {
  registerPushNotificationsAsync,
  getUserStorageData,
  handleNotificationOpen,
} from "./utils";
import LoadingScreen from "./screens/LoadingScreen";
import ErrorBoundary from "./hoc/ErrorBoundary";
import { getCurrentBasketState } from "./actions/BasketActions";

import NavigationService from "./utils/navigationService";

const store = configureStore();

Sentry.init({
  dsn:
    "https://4a40efc15e6c43deab746867789c0301@o429604.ingest.sentry.io/5376532",
  enableInExpoDevelopment: true,
  debug: true,
});

// const localNotification = {
//   id: 1,
//   title: "Testing",
//   body: "The body ",
//   data: { name: "This is the data" },
//   ios: {
//     sound: true,
//     _displayInForeground: true,
//   },
//   android: {
//     name: "Sound",
//     sound: true,
//     vibrate: [0, 250, 250, 250],
//     repeat: false,
//   },
// };
//

function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  const [_authLoad, _setAuthLoad] = useState(false);
  const [_auth, _setAuth] = useState(false);
  const [_pushToken, _setPushToken] = useState("");
  const [_isReady, _setIsReady] = useState(false);
  const [_notificationSubscription, _setNotificationSubscription] = useState(
    null
  );

  const [notification, setNotification] = useState(false);
  const [userData, setUserData] = useState({});

  const notificationListener = useRef();
  const responseListener = useRef();

  const registerForPushNotificationsAsync = async () => {
    const token = await registerPushNotificationsAsync();
    console.log("Token 1111- ", token);

    if (token && token.trim()) {
      _setPushToken(token);
      store.dispatch(setPushToken(token));
    }
  };

  const getNotificationsPermisionsAndSetUserData = () => {
    getUserStorageData().then((response) => {
      setUserData(response);
      // console.log("LLLLLLLLLLOOOOOOOOOOOOGGGGGGGGGGG User Data: ", userData);

      _setNotificationSubscription(
        Notifications2.addNotificationResponseReceivedListener((response) => {
          const data = response.notification.request.content.data;
          console.log("Received Data from Notification: ", data);

          if (data && data.body && Object.keys(data).length) {
            if (data.body && data.body.type) {
              handleNotificationOpen(data.body.type, data);

              // if (data.body.id && !isNaN(+data.body.id))
              //   NavigationService.navigate("OnBoardings", {
              //     trainer: {
              //       trainer_id: data.body.id,
              //       ...data.body,
              //     },
              //     currentUser: { ...userData },
              //   });
            }
          }
        })
      );
    });
  };

  useEffect(() => {
    if ((userData && !userData.notifications) || !userData) {
      Notifications2.dismissAllNotificationsAsync();
      if (_notificationSubscription) _notificationSubscription.remove();

      console.log("NOTY REMOVED");
    } else if (userData && userData.notifications) {
      Notifications2.setNotificationHandler({
        handleNotification: async () => ({
          shouldShowAlert: true,
          shouldPlaySound: true,
          shouldSetBadge: true,
        }),
      });

      registerForPushNotificationsAsync();
    }
  }, [userData]);

  useEffect(() => {
    console.log("YYYYEEEESSS STORE ", store, "PROPS ", props);

    store.dispatch(getCurrentBasketState(true, 0, 15, "desc", false));

    getNotificationsPermisionsAndSetUserData();

    // console.log("NavigationService", NavigationService.navigate("Profile"));

    return () => {
      if (userData && userData.notifications && _notificationSubscription)
        _notificationSubscription.remove();
    };
  }, []);

  const _handleNotification = async (notification) => {
    // const userData = await getUserStorageData();

    // console.log("UUUSSSERRR", userData);

    // if (userData && userData.notifications) {
    // alert("notifi on");
    // }

    console.log("notification", notification);
  };

  const getUserData = async () => {
    const data = await AsyncStorage.getItem("user");
    let currentUserData = null;

    if (data !== null) currentUserData = JSON.parse(data);

    return currentUserData;
  };

  const getUser = async () => {
    const data = await getUserData();

    console.log(">>>>> USER DATA", data);
    if (data !== null) {
      console.log("OKKKAY");
      store.dispatch(setCurrentUser(data));

      setTimeout(() => {
        _setAuthLoad(true);
      }, 3500);

      _setAuth(true);
    } else {
      setTimeout(() => {
        _setAuthLoad(true);
      }, 3500);

      _setAuth(false);
    }
  };

  useEffect(() => {
    getUser();

    YellowBox.ignoreWarnings(["Animated: `useNativeDriver`", "Please report:"]);
    // return () => {
    //   isReadyRef.current = false;
    // };
  }, []);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <ErrorBoundary>
        <Provider store={store}>
          {_auth ? <ModalReview /> : null}

          <View
            style={{
              ...styles.container /*, flex: 1, position: "relative"*/,
            }}
          >
            <StatusBar
              barStyle={
                Platform.OS === "ios" ? "dark-content" : "light-content"
              }
            />

            <AppNavigator auth={_auth} load={_authLoad} />
          </View>
        </Provider>
      </ErrorBoundary>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([require("./assets/images/splash_loading.png")]),
    Font.loadAsync({
      PFDin200: require("./assets/fonts/PFDinDisplayPro-Thin.ttf"),
      PFDin300: require("./assets/fonts/PFDinDisplayPro-Light.ttf"),
      PFDin300B: require("./assets/fonts/PFDinDisplayPro-Black.ttf"),
      PFDin400: require("./assets/fonts/PFDinDisplayPro-Regular.ttf"),
      CompanyRegul: require("./assets/fonts/CompPro-Regular.ttf"),
      CompanyLight: require("./assets/fonts/CompPro-Light.ttf"),
      PFDin500: require("./assets/fonts/PFDinDisplayPro-Medium.ttf"),
      PFDin700: require("./assets/fonts/PFDinDisplayPro-Bold.ttf"),
      BadScript400: require("./assets/fonts/BadScript-Regular.ttf"),
    }),
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
  Sentry.captureException(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});
