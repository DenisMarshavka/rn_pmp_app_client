import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  Easing,
  TouchableOpacity,
} from "react-native";
// import { TouchableOpacity } from 'react-native-gesture-handler';
import { fonts } from "../../constants/theme";

// TODO: Delete this compoennt
export const DropDownHeaderProgram = ({
  title,
  heightTitle,
  children,
  parentCallback,
}) => {
  const icons = {
    up: require("../../assets/icons/drop_down_arrow_up.png"),
    down: require("../../assets/icons/drop_down_arrow_down_program.png"),
  };
  const [animation] = useState(new Animated.Value(heightTitle));
  const [expanded, setExpanded] = useState(true);
  const [maxHeight, setMaxHeight] = useState(0);
  const [minHeight, setMinHeight] = useState(0);

  const toggle = () => {
    Animated.timing(animation, {
      toValue: expanded ? maxHeight : minHeight,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => {
      setExpanded(!expanded);
    });
  };

  const _setMaxHeight = (event) => {
    setMaxHeight(event.nativeEvent.layout.height);
  };

  const _setMinHeight = (event) => {
    animation.setValue(event.nativeEvent.layout.height);
    setMinHeight(event.nativeEvent.layout.height);
  };

  const sendExpanded = () => {
    parentCallback(expanded);
  };

  useEffect(() => {
    sendExpanded();
  }, [expanded]);

  return (
    <Animated.View style={{ height: animation }}>
      <View onLayout={_setMinHeight}>
        {/*<TouchableOpacity*/}
        {/*    activeOpacity={0.8}*/}
        {/*    onPress={() => toggle()}>*/}
        {/*    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>*/}
        {/*        <Text style={[fonts.medium, {fontSize: 18, marginBottom: 33}]}>{title}</Text>*/}
        {/*        <Image source={expanded ? icons['up'] : icons['down']} />*/}
        {/*    </View>*/}
        {/*    */}
        {/*</TouchableOpacity>*/}
      </View>
      <View style={styles.body} onLayout={_setMaxHeight}>
        {children}
      </View>
    </Animated.View>
  );
};

const styles = StyleSheet.create({});
