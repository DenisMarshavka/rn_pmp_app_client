import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { colors, fonts, sizes, fontSize24 } from "../constants/theme";

export const ListScheduleItem = ({ title, date, icon }) => {
  const getIcon = (type) => {
    if (type === 0 || type === 1) {
      return (
        <Image
          source={require("../assets/images/icons/icon-type-fitness.png")}
          style={styles.icon}
        />
      );
    } else {
      return (
        <Image
          source={require("../assets/images/icons/icon-type-beauty.png")}
          style={styles.icon}
        />
      );
    }
  };

  return (
    <LinearGradient
      start={{ x: 1, y: 1 }}
      end={{ x: 0.01, y: 0.01 }}
      colors={["rgba(218, 218, 218, 0.1)", colors.gray]}
      style={styles.gradient}
    >
      <View style={styles.container_line}>
        <View style={{ width: "70%" }}>
          <Text
            numberOfLines={8}
            ellipsizeMode="tail"
            style={[{ color: "#1E2022" }, fonts.normal, fonts.h3]}
          >
            {title}
          </Text>
          <Text
            style={[
              { color: "rgba(30, 32, 34, 0.54)", fontSize: 14, paddingTop: 3 },
              fonts.normal,
            ]}
          >
            {date}
          </Text>
        </View>
        <View>{getIcon(icon)}</View>
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  gradient: {
    marginBottom: 8,
    borderRadius: 10,
  },
  container_line: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: sizes.margin,
    paddingRight: 21,
    paddingTop: 20,
    paddingBottom: 14,
    alignItems: "center",
  },
  icon: {
    width: 24,
    height: 24,
  },
});
