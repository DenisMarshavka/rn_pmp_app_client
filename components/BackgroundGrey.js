import React from "react";
import { Dimensions, View, ScrollView, StyleSheet } from "react-native";

const { height } = Dimensions.get("window");

export const BackgroundGrey = ({ children }) => {
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.background}>{children}</View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  background: {
    width: "100%",
    height,
    backgroundColor: "#FAFAFA"
  }
});
