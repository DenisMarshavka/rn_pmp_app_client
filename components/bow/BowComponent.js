import React from "react";
import { Image, StyleSheet, View, Dimensions } from "react-native";

export const WINDOW_WIDTH = Dimensions.get("window").width;
export const WINDOW_HEIGHT = Dimensions.get("window").height;

export default function BowComponent() {
  return (
    <View>
      <Image
        resizeMode="stretch"
        style={styles.gradientLine}
        source={require("../../assets/images/GradientLine.png")}
      />
      <Image
        style={styles.bow}
        source={require("../../assets/images/bow.png")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  gradientLine: {
    position: "absolute",
    width: WINDOW_WIDTH / 4,
    height: WINDOW_WIDTH / 4,
  },
  bow: {
    position: "absolute",
    width: WINDOW_WIDTH / 3.2,
    height: WINDOW_WIDTH / 3.2,
  },
});
