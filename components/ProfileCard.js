import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import * as Animatable from "react-native-animatable";

import { colors, fonts, sizes } from "../constants/theme";
import SkeletonLoader from "./loaders/SkeletonLoader";
import { validURL } from "../utils";
import { NoAvatarTrainerSvg } from "../assets/icons/NoAvatarTrainerSvg/NoAvatarTrainerSvg";

const ProfileCard = (props) => {
  const {
    item = {},
    isLoading = false,
    isExpert = false,
    avatar = "",
    fullname = "",
    id = null,
    recomendations = "",
    about = "",
    index = 0,
  } = props;
  const Wrap = !isLoading || (item && item.isTest) ? Animatable.View : View;

  return (
    <Wrap
      duration={250}
      delay={index && index < 15 ? index * 250 : 400}
      animation="fadeInUp"
    >
      <TouchableOpacity
        onPress={() => {
          !isLoading
            ? props.navigation.navigate("ItemSpecialist", {
                staff: { avatar, fullname, id, recomendations, about },
                byExpert: isExpert,
                tiffany: props.tiffany,
              })
            : null;
        }}
        activeOpacity={!isLoading ? 0.6 : 1}
      >
        <LinearGradient
          start={{ x: 1.2, y: 1.2 }}
          end={{ x: 0.1, y: 0.1 }}
          colors={[colors.white, colors.gray]}
          style={styles.gradient}
        >
          <View style={styles.container}>
            <SkeletonLoader
              layout={[{ ...styles.avatar }]}
              containerStyle={{ marginRight: 17 }}
              isLoading={isLoading}
            >
              <View style={styles.iconContainer}>
                {!props.avatar || !validURL(props.avatar) ? (
                  <NoAvatarTrainerSvg />
                ) : (
                  <Image source={{ uri: props.avatar }} style={styles.avatar} />
                )}
              </View>
            </SkeletonLoader>

            <SkeletonLoader
              layout={[
                { flex: 1, height: 20, width: 90, marginBottom: 10 },
                { flex: 1, height: 17, width: 180, marginBottom: 10 },
                { flex: 1, height: 30, width: 200 },
              ]}
              containerStyle={{ flex: 1 }}
              isLoading={isLoading}
            >
              <Text numberOfLines={1} ellipsizeMode="tail" style={styles.name}>
                {props.fullname || "Неизвестный"}
              </Text>

              <Text
                numberOfLines={1}
                ellipsizeMode="tail"
                style={styles.position}
              >
                {props.recomendations || ""}
              </Text>

              <Text
                numberOfLines={2}
                ellipsizeMode="tail"
                style={styles.description}
              >
                {props.about || ""}
              </Text>
            </SkeletonLoader>
          </View>
        </LinearGradient>
      </TouchableOpacity>
    </Wrap>
  );
};

export default ProfileCard;

const styles = StyleSheet.create({
  gradient: {
    marginBottom: 10,
    borderRadius: 10,
    minHeight: 102,
  },
  container: {
    flex: 1,
    paddingHorizontal: 9,
    paddingVertical: 16,
    flexDirection: "row",
    alignItems: "center",
  },
  iconContainer: {
    width: 52,
    height: 52,
    borderRadius: 50,
    backgroundColor: "#C4C4C4",
    alignItems: "center",
    justifyContent: "center",
  },
  avatar: {
    width: 52,
    height: 52,
    borderRadius: 52,
  },
  info: {
    paddingLeft: 16,
    flex: 1,
  },
  name: {
    color: colors.orange,
    ...fonts.h3,
    ...fonts.normal,
    alignSelf: "flex-start",
  },
  position: {
    flex: 1,
    paddingVertical: 5,
    ...fonts.description,
    ...fonts.normal,
    color: "#151C26",
    opacity: 0.38,
  },
  description: {
    flex: 1,
    fontSize: 12,
    ...fonts.normal,
    color: "#151C26",
    opacity: 0.38,
  },
});
