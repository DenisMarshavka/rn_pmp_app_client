import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  SafeAreaView,
  Platform,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { connect } from "react-redux";
// import Svg, {Path} from 'react-native-svg'

import back from "../../assets/icons/back.png";
import tick from "../../assets/images/tick.png";
import tickTiffany from "../../assets/images/tick-tiffany.png";
import { BasketSvg } from "../../assets/icons/profile/BasketSvg";
import BasketState from "../../components/basketState";

import { fonts, sizes } from "../../constants/theme";

const Header = (props) => {
  const navigation = useNavigation();
  const {
    backTitle = false,
    isSvgTickIcon = false,
    SvgIcon = false,
    backStyle = {},
    style = {},
    basketCount = 0,
    offsetTopIconBack = 0,
    routeName = "Home",
    receivedParams = {},
    basketIconStyle = {},
    basketShow = false,
    styleHeadTitle = {},
    styleCount = {},
  } = props;

  const isAndroid = Platform.OS !== "ios";
  const isSpecialists =
    props.isSpecialists ||
    (props.route && props.route.params && props.route.params.isSpecialists) ||
    props.tiffany;

  const icon = props.tick
    ? props.tiffany
      ? tickTiffany
      : tick
    : props.customIcon
    ? props.customIcon
    : null;

  const paramsToCloseMenu = { ...receivedParams, ...props, isSpecialists };

  return (
    <SafeAreaView>
      <View style={[styles.container, { ...style }]}>
        <TouchableOpacity
          style={{
            padding: 15,
            zIndex: 1,
            flexDirection: "row",
            alignItems: "center",
            ...backStyle,
          }}
          onPress={() => navigation.goBack()}
        >
          <Image
            source={back}
            style={{
              width: 9,
              height: 15,
              marginRight: backTitle && isAndroid ? 16 : 0,
              marginTop: offsetTopIconBack,
            }}
          />

          {backTitle && isAndroid ? (
            <Text style={{ color: "#000", fontSize: 13 }}>
              {typeof backTitle === "string" && backTitle.trim()
                ? backTitle.trim()
                : "Назад"}
            </Text>
          ) : null}
        </TouchableOpacity>

        <View style={styles.content}>
          <Text
            style={[
              fonts.normal,
              { fontSize: 18, color: "#151C26", ...styleHeadTitle },
            ]}
          >
            {props.title}
          </Text>
        </View>

        <View
          style={{ opacity: props.disabled ? 0.2 : 1, flexDirection: "row" }}
        >
          {basketShow ? (
            <BasketState
              styleCount={styleCount}
              count={basketCount}
              style={{ top: 2, ...basketIconStyle }}
              tiffany={props.tiffany}
              navigation={navigation}
            />
          ) : null}

          {!icon && !props.hide ? (
            <TouchableOpacity
              style={{ padding: 15, paddingRight: 6, zIndex: 1 }}
              onPress={() =>
                navigation.navigate("MainMenu", {
                  routeName,
                  receivedParams: paramsToCloseMenu,
                })
              }
            >
              <Image
                source={
                  props.tiffany || isSpecialists
                    ? require(`../../assets/images/menu-tiffany.png`)
                    : require(`../../assets/images/menuIcon.png`)
                }
                style={styles.back}
              />
            </TouchableOpacity>
          ) : icon ? (
            <TouchableOpacity
              activeOpacity={props.disabled ? 1 : 0.5}
              style={{
                padding: 15,
                paddingRight: 0,
                zIndex: 1,
                alignItems: "center",
                justifyContent: "flex-end",
              }}
              onPress={props.disabled ? null : () => props.handleclick()}
            >
              {!isSvgTickIcon ? (
                <Image source={icon} style={styles.tick} />
              ) : (
                SvgIcon
              )}
            </TouchableOpacity>
          ) : (
            <View />
          )}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default connect((state) => ({
  basketCount: state.currentUser.basketCount,
}))(Header);

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 55,
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    paddingRight: sizes.margin,
    justifyContent: "space-between",
  },
  content: {
    position: "absolute",
    left: 0,
    right: 0,
    display: "flex",
    alignItems: "center",
  },
  back: {
    height: 22,
    width: 22,
    resizeMode: "contain",
  },
  tick: {
    width: 24,
    //height: 18,
    resizeMode: "contain",
  },
});
