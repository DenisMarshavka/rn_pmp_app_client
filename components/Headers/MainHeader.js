import React, { Component } from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image, ScrollView} from 'react-native';

import back from "../../assets/icons/back-2.png";
import arrowDown from "../../assets/images/arrowDown.png";
import hamburger from "../../assets/images/menuIcon.png";
import { fonts, sizes, colors } from "../../constants/theme";
import {connect} from 'react-redux';
import {updateSalons, updateActiveSalon} from '../../reducers/SalonsReducer';
import { SalonsAPI } from '../../api/salons/';

class MainHeader extends Component{

  constructor(){
    super();
    this.state = {
      status: false
    }
  }

  componentDidMount(){
    SalonsAPI.getSalons()
    .then(res => {
      if(res.success) {
        this.props.updateSalons(res.data);
      }
    })
    .catch(res => {
      // console.log(res)
    })
  }

  openSalons = (key, network, companies) => {
    this.setState({status: this.state.status ? false : true});
    this.props.updateActiveSalon(key);
    this.props.navigation.navigate("FitnessStudio", { network: network, companies: companies})
  }

  render(){
    return(
      <>
      <View style={styles.container}>
           {this.props.back && <TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
              <Image source={back} style={{ width: 9, height: 15}} />
           </TouchableOpacity>}
          <TouchableOpacity style={styles.select} onPress={() => this.setState({status: this.state.status ? false : true})}>
              <View style={styles.content}>
                  {this.props.salons.length !== 0 && <><Text style={[{fontSize: 16, paddingRight: 6, textTransform: 'uppercase', color: '#151C26', opacity: this.props.textColor === 'light' ? 0.4 : 1}]}>
                      {this.props.salons[this.props.activeSalon].title}
                  </Text>
                  <Image source={arrowDown} style={{ width: 13, height: 8, resizeMode: 'contain', transform: [{ rotate: this.state.status  ? '180deg' : '0deg'}]}}/></>}
              </View>
          </TouchableOpacity>
           <TouchableOpacity style={{paddingRight: 6}} onPress={() => this.props.navigation.navigate('MainMenu')}>
              <Image source={hamburger} style={styles.hamburger} />
           </TouchableOpacity>
      </View>
      {this.state.status && <View style={styles.contentTitle}>
      <ScrollView>
        {
           this.props.salons.map((item, index) => {
            if(item.companies.length !== 0) {
              return (
                <View key={index} style={{ flexDirection: "row", alignItems: "center", paddingHorizontal: 18, paddingVertical: 18}}>
                  <Image
                    source={require("../../assets/icons/fitness.png")}
                    style={{width: 15, height: 21, resizeMode: 'contain', marginRight: 21}}
                  />
                  <TouchableOpacity
                    onPress={() => this.openSalons(index, item.id, item.companies)}
                  >
                    <Text
                     ellipsizeMode={'tail'}
                     numberOfLines={1}
                      style={[{ color: colors.white, fontSize: fonts.base, paddingRight: 10 }, fonts.normal, fonts.upper]}
                    >
                      { item.title }
                    </Text>
                  </TouchableOpacity>
                </View>
              )
            }
          })
        }
             </ScrollView>
        </View>
      }
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    activeSalon: state.salonsReducer.activeSalon,
    salons: state.salonsReducer.salons
  }
}

export default connect(mapStateToProps, {updateSalons, updateActiveSalon})(MainHeader);

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 55,
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: sizes.margin,
    },
    back: {
      width: 20,
      height: 50,
      display: 'flex',
      justifyContent: 'center',
    },
    select: {
        flexGrow: 1
    },
    content: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
    },
    hamburger: {
        width: 20,
        height: 13.48,
        resizeMode: 'contain'
      },
      contentTitle: {
        backgroundColor: colors.orange,
        height: 220,
      },
});

