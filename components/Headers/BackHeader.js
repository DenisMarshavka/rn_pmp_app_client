import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
} from "react-native";
import { useNavigation, CommonActions } from "@react-navigation/native";
// import Svg, {Path} from 'react-native-svg'

import back from "../../assets/icons/back.png";
import { fonts, sizes, colors } from "../../constants/theme";

const BackHeader = (props) => {
  const navigation = useNavigation();

  const {
    backTitle = false,
    onBack = false,
    routeName = "Home",
    receivedParams = {},
    offsetTopIconBack = 0,
  } = props;

  const isAndroid = Platform.OS !== "ios";

  return (
    <>
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.3}
          style={{ padding: 15, flexDirection: "row", alignItems: "center" }}
          onPress={() => {
            onBack ? onBack() : navigation.dispatch(CommonActions.goBack());
          }}
        >
          <Image
            source={back}
            style={{
              width: 9,
              height: 15,
              marginRight: backTitle && isAndroid ? 16 : 0,
              marginTop: offsetTopIconBack,
            }}
          />

          {backTitle && isAndroid ? (
            <Text style={{ color: "#000", fontSize: 13 }}>
              {typeof backTitle === "string" && backTitle.trim()
                ? backTitle.trim()
                : "Назад"}
            </Text>
          ) : null}
        </TouchableOpacity>

        {!props.hide && (
          <TouchableOpacity
            style={{ padding: 15, paddingRight: 6, zIndex: 1 }}
            onPress={() =>
              navigation.navigate("MainMenu", { routeName, receivedParams })
            }
          >
            <Image
              source={
                props.tiffany
                  ? require(`../../assets/images/menu-tiffany.png`)
                  : require(`../../assets/images/menuIcon.png`)
              }
              style={styles.back}
            />
          </TouchableOpacity>
        )}
      </View>

      {props.title && (
        <View>
          <Text style={styles.title}>{props.title}</Text>
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    position: "relative",
    zIndex: -1,
    width: "100%",
    height: 55,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingRight: sizes.margin,
  },
  back: {
    width: 20,
    height: 14,
    resizeMode: "contain",
  },
  title: {
    color: colors.title,
    ...fonts.normal,
    ...fonts.title,
    paddingTop: 10,
    paddingBottom: 24,
    paddingHorizontal: 16,
  },
});

export default BackHeader;
