import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import { useNavigation } from "@react-navigation/native";

import hamburger from "../../assets/images/menuIcon.png";
import { fonts, sizes } from "../../constants/theme";

const TitleHeader = (props) => {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Text
          style={[
            fonts.normal,
            { fontSize: 28, paddingRight: 6, color: "#151C26" },
          ]}
        >
          {props.title}
        </Text>
      </View>

      <TouchableOpacity
        style={{ padding: 20 }}
        onPress={() => navigation.goBack()}
      >
        <Image source={hamburger} style={styles.hamburger} />
      </TouchableOpacity>
    </View>
  );
};

export default TitleHeader;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 55,
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    paddingLeft: sizes.margin,
  },
  select: {
    flexGrow: 1,
  },
  content: {
    flexGrow: 1,
    alignItems: "center",
    display: "flex",
    flexDirection: "row",
  },
  hamburger: {
    width: 20,
    height: 13.48,
    resizeMode: "contain",
  },
});
