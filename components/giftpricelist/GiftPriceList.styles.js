import { StyleSheet, Dimensions } from "react-native";

import {
  fontSize18,
  WINDOW_HEIGHT,
  fonts,
} from '../../constants/theme';

const styles = StyleSheet.create({
    renderItemContainer: {
      paddingTop: 32,
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    priceContainer: {
      borderBottomColor: 'rgba(0,0,0,0.12)',
      borderBottomWidth: 1,
      flex: 6,
      height: WINDOW_HEIGHT / 15,
    },
    price: {
      fontSize: fontSize18,
      fontFamily: fonts.normal.fontFamily,
    },
    iconContainer: {
      flex: 1,
      height: WINDOW_HEIGHT / 15,
      paddingTop: 1,
    },
    selectPriceTitleContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingTop: 36,
    },
    selectPriceTitle: {
      fontSize: fontSize18,
      fontFamily: fonts.normal.fontFamily,
      color: '#000000',
      lineHeight: 22,
    },
  });


  export default styles;
