import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";

import styles from "./GiftPriceList.styles";
import { formattingPrice } from "./../../utils";

class GiftPriceList extends Component {
  state = {
    showPricesList: false,
    selectedPrice: undefined,
  };

  giftPrices = [
    { price: 5000 },
    { price: 10000 },
    { price: 15000 },
    { price: 20000 },
    { price: 30000 },
    { price: 50000 },
  ];

  selectPrice = (item) => {
    this.setState({ selectedPrice: item });
    if (this.props.onSelect) {
      this.props.onSelect(item);
    }
  };

  listPrices = this.giftPrices.map((item, index) => {
    return (
      <View>
        <TouchableOpacity
          key={item.price}
          onPress={() => {
            this.selectPrice(item.price);
          }}
          style={styles.renderItemContainer}
        >
          <View key={item.price} style={styles.iconContainer}>
            {this.state.selectedPrice === item.price ? (
              <Icon
                type="material-community"
                name={"radiobox-marked"}
                size={18}
                color={"#FF9D04"}
              />
            ) : (
              <Icon
                type="material-community"
                name={"radiobox-blank"}
                size={18}
                color={"rgba(6,6,12,0.12)"}
              />
            )}
          </View>

          <View style={styles.priceContainer}>
            <Text style={styles.price}>{`${formattingPrice(
              item.price
            )} ₽`}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  });

  showPricesListFunction = () => {
    this.setState({ showPricesList: !this.state.showPricesList });
  };

  render() {
    const { showPricesList } = this.state;
    return (
      <View style={{ paddingBottom: 32 }}>
        <TouchableOpacity
          onPress={() => this.showPricesListFunction()}
          style={styles.selectPriceTitleContainer}
        >
          <Text style={styles.selectPriceTitle}>Выберите номинал</Text>
          <Icon
            type="antdesign"
            color={"rgba(0,0,0,.5)"}
            name={showPricesList ? "up" : "down"}
            size={18}
          />
        </TouchableOpacity>
        {showPricesList ? (
          <View style={{ paddingBottom: 64 }}>{this.listPrices}</View>
        ) : null}
      </View>
    );
  }
}

export default GiftPriceList;
