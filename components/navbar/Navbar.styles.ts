import { StyleSheet } from "react-native";

const styles = StyleSheet.create({    
    navBarStyle: {
        color: '#151C26',
        fontWeight: "bold",
        fontFamily: "PF_DinDisplay_Pro",
        fontSize: 18,
        paddingBottom: 24
    },
    containerStyle: {
        backgroundColor: 'transparent',
        justifyContent: 'space-around',
        height: 50,
        marginBottom: 24
    },
    rightComponentStyle: {
        color: '#F46F22'
    }
})

export default styles;