import React from "react";
import { Header, Icon } from "react-native-elements";
import styles from "./Navbar.styles";
import { View, Text, TouchableOpacity } from "react-native";
import { Actions } from "react-native-router-flux";

type IState = {
    title: string,
    backRoute?: string,
    showRightComponent?: boolean
}

type NavbarProps = {
    title: string,
    backRoute?: string,
    showRightComponent?: boolean,
    onPress?: any
}

class Navbar extends React.Component<NavbarProps, IState> {
    state = {
        title: this.props.title,
        backRoute: this.props.backRoute,
        showRightComponent: this.props.showRightComponent
    };

    constructor(props: NavbarProps) {
        super(props)
    }

    render() {
        const { showRightComponent, backRoute, title } = this.state;
        return (
            <Header
                leftComponent={backRoute? <BackArrowComponent backRoute={backRoute} /> : {}}
                centerComponent={{ text: title, style: styles.navBarStyle }}
                rightComponent={showRightComponent ? <RightComponent iconName={'done'} onPress={this.props.onPress} /> : {}}
                containerStyle={styles.containerStyle}
            />
        );
    }
}

export default Navbar;

interface IProps {
    backRoute: string;
}
const BackArrowComponent = ({ backRoute }: IProps) => {
    return (
        <TouchableOpacity onPress={() => Actions.popTo(backRoute)} style={{ marginTop: -20 }}>
            <Icon name={'arrow-back'} size={26} />
        </TouchableOpacity>
    );
};

interface IRightProps {
    iconName: string;
    onPress: any;
}
const RightComponent = ({ iconName, onPress }: IRightProps) => {
    return (
        <TouchableOpacity onPress={onPress} style={{ marginTop: -20 }}>
            <Icon name={iconName} size={26} iconStyle={styles.rightComponentStyle} />
        </TouchableOpacity>
    );
};