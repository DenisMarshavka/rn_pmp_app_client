import * as React from 'react'
import Svg, {Rect} from 'react-native-svg'

const PlusSvgIcon = ({ color, width = 21, height = 21, ...rest }) => (
    <Svg
        width={width}
        height={height}
        viewBox="0 0 21 21"
        fill="none"
        {...rest}>
            <Rect x={9.111} width={2.278} height={20.5} rx={1.139} fill={color} />

            <Rect
                x={20.5}
                y={9.111}
                width={2.278}
                height={20.5}
                rx={1.139}
                transform="rotate(90 20.5 9.111)"
                fill={color}
            />
    </Svg>
);

export default PlusSvgIcon
