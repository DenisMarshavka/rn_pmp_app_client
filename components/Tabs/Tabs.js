import React, { useEffect, useState } from "react";
import { ScrollView, Text, TouchableOpacity, View } from "react-native";
import { Card } from "react-native-paper";
import * as Animatable from "react-native-animatable";

import { getSizeSkeletonLoaders } from "../../utils";
import SkeletonLoader from "../loaders/SkeletonLoader";
import { fonts } from "../../constants/theme";

export const Tabs = ({
  leftContentOffset = true,
  elementStyle = {},
  startActiveTabByName = "",
  startActiveIndex = 0,
  upperCase = false,
  style = {},
  errorContainerStyle = {},
  requestFailed = false,
  loading,
  listData = [],
  getDataByTabActive = () => {},
  onTabActiveIndexSet = () => {},
  offsetHorizontal = 16,
  onDataByCurrentActiveTab = () => {},
}) => {
  const [tabState, setTabState] = useState(0);
  const [tabTypeNameState, setTabTypeNameState] = useState("");
  const [tabs, setTabsData] = useState([]);

  useEffect(() => {
    if (!loading && tabTypeNameState) getDataByTabActive(tabTypeNameState);
  }, [tabTypeNameState, loading]);

  const renderTabsData = () => {
    return loading
      ? getSizeSkeletonLoaders(0, 133, true)
      : !loading && listData && listData.length
      ? listData
      : [];
  };

  useEffect(() => {
    setTabTypeNameState(
      startActiveTabByName.trim()
        ? startActiveTabByName
        : (
            listData && listData[0] && listData[0].title
              ? listData[0].title
              : listData && listData[0] && listData[0].type
          )
        ? listData[0].type
        : ""
    );
    setTabState(startActiveIndex);

    renderTabsData();
  }, []);

  useEffect(() => {
    if ((listData && listData.length) || loading) setTabsData(renderTabsData());
  }, [listData, loading]);

  const renderTypesItem = (typeItem = {}, i = 0, list = []) => {
    const isActive = startActiveTabByName.trim()
      ? typeItem.title === startActiveTabByName ||
        typeItem.type === startActiveTabByName
      : tabState === i;

    return (
      <Animatable.View
        key={`item-${typeItem && typeItem.id ? typeItem.id : Date.now()}`}
        // duration={450}
        // delay={i < tabState ? 350 : i * 350}
      >
        <Card
          style={{
            flex: 1,
            minWidth: 100,
            height: "100%",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row",
            borderWidth: 1,
            marginLeft: i === 0 && leftContentOffset ? offsetHorizontal : 0,
            marginRight: i === list.length - 1 ? offsetHorizontal : 8,
            borderColor: isActive ? "#F46F22" : "#D8D8D8",
            borderRadius: 30,
            backgroundColor: isActive ? "#F46F22" : "#FFF",
            overflow: "hidden",
            ...elementStyle,
          }}
        >
          <TouchableOpacity
            key={(typeItem.id || Date.now()) + Date.now()}
            onPress={() => {
              if (!loading) {
                setTabTypeNameState(
                  typeItem.title || typeItem.type || typeItem.name || ""
                );
                getDataByTabActive(
                  typeItem.title || typeItem.type || typeItem.name || "",
                  typeItem.id
                );
                onTabActiveIndexSet(
                  i,
                  typeItem.id,
                  typeItem.title || typeItem.type || typeItem.name
                );

                setTabState(i);
                onDataByCurrentActiveTab(
                  typeItem.id,
                  i,
                  typeItem.titile || typeItem.type
                );
              }
            }}
            activeOpacity={loading ? 1 : 0.6}
            underlayColor="none"
            style={{
              flex: 1,
            }}
          >
            <SkeletonLoader
              isLoading={loading || (typeItem && typeItem.isTest)}
              containerStyle={{
                flex: 1,
                backgroundColor: isActive ? "#F46F22" : "transparent",
                padding: 8,
                alignItems: "center",
                borderRadius: 30,
              }}
              layout={[{ flex: 1, width: 80, height: 10 }]}
            >
              <Text
                style={[
                  fonts.normal,
                  {
                    fontSize: 13,
                    color: !isActive ? "#000000" : "white",
                    textAlign: "center",
                    textTransform: upperCase ? "uppercase" : "none",
                  },
                ]}
              >
                {typeItem.title || typeItem.type || typeItem.name || "Type"}
              </Text>
            </SkeletonLoader>
          </TouchableOpacity>
        </Card>
      </Animatable.View>
    );
  };

  console.log("LAST servicesTypes", tabs);

  return (
    <>
      {!requestFailed ? (
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          style={{
            flex: 1,
            height: "100%",
            maxHeight: 33,
            marginBottom: 25,
            ...style,
          }}
        >
          {tabs.map((typeItem, index) =>
            renderTypesItem(typeItem, index, tabs)
          )}
        </ScrollView>
      ) : !loading && tabs && !tabs.length && !requestFailed ? (
        <View style={[styles.fullFill, { maxHeight: 33, marginLeft: 16 }]}>
          <Text
            style={{
              width: "100%",
              textAlign: "left",
              color: "#000",
              marginBottom: 25,
            }}
          >
            Список типов пуст
          </Text>
        </View>
      ) : requestFailed ? (
        <Text
          style={{
            width: "100%",
            textAlign: "left",
            marginLeft: leftContentOffset,
            marginVertical: 20,
            marginLeft: offsetHorizontal,
            ...errorContainerStyle,
          }}
        >
          Ошибка получения данных
        </Text>
      ) : null}
    </>
  );
};
