import React from "react";
import { View, TouchableOpacity } from "react-native";

import { renderCountNotification } from "./../../utils";
import { BasketSvg } from "./../../assets/icons/profile/BasketSvg";
import { colors } from "../../constants/theme";

const BasketState = ({
  count = 0,
  styleCount = {},
  style = {},
  navigation,
  tiffany = false,
}) => (
  <View style={{ position: "relative" }}>
    {count && !isNaN(+count)
      ? renderCountNotification(count, { right: -5, top: 5, ...styleCount })
      : null}

    <TouchableOpacity
      onPress={() => navigation.navigate("Basket")}
      activeOpacity={0.5}
      style={{
        padding: 10,
        marginRight: 10,
        paddingRight: 0,
        paddingTop: 12.5,
        ...style,
      }}
    >
      <BasketSvg color={tiffany ? colors.swiper_active : colors.orange} />
    </TouchableOpacity>
  </View>
);

export default BasketState;
