import { StyleSheet } from "react-native";
import { fonts, fontSize14, colors, fontSize16 } from "../../constants/theme";
import Colors from "../../constants/Colors";

const styles = StyleSheet.create({
    inputBack: {
        padding: 20,
        borderRadius: 10,
        backgroundColor: Colors.gray,
    },
    inputBack_diraction: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    password: {
        fontSize: fontSize16,
        fontFamily: fonts.medium.fontFamily,
    },
    error: {
        fontSize: fontSize16,
        fontFamily: fonts.normal.fontFamily, 
        color: '#E02323'
    }
})

export default styles;