import React from "react";
import { Input } from "react-native-elements";
import styles from "./PersonalDataInput.styles";
import { Text, TouchableOpacity, View, Image } from "react-native";
import { colors } from "../../../constants/theme";

enum InputType {
  EMAIL = "email-address",
  NUMERIC = "numeric",
  PHONE = "phone-pad",
}

type IState = {
  id?: string;
  placeholder: string;
  label: string;
  value?: string;
  isRequired?: boolean;
  isSecure?: boolean;
  isFocused: boolean;
  toggleSecure?: boolean;
};

type PersonalDataInputProps = {
  id?: string;
  value?: string;
  defaultData?: string;
  placeholder: string;
  label: string;
  onChange?: (field: string) => void;
  onBlur: () => void;
  onFocus: () => void;
  isRequired?: boolean;
  isSecure?: boolean;
  inputType?: InputType;
  onlySecureText?: boolean;
};

class PersonalDataInput extends React.Component<
  PersonalDataInputProps,
  IState
> {
  state = {
    id: this.props.id,
    placeholder: this.props.placeholder,
    label: this.props.label,
    value: this.props.defaultData ? this.props.defaultData : this.props.value,
    isRequired: this.props.isRequired,
    isSecure: this.props.isSecure,
    isFocused: false,
    toggleSecure: true,
  };

  constructor(props: PersonalDataInputProps) {
    super(props);
  }

  renderToggle() {
    const { isSecure } = this.props;
    const { toggleSecure } = this.state;

    if (!isSecure) return null;

    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={{
          position: "absolute",
          right: 10,
          top: "35%",
          zIndex: 2,
          backgroundColor: "rgb(239, 239, 239)",
        }}
        onPress={() => this.setState({ toggleSecure: !toggleSecure })}
      >
        {toggleSecure ? (
          <Image
            source={require("../../../assets/images/visible.png")}
            width={20}
            height={20}
          />
        ) : (
          <Image
            source={require("../../../assets/images/invisible.png")}
            width={20}
            height={20}
          />
        )}
      </TouchableOpacity>
    );
  }

  static defaultProps = {
    onlySecureText: true,
  };

  onChange = (field: string) => {
    this.setState({ value: field });

    if (this.props.onChange) {
      this.props.onChange(field);
    }
  };

  static getDerivedStateFromProps(props, state) {
    if (props && props.defaultData !== state.value) {
      return {
        value: props.defaultData,
      };
    }

    return null;
  }

  render() {
    const {
      value,
      isSecure,
      isRequired,
      placeholder,
      label,
      toggleSecure,
    } = this.state;
    const { inputType, onFocus = () => {}, onBlur = () => {} } = this.props;

    return (
      <View
        style={{
          flex: 1,
          position: "relative",
          flexDirection: "row",
          // justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Input
          onChangeText={this.onChange}
          keyboardType={inputType ? inputType : "default"}
          value={value}
          rightIcon={
            isRequired ? <Text style={styles.star}>*</Text> : undefined
          }
          containerStyle={styles.containerStyle}
          inputStyle={styles.textInputStyle}
          labelStyle={{
            fontFamily: "PFDin500",
            fontWeight: "normal",
            fontSize: 13,
            color: colors.orange,
          }}
          inputContainerStyle={
            this.state.isFocused
              ? styles.inputContainerFocusStyle
              : styles.inputContainerStyle
          }
          placeholder={placeholder}
          label={label}
          secureTextEntry={isSecure && toggleSecure}
          onFocus={() => {
            this.setState({ isFocused: true });
            onFocus();
          }}
          onBlur={() => {
            this.setState({ isFocused: false });
            onBlur();
          }}
          {...this.props}
        />

        {this.props.onlySecureText ? null : this.renderToggle()}
      </View>
    );
  }
}

export default PersonalDataInput;
