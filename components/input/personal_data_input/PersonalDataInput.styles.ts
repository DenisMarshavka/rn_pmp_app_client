import { StyleSheet } from "react-native";
import { colors, fonts } from "../../../constants/theme";

const styles = StyleSheet.create({
    textInputStyle: {
        fontFamily: fonts.normal.fontFamily,
        fontSize: 18,
        lineHeight: 24,
        color: colors.title,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0
    },
    labelInputStyle: {
    },
    containerStyle: {
        marginBottom: 14
    },
    inputContainerStyle: {
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0, 0, 0, 0.12)'
    },
    inputContainerFocusStyle: {
        borderBottomWidth: 1,        
        borderBottomColor: colors.orange
    },
    star: {
        fontSize: 24, 
        color: '#151C26', 
        opacity: 0.54
    }
})

export default styles;