import React from "react";
import styles from "./AuthInput.styles";
import { LinearGradient } from "expo-linear-gradient";
import { View, Image, TextInput, TouchableOpacity, Text } from "react-native";
import { fonts, fontSize16 } from "../../constants/theme";
enum InputType {
  EMAIL = "email-address",
  NUMERIC = "numeric",
  PHONE = "phone-pad",
}

type IState = {
  title: string;
  toggleSecure: boolean;
};

type AuthInputProps = {
  title: string;
  secure?: boolean;
  onlySecureText?: boolean;
  validateText?: string;
  isValidate?: boolean;
  inputType: InputType;
  value?: string;
  style: object;
  maxLength?: number;
  onChangeText?: (text: string) => void;
};

class AuthInput extends React.Component<AuthInputProps, IState> {
  state = {
    title: this.props.title,
    toggleSecure: true,
  };

  constructor(props: AuthInputProps) {
    super(props);
  }

  renderToggle() {
    const { secure } = this.props;
    const { toggleSecure } = this.state;

    if (!secure) return null;

    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => this.setState({ toggleSecure: !toggleSecure })}
      >
        {toggleSecure ? (
          <Image
            source={require("../../assets/images/visible.png")}
            width={20}
            height={20}
          />
        ) : (
          <Image
            source={require("../../assets/images/invisible.png")}
            width={20}
            height={20}
          />
        )}
      </TouchableOpacity>
    );
  }

  validateError() {
    const { validateText, isValidate } = this.props;
    if (isValidate) return null;

    return (
      <Text style={[styles.error, { marginBottom: 5 }]}>{validateText}</Text>
    );
  }

  render() {
    const { inputType, onlySecureText, secure, value, maxLength } = this.props;
    const { toggleSecure, title } = this.state;
    const secureTextEntry = onlySecureText
      ? true
      : secure
      ? toggleSecure
      : false;

    return (
      <View>
        {this.validateError()}

        <LinearGradient
          colors={["rgba(255, 255, 255, 0.5)", "rgba(218, 218, 218, 0.1)"]}
          start={[0.5, 0.5]}
          style={styles.inputBack}
        >
          <View style={{ ...styles.inputBack_diraction }}>
            <TextInput
              value={value}
              style={[styles.password, { ...this.props.style }]}
              placeholder={title}
              placeholderTextColor="rgba(21, 28, 38, 0.5)"
              secureTextEntry={secureTextEntry}
              keyboardType={inputType ? inputType : "default"}
              onChangeText={this.props.onChangeText}
              maxLength={maxLength}
            />
            {onlySecureText ? null : this.renderToggle()}

            {this.props.children}
          </View>
        </LinearGradient>
      </View>
    );
  }
}

export default AuthInput;
