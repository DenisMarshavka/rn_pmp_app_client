import React from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { fonts, colors } from "../../constants/theme";

const { height } = Dimensions.get("window");

export const Logout = ({ yesHander, noHandler }) => {
  return (
    <View style={styles.background}>
      <View style={styles.block}>
        <View style={{ paddingHorizontal: 20 }}>
          <Text
            style={[
              {
                textAlign: "center",
                lineHeight: 18,
                color: "#000000",
                fontSize: 14,
              },
              fonts.normal,
            ]}
          >
            Вы действительно хотите выйти из аккаунта?
          </Text>
        </View>

        <View
          style={{
            flexDirection: "row",
            marginTop: 15,
            // justifyContent: "space-around",
            borderTopColor: "rgba(0, 0, 0, 0.15)",
            borderTopWidth: 1,
            width: "100%",
            paddingVertical: 14,
          }}
        >
          <TouchableOpacity
            activeOpacity={0.7}
            style={{ width: "50%" }}
            onPress={() => noHandler()}
          >
            <Text
              style={[
                {
                  textAlign: "center",
                  color: colors.title,
                  fontSize: 17,
                },
                fonts.normal,
              ]}
            >
              Нет
            </Text>
          </TouchableOpacity>
          <View
            style={{
              position: "absolute",
              width: 1,
              backgroundColor: "rgba(0, 0, 0, 0.15)",
              left: "50%",
              right: "50%",
              bottom: 0,
              top: 0,
            }}
          ></View>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => yesHander()}
            style={{ width: "50%" }}
          >
            <Text
              style={[
                {
                  textAlign: "center",
                  color: colors.title,
                  fontSize: 17,
                },
                fonts.normal,
              ]}
            >
              Да
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    backgroundColor: "rgba(50, 50, 50, 0.502463)",
    position: "absolute",
    width: "100%",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    paddingHorizontal: 53,
    justifyContent: "center",
  },
  block: {
    backgroundColor: "#f7f7f7",
    paddingTop: 38,
    borderRadius: 14,
    alignItems: "center",
  },
});
