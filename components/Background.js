import React from "react";
import { Dimensions, ImageBackground, View, StyleSheet, SafeAreaView } from "react-native";

const { height } = Dimensions.get("window");

export const Background = ({ style = {}, children, bg }) => {
  return (
    <View style={[ styles.background, bg && {backgroundColor: bg, ...style} ]}>
      <SafeAreaView style={{flex: 1}}>
        { children }
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    position: 'relative',
    width: "100%",
    height,
    backgroundColor: '#FFFFFF'
  }
});
