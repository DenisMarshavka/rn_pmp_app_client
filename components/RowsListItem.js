import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

import { colors, fonts, sizes } from "../constants/theme";
import SkeletonLoader from "./loaders/SkeletonLoader";

export const RowsListItem = ({
  title,
  day,
  isLoading = false,
  isProgramsItem = false,
}) => {
  const getDayTextFormat = (day) =>
    day === 1 ? " день" : day > 1 && 5 > day ? " дня" : " дней";

  return (
    <LinearGradient
      start={{ x: 1.8, y: 1.8 }}
      end={{ x: 0.1, y: 0.1 }}
      colors={[colors.white, colors.gray]}
      style={styles.gradient}
    >
      <View
        style={{
          ...styles.container_line,
          justifyContent: isProgramsItem ? "space-between" : "flex-start",
        }}
      >
        <SkeletonLoader
          layout={[{ width: 190, height: 35 }]}
          containerStyle={{ flex: 1, paddingRight: 10 }}
          isLoading={isLoading}
        >
          <Text
            style={[{ color: colors.text }, fonts.normal, fonts.h3]}
            numberOfLines={2}
            ellipsizeMode="tail"
          >
            {title}
          </Text>
        </SkeletonLoader>

        {isProgramsItem && day ? (
          <SkeletonLoader
            layout={[{ width: 190, height: 35 }]}
            containerStyle={{ flex: 1, paddingRight: 10 }}
            isLoading={isLoading}
          >
            <Text style={[{ color: colors.orange }, fonts.normal, fonts.h2]}>
              {day + getDayTextFormat(day)}
            </Text>
          </SkeletonLoader>
        ) : null}
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  gradient: {
    marginBottom: 11,
    borderRadius: 10,
  },
  container_line: {
    width: "100%",
    height: 66,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: sizes.margin,

    alignItems: "center",
  },
});
