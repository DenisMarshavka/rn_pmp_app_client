import { LinearGradient } from 'expo-linear-gradient'
import React from 'react'
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native'

import { colors, fonts, sizes } from '../constants/theme';
import {NoAvatarTrainerSvg} from "../assets/icons/NoAvatarTrainerSvg/NoAvatarTrainerSvg";

export const ListSpecialistItem = ({ name, profile = '', iconImg, tiffany, btPress }) => (
  <LinearGradient
    start={{ x: 1.0, y: 1.0 }}
    end={{ x: 0.6, y: 0.6 }}
    colors={[colors.white, colors.gray]}
    style={styles.gradient}
  >
    <View style={styles.container_line}>
      {
        iconImg ?

        <Image source={{ uri: iconImg }} style={{ width: 40, height: 40, borderRadius: 40, resizeMode: 'cover' }} /> :

        <NoAvatarTrainerSvg />
      }

      <View
        style={{
          marginLeft: sizes.margin
        }}
      >
        <Text style={[tiffany ? { color: '#0FD8D8' } : { color: colors.orange }, fonts.normal, fonts.h3]}>
          { name || 'Неизвестный тренер' }
        </Text>

        <Text style={[styles.rigth_text, fonts.normal, fonts.h3]}>
          { profile.length > 35 ? `${profile.slice(0,35)}...` : profile }
        </Text>

        <TouchableOpacity onPress={btPress} style={{width: 124, padding: 8, borderRadius: 30,
            backgroundColor: tiffany ? '#0FD8D8' : colors.orange, justifyContent: 'center', alignItems: 'center',}}>
            <Text style={styles.bt}>Выбрать</Text>
        </TouchableOpacity>
      </View>
    </View>
  </LinearGradient>
);

const styles = StyleSheet.create({
  gradient: {
    marginBottom: 8,
    borderRadius: 10,
    opacity: 0.9,
    marginHorizontal: 16
  },
  container_line: {
    width: "100%",
    //height: 72,
    flexDirection: "row",
    paddingHorizontal: sizes.margin,
    //alignItems: "center",
    paddingVertical: 11,
    paddingTop: 16,
  },
  rigth_text: {
    color: '#151C26',
    opacity: .38,
    paddingVertical: 8
  },
  bt: {
    color: '#FAFAFA',
    fontSize: 14,
    ...fonts.normal
  }
});
