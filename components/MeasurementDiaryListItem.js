import React from 'react'
import { LinearGradient } from 'expo-linear-gradient'
import { StyleSheet, Text, View } from 'react-native'

import { colors } from '../styles'
import { fonts, sizes } from "../constants/theme"
import SkeletonLoader from "./loaders/SkeletonLoader"


const MeasurementDiaryListItem = props => {
  const { chest, hips, waist, date, currentTab, weight, prevWeight, loading } = props;

  return (
    <View style={styles.mainView}>
      <LinearGradient start={[1.4, 0.8]} end={[0.0, 0.0]} colors={['#FFFFFF', '#DADADA']} style={styles.linearGradient} />

      {
        currentTab ?

        <SkeletonLoader
            layout={[
              {marginLeft: 16, width: 55, height: 20},
            ]}
            isLoading={loading}
        >
          <Text style={[ styles.measurementWeight, {color: prevWeight && prevWeight > weight ? '#E02323' : '#0FD8D8'} ]}>
            {weight} кг
          </Text>
        </SkeletonLoader> :

        <SkeletonLoader
            layout={[
              {marginLeft: 16, width: 165, height: 20},
            ]}
            isLoading={loading}
        >
          <Text style={styles.measurementText}>
            Грудь {chest}; Талия {waist}; Бедра {hips}
          </Text>
        </SkeletonLoader>
      }

      <SkeletonLoader
          layout={[
            {marginRight: 16, width: 70, height: 20},
          ]}
          isLoading={loading}
      >
        <Text style={styles.measurementDate}>
          {date}
        </Text>
      </SkeletonLoader>
    </View>
  )
};

const styles = StyleSheet.create({
  linearGradient: {
    position: 'absolute',
    borderRadius: 10,
    flex: 1,
    width: '100%',
    height: 56,
  },
  mainView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 58,
    marginVertical: 4,
  },
  measurementText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 16,
    marginLeft: 16,
    lineHeight: 19,
    color: colors.mainBlack,
  },
  measurementWeight: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    marginLeft: 16,
    color: '#0FD8D8'
  },
  measurementDate: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 14,
    marginRight: 16,
    lineHeight: 17,
    color: '#1E2022',
    opacity: 0.54,
  },
});


export default MeasurementDiaryListItem
