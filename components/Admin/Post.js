import React from "react";
import { View, StyleSheet, Text, Image } from "react-native";
import { useRoute } from "@react-navigation/native";
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/Entypo";
import ModalDropdown from "react-native-modal-dropdown";
import { THEME } from "../theme";

export const Post = ({ post }) => {
  const route = useRoute();
  const routeName = route.name;

  let source;

  if (post.img !== "") {
    source = {
      uri: post.img,
    };
  } else {
    source = require("../../assets/default.png");
  }

  const d = new Date(post.date);
  const date = d.getDate();

  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "June",
    "July",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  const monthIndex = d.getMonth();
  const monthName = months[monthIndex];

  const formattedDate = `${monthName} ${date}`;

  const colorMain =
    routeName === "Main" ? ["#fff", "#dadada"] : ["#FAFAFA", "#FAFAFA"];

  const styleMain = routeName === "Main" ? styles.post : styles.postFeedBack;

  const textSizeMain =
    routeName === "Main" ? styles.name : styles.nameTextFeedBack;

  const descriptionMain =
    routeName === "Main" ? styles.description : styles.descriptionFeedBack;

  return (
    <LinearGradient
      style={styleMain}
      colors={colorMain}
      start={{ x: 1, y: -1 }}
    >
      <Image style={styles.image} source={source} />
      <View style={styles.column}>
        <Text style={textSizeMain}>
          {post.firstName} {post.firstName}
        </Text>
        <Text style={descriptionMain}>{post.text}</Text>
      </View>
      {routeName === "Main" ? (
        <ModalDropdown
          options={["Редактировать", "Удалить"]}
          dropdownStyle={styles.dropdownStyle}
          dropdownTextStyle={styles.textStyle}
          textStyle={styles.text}
          onSelect={(idx, value) => {
            if (idx === 0) {
              // console.log(value)
            } else {
              // console.log(value)
            }
          }}
          renderSeparator={() => <View />}
        >
          <Icon
            style={styles.button}
            size={20}
            name="dots-three-horizontal"
            color={THEME.TEXT_COLOR}
          />
        </ModalDropdown>
      ) : (
        <Text style={styles.datePost}> {formattedDate} </Text>
      )}
      {routeName === "FeedBack" ? <View style={styles.line} /> : null}
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  post: {
    marginBottom: 15,
    overflow: "hidden",
    flexDirection: "row",
    paddingHorizontal: 16,
    paddingVertical: 16,
    backgroundColor: THEME.GRAY_COLOR,
    borderRadius: 10,
  },
  postFeedBack: {
    position: "relative",
    overflow: "hidden",
    flexDirection: "row",
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  column: {
    marginRight: "auto",
  },
  image: {
    width: 40,
    height: 40,
    marginRight: 10,
    borderRadius: 100,
  },
  name: {
    color: THEME.MAIN_COLOR,
    fontSize: 16,
    marginBottom: 3,
    lineHeight: 19,
    fontFamily: "PFDinTextCompPro-Regular",
  },
  nameTextFeedBack: {
    color: THEME.MAIN_COLOR,
    fontSize: 18,
    marginBottom: 3,
    lineHeight: 22,
    fontFamily: "PFDinTextCompPro-Regular",
  },
  description: {
    fontSize: 14,
    color: THEME.TEXT_COLOR,
    opacity: 0.38,
    fontFamily: "PFDinTextCompPro-Regular",
  },
  descriptionFeedBack: {
    fontSize: 14,
    color: THEME.TEXT_COLOR,
    opacity: 0.54,
    fontFamily: "PFDinTextCompPro-Regular",
  },
  button: {
    opacity: 0.54,
  },
  dropdownStyle: {
    width: 138,
    height: 111,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.24,
    shadowRadius: 2,
    elevation: 1,
    borderRadius: 4,
    paddingTop: 7,
    paddingBottom: 7,
  },
  textStyle: {
    fontSize: 16,
    lineHeight: 25,
    color: "rgba(0, 0, 0, 0.87)",
    fontFamily: "PFDinTextCompPro-Thin",
    paddingLeft: 16,
  },
  text: {
    color: THEME.TEXT_COLOR,
  },
  datePost: {
    color: THEME.TEXT_COLOR,
    opacity: 0.54,
    fontSize: 14,
    fontFamily: "PFDinTextCompPro-Regular",
  },
  line: {
    position: "absolute",
    bottom: 0,
    right: 0,
    width: "90%",
    height: 1,
    backgroundColor: "rgba(0, 0, 0, 0.12)",
  },
});
