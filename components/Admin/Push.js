import React from "react";
import { View, StyleSheet, Text, Image } from "react-native";
import { THEME } from "../theme";

export const Push = ({ push }) => {
  let source;

  if (push.img !== "") {
    source = {
      uri: push.img,
    };
  } else {
    source = require("../../assets/default.png");
  }

  const d = new Date(push.date);
  const date = d.getDate();

  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "June",
    "July",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  const monthIndex = d.getMonth();
  const monthName = months[monthIndex];
  const formattedDate = `${monthName} ${date}`;

  return (
    <View style={{ backgroundColor: "#FAFAFA" }}>
      <View style={styles.chat}>
        <Image style={styles.image} source={source} />
        <View style={styles.column}>
          <Text style={styles.title}>{push.title}</Text>
          <Text style={styles.description}>{push.text}</Text>
        </View>
        <Text style={styles.datePost}> {formattedDate}</Text>
      </View>
      <View style={styles.line} />
    </View>
  );
};

const styles = StyleSheet.create({
  chat: {
    marginBottom: 0,
    overflow: "hidden",
    flexDirection: "row",
    paddingHorizontal: 16,
    paddingVertical: 16,
    backgroundColor: "#fafafa",
    borderRadius: 10,
  },
  column: {
    marginRight: "auto",
  },
  image: {
    width: 40,
    height: 40,
    marginRight: 10,
    borderRadius: 100,
  },
  title: {
    color: THEME.TEXT_COLOR,
    fontSize: 16,
    marginBottom: 3,
    lineHeight: 19,
    fontFamily: "PFDinTextCompPro-Regular",
  },
  description: {
    fontSize: 14,
    color: THEME.TEXT_COLOR,
    opacity: 0.38,
    fontFamily: "PFDinTextCompPro-Regular",
  },
  button: {
    opacity: 0.54,
  },
  dropdownStyle: {
    width: 138,
    height: 111,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.24,
    shadowRadius: 2,
    elevation: 1,
    borderRadius: 4,
    paddingTop: 7,
    paddingBottom: 7,
  },
  textStyle: {
    fontSize: 16,
    lineHeight: 25,
    color: "rgba(0, 0, 0, 0.87)",
    fontFamily: "PFDinTextCompPro-Thin",
    paddingLeft: 16,
  },
  text: {
    color: THEME.TEXT_COLOR,
  },
  datePost: {
    color: THEME.TEXT_COLOR,
    opacity: 0.54,
    fontSize: 14,
    fontFamily: "PFDinTextCompPro-Regular",
  },
  line: {
    position: "absolute",
    bottom: 0,
    right: 0,
    width: "85%",
    height: 1,
    backgroundColor: "rgba(0, 0, 0, 0.12)",
  },
});
