import SkeletonContent from "./SkeletonContent";
import React from "react";

const SkeletonLoader = ({
  item = null,
  layout = [],
  containerStyle = {},
  isLoading = false,
  children,
}) => (
  <SkeletonContent
    containerStyle={containerStyle}
    layout={layout}
    isLoading={
      isLoading ||
      (item &&
        typeof item === "object" &&
        Object.keys(item).length &&
        item.isTest !== undefined)
    }
  >
    {children}
  </SkeletonContent>
);

export default SkeletonLoader;
