import React from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import SkeletonLoader from "./SkeletonLoader";
import * as Animatable from "react-native-animatable";

import { formattingPrice } from "../../utils";

const { width } = Dimensions.get("window");

// TODO: Will check this component for using
const SkeletonProductItem = ({
  style = {},
  isLoading = true,
  product = {},
  onProductPress = () => {},
  index = 0,
  isListRendering = false,
}) => {
  const Wrap = !isLoading && isListRendering ? Animatable.View : View;

  console.log("product", product);

  return (
    <Wrap
      style={{ flex: 1 }}
      duration={250}
      delay={index < 6 ? index * 250 : 400}
      animation="fadeInUp"
    >
      <TouchableOpacity
        style={[
          {
            flex: 1,
            borderRadius: 10,
            minHeight: 250,
          },
          { ...style },
        ]}
        activeOpacity={isLoading ? 1 : 0.9}
        onPress={() => {
          !isLoading ? onProductPress() : null;
        }}
      >
        <View style={{ flex: 1, borderRadius: 10 }}>
          <SkeletonLoader
            isLoading={isLoading}
            containerStyle={{
              flex: 1,
              height: 184,
              backgroundColor: "grey",
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
            }}
            layout={[{ flex: 1 }]}
          >
            <Image
              source={
                product.img
                  ? { uri: product.img }
                  : require("../../assets/images/notProductImage.png")
              }
              style={styles.img}
            />
          </SkeletonLoader>

          <View style={styles.infoSlide}>
            <SkeletonLoader
              isLoading={isLoading}
              containerStyle={{ flex: 1 }}
              layout={[{ flex: 1, width: 148, maxHeight: 22 }]}
            >
              <Text style={styles.titleSlide} numberOfLines={1}>
                {product.title || "Неизвестный продукт"}
              </Text>
            </SkeletonLoader>

            <SkeletonLoader
              isLoading={isLoading}
              containerStyle={{ flex: 1 }}
              layout={[
                { flex: 1, width: 50, maxHeight: 22, alignSelf: "flex-end" },
              ]}
            >
              <Text
                style={{
                  ...styles.priceSlide,
                  textTransform: product.have ? "none" : "uppercase",
                }}
              >
                {(product &&
                  product.steps &&
                  product.steps[0] &&
                  formattingPrice(product.steps[0].price)) ||
                  formattingPrice(product.price) ||
                  0}{" "}
                ₽
              </Text>
            </SkeletonLoader>
          </View>
        </View>
      </TouchableOpacity>
    </Wrap>
  );
};

export default SkeletonProductItem;

const styles = StyleSheet.create({
  img: {
    width: width - 32,
    height: 184,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    resizeMode: "cover",
  },
  infoSlide: {
    flex: 1,
    height: 72,
    backgroundColor: "#EAEAEA",
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 16,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  titleSlide: {
    width: "100%",
    maxWidth: "98%",
    fontSize: 18,
    lineHeight: 18,
    fontWeight: "500",
    color: "#000000",
    fontFamily: "PFDin500",

    alignSelf: "flex-start",
  },
  priceSlide: {
    fontSize: 18,
    color: "#F46F22",
    fontWeight: "500",
    textTransform: "uppercase",
    fontFamily: "PFDin500",

    alignSelf: "flex-end",
  },
});
