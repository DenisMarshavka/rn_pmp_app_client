import React from 'react';
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { colors, fonts } from '../constants/theme';

const { height } = Dimensions.get("window");

export const Evaluate = ({ parentCallback, navigation }) => {
  return (
    <View style={styles.background}>
      <View style={styles.block}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => parentCallback(false)}
          style={{ position: "absolute", right: 17, top: 17 }}
        >
          <Image source={require("../assets/images/closeIcon.png")} />
        </TouchableOpacity>
        <View style={{ paddingHorizontal: 16 }}>
          <Image source={require("../assets/images/positiveVote.png")} />
        </View>
        <View style={{ marginTop: 34, paddingHorizontal: 20 }}>
          <Text
            style={[
              {
                textAlign: "center",
                lineHeight: 18,
                color: "#000000",
                fontSize: 14
              },
              fonts.normal
            ]}
          >
            Имя, понравилось ли Вам сегодня? Пожалуйста, оцените тренировку.
          </Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            marginTop: 15,
            // justifyContent: "space-around",
            borderTopColor: "#000000",
            borderTopWidth: 1,
            width: "100%",
            paddingVertical: 14
          }}
        >
          <TouchableOpacity activeOpacity={0.7} style={{ width: "50%" }}>
            <Text
              style={[
                {
                  textAlign: "center",
                  color: colors.title,
                  fontSize: 17,
                  borderRightColor: "#000000"
                },
                fonts.normal
              ]}
            >
              Позже
            </Text>
          </TouchableOpacity>
          <View
            style={{
              position: "absolute",
              width: 1,
              backgroundColor: "#000000",

              left: "50%",
              right: "50%",
              bottom: 0,
              top: 0
            }}
          ></View>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => navigation.navigate("ResultDiaryScreen")}
            style={{ width: "50%" }}
          >
            <Text
              style={[
                {
                  textAlign: "center",
                  color: colors.orange,
                  fontSize: 17
                },
                fonts.normal
              ]}
            >
              Оценить
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    backgroundColor: "#323232",
    position: "absolute",
    width: "100%",
    height: "100%",
    top: 0,
    left: 0,
    paddingHorizontal: 53,
    justifyContent: "center"
  },
  block: {
    backgroundColor: "#f7f7f7",
    paddingTop: 38,

    borderRadius: 14,
    alignItems: "center"
  }
});
