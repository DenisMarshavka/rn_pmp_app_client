import React from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  default as Alert,
} from "react-native";

import arrowBack from "../assets/images/arrowBack.png";
import arrowDown from "../assets/images/arrowDown.png";
import historyIcon from "../assets/images/paymentHistory.png";
import menuIcon from "../assets/images/menuIcon.png";

import { colors } from "../styles";
import { fonts, sizes } from "../constants/theme";

const CustomHeader = (props) => (
  <View style={[styles.container, props.style]}>
    {props.back && (
      <TouchableOpacity
        style={styles.backContainer}
        onPress={() => props.navigateToMenu()}
      >
        <Image source={arrowBack} style={styles.arrowBack} />

        {props.backLabel && (
          <View style={[styles.backContainer, styles.labelContainer]}>
            <Text style={styles.labelText}>{props.backLabel}</Text>
            <Image source={arrowDown} style={styles.downArrow} />
          </View>
        )}
      </TouchableOpacity>
    )}

    {props.title && (
      <Text numberOfLines={1} style={props.titleTextStyle}>
        {props.title}
      </Text>
    )}

    <View>
      {props.menu && (
        <TouchableOpacity
          onPress={() => props.navigateOnMenuTap()}
          style={styles.menuButtonContainer}
        >
          <Image
            source={menuIcon}
            style={styles.menuIcon}
            resizeMode={"center"}
          />
        </TouchableOpacity>
      )}

      {props.history && (
        <TouchableOpacity
          onPress={() => props.navigateToHistory()}
          style={styles.menuButtonContainer}
        >
          <Image source={historyIcon} style={styles.historyIcon} />
        </TouchableOpacity>
      )}

      {props.avatarSource ? (
        <TouchableOpacity
          onPress={() => {
            props.avatarAction();
          }}
          style={styles.avatarIcon}
        >
          <Image
            source={
              !props.noAvatar ? { uri: props.avatarSource } : props.avatarSource
            }
            style={{
              flex: 1,
              height: 30,
              width: 30,
              borderRadius: 30,
              resizeMode: "cover",
            }}
          />
        </TouchableOpacity>
      ) : null}
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 55,
  },
  backContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: 8,
    paddingRight: 8,
  },
  arrowBack: {
    tintColor: colors.mainBlack,
    height: 15,
    width: 9,
  },
  labelContainer: {
    paddingHorizontal: 3,
  },
  labelText: {
    paddingTop: 5,
    paddingHorizontal: 6,
    color: colors.mainBlack,
    fontFamily: fonts.normal.fontFamily,
    fontSize: 13,
    fontWeight: "normal",
    letterSpacing: -0.08,
    textTransform: "uppercase",
  },
  downArrow: {
    tintColor: colors.mainOrange,
    height: 5,
    width: 9,
  },
  menuButtonContainer: {
    height: 38,
    width: 38,
    borderRadius: 19,
    justifyContent: "center",
    alignItems: "center",
  },
  menuIcon: {
    tintColor: colors.mainOrange,
    height: 15,
    width: 21,
  },
  historyIcon: {
    tintColor: colors.mainBlack,
    height: 24,
    width: 24,
  },
  avatarIcon: {
    height: 30,
    width: 30,
  },
});

export default CustomHeader;
