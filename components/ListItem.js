import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Animatable from "react-native-animatable";

import { colors, fonts, sizes } from "../constants/theme";
import SkeletonLoader from "./loaders/SkeletonLoader";
import { formattingPrice } from "../utils";

export const ListItem = ({ index = 0, title, price, isLoading }) => {
  const Wrap = !isLoading ? Animatable.View : View;

  return (
    <Wrap
      duration={250}
      delay={index < 15 ? index * 250 : 1000}
      animation="fadeInUp"
    >
      <LinearGradient
        start={{ x: 1.2, y: 1.2 }}
        end={{ x: 0.1, y: 0.1 }}
        colors={[colors.white, colors.gray]}
        style={styles.gradient}
      >
        <SkeletonLoader
          layout={[
            { width: 190, height: 35, paddingRight: 10 },
            { width: 56, height: 18, justifySelf: "flex-end" },
          ]}
          containerStyle={styles.container_line}
          isLoading={isLoading}
        >
          <View style={{ flex: 1, paddingRight: 10 }}>
            <Text
              style={[{ color: colors.text }, fonts.normal, fonts.h3]}
              numberOfLines={2}
              ellipsizeMode="tail"
            >
              {title || "Услуга"}
            </Text>
          </View>

          <View>
            <Text style={[{ color: colors.orange }, fonts.normal, fonts.h2]}>
              {price ? formattingPrice(price) + " ₽" : 0}
            </Text>
          </View>
        </SkeletonLoader>
      </LinearGradient>
    </Wrap>
  );
};

const styles = StyleSheet.create({
  gradient: {
    marginBottom: 8,
    borderRadius: 10,
  },
  container_line: {
    width: "100%",
    height: 66,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: sizes.margin,

    alignItems: "center",
  },
});
