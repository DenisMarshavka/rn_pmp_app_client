import React, { useEffect, useState } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  StyleSheet,
  AsyncStorage,
} from "react-native";
import * as Animatable from "react-native-animatable";

import { colors, fonts } from "../../constants/theme";
import SkeletonLoader from "../../components/loaders/SkeletonLoader";
import removeIcon from "../../assets/images/removeIcon.png";
import dropDownIcon from "../../assets/images/arrowDropDown.png";
import { validURL, formattingPrice } from "../../utils";

const BasketListItem = (props) => {
  const {
    item = {},
    index = 0,
    sliderOpenHandler = () => {},
    sliderOpenCertificateHandler = () => {},
    removeProductItemFromBasket = () => {},

    loading = true,
  } = props;

  console.log("BASKET PROPS", props);

  const [isFetching, setIsFetching] = useState(true);
  const [blackListForChangingCount, setBlackListForChangingCount] = useState(
    []
  );

  useEffect(() => {
    if (!loading) setIsFetching(false);
  }, [loading]);

  useEffect(() => {
    getDisabledProductsSliderCountList().then((list) => {
      setBlackListForChangingCount(list);

      // console.log("disabledProductsSliderCountList", list);
    });

    return () => {
      setIsFetching(true);
    };
  }, []);

  const {
    id = null,
    img = "",
    image = "",
    title = "Неизвестный продукт",
    name = "Неизвестный продукт",
    count = 0,
    price = 0,
  } = item;

  let itemTitle = "Неизвестный продукт";
  if (name && name.trim()) itemTitle = name.trim();
  if (title && title.trim()) itemTitle = title.trim();
  // name && name.trim() && name !== undefined
  //   ? name.trim()
  //   : title && title.trim()
  //   ? title
  //   : "Неизвестный продукт";

  const screenTitleForOpen =
    itemTitle !== "Неизвестный продукт" &&
    item &&
    item.type !== "nominal" &&
    item.type !== "step"
      ? "ItemBasketProduct"
      : itemTitle !== "Неизвестный продукт" && item.type !== "step"
      ? "Certificate"
      : "";
  const paramsForOpenScreenProduct =
    itemTitle !== "Неизвестный продукт" &&
    item &&
    item.type !== "nominal" &&
    item.type !== "step"
      ? { item }
      : itemTitle !== "Неизвестный продукт" && item.type !== "step"
      ? { item: { id: item.certificateId } }
      : null;

  // console.log(
  //   item.type,
  //   "screenTitleForOpen",
  //   screenTitleForOpen,
  //   "paramsForOpenScreenProduct",
  //   paramsForOpenScreenProduct
  // );
  //
  console.log("BAsket Item: ", item, "name", name, "title", title, itemTitle);

  const isLoading = loading || isFetching || (item && item.isTest);
  const Wrap = !isLoading ? Animatable.View : View;

  // console.log("img", img);

  const getDisabledProductsSliderCountList = async () =>
    await AsyncStorage.getItem("disableSliderForProducts").then((res) =>
      JSON.parse(res) && JSON.parse(res).products
        ? JSON.parse(res).products
        : []
    );

  return (
    <Wrap
      duration={350}
      delay={index * 350}
      animation="fadeInUp"
      style={styles.listItemStyle}
    >
      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => {
          if (screenTitleForOpen && paramsForOpenScreenProduct) {
            props.navigation.navigate(
              screenTitleForOpen,
              paramsForOpenScreenProduct
            );
          }
        }}
      >
        <SkeletonLoader
          layout={[{ ...styles.pictureListItem }]}
          containerStyle={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingRight: 4,
          }}
          isLoading={isLoading}
        >
          <Image
            source={
              img && img.trim() && validURL(img)
                ? { uri: img }
                : image && image.trim() && validURL(image)
                ? { uri: image }
                : require("../../assets/images/notProductImage.png")
            }
            style={styles.pictureListItem}
          />
        </SkeletonLoader>
      </TouchableOpacity>

      <View style={styles.itemInfo}>
        <View style={styles.itemNameLine}>
          <TouchableOpacity
            activeOpacity={0.6}
            style={{ flex: 1, width: "80%" }}
            onPress={() => {
              if (screenTitleForOpen && paramsForOpenScreenProduct) {
                props.navigation.navigate(
                  screenTitleForOpen,
                  paramsForOpenScreenProduct
                );
              }
            }}
          >
            <SkeletonLoader
              layout={[{ height: 40, width: 135 }]}
              isLoading={isLoading}
            >
              <Text numberOfLines={2} line style={styles.itemName}>
                {item.title || item.name}
              </Text>
            </SkeletonLoader>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={!loading ? 0.2 : 1}
            style={{ padding: 10, paddingTop: 2, paddingRight: 0 }}
            onPress={() => {
              !loading && !isFetching
                ? removeProductItemFromBasket(item, { id, type: item.type })
                : null;
            }}
          >
            <Image source={removeIcon} />
          </TouchableOpacity>
        </View>

        <View style={styles.itemCountAndPriceLine}>
          {item.type !== "nominal" &&
          item.type !== "step" &&
          itemTitle !== "Неизвестный продукт" &&
          !blackListForChangingCount.includes(item.id) ? (
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => sliderOpenHandler(item)}
              style={styles.countDropDownStyle}
            >
              <SkeletonLoader
                layout={[{ height: 20, width: 75, marginTop: 5 }]}
                isLoading={loading || isFetching}
              >
                <View style={{ width: "100%" }}>
                  <Text style={styles.countStyle}>Количество: {count}</Text>

                  <Image source={dropDownIcon} style={styles.dropDownIcon} />
                </View>
              </SkeletonLoader>
            </TouchableOpacity>
          ) : (
            <View />
          )}

          <SkeletonLoader
            layout={[
              { height: 20, width: 40, marginTop: 5, marginLeft: "auto" },
            ]}
            isLoading={isLoading}
          >
            <Text
              style={{
                ...styles.priceText,
                marginTop:
                  item.type !== "nominal" &&
                  item.type !== "step" &&
                  itemTitle !== "Неизвестный продукт"
                    ? 0
                    : 20,
              }}
            >
              {formattingPrice(price)} ₽
            </Text>
          </SkeletonLoader>
        </View>
      </View>
    </Wrap>
  );
};

export default BasketListItem;

const styles = StyleSheet.create({
  background: {
    width: "100%",
    height: "100%",
    position: "relative",
  },
  pictureListItem: {
    width: 80,
    height: 80,
  },
  itemInfo: {
    flex: 1,
    marginLeft: 16,
    paddingVertical: 10,
    justifyContent: "space-between",
  },
  itemNameLine: {
    width: "100%",

    flexDirection: "row",
    justifyContent: "space-between",
  },
  listItemStyle: {
    borderRadius: 10,
    minHeight: 112,
    flex: 1,
    width: "100%",
    backgroundColor: "#FFF",
    padding: 16,
    marginBottom: 8,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",

    // shadowColor: "#000",
    // shadowOffset: {
    //   width: 0,
    //   height: 3,
    // },
    // shadowOpacity: 0.1,
    // shadowRadius: 4.65,
    //
    // elevation: 7,
  },
  itemCountAndPriceLine: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 0,
  },
  countDropDownStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",

    alignSelf: "flex-start",
  },
  dropDownIcon: {
    marginBottom: 6,
    marginLeft: 10,
  },
  countStyle: {
    width: "100%",

    marginTop: 5,
    fontSize: 14,
    lineHeight: 17,
    color: colors.mainBlack,
    opacity: 0.54,
    fontFamily: fonts.normal.fontFamily,
  },
  itemName: {
    fontSize: 18,
    lineHeight: 18.5,
    marginTop: 4,
    opacity: 0.87,
    fontFamily: fonts.normal.fontFamily,
  },
  priceText: {
    fontSize: 16,
    lineHeight: 17,
    fontWeight: "500",
    alignSelf: "flex-end",
    color: colors.orange,
    textAlign: "right",
    opacity: 0.87,
    fontFamily: fonts.medium.fontFamily,
  },
});
