import React, { useState, useEffect } from "react";
import { View, Text, ScrollView, Dimensions } from "react-native";
import * as Animatable from "react-native-animatable";

import { getSizeSkeletonLoaders, isNotEndContentList } from "../utils";

const ListWithInfo = ({
  isHorizontalList = false,
  defaultHeight = 0,

  mocsDataOffsetTop = 0,
  mocsDataListElementSize = 0,
  mocsIsEasyData = true,
  mocsItemBody = {},

  children = [],

  data = [],
  offsetContentList = 0,
  limitContentList = 15,
  listMeta = {},
  loading = true,
  moreLoading = false,
  error = false,

  existLikeKeys = false,

  titleError = "Ошиибка получения данных",
  titleEmpty = "Список пуст",

  heightSizeFromDimensionsScreenSize = true,
  heightDimensionsContainer = 1.7,
  mainContainerStyle = {},
  containerListStyle = {},
  containerInfoStyle = {},
  textStyle = {},

  requestDidMountCopmponent = () => {},
  requestWillUnmountComponent = () => {},
  callBack = () => {},

  WrapListComponent = ScrollView,
  wrapListParams = {},

  withWrapElementComponent = false,
  WrapElementComponent = null,
  wrapElemntComponentProps = {},

  withTwoWrapElementComponent = false,
  TwoWrapElementComponent = null,
  twoWrapElemntComponentProps = {},

  withThreeWrapElementComponent = false,
  ThreeWrapElementComponent = null,
  threeWrapElemntComponentProps = {},

  elementKeyName = "product-item",

  renderListItem = () => {},
  restRenderListItemProps = {},

  onMoreDataList = () => {},

  withRenderingAnimation = true,
  renderingAniationName = "fadeInUp",
  durationListItemRendering = 350,
}) => {
  const [fetchFinished, setFetchFinished] = useState(false);

  useEffect(() => {
    if (!loading) setFetchFinished(true);
  }, [loading]);

  useEffect(() => {
    requestDidMountCopmponent();

    return () => {
      setFetchFinished(false);

      requestWillUnmountComponent();
    };
  }, []);

  let dataList =
    loading || !fetchFinished
      ? getSizeSkeletonLoaders(
          mocsDataOffsetTop,
          mocsDataListElementSize,
          isHorizontalList,
          mocsIsEasyData,
          mocsItemBody
        )
      : !loading && fetchFinished && data.length
      ? data
      : [];

  const moreLoadingList = moreLoading
    ? getSizeSkeletonLoaders(
        mocsDataOffsetTop,
        mocsDataListElementSize,
        isHorizontalList,
        mocsIsEasyData,
        mocsItemBody
      )
    : [];
  const isEmptyList = !dataList || !dataList.length;

  // dataList = !fetchFinished ? [] : dataList;

  console.log("List with Info REsponse: ", dataList);

  const height = heightSizeFromDimensionsScreenSize
    ? Dimensions.get("screen").height / heightDimensionsContainer
    : defaultHeight;

  const renderKey = (item, index) => {
    let key = Date.now();

    if (item && index !== false) {
      key =
        (elementKeyName ? elementKeyName : "item") +
        (item && item.id ? item.id : Date.now() + index);

      if (existLikeKeys) key = key + index;
    }

    console.log("KEYY: ", key);

    return key;
  };

  const loadMore = async () => {
    if (listMeta) {
      const { count } = listMeta || {};
      console.log("LIST COUNT: ", count);

      const offsetContent = data.length;

      console.log(
        "UPROVE TO LOAD MORE: ",
        !isEmptyList && offsetContent !== count && !loading && !error,
        "offset Content LIST: ",
        offsetContent
      );

      if (!isEmptyList) {
        if (offsetContent < count && !loading && !moreLoading && !error) {
          await onMoreDataList(offsetContent, limitContentList);
        }
      }
    }
  };

  const isDataWithMocks = dataList.findIndex(
    (product) => product && product.isTest
  );
  // console.log(
  //   "isDataWithMocks",
  //   isDataWithMocks,
  //   "fetchFinished",
  //   fetchFinished,
  //   "loading",
  //   loading,
  //   "error",
  //   error
  // );

  const WrapWithAnimation =
    withRenderingAnimation && !loading ? Animatable.View : View;

  const generateDelayRenderAnimation = (index = 0) => {
    let delay = 1000;

    console.log("IINNNDEXXX: ", index, "loading: ", loading);

    if (loading || moreLoading) return 0;

    delay =
      dataList && dataList.length && dataList.length > limitContentList
        ? (index - dataList.length) * durationListItemRendering
        : index * durationListItemRendering;

    return delay;
  };

  const renderList = (list = [], isMore = false) => {
    const isLoading = !isMore ? loading : moreLoading;

    if (isMore && !moreLoading) return null;

    return (
      <>
        {withWrapElementComponent && WrapElementComponent
          ? list.map((item, index) => (
              <WrapWithAnimation
                duration={durationListItemRendering}
                delay={generateDelayRenderAnimation(index)}
                {...wrapElemntComponentProps}
                animation={renderingAniationName}
                key={renderKey(item, index)}
              >
                <WrapElementComponent {...wrapElemntComponentProps}>
                  {renderListItem(
                    item,
                    index,
                    list,
                    isLoading,
                    restRenderListItemProps
                  )}
                </WrapElementComponent>
              </WrapWithAnimation>
            ))
          : withTwoWrapElementComponent && TwoWrapElementComponent
          ? list.map((item, index) => (
              <WrapWithAnimation
                duration={durationListItemRendering}
                delay={generateDelayRenderAnimation(index)}
                animation={renderingAniationName}
                {...wrapElemntComponentProps}
                key={renderKey(item, index)}
              >
                <TwoWrapElemntComponent {...twoWrapElemntComponentProps}>
                  {renderListItem(
                    item,
                    index,
                    list,
                    isLoading,
                    restRenderListItemProps
                  )}
                </TwoWrapElemntComponent>
              </WrapWithAnimation>
            ))
          : withThreeWrapElementComponent && ThreeWrapElementComponent
          ? list.map((item, index) => (
              <WrapWithAnimation
                duration={durationListItemRendering}
                delay={generateDelayRenderAnimation(index)}
                animation={renderingAniationName}
                {...wrapElemntComponentProps}
                key={renderKey(item, index)}
              >
                <TwoWrapElemntComponent {...twoWrapElemntComponentProps}>
                  <ThreeWrapElemntComponent {...threeWrapElemntComponentProps}>
                    {renderListItem(
                      item,
                      index,
                      list,
                      isLoading,
                      restRenderListItemProps
                    )}
                  </ThreeWrapElemntComponent>
                </TwoWrapElemntComponent>
              </WrapWithAnimation>
            ))
          : list.map((item, index) => (
              <WrapWithAnimation
                duration={durationListItemRendering}
                delay={generateDelayRenderAnimation(index)}
                animation={renderingAniationName}
                {...wrapElemntComponentProps}
                key={renderKey(item, index)}
              >
                {renderListItem(
                  item,
                  index,
                  list,
                  isLoading,
                  restRenderListItemProps
                )}
              </WrapWithAnimation>
            ))}
      </>
    );
  };

  return (
    <View style={{ flex: 1, ...mainContainerStyle }}>
      {(!error && dataList && dataList.length) ||
      ((loading || moreLoading) && !fetchFinished) ? (
        <WrapListComponent
          {...wrapListParams}
          onScroll={({ nativeEvent }) => {
            if (isNotEndContentList(nativeEvent)) loadMore();
          }}
        >
          {renderList(dataList)}

          {renderList(dataList, true)}
        </WrapListComponent>
      ) : !error && data && !data.length && fetchFinished && !loading ? (
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            minHeight: height,
            ...containerInfoStyle,
          }}
        >
          <Text
            style={{
              width: "100%",
              textAlign: "center",
              color: "#000",
              ...textStyle,
            }}
          >
            {titleEmpty}
          </Text>
        </View>
      ) : fetchFinished && !loading && error ? (
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            minHeight: height,
            ...containerInfoStyle,
          }}
        >
          <Text
            style={{
              width: "100%",
              textAlign: "center",
              color: "#000",
              ...textStyle,
            }}
          >
            {titleError}
          </Text>
        </View>
      ) : null}
    </View>
  );
};

export default ListWithInfo;
