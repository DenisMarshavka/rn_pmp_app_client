import React from "react";
import { StyleSheet, View, Dimensions } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

export default function BackGroundGradient() {
  return (
    <View style={styles.linearGradient}>
      <LineGradient angle={180} location={[0, 1]} />
    </View>
  );
}

const LineGradient = ({ angle, location }) => {
  return (
    <LinearGradient
      useAngle
      angle={angle}
      angleCenter={{ x: 0.5, y: 0.5 }}
      locations={location}
      colors={["rgba(0,0,0,0)", "#000000"]}
      style={styles.linearGradient}
    />
  );
};

export const WINDOW_WIDTH = Dimensions.get("window").width;
export const WINDOW_HEIGHT = Dimensions.get("window").height;

const styles = StyleSheet.create({
  linearGradient: {
    height: WINDOW_HEIGHT / 4,
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    borderRadius: 8,
  },
});
