import React from 'react';
import {TextInput} from 'react-native';
import {
  fontSize18,
  fonts,
} from '../constants/theme';

export const TextInputCustom = (props) => {
  return (
    <TextInput
      {...props}
      editable
      style={{
        fontFamily: fonts.normal.fontFamily,
        color: '#000000',
        fontSize: fontSize18,
        paddingLeft: 0,
      }}
    />
  );
};
