import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  SafeAreaView,
  Dimensions,
  AsyncStorage,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { WebView } from "react-native-webview";

import SwipeBottomSheet from "./../SwipeBottomSheet";
import { colors, dimensions } from "../../styles";

const { height: deviceHeight, width: deviceWidth } = Dimensions.get("window");

const SwipeWebView = ({
  heightSheet = deviceHeight / 2,
  uri = "",
  swipeModalRef = {},
  setSwipeModalRef = () => {},
  onBackDropPress = () => {},
  onSuccessOperation = () => {},
  setCurrentWebUrl = () => {},
  onUpdateBasketState = () => {},
}) => {
  const [webView, setWebView] = useState(null);
  const [loading, setLoading] = useState(true);
  const [currentDOMWebView, setCurrentDOMWebView] = useState("");

  useEffect(() => {
    console.log("currentDOMWebView", currentDOMWebView);
  }, [currentDOMWebView]);

  const handleWebViewNavigationStateChange = async (newUrl = {}) => {
    const { url, loading } = newUrl;
    // const history = [];

    setLoading(loading);

    await AsyncStorage.setItem(
      "paymentsOperation",
      JSON.stringify({ hasPaymentsOperation: true })
    );

    // history.push(url);

    // if (
    //   history.length &&
    //   history[history.length - 2] &&
    //   history[history.length - 1]
    // ) {
    //   if (history[history.length - 2] !== history[history.length - 1])
    //     onUpdateBasketState();
    // }

    // var payload =
    //   url.indexOf("&native=") > -1
    //     ? JSON.parse(url.replace("%", " ").split("&native=")[1])
    //     : { functionName: "" };
    //
    // console.log(
    //   "payloadpayload",
    //   payload,
    //   'url.split("&native=")',
    //   url.split("&native=")
    // );
    //
    // switch (payload.functionName) {
    //   case "success":
    //     onSuccessOperation(payload.data);
    //
    //     alert("success");
    //     break;
    //
    //   case "error":
    //     alert("error");
    //     break;
    //
    //   case "dom":
    //     alert("dom");
    //
    //     if (payload.data) setCurrentDOMWebView(payload.data);
    //     break;
    //
    //   default:
    //     alert("tttt");

    console.log(
      "url: ",
      url,
      newUrl,
      "loadingloading",
      loading,
      "currentDOMWebView",
      currentDOMWebView
    );
  };

  useEffect(() => {
    setLoading(true);

    return () => {
      setLoading(true);
    };
  }, []);

  return (
    <SafeAreaView style={{ backgroundColor: "#fff" }}>
      <SwipeBottomSheet
        heightSheet={heightSheet}
        title={"Оплата"}
        swipeModalRef={swipeModalRef}
        onBackDropPress={onBackDropPress}
      >
        <ScrollView
          style={{
            height: deviceHeight,
            width: deviceWidth,
            position: "relative",
          }}
          contentContainerStyle={{ flex: 1, paddingBottom: 150 }}
        >
          {loading ? (
            <View
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                flex: 1,
                zIndex: 2,
                width: deviceWidth,
                height: Dimensions.get("screen").height / 1.6,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "#fff",
              }}
            >
              <ActivityIndicator size="large" color={colors.mainOrange} />
            </View>
          ) : null}

          <WebView
            scalesPageToFit={true}
            ref={(ref) => {
              setSwipeModalRef(ref);
              return swipeModalRef;
            }}
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              flex: 1,
              width: deviceWidth,
              zIndex: loading ? -10 : 1,
              opacity: !loading,
            }}
            source={{ uri }}
            onNavigationStateChange={handleWebViewNavigationStateChange}
            cacheEnabled={true}
            cacheMode={"LOAD_DEFAULT"}
            sharedCookiesEnabled={true}
          />
        </ScrollView>
      </SwipeBottomSheet>
    </SafeAreaView>
  );
};

export default SwipeWebView;
