import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Icon } from "react-native-elements";

import { colors, fonts } from "../../constants/theme";
import { NoAvatarTrainerSvg } from "../../assets/icons/NoAvatarTrainerSvg/NoAvatarTrainerSvg";
import { validURL } from "../../utils";
import SkeletonLoader from "../loaders/SkeletonLoader";

const MAX_RATE = 5;

export const MapReviewsItem = ({ data, isLoading }) => {
  const {
    id,
    text,
    date,
    points,
    user_img,
    client_firstname,
    client_lastname,
    client_middlename,
  } = data;

  const getFullName = () => {
    let name = "";
    if (client_firstname) {
      name += client_firstname;
    }

    if (client_middlename) {
      name += ` ${client_middlename}`;
    }

    if (client_lastname) {
      name += ` ${client_lastname}`;
    }
    return name;
  };

  const renderStars = () => {
    let list = [];

    for (let i = 1; i <= MAX_RATE; i++) {
      list.push(
        <Icon
          key={"star-" + i}
          type="material-community"
          name={"star"}
          size={18}
          color={i <= +points ? "#0FD8D8" : "rgba(15,216,216,0.24)"}
        />
      );
    }

    return list.map((item) => item);
  };

  return (
    <View
      style={{
        flexDirection: "row",
        backgroundColor: colors.white,
        padding: 16,
        marginTop: 5,
      }}
    >
      <SkeletonLoader
        isLoading={isLoading}
        containerStyle={{}}
        layout={[{ width: 40, height: 40, borderRadius: 40 }]}
      >
        {user_img || validURL(user_img) ? (
          <Image source={{ uri: user_img }} style={styles.avatar} />
        ) : (
          <NoAvatarTrainerSvg width={40} height={40} />
        )}
      </SkeletonLoader>

      <View
        style={{
          flexDirection: "column",
          marginLeft: 26,
          paddingRight: 20,
        }}
      >
        <SkeletonLoader
          isLoading={isLoading}
          containerStyle={{}}
          layout={[{ width: 130, height: 14, marginBottom: 10 }]}
        >
          {getFullName() !== "" && (
            <Text
              style={[
                fonts.medium,
                { color: "rgb(0, 0, 0)", fontSize: 14, marginBottom: 10 },
              ]}
            >
              {getFullName()}
            </Text>
          )}
        </SkeletonLoader>

        {data.trainer_fullname ? (
          <SkeletonLoader
            isLoading={isLoading}
            containerStyle={{}}
            layout={[{ width: "100%", height: 14, marginBottom: 10 }]}
          >
            <Text
              style={[
                fonts.medium,
                { color: "rgb(0, 0, 0)", fontSize: 12, marginBottom: 10 },
              ]}
            >
              О спериалисте: {data.trainer_fullname}
            </Text>
          </SkeletonLoader>
        ) : null}

        <SkeletonLoader
          isLoading={isLoading}
          containerStyle={{}}
          layout={[{ width: 80, height: 14, marginBottom: 19 }]}
        >
          {points && +points >= 0 && (
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginBottom: 19,
              }}
            >
              {renderStars()}

              <Text
                style={[
                  fonts.medium,
                  {
                    color: "rgba(0, 0, 0, 0.54)",
                    fontSize: 12,
                    marginLeft: 8,
                  },
                ]}
              >
                {`${points},0`}
              </Text>
            </View>
          )}
        </SkeletonLoader>

        <SkeletonLoader
          isLoading={isLoading}
          containerStyle={{}}
          layout={[{ width: 240, height: 30 }]}
        >
          <Text
            style={[
              fonts.medium,
              {
                color: "rgba(0, 0, 0, 0.54)",
                fontSize: 14,
                lineHeight: 20,
              },
            ]}
          >
            {text}
          </Text>
        </SkeletonLoader>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
});
