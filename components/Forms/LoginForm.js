import React, { useEffect } from "react";
import {
  Button,
  StyleSheet,
  Text,
  TextInput,
  View,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import { Formik } from "formik";
import * as Yup from "yup";
import { ToastView } from "../ToastView";

const width = Math.round(Dimensions.get("window").width);
const height = Math.round(Dimensions.get("window").height);

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
const LoginSchema = Yup.object().shape({
  phone: Yup.string()
    .required("Обязательное поле")
    .matches(phoneRegExp, "Некорректный номер телефона"),
  password: Yup.string()
    .required("Обязательное поле")
    .min(6, "Пароль слишком короткий - минимум 6 символов."),
});

export const LoginForm = ({ loading, onHandleSubmitLogForm }) => {
  return (
    <Formik
      style={{ width, flex: 1 }}
      initialValues={{
        phone: "",
        password: "",
      }}
      onSubmit={(values, { resetForm }) =>
        onHandleSubmitLogForm(values, resetForm)
      }
      validationSchema={LoginSchema}
    >
      {({
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        touched,
        values,
      }) => (
        <View style={{ width }}>
          {errors.phone && touched.phone ? (
            <Text style={styles.error}>{errors.phone}</Text>
          ) : null}
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Номер телефона</Text>

            <TextInput
              keyboardType="phone-pad"
              onBlur={handleBlur("phone")}
              onChangeText={handleChange("phone")}
              placeholder="89101111111"
              style={styles.input}
              textContentType="telephoneNumber"
              value={values.phone}
            />
          </View>

          {errors.password && touched.password ? (
            <Text style={styles.error}>{errors.password}</Text>
          ) : null}
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Пароль</Text>

            <TextInput
              onBlur={handleBlur("password")}
              onChangeText={handleChange("password")}
              placeholder="Введите пароль"
              secureTextEntry={true}
              style={[styles.input]}
              textContentType="password"
              value={values.password}
            />
          </View>

          <TouchableOpacity
            style={styles.button}
            onPress={handleSubmit}
            activeOpacity={0.92}
          >
            <Text style={styles.buttonTxt}>Войти</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.rememberBtn} activeOpacity={0.8}>
            <Text style={styles.rememberBtnText}>Забыли пароль</Text>
          </TouchableOpacity>
        </View>
      )}
    </Formik>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    position: "relative",
  },
  label: {
    fontFamily: "Montserrat400",
    position: "absolute",
    color: "#ffffff",
    opacity: 0.7,
    fontSize: 12,
    lineHeight: 15,
    top: "14%",
    left: "12%",
    letterSpacing: 0.5,
  },
  input: {
    fontFamily: "Montserrat500",
    fontSize: 16,
    lineHeight: 12,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    color: "#ffffff",
    opacity: 0.87,
    height: 66,
    marginBottom: 8,
    paddingBottom: 1,
    paddingLeft: 44,
    paddingRight: 4,
    paddingTop: 20,
  },
  inputPasswordActive: {
    letterSpacing: 4.5,
    fontSize: 24,
    lineHeight: 29,
  },
  button: {
    padding: 13,
    backgroundColor: "#009740",
    width: width,
    flex: 1,
    overflow: "hidden",
    marginTop: 24,
    height: 53,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonTxt: {
    color: "#ffffff",
    fontSize: 20,
    letterSpacing: 0.4,
    fontFamily: "Montserrat500",
    textAlign: "center",
  },
  rememberBtn: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 24,
  },
  rememberBtnText: {
    color: "#fff",
    fontFamily: "Montserrat500",
    fontSize: 20,
    letterSpacing: 0.4,
  },
  error: {
    fontSize: 10,
    color: "yellow",
    paddingLeft: 5,
    paddingBottom: 2,
  },
});
