import React from "react";
import {
  Button,
  StyleSheet,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Dimensions,
  View,
} from "react-native";
import { Formik } from "formik";
import * as Yup from "yup";

const width = Math.round(Dimensions.get("window").width);
const height = Math.round(Dimensions.get("window").height);

const phoneRegExp = /^[6-9]\d{9}$/;
const SignupSchema = Yup.object().shape({
  phone: Yup.string()
    .required("Обязательное поле")
    .matches(
      /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/,
      "Некорректный номер телефона"
    ),
  password: Yup.string()
    .required("Обязательное поле")
    .min(6, "Пароль слишком короткий - минимум 6 символов."),
  password_confirmation: Yup.string()
    .required("Обязательное поле")
    .oneOf([Yup.ref("password"), null], "Пароли должны совпадать"),
});

export const RegisterForm = ({ onHandleSubmitRegForm, setConfirmFocus }) => {
  return (
    <Formik
      style={{ width, flex: 1 }}
      initialValues={{
        phone: "",
        password: "",
        password_confirmation: "",
        group_id: 1,
      }}
      validationSchema={SignupSchema}
      onSubmit={(values, { resetForm }) =>
        onHandleSubmitRegForm(values, resetForm)
      }
    >
      {({
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        touched,
        values,
      }) => (
        <KeyboardAvoidingView behavior="padding" enabled>
          <ScrollView>
            <View style={{ width }}>
              {errors.phone && touched.phone ? (
                <Text style={styles.error}>{errors.phone}</Text>
              ) : null}
              <View style={styles.inputContainer}>
                <Text style={styles.label}>Номер телефона</Text>
                <TextInput
                  keyboardType="phone-pad"
                  onBlur={handleBlur("phone")}
                  onChangeText={handleChange("phone")}
                  placeholder="89101111111"
                  style={styles.input}
                  textContentType="telephoneNumber"
                  value={values.phone}
                />
              </View>

              {errors.password && touched.password ? (
                <Text style={styles.error}>{errors.password}</Text>
              ) : null}
              <View style={styles.inputContainer}>
                <Text style={styles.label}>Пароль</Text>
                <TextInput
                  onBlur={handleBlur("password")}
                  onChangeText={handleChange("password")}
                  placeholder="Введите пароль"
                  secureTextEntry={true}
                  style={[styles.input]}
                  textContentType="password"
                  value={values.password}
                />
              </View>

              {errors.password_confirmation && touched.password_confirmation ? (
                <Text style={styles.error}>{errors.password_confirmation}</Text>
              ) : null}
              <View style={styles.inputContainer}>
                <Text style={styles.label}>Подтвердите пароль</Text>
                <TextInput
                  onFocus={() => setConfirmFocus(true)}
                  onBlur={() => {
                    handleBlur("password_confirmation");
                    setConfirmFocus(false);
                  }}
                  onChangeText={handleChange("password_confirmation")}
                  placeholder="Повторно введите пароль"
                  secureTextEntry={true}
                  style={[styles.input]}
                  textContentType="password"
                  value={values.password_confirmation}
                />
              </View>

              <TouchableOpacity
                style={styles.button}
                onPress={handleSubmit}
                activeOpacity={0.92}
              >
                <Text style={styles.buttonTxt}>Зарегистрироваться</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      )}
    </Formik>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    position: "relative",
  },
  label: {
    fontFamily: "Montserrat400",
    position: "absolute",
    color: "#ffffff",
    opacity: 0.7,
    fontSize: 12,
    lineHeight: 15,
    top: "14%",
    left: "12%",
    letterSpacing: 0.5,
  },
  input: {
    fontFamily: "Montserrat500",
    fontSize: 16,
    lineHeight: 12,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    color: "#ffffff",
    opacity: 0.87,
    height: 66,
    marginBottom: 8,
    paddingBottom: 1,
    paddingLeft: 44,
    paddingRight: 4,
    paddingTop: 20,
  },
  inputPasswordActive: {
    letterSpacing: 4.5,
    fontSize: 24,
    lineHeight: 29,
  },
  button: {
    padding: 13,
    backgroundColor: "#009740",
    width: width,
    flex: 1,
    overflow: "hidden",
    marginTop: 24,
    height: 53,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonTxt: {
    color: "#ffffff",
    fontSize: 20,
    letterSpacing: 0.4,
    fontFamily: "Montserrat500",
    textAlign: "center",
  },
  error: {
    fontSize: 10,
    color: "yellow",
    paddingLeft: 5,
    paddingBottom: 2,
  },
});
