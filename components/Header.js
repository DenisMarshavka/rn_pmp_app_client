import React from "react";
import { useNavigation } from "@react-navigation/native";
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
} from "react-native";
import Svg, { Path } from "react-native-svg";
import { connect } from "react-redux";

import back from "../assets/icons/back.png";
import hamburger from "../assets/images/menuIcon.png";
import hamburgerTiffany from "../assets/images/menu-tiffany.png";
import { fonts, sizes } from "../constants/theme";
import { Icon } from "react-native-elements";
import { validURL } from "../utils";
import { BasketSvg } from "../assets/icons/profile/BasketSvg";
import BasketState from "../components/basketState";

const Header = (props) => {
  const {
    offsetTopIconBack = 0,
    style = {},
    styleHeadTitle = {},
    basketCount = 0,
    routeName = "Home",
    isTiffanyStyle = false,
    backTitle = false,
    title = "",
    deg,
    backimg,
    isHamburger = false,
    isSubmit = false,
    submitPress = () => {},
    headerLeft,
    imageSubmit = "",
    styleSubmit = {},
    onHamburgerOpen = () => {},
    basketIconStyle = {},
    disabledBtn = false,
    basketShow = false,
  } = props;

  const navigation = useNavigation();
  const isAndroid = Platform.OS !== "ios";

  return (
    <View style={[styles.container, { ...style }]}>
      <TouchableOpacity
        style={styles.left_header}
        onPress={() => navigation.goBack()}
      >
        <View style={{ padding: 15 }}>
          <Image
            source={back}
            style={{
              width: 9,
              height: 15,
              top: offsetTopIconBack,
              marginRight: backTitle && isAndroid ? 16 : 0,
            }}
          />
        </View>

        {/*<View style={{marginRight: backTitle && isAndroid ? 16 : 0, marginTop: offsetTopIconBack, paddingRight: backTitle && isAndroid ? 10 : 0, paddingBottom: 10}}>*/}

        {/*<View style={{marginRight: backTitle && isAndroid ? 16 : 0, marginTop: offsetTopIconBack}}>*/}
        {/*  <Svg width={12} height={21} viewBox="0 0 12 21" fill="none" {...props}>*/}
        {/*    <Path*/}
        {/*        d="M10 19l-8-8.5L10 2"*/}
        {/*        stroke={'#000'}*/}
        {/*        strokeWidth={3}*/}
        {/*        strokeLinecap="round"*/}
        {/*        strokeLinejoin="round"*/}
        {/*    />*/}
        {/*  </Svg>*/}
        {/*</View>*/}

        {backTitle && isAndroid ? (
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Image
              source={back}
              style={{
                width: 9,
                height: 15,
                marginRight: backTitle && isAndroid ? 16 : 0,
                marginTop: offsetTopIconBack,
              }}
            />

            <Text
              style={{
                color: "#000",
                fontSize: 13,
                textAlign: "crnter",
              }}
            >
              {typeof backTitle === "string" && backTitle.trim()
                ? backTitle.trim()
                : "Назад"}
            </Text>
          </View>
        ) : null}
      </TouchableOpacity>

      <View style={styles.center_header}>
        <Text
          style={[
            styles.headerTitle,
            { ...styleHeadTitle, textAlign: "center" },
          ]}
        >
          {title}
        </Text>
      </View>

      <View style={styles.right_header}>
        {basketShow ? (
          <BasketState
            count={basketCount}
            // style={{ ...basketIconStyle, top: 2 }}
            navigation={navigation}
          />
        ) : null}

        {isHamburger && onHamburgerOpen && (
          <View
            style={{
              opacity: !disabledBtn ? 1 : 0.5,
              paddingRight: sizes.margin,
            }}
          >
            <TouchableOpacity
              activeOpacity={!disabledBtn ? 0.7 : 1}
              onPress={() =>
                !disabledBtn && routeName
                  ? navigation.navigate(routeName)
                    ? !disabledBtn
                    : onHamburgerOpen()
                  : null
              }
            >
              <Image
                source={isTiffanyStyle ? hamburgerTiffany : hamburger}
                style={styles.hamburger}
              />
            </TouchableOpacity>
          </View>
        )}

        {isSubmit && (
          <View
            style={{
              opacity: !disabledBtn ? 1 : 0.5,
              paddingRight: sizes.margin,
            }}
          >
            <TouchableOpacity
              activeOpacity={!disabledBtn ? 0.7 : 1}
              onPress={() => (!disabledBtn ? submitPress() : null)}
            >
              {!imageSubmit ? (
                <Icon
                  name="check"
                  type="material-community"
                  color="#F46F22"
                  size={28}
                />
              ) : (
                <Image
                  source={imageSubmit}
                  style={styleSubmit}
                  resizeMode={"contain"}
                />
              )}
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
};

export default connect((state) => ({
  basketCount: state.currentUser.basketCount,
}))(Header);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    //marginTop: 40,
    paddingBottom: 10,
  },
  btn: {
    padding: 5,
  },
  headerTitle: {
    fontSize: sizes.headerName,
    ...fonts.normal,
  },
  left_header: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
  },
  center_header: {
    width: "90%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    flex: 3,
  },
  right_header: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  name: {
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    fontSize: 13,
    opacity: sizes.opcity,
    color: "black",
  },
  arrow_down: {
    marginLeft: 6,
  },
  menuIcon: {},
  hamburger: {
    width: 20,
    height: 14,
  },
});
