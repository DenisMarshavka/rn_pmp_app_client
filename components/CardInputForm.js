import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Text, Image } from 'react-native';
import { Formik } from 'formik';
import InputField from './InputField';
import { colors } from '../styles';
import radioButtonActive from '../assets/images/radioButtonActive.png';
import { fonts, sizes } from "../constants/theme";

export const CardInputForm = () => {
  const [saveUser, setSaveUser] = useState(false);
  return (
    <View style={[styles.scene]}>
      <Formik
        initialValues={{ cardNumber: '', cardholderName: '', date: '', CVV: '', saveMe: false  }}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2));

            actions.setSubmitting(false);
          }, 1000);
        }}
      >
        {props => (
          <View>
            <InputField
                label={'Номер карты'}
                placeholder={'1111 1111 1111 1111'}
                mask={"[0000] [0000] [0000] [0000]"}
                onChange={props.handleChange('cardNumber')}
            />

            <InputField
                label={'Имя владельца'}
                placeholder={'Tyler Parker'}
                onChange={props.handleChange('cardholderName')}
            />

            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
              <InputField
                  label={'Дата'}
                  placeholder={''}
                  style={{width: 145}}
                  onChange={props.handleChange('date')}
              />

              <InputField
                  label={'CVV'}
                  placeholder={''}
                  onChange={props.handleChange('CVV')}
              />
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TouchableOpacity style={{
                height: 28,
                width: 28,
                borderRadius: 50,
                borderWidth: 2,
                borderColor: colors.mainOrange,
                alignItems: 'center',
                justifyContent: 'center',
              }}
                onPress={() => setSaveUser(!saveUser)}
              >
                {
                  saveUser ?
                    <Image
                      source={radioButtonActive}
                    />
                    : null
                }
              </TouchableOpacity>

              <Text style={styles.saveInfoText}>
                Сохранить информацию
              </Text>
            </View>
          </View>
        )}
      </Formik>
    </View>
)};


const styles = StyleSheet.create({
  scene: {
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 25,
  },
  saveInfoText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 16,
    lineHeight: 20,
    color: colors.mainBlack,
    marginLeft: 13,
    marginTop: 5,
  }
});
