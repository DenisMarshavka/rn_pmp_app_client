import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { colors, fonts, sizes } from '../constants/theme';

export const ListItemDiary = ({ data }) => {
  return (
    <LinearGradient
      start={{ x: 1.2, y: 1.2 }}
      end={{ x: 0.1, y: 0.1 }}
      colors={[colors.white, colors.gray]}
      style={styles.gradient}
    >
      <View style={styles.container_line}>
        <View>
          <Text
            style={[
              { color: data.weight > 65 ? "#0FD8D8" : "#E02323" },
              fonts.bold,
              fonts.h2
            ]}
          >
            {data.weight} кг
          </Text>
        </View>
        <View>
          <Text
            style={[
              { color: "#1E2022", opacity: 0.54, fontSize: 14 },
              fonts.normal
            ]}
          >
            {data.date}
          </Text>
        </View>
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  gradient: {
    marginBottom: 8,
    borderRadius: 10
  },
  container_line: {
    width: "100%",
    height: 56,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: sizes.margin,
    alignItems: "center"
  }
});
