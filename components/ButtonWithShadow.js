import React from 'react';
import {
  Image, StyleSheet, Text, View, TouchableOpacity, ImageBackground,
} from 'react-native'

import arrowBack from '../assets/images/arrowBack.png';
import arrowDown from '../assets/images/arrowDown.png';
import historyIcon from '../assets/images/paymentHistory.png';
import menuIcon from '../assets/images/menuIcon.png';

import { dimensions } from '../styles';
import { colors, fonts, sizes } from "../constants/theme";

const ButtonWithShadow = props => (
  <TouchableOpacity style={[styles.buttonStyle, styles.buttonShadow, props.style]} onPress={() => props.onPress()}>
      <Text style={styles.buttonText}>
        {props.label}
      </Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  buttonShadow: {
    shadowColor: colors.absoluteBlack,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.2,
    shadowRadius: 20,
    elevation: 32,
  },
  buttonText: {
    marginTop: 8,
    fontFamily: fonts.normal.fontFamily,
    fontSize: 14,
    lineHeight: 17,
    fontWeight: '500',
    textTransform: 'uppercase',
    letterSpacing: 0.16,
    color: colors.mainWhite,
  },
  buttonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 52,
    zIndex: 2,
    backgroundColor: colors.mainOrange,
    borderRadius: 10,
    marginHorizontal: 16,
  },
});

export default ButtonWithShadow;
