import React, { useState, useEffect } from "react";
import {
  Dimensions,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  TextInput,
  Platform,
  AsyncStorage,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
} from "react-native";
import Modal from "react-native-modal";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";

import { ModalReviewImageSvg } from "../../assets/icons/ModalReviewImageSvg";
import { colors } from "../../constants/theme";
// import * as RootNavigation from "../../utils/RootNavigation";
import { showAlert, getUserStorageData } from "../../utils";
import { UserAPI } from "../../api/user";
import { setCompanyReview } from "../../actions";

const ModalReview = ({
  setCompanyReview = () => {},
  loading = false,
  error = false,
  success = false,

  currentUser = {},
}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [ratingCompany, setRatingCompany] = useState(3);
  const [textReview, setTextReview] = useState("");
  const [sended, setSended] = useState(false);
  const [showToggleModal, setToggleShowModal] = useState(false);
  const [showTimeOut, setShowTimeOut] = useState(null);

  const existingUserData = currentUser && Object.keys(currentUser).length;
  const userId = existingUserData && existingUserData.id;
  const userName =
    existingUserData && currentUser.firstname ? currentUser.firstname : "";

  const toggleModal = (isOpen = false) =>
    setModalVisible(!isOpen ? !modalVisible : true);

  const checkExistingCompanyReview = async () =>
    await AsyncStorage.getItem("companyReviewExisting");

  const checkCompanyReviewCanceled = async () =>
    await AsyncStorage.getItem("companyReviewExistingCancelCount").then((res) =>
      res ? JSON.parse(res) : {}
    );

  const handleReviewSend = () => {
    if (textReview && textReview.trim() && textReview.trim().length >= 10) {
      setSended(true);

      setCompanyReview(textReview, ratingCompany);
    } else
      showAlert(
        `Ошибка`,
        !textReview.trim().length
          ? "Отзыв не может быть пустым :("
          : `Длина символов отзыва очень маленькая (Минимальная длина 10 символов)\n Сейчас ${
              textReview.trim().length
            } символов!`,
        [
          {
            title: "Хорошо",
            onPress: () => {},
            style: "success",
          },
        ]
      );
  };

  const handleCancel = () => {
    toggleModal();

    checkCompanyReviewCanceled().then((currentCount) => {
      const newCount =
        currentCount &&
        currentCount.number !== undefined &&
        !isNaN(+currentCount.number)
          ? +currentCount.number + 1
          : 0;

      console.log("newCount", newCount, "currentCount", currentCount);

      AsyncStorage.setItem("companyReviewExisting", "false");
      AsyncStorage.setItem(
        "companyReviewExistingCancelCount",
        JSON.stringify({ number: newCount })
      );
    });
  };

  useEffect(() => {
    console.log(
      "existingUserDataexistingUserDataexistingUserDataexistingUserData",
      existingUserData
    );

    if (existingUserData) setToggleShowModal(modalVisible);

    if (!modalVisible) showModalNow();
  }, [modalVisible]);

  const showModalNow = async () => {
    // let timeToShow = Math.ceil(Math.random() * 60) + 0;
    let timeToShow = Math.ceil(Math.random() * 1200) + 300;

    if (showTimeOut) clearTimeout(showTimeOut);

    const currentUserData = await getUserStorageData();

    if (
      currentUserData &&
      Object.keys(currentUserData).length &&
      currentUserData.login
    ) {
      checkExistingCompanyReview().then((existing) => {
        setShowTimeOut(
          setTimeout(() => {
            checkCompanyReviewCanceled().then((currentCountCanceled) => {
              console.log(
                "CURRENT companyReviewExisting",
                existing,
                timeToShow,
                currentUser,
                "currentCountCanceled",
                currentCountCanceled
              );

              console.log(
                "111111111111111111111111111111",
                timeToShow * 1000,
                existing === "false",
                !existing,
                "2222222222222222",
                !currentCountCanceled,
                currentCountCanceled.number === undefined,
                isNaN(+currentCountCanceled.number),
                +currentCountCanceled.number < 4
              );

              if (
                !currentCountCanceled ||
                !currentCountCanceled.number ||
                (currentCountCanceled.number &&
                  isNaN(+currentCountCanceled.number)) ||
                (currentCountCanceled.number &&
                  +currentCountCanceled.number < 4)
              ) {
                toggleModal(true);

                if (
                  !currentCountCanceled ||
                  currentCountCanceled.number === undefined ||
                  isNaN(+currentCountCanceled.number)
                )
                  AsyncStorage.setItem(
                    "companyReviewExistingCancelCount",
                    JSON.stringify({ number: 0 })
                  );
              }
            });
          }, timeToShow * 1000)
        );

        // if (!existing || existing === "false") {
        //   showTimeOut = setTimeout(() => {
        //     toggleModal(true);
        //   }, timeToShow * 1000);
        // }
      });
    }
  };

  useEffect(() => {
    showModalNow();

    return () => {
      setSended(false);

      clearTimeout(showTimeOut);
      setShowTimeOut(null);
    };
  }, []);

  useEffect(() => {
    if (success && !loading && !error && sended) {
      setTextReview("");

      AsyncStorage.setItem("companyReviewExisting", "true");

      showAlert(
        `Поздравляем`,
        "Отзыв был успешно создан\n Отличных вам результатов!",
        [
          {
            title: "Ок",
            onPress: () => {
              setSended(false);
              toggleModal();
            },
            style: "error",
          },
        ]
      );
    } else if (!success && !loading && sended)
      showAlert(
        `Ошибка`,
        "Не удалось coздать отзыв :( Произошла ошибка.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27.",
        [
          {
            title: "Закрыть",
            onPress: () => {
              setSended(false);
            },
            style: "error",
          },
        ]
      );
  }, [success, loading, error]);

  const generateStars = () => {
    const stars = [];
    const lengthStarsEmpty = 5 - ratingCompany;

    for (let a = 1; a <= 5; a++) {
      stars.push(
        <TouchableOpacity
          key={`star-${a}`}
          activeOpacity={0.5}
          onPress={() => setRatingCompany(a)}
        >
          {a <= 5 - lengthStarsEmpty ? (
            <Ionicons name="md-star" size={31} color={colors.orange} />
          ) : (
            <Ionicons name="md-star-outline" size={31} color={"#B9B9B9"} />
          )}
        </TouchableOpacity>
      );
    }

    return stars;
  };

  return (
    <Modal
      isVisible={showToggleModal}
      backdropTransitionInTiming={1000}
      style={{
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,

        zIndex: 999,
        height: Dimensions.get("screen").height,
        width: Dimensions.get("screen").width,
      }}
      onModalWillShow={() => console.log("+++++++++++++++++++++++++++")}
    >
      <KeyboardAvoidingView
        behavior={"padding"}
        style={{ flex: 1, top: Dimensions.get("screen").height / 11 }}
      >
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <SafeAreaView>
              <View
                style={{
                  left: -Dimensions.get("screen").width / 20,
                  height: Dimensions.get("screen").height,
                  width: Dimensions.get("screen").width,
                  justifyContent: "center",
                  alignItems: "center",
                  flex: 1,
                }}
              >
                <View
                  style={{
                    position: "relative",
                    top: -100,
                    paddingTop: 20,
                    height: 406,
                    width: 270,
                    backgroundColor: "rgba(248, 248, 248, 1)",
                    borderRadius: Platform.OS === "ios" ? 15 : 0,
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  {loading ? (
                    <View
                      style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center",
                        paddingBottom: 23,
                      }}
                    >
                      <ActivityIndicator size={"large"} color={colors.orange} />
                    </View>
                  ) : (
                    <>
                      <View
                        style={{
                          width: "100%",
                          justifyContent: "flex-start",
                          alignItems: "center",
                          paddingLeft: 20,
                          paddingRight: 20,
                          paddingBottom: 25,
                        }}
                      >
                        <TouchableOpacity
                          onPress={handleCancel}
                          activeOpacity={0.5}
                          style={{ position: "absolute", top: 0, right: 20 }}
                        >
                          <AntDesign name="close" size={25} color={"#151C26"} />
                        </TouchableOpacity>

                        <ModalReviewImageSvg />

                        <Text
                          style={{
                            width: "80%",
                            marginTop: 25,
                            color: "#000",
                            textAlign: "center",
                            fontSize: 13,
                            lineHeight: 18,
                          }}
                        >
                          {userName && userName.trim()
                            ? `${userName}, понравились ли Вам наши сеансы? Пожалуйста, оцените их.`
                            : "Понравились ли Вам наши сеансы? Пожалуйста, оцените их."}
                        </Text>
                      </View>

                      <View
                        style={{
                          width: "100%",
                          justifyContent: "space-around",
                          flexDirection: "row",
                          alignItems: "center",
                          paddingLeft: 44,
                          paddingRight: 44,
                          height: 55,
                          borderWidth: 1,
                          borderTopColor: "rgba(0, 0, 0, 0.15)",
                          borderBottomColor: "rgba(0, 0, 0, 0.15)",
                          borderRightWidth: 0,
                          borderLeftWidth: 0,
                        }}
                      >
                        {generateStars()}
                      </View>

                      <TextInput
                        value={textReview}
                        style={{
                          flex: 1,
                          width: "100%",
                          padding: 15,
                          paddingTop: 15,
                          paddingBottom: 0,
                          color: "#000",
                          textAlign: "left",
                        }}
                        placeholder={"Ваши впечатления"}
                        multiline={true}
                        numberOfLines={12}
                        onChangeText={setTextReview}
                        textAlignVertical="top"
                      />

                      <View
                        style={{
                          width: "100%",
                          borderTopWidth: 1,
                          borderTopColor: "rgba(0, 0, 0, 0.15)",
                          flexDirection: "row",
                        }}
                      >
                        <TouchableOpacity
                          style={{
                            width: 135,
                            justifyContent: "center",
                            alignItems: "center",
                            borderRightColor: "rgba(0, 0, 0, 0.15)",
                            borderRightWidth: 1,
                            paddingTop: 13,
                            paddingBottom: 20,
                          }}
                          activeOpacity={0.5}
                          onPress={handleCancel}
                        >
                          <Text style={{ fontSize: 17 }}>Позже</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                          style={{
                            flex: 1,
                            justifyContent: "center",
                            alignItems: "center",
                            paddingTop: 13,
                            paddingBottom: 20,
                          }}
                          activeOpacity={0.5}
                          onPress={handleReviewSend}
                        >
                          <Text style={{ color: colors.orange, fontSize: 17 }}>
                            Оценить
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </>
                  )}
                </View>
              </View>
            </SafeAreaView>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </Modal>
  );
};

//
//
const mapStateToProps = (state) => ({
  loading: state.mainReducer.companyReviewLoading,
  error: state.mainReducer.companyReviewError,
  success: state.mainReducer.companyReviewSuccess,

  currentUser: state.currentUser.currentUser,
});

export default connect(mapStateToProps, { setCompanyReview })(ModalReview);
