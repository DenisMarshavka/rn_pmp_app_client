import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    textInputStyle: {
        fontFamily: "PF_DinDisplay_Pro",
        fontSize: 18,
        lineHeight: 24,
        color: '#151C26',
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0
    },
    labelInputStyle: {
        fontFamily: "PF_DinDisplay_Pro",
        fontSize: 13,
        lineHeight: 18,        
        color: '#F46F22'
    },
    containerStyle: {
        marginBottom: 14
    },
    inputContainerStyle: {
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0, 0, 0, 0.12)'
    },
    inputContainerFocusStyle: {
        borderBottomWidth: 1,        
        borderBottomColor: '#F46F22'
    }
})

export default styles;