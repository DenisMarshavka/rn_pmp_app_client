import React from "react";
import { Input } from 'react-native-elements';
import RequiredIcon from "../requiredIcon/requiredIcon";
import styles from "./TextInput.styles";
import { NativeSyntheticEvent, TextInputChangeEventData } from "react-native";

type IState = {
    id: string,
    placeholder: string,
    label: string,
    value?: string,
    isRequired?: boolean,
    isSecure?: boolean,
    isFocused: boolean
}

type TextInputProps = {
    id: string,
    value?: string,
    placeholder: string,
    label: string,
    onChange?: (id: string, field: string) => void,
    isRequired?: boolean,
    isSecure?: boolean
}

class TextInput extends React.Component<TextInputProps, IState> {
    state = {
        id: this.props.id,
        placeholder: this.props.placeholder,
        label: this.props.label,
        value: this.props.value,
        isRequired: this.props.isRequired,
        isSecure: this.props.isSecure,
        isFocused: false
    };

    constructor(props: TextInputProps) {
        super(props)
    }

    onChange = (field: string) => {
        this.setState({ 'value': field });
        if (this.props.onChange) {
            this.props.onChange(this.state.id, field);
        }
    }

    render() {
        const { value, isSecure, isRequired, placeholder, label } = this.state;
        return (
            <Input
                onChangeText={this.onChange}
                value={value}
                rightIcon={isRequired ? <RequiredIcon /> : undefined}
                containerStyle={styles.containerStyle}
                inputStyle={styles.textInputStyle}
                labelStyle={styles.labelInputStyle}
                inputContainerStyle={this.state.isFocused ? styles.inputContainerFocusStyle : styles.inputContainerStyle}
                placeholder={placeholder}
                label={label}
                secureTextEntry={isSecure}
                onFocus={() => this.setState({ 'isFocused': true })}
                onBlur={() => this.setState({ 'isFocused': false })}
            />
        );
    }
}

export default TextInput;