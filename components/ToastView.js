import Toast from "react-native-root-toast";
import { colors } from "../constants/theme";

export const ToastView = (text, duration = 0) =>
  Toast.show(text, {
    duration: duration || Toast.durations.LONG,
    position: Toast.positions.BOTTOM,
    shadow: true,
    animation: true,
    hideOnPress: true,
    delay: 0,
    backgroundColor: colors.orange,
  });
