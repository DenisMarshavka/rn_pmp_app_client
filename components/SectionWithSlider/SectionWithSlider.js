import React from "react";
import { Dimensions, Text, TouchableOpacity, View } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { useNavigation } from "@react-navigation/native";

import { fonts, sizes } from "../../constants/theme";

const { width } = Dimensions.get("window");

export const SectionWithSlider = (props) => {
  const {
    activeSlideIndex = 0,
    handleSlideRender = () => {},
    isLoading = false,
    data = [],
    requestFailed = false,
    title = "",
    handleActiveInviteSlideIndexSet = () => {},
    buttonHeadTitle = "",
    onButtonHeadPress = () => {},
  } = props;

  const navigation = useNavigation();

  console.log("ädatadatadatadatadata", data);

  return (
    <View
      style={{ marginTop: 45, minHeight: 367, paddingHorizontal: sizes.margin }}
    >
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        {title.trim() ? (
          <Text style={[{ color: "#1E2022" }, fonts.normal, fonts.h2]}>
            {title}
          </Text>
        ) : (
          <View />
        )}
        {buttonHeadTitle.trim() ? (
          <TouchableOpacity
            activeOpacity={isLoading || !data || !data.length ? 1 : 0.5}
            onPress={() => {
              !requestFailed && !isLoading && data && data.length
                ? onButtonHeadPress()
                : null;
            }}
          >
            <Text
              style={[
                {
                  color: "#F46F22",
                  fontSize: 15,
                  opacity: !requestFailed && !isLoading ? 1 : 0.5,
                },
                fonts.upper,
                fonts.normal,
              ]}
            >
              {buttonHeadTitle}
            </Text>
          </TouchableOpacity>
        ) : (
          <View />
        )}
      </View>

      <View
        style={{
          marginTop: 23,
          width: "100%",
        }}
      >
        {!requestFailed && data && data.length ? (
          <View
            style={{
              flex: 1,
              marginBottom: data.length && data.length > 1 ? 0 : 17,
            }}
          >
            <Carousel
              data={data.length ? data : [{ id: 1 }, { id: 2 }, { id: 3 }]}
              renderItem={({ item }) =>
                handleSlideRender(
                  item,
                  () =>
                    navigation.navigate(
                      !item.isDetox ? "ProgramScreen" : "DetoxProgramScreen",
                      {
                        programId: item.id,
                      }
                    ),
                  isLoading
                )
              }
              onSnapToItem={handleActiveInviteSlideIndexSet}
              layout={"stack"}
              sliderWidth={width - 32}
              itemWidth={width - 32}
              contentContainerCustomStyle={{ borderRadius: 10 }}
              inactiveSlideOpacity={1}
              inactiveSlideScale={1}
              layoutCardOffset={1}
              activeSlideAlignment={"center"}
            />

            <Pagination
              dotsLength={data && data.length >= 3 ? 3 : data.length}
              activeDotIndex={activeSlideIndex}
              animatedDuration={10}
              dotStyle={{
                width: 10,
                height: 10,
                borderRadius: 5,
                backgroundColor: "#0FD8D8",
              }}
              inactiveDotStyle={{
                backgroundColor: "rgba(0, 0, 0, 0.2)",
              }}
              inactiveDotOpacity={1}
              inactiveDotScale={1}
            />
          </View>
        ) : !isLoading && !requestFailed && data && !data.length ? (
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              minHeight: 250,
            }}
          >
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Спиок продуктов пуст
            </Text>
          </View>
        ) : requestFailed ? (
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              minHeight: 250,
            }}
          >
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Ошибка получения данных
            </Text>
          </View>
        ) : null}
      </View>
    </View>
  );
};
