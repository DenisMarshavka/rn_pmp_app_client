import React from "react";
import { Text, View, StyleSheet, Dimensions, Platform } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { ProgressCircle } from "react-native-svg-charts";
import * as Animatable from "react-native-animatable";

import SkeletonLoader from "../loaders/SkeletonLoader";
import { colors, fonts, sizes } from "../../constants/theme";

const { width } = Dimensions.get("window");

const InfoCardWithGraphicsIndicator = ({
  indicatorColor = "red",
  fillIndicatorColor = "#000",
  index = 0,
  item = {},
  duration = 100,
  delay = 100,
  animation = "fadeIn",
  loading = false,
  title = "",
  currentStatus = 0,
  endValue = 0,
  bottomChildren = [],
  toNumberText = 0,
  toNumberStyle = {},
  doneStatus = false,
}) => {
  const isLoading = loading || (item && item.isTest);
  const Wrap = !isLoading ? Animatable.View : View;

  return (
    <Wrap duration={duration} delay={delay} animation={animation}>
      <View style={[styles.card, { paddingBottom: 12 }]}>
        <SkeletonLoader
          isLoading={isLoading}
          layout={[{ width: 80, height: 20 }]}
        >
          <Text
            style={[
              fonts.normal,
              {
                color: "rgba(21, 28, 38, 0.87)",
                fontSize: 16,
                textAlign: "center",
                paddingHorizontal: 10,
              },
            ]}
          >
            {title}
          </Text>
        </SkeletonLoader>

        <SkeletonLoader
          isLoading={isLoading}
          containerStyle={styles.card_container}
          layout={[{ width: 70, height: 70, borderRadius: 70 }]}
        >
          {(currentStatus !== endValue && doneStatus) || !doneStatus ? (
            <Text style={[styles.pie_value, fonts.bold, { ...toNumberStyle }]}>
              {toNumberText}
            </Text>
          ) : null}

          {(currentStatus !== endValue && doneStatus) || !doneStatus ? (
            <ProgressCircle
              style={{ width: "100%", height: "100%" }}
              progress={currentStatus / endValue}
              progressColor={fillIndicatorColor}
              backgroundColor={indicatorColor}
              startAngle={0}
              endAngle={100}
              animate
              strokeWidth={4}
            />
          ) : (
            <MaterialIcons name="done" size={60} color={fillIndicatorColor} />
          )}
        </SkeletonLoader>

        {bottomChildren &&
          bottomChildren.length &&
          bottomChildren.map((child, i) => {
            if (
              child.content &&
              child.skeletonLayoutStyle &&
              child.skeletonLayoutStyle.length
            ) {
              return (
                <SkeletonLoader
                  key={`child-${child && child.id ? child.id : i}`}
                  isLoading={isLoading}
                  containerStyle={
                    child.containerStyle ? { ...child.containerStyle } : {}
                  }
                  layout={[...child.skeletonLayoutStyle]}
                >
                  {child.content}
                </SkeletonLoader>
              );
            }
          })}
      </View>
    </Wrap>
  );
};

export default InfoCardWithGraphicsIndicator;

const styles = StyleSheet.create({
  card: {
    flex: 1,
    width: width / 2 - 22,
    backgroundColor: colors.white,
    borderWidth: 1,
    borderColor: "rgba(21, 28, 38, 0.12)",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 19,
    paddingBottom: 61,
    position: "relative",
  },
  card_container: {
    height: 75,
    width: 70,
    marginTop: 19,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  pie_value: {
    transform: [{ translateY: Platform.OS === "ios" ? 5 : 0 }],
    position: "absolute",
    color: colors.title,
    fontSize: 28,
  },
  card_title: {
    fontSize: 14,
    color: colors.title,
    opacity: 0.5,
    marginTop: 7,
  },
});
