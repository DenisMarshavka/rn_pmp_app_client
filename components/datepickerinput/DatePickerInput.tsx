import React from "react";
import { Input } from "react-native-elements";
import { Keyboard, Platform, View, TouchableOpacity, Text } from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import styles from "./DatepickerInput.styles";
import moment from "moment";
import { colors } from "../../constants/theme";

type IState = {
  show: boolean;
  id?: string;
  date: Date;
  label: string;
  placeholder: string;
  isRequired?: boolean;
  isFocused: boolean;
};

type DatePickerInputProps = {
  show: boolean;
  id?: string;
  date: Date;
  onPress?: (date: any) => void;
  label: string;
  placeholder: string;
  isRequired?: boolean;
  setDate?: (date: any) => void;
};

class DatePickerInput extends React.Component<DatePickerInputProps, IState> {
  state = {
    id: this.props.id,
    show: this.props.show,
    date: this.props.date,
    label: this.props.label,
    placeholder: this.props.placeholder,
    isRequired: this.props.isRequired,
    isFocused: false,
  };

  constructor(props: DatePickerInputProps) {
    super(props);
  }

  static getDerivedStateFromProps(props, state) {
    if (props && props.show !== state.show && Platform.OS === "ios") {
      return {
        show: props.show,
      };
    }

    return null;
  }

  componentDidMount() {
    this.setState({ show: this.props.show });
  }

  show = () => {
    this.setState({
      show: true,
    });
  };

  setDate = (event: any, date: any) => {
    Keyboard.dismiss();
    date = date || this.state.date;
    this.setState({
      show: Platform.OS === "ios" ? true : false,
      date,
    });

    if (this.props.setDate) {
      this.props.setDate(date);
    }
  };

  showDatepicker = () => {
    this.props.onPress();

    this.setState({ show: !this.state.show });

    if (Platform.OS === "ios")
      this.setState({ isFocused: !this.state.isFocused });
  };

  render() {
    const { show, date, label, placeholder, isRequired } = this.state;

    return (
      <View>
        <TouchableOpacity
          onPress={this.showDatepicker}
          style={{ flex: 1, width: "100%" }}
        >
          <Input
            editable={false}
            value={moment(date).format("DD.MM.YYYY")}
            rightIcon={
              isRequired ? <Text style={styles.star}>*</Text> : undefined
            }
            containerStyle={styles.containerStyle}
            inputContainerStyle={
              this.state.isFocused
                ? styles.inputContainerFocusStyle
                : styles.inputContainerStyle
            }
            inputStyle={styles.textInputStyle}
            labelStyle={{
              fontFamily: "PFDin500",
              fontWeight: "normal",
              fontSize: 13,
              color: colors.orange,
            }}
            placeholder={placeholder}
            label={label}
            onFocus={() => {
              this.setState({ isFocused: true });
              this.showDatepicker();
            }}
            onBlur={() => this.setState({ isFocused: false, show: false })}
          />
        </TouchableOpacity>

        {show && (
          <DateTimePicker
            value={date}
            mode={"date"}
            is24Hour={true}
            display="default"
            onChange={this.setDate}
          />
        )}
      </View>
    );
  }
}

export default DatePickerInput;
