import { StyleSheet } from "react-native";
import { fonts } from "../../constants/theme";

const styles = StyleSheet.create({
  textInputStyle: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 18,
    lineHeight: 24,
    color: "#151C26",
    paddingTop: 0,
    paddingBottom: 0,
  },
  labelInputStyle: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 13,
    lineHeight: 18,
    color: "#F46F22",
  },
  containerStyle: {
    marginBottom: 14,
  },
  inputContainerFocusStyle: {
    borderBottomWidth: 1,
    borderColor: "#F46F22",
  },
  inputContainerStyle: {
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0, 0, 0, 0.12)",
  },
  star: {
    fontSize: 24,
    color: "#151C26",
    opacity: 0.54,
  },
});

export default styles;
