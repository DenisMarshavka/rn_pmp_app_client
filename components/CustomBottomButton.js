import React from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  Platform,
} from "react-native";

import { BoxShadow, BorderShadow } from "react-native-shadow";

import { colors, dimensions } from "../styles";
import { fonts, sizes, WINDOW_WIDTH } from "../constants/theme";

const CustomBottomButton = (props) => (
  <View style={{ ...props.style }}>
    {/*<BorderShadow setting={{*/}
    {/*  width: WINDOW_WIDTH,*/}
    {/*  color: "#000",*/}
    {/*  border: 100,*/}
    {/*  opacity: 0.6,*/}
    {/*  side: 'top',*/}
    {/*  inset: false*/}
    {/*}}>*/}
    <TouchableOpacity
      style={[
        styles.overviewStyles,
        {
          bottom: Platform.OS !== "ios" ? 20 : 0,
          backgroundColor: !props.disabled
            ? colors.mainOrange
            : colors.mainGrey,
        },
      ]}
      onPress={() => (!props.disabled ? props.onPress() : null)}
      activeOpacity={0.93}
    >
      <View style={[styles.buttonStyle, styles.buttonShadow]}>
        <Text style={styles.buttonText}>{props.label.toUpperCase()}</Text>
      </View>
    </TouchableOpacity>

    <View />
    {/*</BorderShadow>*/}
  </View>
);

const styles = StyleSheet.create({
  buttonShadow: {
    // shadowColor: colors.absoluteBlack,
    // shadowOffset: {
    //   width: 0,
    //   height: -10,
    // },
    // shadowOpacity: 0.2,
    // shadowRadius: 15,
    // elevation: 32,
    // zIndex: 59,
  },
  buttonText: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 14,
    letterSpacing: 0.16,
    color: colors.mainWhite,
  },
  overviewStyles: {
    position: "absolute",
    left: 0,
    width: "100%",
    height: 75,
    zIndex: 59,

    // elevation: 68,
    backgroundColor: colors.mainOrange,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  buttonStyle: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    borderTopColor: colors.mainBlack,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    zIndex: 59,
    // position: 'absolute',
    // bottom: 0,
  },
});

export default CustomBottomButton;
