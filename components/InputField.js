import React from 'react';
import {
  StyleSheet, Text, View, TextInput,
} from 'react-native';
// import TextInputMask from 'react-native-text-input-mask';
import { colors } from '../styles';
import { fonts, sizes } from "../constants/theme";

const InputField = props => {

 return (
   <View style={[styles.inputFieldContainer, props.style]}>
     <Text style={styles.labelStyle}>{props.label}</Text>
     {
       // props.mask
       //   ? <TextInputMask
       //     value={props.value} style={styles.inputContainer} placeholder={props.placeholder}
       //     onChangeText={(formatted, extracted) => {
       //       console.log(formatted) // +1 (123) 456-78-90
       //       console.log(extracted) // 1234567890
       //       props.onChange(extracted)
       //     }}
       //     mask={props.mask}
       //   />
          <TextInput value={props.value} style={styles.inputContainer} placeholder={props.placeholder} onChange={props.onChange} />
     }
   </View>
 )
};

const styles = StyleSheet.create({
  inputFieldContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 20,
    paddingBottom: 15,
    borderColor: 'rgba(21,28,38, 0.1)',
    borderBottomWidth: 1,
    marginBottom: 25,
  },
  inputContainer: {
    width: 120,
    fontSize: 16,
    fontFamily: fonts.normal.fontFamily,
    lineHeight: 20,
    color: colors.mainBlack,
  },
  labelStyle: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 16,
    opacity: 0.6,
    color: colors.mainBlack,
  },
});

export default InputField;
