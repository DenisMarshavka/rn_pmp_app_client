import React, { useEffect, useState } from "react";
import { Animated, Image, Dimensions } from "react-native";

const { width, height } = Dimensions.get("screen");

const Spin = () => {
  const spinValue = new Animated.Value(0.1);
  const [isStart, setIsStart] = useState(false);

  useEffect(() => {
    setIsStart(true);

    return () => {
      setIsStart(false);
    };
  }, []);

  useEffect(() => {
    if (isStart) spin();
  }, [isStart]);

  const spin = () => {
    Animated.loop(
      // Animated.sequence([
      Animated.timing(spinValue, {
        toValue: 1,
        duration: 450,
        useNativeDriver: true,
      })
      // ])
    ).start();
  };

  const rotate = spinValue.interpolate({
    inputRange: [0, 1],
    outputRange: ["0deg", "360deg"],
  });

  return (
    <Animated.View
      style={{
        transform: [
          {
            rotate,
          },
        ],
      }}
      useNativeDriver={true}
    >
      <Image
        source={require("../../assets/images/loading.png")}
        style={{
          width: width / 5,
          height: width / 5,
        }}
      />
    </Animated.View>
  );
};

export default Spin;
