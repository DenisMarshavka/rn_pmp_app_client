import { StyleSheet } from "react-native";
import { fonts, fontSize14, colors } from "../../constants/theme";

const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: colors.orange,
    borderRadius: 10,
    height: 52,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.2,
    shadowRadius: 20,
    elevation: 10,
  },
  buttonTitleStyle: {
    fontFamily: fonts.medium.fontFamily,
    fontWeight: "500",
    fontSize: fontSize14,
    color: colors.white,
    textTransform: "uppercase",
  },
});

export default styles;
