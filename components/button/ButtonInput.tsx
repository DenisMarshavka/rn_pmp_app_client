import React from "react";
import { TouchableOpacity } from "react-native";
import { Button, Text } from "react-native-elements";

import styles from "./ButtonInput.styles";

type ButtonInputProps = {
  title: string;
  style: object;
  onClick?: () => void;
  disabled: boolean;
};

class ButtonInput extends React.Component<ButtonInputProps, IState> {
  constructor(props: ButtonInputProps) {
    super(props);
  }

  onClick = () => {
    if (this.props.onClick) {
      this.props.onClick();
    }
  };

  render() {
    const { disabled, tiffany } = this.props;

    return (
      <TouchableOpacity
        activeOpacity={!disabled ? 0.5 : 1}
        onPress={() => (disabled ? null : this.onClick())}
      >
        <Button
          title={this.props.title}
          buttonStyle={[
            styles.buttonStyle,
            tiffany && { backgroundColor: !disabled ? "#0FD8D8" : "#E0E1E2" },
            { ...this.props.style },
          ]}
          titleStyle={styles.buttonTitleStyle}
          onPress={this.onClick}
          disabled={disabled}
        >
          {this.props.title}
        </Button>
      </TouchableOpacity>
    );
  }
}

export default ButtonInput;
