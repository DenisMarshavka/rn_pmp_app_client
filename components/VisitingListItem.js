import React from "react";
import { LinearGradient } from "expo-linear-gradient";
import { StyleSheet, Text, View, Image } from "react-native";
import moment from "moment/moment";

import { colors } from "../styles";
import { fonts } from "../constants/theme";
import SkeletonLoader from "./loaders/SkeletonLoader";

const fitness = require("../assets/images/icons/fitness.png");
const beauty = require("../assets/images/icons/beauty.png");

const getIcon = (type) => {
  if (type === 0 || type === 1) {
    return (
      <Image
        style={styles.logo}
        source={require("../assets/images/icons/icon-type-fitness.png")}
        resizeMode={"contain"}
      />
    );
  } else {
    return (
      <Image
        style={styles.logo}
        source={require("../assets/images/icons/icon-type-beauty.png")}
        resizeMode={"contain"}
      />
    );
  }
};

const VisitingListItem = (props) => {
  const { train_type_id, updatedAt, title, loading } = props;

  return (
    <View style={styles.mainView}>
      <LinearGradient
        start={[1.4, 0.8]}
        end={[0.0, 0.0]}
        colors={["#FFFFFF", "#DADADA"]}
        style={styles.linearGradient}
      />

      <View style={{ width: "80%" }}>
        <SkeletonLoader
          layout={[{ width: 220, height: 20, marginLeft: 19 }]}
          isLoading={loading}
        >
          {title && title.trim() ? (
            <Text style={styles.measurementWeight}>{title}</Text>
          ) : null}
        </SkeletonLoader>

        <SkeletonLoader
          layout={[{ width: 50, height: 17, marginTop: 3, marginLeft: 19 }]}
          isLoading={loading}
        >
          {updatedAt && updatedAt.trim() ? (
            <Text style={styles.measurementText}>
              {moment(updatedAt).format("DD.MM.YYYY")}
            </Text>
          ) : null}
        </SkeletonLoader>
      </View>

      <SkeletonLoader layout={[{ ...styles.logo }]} isLoading={loading}>
        {train_type_id !== false ? getIcon(train_type_id) : null}
      </SkeletonLoader>
    </View>
  );
};

const styles = StyleSheet.create({
  linearGradient: {
    position: "absolute",
    borderRadius: 10,
    flex: 1,
    width: "100%",
    height: 56,
  },
  mainView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 58,
    marginVertical: 4,
  },
  measurementText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 14,
    marginLeft: 16,
    color: colors.mainBlack,
    opacity: 0.5,
  },
  measurementWeight: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 14,
    marginLeft: 16,
  },
  logo: {
    marginRight: 16,

    width: 24,
    height: 24,
  },
});

export default VisitingListItem;
