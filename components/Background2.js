import React from "react";
import { Dimensions, ImageBackground, View, StyleSheet, SafeAreaView } from "react-native";

const { height } = Dimensions.get("window");

export const Background2 = ({ children }) => {
  return (
    <View>
      <ImageBackground
        source={require("../assets/images/background/foodDiary.png")}
        resizeMode="stretch"
        style={styles.background}
      >
              <SafeAreaView style={{flex: 1}}>
        {children}
        </SafeAreaView>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    width: "100%",
    height
  }
});
