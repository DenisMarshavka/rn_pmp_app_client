import React, { createRef } from "react";
import {
  Text,
  View,
  Dimensions,
  Animated,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { ListSpecialistItem } from "../../components/ListSpecialistItem";

import SlidingUpPanel from "rn-sliding-up-panel";
import { colors, fonts, sizes } from "../../constants/theme";
import PropTypes from "prop-types";

const { height } = Dimensions.get("window");

class SwipeBottomSheet extends React.Component {
  constructor() {
    super();
    this.state = {
      status: false,
      heightSheet: 0,
    };

    this.heightSheet = this.state.heightSheet;
  }

  static defaultProps = {
    draggableRange: { top: height + this.heightSheet, bottom: 0 },
  };

  _draggedValue = new Animated.Value(0);

  componentDidMount() {
    const { heightSheet = 0 } = this.props;

    this.listener = this._draggedValue.addListener(this.onAnimatedValueChange);

    this.setState({ heightSheet });

    console.log("SPECIALISDT COMPANY ID", this.props.companyId);
  }

  onAnimatedValueChange({ value }) {
    //console.log(value);
  }

  render() {
    const { top, bottom } = this.props.draggableRange;
    const service = this.props.service;
    const { byExpert = false, onBackDropPress = () => {} } = this.props;

    const swipeHeight = !this.props.heightSheet
      ? height - (height / 100) * 35
      : this.props.heightSheet;

    console.log("Show height: ", swipeHeight);

    return (
      <SlidingUpPanel
        backdropStyle={{
          backgroundColor: "grey",
          opacity: 0.5,
        }}
        draggableRange={{
          top: swipeHeight,
          bottom: 0,
        }}
        animatedValue={this._draggedValue}
        backdropOpacity={0.5}
        snappingPoints={[360]}
        allowDragging={this.state.allowDragging}
        friction={0.3}
        ref={this.props.swipeModalRef}
        onBackDropPress={onBackDropPress}
      >
        <View style={styles.panel}>
          <View style={styles.panelHeader}>
            <View style={styles.icon} />

            {this.props.title && this.props.title.trim() ? (
              <Text style={[styles.textHeader, fonts.bold, fonts.h2]}>
                {this.props.title}
              </Text>
            ) : null}
          </View>

          <View style={styles.container}>
            <ScrollView
              onTouchStart={() => this.setState({ allowDragging: false })}
              onTouchEnd={() => {
                this.setState({ allowDragging: true });
              }}
              onTouchCancel={() => {
                this.setState({ allowDragging: true });
              }}
              style={{ height: "100%" }}
              contentContainerStyle={{ ...this.props.contentContainerStyle }}
            >
              {this.props.children}
            </ScrollView>
          </View>
        </View>
      </SlidingUpPanel>
    );
  }
}

SwipeBottomSheet.propTypes = {
  swipeModalRef: PropTypes.object,
};

export default SwipeBottomSheet;

const styles = {
  container: {
    //flex: 1,
    backgroundColor: "#fff",
    position: "relative",
    // marginBottom: 50,
  },
  fullFill: {
    flex: 1,
    minHeight: height / 2.4,
    alignItems: "center",
    justifyContent: "center",
  },
  panel: {
    flex: 1,
    backgroundColor: "white",
    position: "relative",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    height: 75,
    backgroundColor: "#fff",
    //justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 0.18,
    shadowRadius: 8,
    shadowColor: "#000000",
    shadowOffset: { height: 1, width: 0 },
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    position: "relative",
  },
  textHeader: {
    color: colors.title,
    opacity: 0.87,
    marginTop: "8%",
  },
  icon: {
    backgroundColor: colors.title,
    borderRadius: 10,
    opacity: 0.12,
    width: 44,
    height: 4,
    position: "absolute",
    top: 15,
  },
  gradient: {
    marginBottom: 8,
    borderRadius: 10,
    marginHorizontal: 16,
  },
};
