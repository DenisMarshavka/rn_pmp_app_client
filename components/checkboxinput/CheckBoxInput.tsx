import * as React from "react";
import { CheckBox } from "react-native-elements";
import styles from "./CheckBoxInput.styles";

type IState = {
  title: string;
  checked?: boolean;
  style: {};
};

type CheckBoxInputProps = {
  title: string;
  checked?: boolean;
  onPress?: () => void;
  notPressWhenDisabled?: boolean;
  style: {};
};

class CheckBoxInput extends React.Component<CheckBoxInputProps, IState> {
  state = {
    title: this.props.title,
    checked: this.props.checked,
    style: this.props.style,
    disabled: this.props.disabled,
  };

  constructor(props: CheckBoxInputProps) {
    super(props);
  }

  onPress = () => {
    // this.setState({ 'checked': true });
    if (this.props.onPress) {
      this.props.onPress();
    }
  };

  static defaultProps = {
    notPressWhenDisabled: false,
  };

  render() {
    return (
      <CheckBox
        containerStyle={styles.checkBoxStyle}
        textStyle={[
          styles.checkBoxTextStyle,
          this.state.style,
          { opacity: this.state.disabled ? 0.5 : 1 },
        ]}
        title={this.state.title}
        onPress={() =>
          this.state.disabled && this.props.notPressWhenDisabled
            ? null
            : this.onPress()
        }
        checkedIcon="dot-circle-o"
        uncheckedIcon="circle-o"
        checkedColor={this.props.tiffany ? "#0FD8D8" : "#F46F22"}
        checked={this.props.checked}
      />
    );
  }
}

export default CheckBoxInput;
