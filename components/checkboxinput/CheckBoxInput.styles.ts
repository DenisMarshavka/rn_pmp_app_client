import { StyleSheet } from "react-native";
import { fonts } from "../../constants/theme";

const styles = StyleSheet.create({
    checkBoxStyle: {
        borderWidth: 0,
        paddingLeft: 0,
        margin: 0,
        padding: 0,
        marginLeft: 0,
        backgroundColor: 'transparent'
    },
    checkBoxTextStyle: {
        marginLeft: 0,
        fontFamily: fonts.medium.fontFamily,
        fontWeight: "500",
        fontSize: 13,
        color: '#1E2022',
    }
})

export default styles;
