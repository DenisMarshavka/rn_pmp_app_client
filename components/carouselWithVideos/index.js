import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Modal,
  View,
  ScrollView,
  Dimensions,
  Image,
} from "react-native";
import YoutubePlayer from "react-native-youtube-iframe";
import ImageView from "react-native-image-view";

import { fonts, colors, sizes } from "../../constants/theme";

const CarouselWithVideos = ({
  sections = [],
  isModalWithImages = false,
  itemSize = { height: 0, width: 0 },
}) => {
  const [showVideo, setShowVideo] = useState(false);
  const [videoId, setVideoId] = useState(null);
  const [visibleModal, setVisibleModal] = useState([]);
  const [currentImages, setCurrentImages] = useState(null);
  const [imageIndex, setImageIndex] = useState(0);

  let modalRef = useRef();

  console.log("modalRef", modalRef);

  const showVideoSection = (video_id, block) => {
    if (video_id) setVideoId(video_id);

    if (!block) showVideo ? setShowVideo(false) : setShowVideo(true);
  };

  const renderCarouselSection = (
    sectionTitle = "",
    dataList = [],
    isImages = false
  ) => (
    <View style={{ marginTop: 30, flex: 1 }}>
      {sectionTitle && sectionTitle.trim() ? (
        <Text
          style={[fonts.normal, fonts.h2, { paddingHorizontal: sizes.margin }]}
        >
          {sectionTitle}
        </Text>
      ) : null}

      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={{
          flex: 1,
          paddingHorizontal: sizes.margin,
          marginTop: 20,
        }}
      >
        {dataList.map((item, i) => {
          const block =
            item.block !== undefined
              ? item.block
              : item.active !== undefined
              ? !item.active
              : false;

          return (
            <TouchableOpacity
              key={`section-exercises-image-${item.id}-${item.name}-${i}`}
              onPress={() => {
                if (!isImages) {
                  if (!block) showVideoSection(item.video_link, block);
                } else {
                  setVisibleModal(true);
                  setImageIndex(i);
                }
              }}
              activeOpacity={block ? 1 : 0.5}
              style={{
                marginRight: i !== dataList.length - 1 ? 9 : sizes.margin * 2,
                alignItems: "center",
                flex: 1,
                width:
                  dataList.length === 1 && isImages
                    ? Dimensions.get("screen").width - sizes.margin * 2
                    : itemSize.width,
              }}
            >
              <Image
                source={{ uri: !isImages ? item.preview_img : item }}
                style={{
                  width: "100%",
                  height: itemSize.height,
                  borderRadius: 7,
                }}
              />

              {block && !isImages ? (
                <View
                  style={{
                    maxHeight: itemSize.height,
                    ...StyleSheet.absoluteFillObject,
                    backgroundColor: "rgba(22, 22, 22, 0.86)",
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 7,
                  }}
                >
                  <Image
                    source={require("../../assets/icons/block.png")}
                    style={{
                      width: 26,
                      height: 26,
                    }}
                  />

                  {item.start && (
                    <Text
                      style={{ color: "#fff", paddingTop: 7, fontSize: 11 }}
                    >
                      {item.start}
                    </Text>
                  )}
                </View>
              ) : null}

              {item && item.name && item.name.trim() ? (
                <Text
                  numberOfLines={3}
                  style={[
                    fonts.normal,
                    fonts.description,
                    {
                      color: "rgba(30, 32, 34, 0.54)",
                      marginTop: 10,
                      width: "100%",
                      flex: 1,
                      textAlign: "center",
                    },
                  ]}
                >
                  {item.name}
                </Text>
              ) : null}

              {item && item.time && item.time.trim() ? (
                <Text
                  style={[
                    fonts.normal,
                    fonts.description,
                    { color: colors.title, marginTop: 6 },
                  ]}
                >
                  {item.time}
                </Text>
              ) : null}
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );

  useEffect(() => {
    const images = [];

    if (
      sections &&
      sections[0] &&
      sections[0].images &&
      sections[0].images.length
    )
      sections[0].images.forEach((item) => {
        if (item && typeof item === "string" && item.trim())
          images.push({
            source: { uri: item },
            width: Dimensions.get("screen").width,
            height: Dimensions.get("screen").width / 1.7,
          });
      });

    if (images.length) setCurrentImages(images);

    console.log("images", images);

    return () => {
      setCurrentImages([]);
      setImageIndex(0);
      setVisibleModal(false);
    };
  }, [sections]);

  return (
    <>
      {sections.map((item) => {
        console.log(
          "ITTEEEMM VIDEOS",
          item.videos,
          "ITTEEEMM IMAGES",
          item.images
        );

        if (item) {
          return item.videos && item.videos.length
            ? renderCarouselSection(item.name, item.videos)
            : item.images && item.images.length
            ? renderCarouselSection(item.name, item.images, true)
            : null;
        }

        return null;
      })}
      {!isModalWithImages ? (
        <Modal
          animationType="slide"
          transparent={true}
          visible={showVideo}
          onRequestClose={() => setShowVideo(false)}
        >
          <View style={styles.centeredView}>
            <TouchableOpacity
              onPress={() => showVideoSection()}
              style={styles.videoShadow}
            />

            <YoutubePlayer
              height={200}
              width={"100%"}
              videoId={videoId}
              play={true}
              onChangeState={(event) => console.log(event)}
              onPlaybackQualityChange={(q) => console.log(q)}
              volume={50}
              playbackRate={1}
              playerParams={{
                cc_lang_pref: "us",
                showClosedCaptions: true,
              }}
            />
          </View>
        </Modal>
      ) : null}

      {visibleModal &&
      currentImages &&
      currentImages &&
      currentImages.length ? (
        <ImageView
          images={currentImages}
          animationType={"fade"}
          imageIndex={imageIndex}
          isVisible={visibleModal}
          onClose={() => {
            setImageIndex(0);
            setVisibleModal(false);
          }}
          onImageChange={setImageIndex}
        />
      ) : null}
    </>
  );
};

export default CarouselWithVideos;

const styles = StyleSheet.create({
  videoShadow: {
    opacity: 0.7,
    position: "absolute",
    width: "100%",
    height: "100%",
    top: 0,
    backgroundColor: "#000",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
