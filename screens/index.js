export TestScreen from "./TestScreen";
export LoadingScreen from "./LoadingScreen";
export RegAuthScreen from "./Auth/RegAuthScreen";
export LoginScreen from "./LoginScreen";

export HomeScreen from "./Client/HomeScreen";
export { BeautyStudioScreen } from "./Client/BeautyStudioScreen";
export { FitnessStudioScreen } from "./Client/FitnessStudioScreen";
export MainMenuScreen from "./Client/MainMenuScreen";
export AboutScreen from "./Client/About/AboutScreen";
export ChatScreen from "./Client/Chat";
export ChatDialogScreen from "./Client/Chat/ChatDialogScreen";
export ReviewsScreen from "./Client/Reviews";
export CreateReview from "./Client/Reviews/CreateReview";
export ListServiceScreen from "./Client/ListServiceScreen";
export { ListProductScreen } from "./Client/ListProductScreen";
export ItemServiceScreen from "./Client/ItemServiceScreen";
export ItemSpecialistScreen from "./Client/ItemSpecialistScreen";
export SelectServiceDateScreen from "./Client/SelectServiceDateScreen";
export SubmitServiceScreen from "./Client/SubmitServiceScreen";
export ProfileScreen from "./Client/Profile/ProfileScreen";
export StatisticsScreen from "./Client/Profile/StatisticsScreen";
export ScheduleScreen from "./Client/Profile/ScheduleScreen";
export FoodDiaryScreen from "./Client/Profile/foodDiary/FoodDiaryScreen";
export { AddDishScreen } from "./Client/Profile/addDish/AddDishScreen";
export MyProgramScreen from "./Client/Profile/myProgram/MyProgramScreen";
export CertificatesScreen from "./Client/certificates/CertificatesScreen";
export ProgramScreen from "./Client/Profile/myProgram/ProgramScreen";
export DetoxProgramScreen from "./Client/DetoxProgram";
export CertificateScreen from "./Client/certificates/CertificateScreen";

export NewsScreen from "./Client/News/NewsScreen";
export ArticleScreen from "./Client/News/ArticleScreen";
export ArticleDetailsScreen from "./Client/News/ArticleDetailsScreen";

export NotificationScreen from "./Client/Notifications";
export Points from "./Client/Points/Points";
export PersonalDataScreen from "./Client/Profile/PersonalData/PersonalDataScreen";
export SettingsScreen from "./Client/Settings/SettingsScreen";
export SupportScreen from "./Client/Settings/Support/SupportScreen";
export PrivacyScreen from "./Client/Settings/Privacy/PrivacyScreen";
export ProgramsScreen from "./Client/ProgramsScreen";
export NewProducts from "./Client/ProgramsScreen";

export Specialists from "./Client/Specialists";
export Coaches from "./Client/Coaches";
export EmployeeCard from "./Client/EmployeeCard";
export ListServiceSpecialists from "./Client/ServiceSpecialists/ListServiceSpecialists";
export StudioSelection from "./Client/StudioSelection";
export Program from "./Client/Program";
export VisitingHistory from "./Client/Profile/VisitingHistory";
/*--Confirm--*/
export {
  ConfirmPasswordScreen,
  ConfirmPhoneScreen,
  ConfirmSmsCodeScreen,
} from "./Client/Confirm";
export MapScreen from "./Client/Map/MapScreen";
export ItemBasketProductScreen from "./Client/barShop/ItemBasketProductScreen";

/*--Basket--*/
export {
  MenuListScreen,
  BasketScreen,
  PaymentScreen,
  HistoryScreen,
} from "./Client/barShop";

/*--Result Diary--*/
export {
  DiaryResultScreen,
  AddWeightMeasurementScreen,
  AddBodyMeasurementScreen,
} from "./Client/resultDiary";
