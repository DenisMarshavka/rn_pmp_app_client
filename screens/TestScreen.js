import React from 'react';
import { TouchableOpacity, ScrollView, View, Text, StyleSheet } from 'react-native';
import { Divider } from 'react-native-elements';

const TestScreen = ({ navigation }) => {
  return (
    <ScrollView>
      <View
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: 50,
        }}
      >
        <TouchableOpacity onPress={() => navigation.navigate('Home')} style={styles.btn}>
          <Text>Home</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('MainMenu')} style={styles.btn}>
          <Text>Main menu</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('FitnessStudio')} style={styles.btn}>
          <Text>FitnessStudio</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('BeautyStudio')} style={styles.btn}>
          <Text>BeautyStudio</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('ChangePassword')} style={styles.btn}>
          <Text>Change Password</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('About')} style={styles.btn}>
          <Text>About</Text>
        </TouchableOpacity>

        <Divider style={styles.divider} />
        <Text style={styles.title}>Выбор пакета</Text>
        <TouchableOpacity onPress={() => navigation.navigate('ListProducts')} style={styles.btn}>
          <Text>ListProducts</Text>
        </TouchableOpacity>

        <Divider style={styles.divider} />
        <Text style={styles.title}>Выбор услуги</Text>
        <TouchableOpacity onPress={() => navigation.navigate('ListServices')} style={styles.btn}>
          <Text>ListServices</Text>
        </TouchableOpacity>

        <Divider style={styles.divider} />
        <Text style={styles.title}>Экраны "Услуга" и "Специалист"</Text>
        <TouchableOpacity onPress={() => navigation.navigate('ItemService')} style={styles.btn}>
          <Text>ItemService</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('ItemSpecialist')} style={styles.btn}>
          <Text>ItemSpecialist</Text>
        </TouchableOpacity>

        <Divider style={styles.divider} />
        <Text style={styles.title}>Bar Screens</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Basket')} style={styles.btn}>
          <Text>Basket</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('History')} style={styles.btn}>
          <Text>History</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('Item')} style={styles.btn}>
          <Text>Item</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('MenuList')} style={styles.btn}>
          <Text>MenuList</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('Payment')} style={styles.btn}>
          <Text>Payment</Text>
        </TouchableOpacity>

        <Divider style={styles.divider} />
        <Text style={styles.title}>Feedbacks</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Feedbacks')} style={styles.btn}>
          <Text>Feedbacks</Text>
        </TouchableOpacity>

        <Divider style={styles.divider} />
        <Text style={styles.title}>News</Text>
        <TouchableOpacity onPress={() => navigation.navigate('News')} style={styles.btn}>
          <Text>News</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Article')} style={styles.btn}>
          <Text>Article</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('ArticleDetails')} style={styles.btn}>
          <Text>ArticleDetails</Text>
        </TouchableOpacity>


        <Divider style={styles.divider} />
        <Text style={styles.title}>Notifications</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Notifications')} style={styles.btn}>
          <Text>Notifications</Text>
        </TouchableOpacity>

        <Divider style={styles.divider} />
        <Text style={styles.title}>Points</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Points')} style={styles.btn}>
          <Text>Points</Text>
        </TouchableOpacity>


        <Divider style={styles.divider} />
        <Text style={styles.title}>Chat</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Chat')} style={styles.btn}>
          <Text>Chat</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('ChatDialog')} style={styles.btn}>
          <Text>ChatDialog</Text>
        </TouchableOpacity>


        <Divider style={styles.divider} />
        <Text style={styles.title}>Result Diary</Text>
        <TouchableOpacity onPress={() => navigation.navigate('AddBodyMeasurement')} style={styles.btn}>
          <Text>AddBodyMeasurement</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('AddWeightMeasurement')} style={styles.btn}>
          <Text>AddWeightMeasurement</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('DiaryResult')} style={styles.btn}>
          <Text>DiaryResult</Text>
        </TouchableOpacity>

        <Divider style={styles.divider} />
        <Text style={styles.title}>Профиль</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={styles.btn}>
          <Text>Profile</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Statistics')} style={styles.btn}>
          <Text>Статистика посещений</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Schedule')} style={styles.btn}>
          <Text>Расписание</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('FoodDiary')} style={styles.btn}>
          <Text>Дневник питания</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('AddDish')} style={styles.btn}>
          <Text>Добавить блюдо</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Certificate')} style={styles.btn}>
          <Text>Сертификаты</Text>
        </TouchableOpacity>


        <Divider style={styles.divider} />
        <Text style={styles.title}>--ТРЕНЕР--</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={styles.btn}>
          <Text>AddTaskScreen</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={styles.btn}>
          <Text>ClientsScreen</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={styles.btn}>
          <Text>TaskScreen</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={styles.btn}>
          <Text>TasksScreen</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={styles.btn}>
          <Text>TimetableScreen</Text>
        </TouchableOpacity>


        <Divider style={styles.divider} />
        <Text style={styles.title}>--АДМИН--</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={styles.btn}>
          <Text>MainScreen</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={styles.btn}>
          <Text>CreateScreen</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={styles.btn}>
          <Text>FeedbackScreen</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={styles.btn}>
          <Text>PushScreen</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  )
}

export default TestScreen;

const styles = StyleSheet.create({
  btn: {
    marginBottom: 20,
  },
  title: {
    fontWeight: 'bold',
    textAlign: 'left',
    marginBottom: 13,
  },
  divider: {
    backgroundColor: 'blue',
    height: 1,
    width: '100%',
    marginBottom: 20,
  },
});
