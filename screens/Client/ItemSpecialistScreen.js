import React, { useState, useEffect } from "react";
import YoutubePlayer from "react-native-youtube-iframe";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Modal,
  Dimensions,
  ActivityIndicator,
  SafeAreaView,
} from "react-native";
import HTMLView from "react-native-htmlview";
import moment from "moment";
import { connect } from "react-redux";

import BackHeader from "../../components/Headers/BackHeader";
import { getTrainerInfoById } from "../../actions/TrainersActions";
import ButtonInput from "../../components/button/ButtonInput.tsx";
import { ToastView } from "../../components";
import { Background } from "../../components/Background";
import { Evaluate } from "../../components/Evaluate";
import Header from "../../components/Header";
import { colors, fonts, sizes, WINDOW_WIDTH } from "../../constants/theme";
import { ChatAPI } from "../../api/chat";
import { StaffAPI } from "../../api/staff";
import { getUserStorageData, validURL } from "../../utils";
import { NoAvatarTrainerSvg } from "../../assets/icons/NoAvatarTrainerSvg/NoAvatarTrainerSvg";
import { getStaffScheduleByToday } from "../../actions/StaffActions";

let deviceWidth = Dimensions.get("window").width;

const gallery = [
  {
    id: "AVAc1gYLZK0",
    preview_image:
      "https://cdn.lifehacker.ru/wp-content/uploads/2018/03/cover-2_1520669590-1140x570.jpg",
  },
  {
    id: "rj73lCWeZQw",
    preview_image:
      "https://img.championat.com/s/735x490/news/big/n/w/pjat-uprazhnenij-dlja-trenirovki-doma-video_1575575829142046365.jpg",
    block: true,
  },
  {
    id: "AVAc1gYLZK0",
    preview_image:
      "https://posta-magazine.ru/wp-content/uploads/2020/01/l_main_best-fitness-strategy_posta-magazine.jpg",
    block: true,
  },
];

const ItemSpecialistScreen = ({
  navigation,
  route,
  companyIdAndStaffIdLoading = false,
  companyIdAndStaffIdData = {},
  companyIdAndStaffIdError = false,
  getStaffScheduleByToday = () => {},

  getTrainerInfoById = () => {},
  currentTrainerLoading = true,
  currentTrainer = {},
  currentTrainerError = null,
}) => {
  const [evaluateModal, setEvaluateModal] = useState(false);
  const [showVideo, setShowVideo] = useState(false);
  const [videoId, setVideoId] = useState(null);

  const staff = route.params && route.params.staff ? route.params.staff : {};
  const service =
    route.params.service !== undefined ? route.params.service : {};
  const fromChat =
    route.params.fromChat !== undefined ? route.params.fromChat : false;
  const { companyId = false, fromOnlineService = false } =
    route && route.params
      ? route.params
      : {}
      ? companyIdAndStaffIdData.company_id
      : false;
  const byExpert = route.params.byExpert ? route.params.byExpert : false;
  const fromService = route.params.fromService
    ? route.params.fromService
    : false;

  const [lessonsLoading, setLessonsLoading] = useState(true);
  const [lessonsError, setLessonsError] = useState(null);
  const [lessonsData, setLessonsData] = useState([]);

  const [lessonsOffset, setLessonsOffset] = useState(0);
  const [lessonsLimit, setLessonsLimit] = useState(15);

  useEffect(() => {
    const date = new Date();
    const currentDate = moment(date).format("YYYY-MM-DD");

    let lessonsResponse = null;

    if (staff && Object.keys(staff).length && staff.id) {
      getStaffScheduleByToday(
        staff.id,
        staff.yid,
        companyId,
        currentDate,
        currentDate,
        byExpert
      );

      console.log("staffffffff", staff);

      if (staff.id) {
        getTrainerInfoById(staff.id);

        gettrainerLessonsVideos(staff.id);
      }
    } else {
      setLessonsLoading(false);
      setLessonsError(true);

      console.log("Error leassns 1", staff);
      ToastView(
        "Ошибка 203! Ошибка получения данных видеоуроков.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27."
      );
    }

    // console.log("SCREEEN SCPECILIST COMPANY ID", companyId, "staff", staff);

    return () => {
      setLessonsData([]);
      setLessonsLoading(true);
      setLessonsError(null);
      setLessonsLimit(0);
      setLessonsOffset(15);
    };
  }, [staff]);

  const gettrainerLessonsVideos = async (trainerId = NaN) => {
    let lessonsResponse = null;

    if (trainerId && !isNaN(trainerId)) {
      StaffAPI.getLessonsByTrainerId(staff.id)
        .then((response) => {
          lessonsResponse = response;

          if (
            response &&
            response.status &&
            response.status === "success" &&
            response.data &&
            response.data.rows
          ) {
            setLessonsLoading(false);
            setLessonsData(response.data.rows);
          } else {
            setLessonsLoading(false);
            setLessonsError(true);

            ToastView(
              "Ошибка 201! Ошибка получения данных видеоуроков.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27."
            );

            console.log(
              "Error2 StaffAPI.getLessonsByTrainerId --- ",
              lessonsResponse
            );
          }

          console.log("lessonsResponse", lessonsResponse);
        })
        .catch((e) => {
          setLessonsLoading(false);
          setLessonsError(true);

          ToastView(
            "Ошибка 202! Ошибка получения данных видеоуроков.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27."
          );

          console.log("Error StaffAPI.getLessonsByTrainerId --- ", e);
        });
    }
  };

  const callback = (evaluateModal) => {
    setEvaluateModal(evaluateModal);
  };

  const goToSelectDate = () => {
    navigation.navigate("SelectServiceDate", {
      companyId,
      byExpert,
      service,
      staff,
      isOnlineService: fromOnlineService,
      tiffany: route.params.tiffany,
      trainType: route.params.trainType,
    });
  };

  const sendMessageToTrainer = async () => {
    const userData = await getUserStorageData();
    let newMessage = {};

    const existUserData =
      userData && Object.keys(userData) && Object.keys(userData).length;

    // if (existUserData && staff && staff.id !== false) {
    //   newMessage = {
    //     user_id: userData.id,
    //     trainer_id: staff.id,
    //     sender_type: 0,
    //     receiver_type: 1,
    //     message: "Здравствуйте!",
    //   };
    // }

    if (Object.keys(newMessage).length) {
      await ChatAPI.sendMessage(newMessage);

      const login = staff && staff.fullname ? staff.fullname : "";
      const img = staff && staff.avatar ? staff.avatar : noAvatar;

      if (
        login &&
        staff &&
        staff.id &&
        existUserData &&
        newMessage &&
        Object.keys(newMessage) &&
        Object.keys(newMessage).length
      ) {
        navigation.navigate("ChatDialog", {
          trainer: { trainer_id: staff.id, login, img },
        });
      }
    }
  };

  const showVideoSection = (video_id, block) => {
    if (video_id) {
      setVideoId(video_id);
    }

    if (!block) setShowVideo(!showVideo);
  };

  const CommonWrapper = !currentTrainerLoading ? ScrollView : View;

  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      {fromChat ? (
        <SafeAreaView>
          <Header
            back={true}
            title={"Профиль тренера"}
            routeName={route && route.name ? route.name : ""}
            backTitle={"Назад"}
            style={{ paddingTop: 5 }}
          />
        </SafeAreaView>
      ) : (
        <SafeAreaView>
          <BackHeader
            tiffany={route.params.tiffany}
            backTitle={"Назад"}
            routeName={route && route.name ? route.name : ""}
            receivedParams={route.params || {}}
          />
        </SafeAreaView>
      )}

      <CommonWrapper
        showsVerticalScrollIndicator={false}
        style={{ marginBottom: !fromService ? 100 : "40" }}
        contentContainerStyle={{ paddingBottom: !fromService ? 50 : 0 }}
      >
        <View
          style={{ paddingHorizontal: 16, flex: 1, justifyContent: "center" }}
        >
          <View style={styles.profile}>
            {currentTrainerLoading ? (
              <View
                style={{
                  ...styles.fullFill,
                  minHeight: Dimensions.get("screen").height / 1.3,
                }}
              >
                <ActivityIndicator size="large" color={colors.orange} />
              </View>
            ) : !currentTrainerError &&
              currentTrainer &&
              Object.keys(currentTrainer).length ? (
              <>
                {currentTrainer &&
                currentTrainer.avatar &&
                validURL(currentTrainer.avatar) ? (
                  <Image
                    style={{
                      marginTop: 34,
                      width: 82,
                      height: 82,
                      borderRadius: 82,
                    }}
                    source={{ uri: currentTrainer.avatar }}
                  />
                ) : (
                  <NoAvatarTrainerSvg height={82} width={82} />
                )}

                <View style={{ flex: 1, width: "100%", alignItem: "center" }}>
                  <Text
                    style={[
                      {
                        marginTop: 26,
                        color: colors.title,
                        fontSize: 28,
                        textAlign: "center",
                      },
                      fonts.bold,
                    ]}
                  >
                    {currentTrainer.fullname ||
                      `Неизвестный ${byExpert ? "специалист" : "тренер"}`}
                  </Text>

                  <Text
                    style={{
                      flex: 1,
                      width: "100%",
                      textAlign: "center",
                      color: colors.title,
                      opacity: 0.4,
                    }}
                  >
                    {byExpert ? "Специалист" : "Тренер"}
                  </Text>

                  <Text
                    style={[
                      {
                        marginTop: 7,
                        color: colors.title,
                        fontSize: 14,
                        opacity: sizes.opcity,
                      },
                      fonts.normal,
                    ]}
                  >
                    {staff.specialization}
                  </Text>

                  <View style={{ marginTop: 28 }}>
                    {/*<Text style={styles.sectionsTitle}>Описание</Text>*/}

                    <HTMLView
                      value={currentTrainer.about}
                      stylesheet={{
                        span: {
                          color: colors.title,
                          fontSize: 15,
                          lineHeight: 22,
                          fontFamily: fonts.normal.fontFamily,
                          fontWeight: fonts.normal.fontStyle,
                        },
                      }}
                    />
                  </View>

                  {currentTrainer.recomendations &&
                  currentTrainer.recomendations.trim() ? (
                    <View style={{ marginTop: 28 }}>
                      <Text style={styles.sectionsTitle}>Рекомендации</Text>

                      <HTMLView
                        value={currentTrainer.recomendations}
                        stylesheet={{
                          span: {
                            color: colors.title,
                            fontSize: 15,
                            lineHeight: 22,
                            fontFamily: fonts.normal.fontFamily,
                            fontWeight: fonts.normal.fontStyle,
                          },
                        }}
                      />
                    </View>
                  ) : null}
                </View>
              </>
            ) : (
              <View style={styles.fullFill}>
                <Text
                  style={{
                    width: "60%",
                    height: "100%",
                    textAlign: "center",
                    color: "#000",
                  }}
                >
                   Ошибка получения данных видеоуроков. Пожалуйста, обратитесь в
                  поддержку по телефону +7 495 797 94 27
                </Text>
              </View>
            )}
          </View>
        </View>

        {!currentTrainerLoading ? (
          <View style={{ marginTop: 28 }}>
            <Text style={[styles.sectionsTitle, { marginLeft: 16 }]}>
              Видеоуроки
            </Text>

            {lessonsLoading ? (
              <View style={{ ...styles.fullFill, height: 200 }}>
                <ActivityIndicator size="large" color={colors.orange} />
              </View>
            ) : !lessonsError &&
              !lessonsLoading &&
              lessonsData &&
              lessonsData.length ? (
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                style={{ marginTop: 20 }}
              >
                {lessonsData.map((item, i) => (
                  <TouchableOpacity
                    key={`exercises-image-${i}`}
                    onPress={() => showVideoSection(item.key, item.block)}
                    style={{
                      marginRight: i !== lessonsData.length - 1 ? 9 : 16,
                      marginLeft: !i ? 16 : 0,
                      alignItems: "center",
                      maxHeight: 200,
                    }}
                    activeOpacity={!item.block ? 0.5 : 1}
                  >
                    <Image
                      source={{ uri: item.img }}
                      style={{
                        width: Math.ceil(Dimensions.get("screen").width / 1.1),
                        height: Math.ceil(
                          Dimensions.get("screen").height / 3.1
                        ),
                        maxHeight: 200,
                        borderRadius: 7,
                      }}
                    />

                    {!item.block ? null : (
                      <View
                        style={{
                          flex: 1,
                          ...StyleSheet.absoluteFillObject,
                          backgroundColor: "rgba(22, 22, 22, 0.86)",
                          justifyContent: "center",
                          alignItems: "center",
                          borderRadius: 7,
                        }}
                      >
                        <Image
                          source={require("../../assets/icons/block.png")}
                          style={{
                            width: 26,
                            height: 26,
                          }}
                        />
                      </View>
                    )}
                  </TouchableOpacity>
                ))}
              </ScrollView>
            ) : !lessonsError &&
              !lessonsLoading &&
              lessonsData &&
              !lessonsData.length ? (
              <View style={[styles.fullFill, { height: 200 }]}>
                <Text
                  style={{
                    width: "100%",
                    textAlign: "center",
                    color: "#000",
                  }}
                >
                  Список пуст{" "}
                </Text>
              </View>
            ) : (
              <View style={[styles.fullFill, { height: 200 }]}>
                <Text
                  style={{
                    width: "100%",
                    textAlign: "center",
                    color: "#000",
                  }}
                >
                  Ошибка получения данных
                </Text>
              </View>
            )}
          </View>
        ) : null}
      </CommonWrapper>

      {!currentTrainerLoading ? (
        <View
          style={{
            width: "100%",
            paddingHorizontal: 16,
            flex: 1,
            justifyContent: "center",
            paddingVertical: 10,
            paddingBottom: 0,
            position: "absolute",
            bottom: 0,
            left: 0,
            backgroundColor: "#fff",
          }}
        >
          {companyIdAndStaffIdLoading ? (
            <View style={[styles.fullFill, { marginTop: 25 }]}>
              <ActivityIndicator color={colors.mainOrange} />
            </View>
          ) : !fromChat && fromService ? (
            <TouchableOpacity
              activeOpacity={companyIdAndStaffIdLoading ? 1 : 0.7}
              style={{
                width: WINDOW_WIDTH - 30,
                marginTop: 20,
                marginBottom: 45,
              }}
            >
              <ButtonInput
                //disabled={!!companyIdAndStaffIdError || !companyIdAndStaffIdData || !companyIdAndStaffIdData.company_id || !companyIdAndStaffIdData.yid}
                tiffany={route.params.tiffany}
                title="ЗАПИСАТЬСЯ"
                onClick={() => {
                  !companyIdAndStaffIdLoading ? goToSelectDate() : null;
                }}
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              activeOpacity={companyIdAndStaffIdLoading ? 1 : 0.7}
              style={{
                width: WINDOW_WIDTH - 30,
                marginTop: 20,
                marginBottom: 45,
              }}
            >
              <ButtonInput
                //disabled={!!companyIdAndStaffIdError || !companyIdAndStaffIdData || !companyIdAndStaffIdData.company_id || !companyIdAndStaffIdData.yid}
                tiffany={route.params.tiffany}
                title="НАПИСАТЬ"
                onClick={() => {
                  sendMessageToTrainer();
                }}
              />
            </TouchableOpacity>
          )}
        </View>
      ) : null}

      {evaluateModal && (
        <Evaluate parentCallback={callback} navigation={navigation} />
      )}

      <Modal
        animationType="slide"
        transparent={true}
        visible={showVideo}
        onRequestClose={() => setShowVideo(false)}
      >
        <View style={styles.centeredView}>
          <TouchableOpacity
            onPress={() => showVideoSection()}
            style={styles.videoShadow}
          />

          <YoutubePlayer
            height={200}
            width={"100%"}
            videoId={videoId}
            play={true}
            onChangeState={(event) => console.log(event)}
            onPlaybackQualityChange={(q) => console.log(q)}
            volume={50}
            playbackRate={1}
            playerParams={{
              cc_lang_pref: "us",
              showClosedCaptions: true,
            }}
          />
        </View>
      </Modal>
    </View>
  );
};

const mapStateToProps = (state) => ({
  companyIdAndStaffIdLoading: state.ServicesReducer.companyIdAndStaffIdLoading,
  companyIdAndStaffIdData: state.ServicesReducer.companyIdAndStaffIdData,
  companyIdAndStaffIdError: state.ServicesReducer.companyIdAndStaffIdError,

  currentTrainerLoading: state.TrainersReducer.currentTrainerLoading,
  currentTrainer: state.TrainersReducer.currentTrainer,
  currentTrainerError: state.TrainersReducer.currentTrainerError,
});

const mapDispatchToProps = {
  getStaffScheduleByToday,
  getTrainerInfoById,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemSpecialistScreen);

const styles = StyleSheet.create({
  fullFill: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  profile: {
    alignItems: "center",
  },
  sectionsTitle: {
    width: "100%",
    textAlign: "left",
    marginBottom: 10,
    fontSize: 18,
  },

  slide: {
    maxHeight: 196,
    width: "100%",
    maxWidth: deviceWidth - 55,
    backgroundColor: "#dedede",
    borderRadius: 10,
    overflow: "hidden",
    marginRight: 10,
  },
  slidePhoto: {
    width: "100%",
    resizeMode: "cover",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  videoShadow: {
    opacity: 0.7,
    position: "absolute",
    width: "100%",
    height: "100%",
    top: 0,
    backgroundColor: "#000",
  },
  description_profile: {},
  recommendations: {},
  recommendations_text: {},
});
