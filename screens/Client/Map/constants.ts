export const markSudios = [{
    latlng: {
      latitude: 55.7113376,
      longitude: 37.6048969,
    },
    title: 'PMP Olymp',
    address: '​ул. Серпуховский Вал, 21'
  },
  {
    latlng: {
      latitude: 55.7755141,
      longitude: 37.5987448,
    },
    title: 'PMP Kvartal',
    address: 'ул. Фадеева, 4а Тверской район'
  }];
export const markParkings = [{
    latlng: {
      latitude: 55.712103,
      longitude: 37.607122,
    },
    studio: 'PMP Olymp',
    title: 'Парковка',
    address: 'PMP Olymp - Парковка'
  },
  {
    latlng: {
      latitude: 55.711467,
      longitude: 37.609152,
    },
    studio: 'PMP Olymp',
    title: 'Парковка',
    address: 'PMP Olymp - Парковка'
  },
  {
    latlng: {
      latitude: 55.711456,
      longitude: 37.609771,
    },
    studio: 'PMP Olymp',
    title: 'Парковка',
    address: 'PMP Olymp - Парковка'
  },
  {
    latlng: {
      latitude: 55.711458,
      longitude: 37.610197,
    },
    studio: 'PMP Olymp',
    title: 'Парковка',
    address: 'PMP Olymp - Парковка'
  },
  {
    latlng: {
      latitude: 55.711147,
      longitude:  37.610965,
    },
    studio: 'PMP Olymp',
    title: 'Парковка',
    address: 'PMP Olymp - Парковка'
  },
  {
    latlng: {
      latitude: 55.710953,
      longitude:  37.610782,
    },
    studio: 'PMP Olymp',
    title: 'Парковка',
    address: 'PMP Olymp - Парковка'
  },
  {
    latlng: {
      latitude: 55.710953,
      longitude:  37.610782,
    },
    studio: 'PMP Olymp',
    title: 'Парковка',
    address: 'PMP Olymp - Парковка'
  },
  {
    latlng: {
      latitude: 55.775917,
      longitude:  37.60297,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.77571,
      longitude:  37.601596,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.775425,
      longitude: 37.600211,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.77541,
      longitude: 37.59980,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.775249,
      longitude: 37.599458,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.775386,
      longitude: 37.597232,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.775441,
      longitude: 37.597486,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.775343,
      longitude: 37.597645,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.774941,
      longitude: 37.59807,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.774957,
      longitude: 37.598368,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.774729,
      longitude: 37.598926,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.77447,
      longitude: 37.598977,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  },
  {
    latlng: {
      latitude: 55.774437,
      longitude: 37.599451,
    },
    studio: 'PMP Kvartal',
    title: 'Парковка',
    address: 'PMP Kvartal - Парковка'
  }]
export const workTime = [{
    day: 'суббота',
    time: '08:00–22:00'
  },{
    day: 'воскресенье',
    time: '08:00–22:00'
  },{
    day: 'понедельник',
    time: '08:00–22:00'
  },{
    day: 'вторник',
    time: '08:00–22:00'
  },{
    day: 'среда',
    time: '08:00–22:00'
  },{
    day: 'четверг',
    time: '08:00–22:00'
  },{
    day: 'пятница',
    time: '08:00–22:00'
}]
export const parkingAddress = {
    'PMP Kvartal': ['Фадеева, 4а', 'Фадеева, 6 ст3', 'Фадеева, 6 ст2', 'Фадеева, 7 ст1', '​Фадеева, 8', 'Фадеева, 5', 'Фадеева, 5 ст1', 'Тверской-Ямской 1-й переулок, 13/5', 'олгоруковская, 23 ст1', 'Долгоруковская, 15 к3', '​Долгоруковская, 7'],
    'PMP Olymp': ['Шаболовка, 69/32​Серпуховский Вал, 32', 'Серпуховский Вал, 21 к1', 'Серпуховский Вал, 17']
};