import React, { useState, useRef, useEffect } from "react";
import {
  View,
  Animated,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
  SafeAreaView,
} from "react-native";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import { connect } from "react-redux";

import SlidingUpPanel from "rn-sliding-up-panel";
import { sizes, fonts, colors } from "../../../constants/theme";
import { LinearGradient } from "expo-linear-gradient";
import {
  markSudios,
  markParkings,
  workTime,
  parkingAddress,
} from "./constants";
import Header from "../../../components/Headers/Header";
import {
  getCompanyReviews,
  cleanCompanyReviews,
} from "../../../actions/ReviewsActions";
import { MapReviewsItem } from "../../../components/Reviews/MapReviewsItem";
import { getSizeSkeletonLoaders, isNotEndContentList } from "../../../utils";

const { height } = Dimensions.get("window");

const MapScreen = ({
  route,
  getCompanyReviews = () => {},
  cleanCompanyReviews = () => {},
  reviewsLoading = false,
  reviewsData = [],
  requestFailed = false,
  reviewsMoreLoading = false,
  reviewsMeta = null,
}) => {
  const [isActive, setIsActive] = useState(true);
  const [title, setTitle] = useState("PMP Olymp");
  const [address, setAddress] = useState("ул. Серпуховский Вал, 21");
  const [loading, setLoading] = useState(true);
  const [_limit, _setLimit] = useState(5);

  let _mapView = useRef(null);
  const [animateTime, setAnimateTime] = useState(null);

  const isEmptyReviews = !reviewsData || !reviewsData.length;

  const draggableRange = {
    top: height - (height / 100) * 23,
    bottom: 92,
  };
  const _draggedValue = useRef(new Animated.Value(draggableRange.bottom))
    .current;

  let _panel = useRef(null);

  useEffect(() => {
    if (_mapView) {
      _mapView.animateToRegion(
        {
          latitude: 55.735355,
          longitude: 37.617423,
          latitudeDelta: 0.15,
          longitudeDelta: 0.15,
        },
        1000
      );
    }
  }, [_mapView, isActive]);

  // componentDidMount() {
  // this.listener = this._draggedValue.addListener(this.onAnimatedValueChange)

  // }

  // onAnimatedValueChange({value}) {
  //     //console.log(value);
  // }
  //

  useEffect(() => {
    async function loadData() {
      await getCompanyReviews(0, _limit);
    }
    loadData();

    return () => {
      cleanCompanyReviews();
    };
  }, []);

  const loadMore = async () => {
    if (reviewsMeta) {
      const { count } = reviewsMeta || {};

      const offsetReviews = reviewsData.length;

      if (!isEmptyReviews) {
        if (offsetReviews !== count && !reviewsLoading && !requestFailed) {
          await getCompanyReviews(offsetReviews, _limit);
        }
      }
    }
  };

  const renderReviews = (reviewList) => {
    return reviewList.map((item, index) => {
      return (
        <MapReviewsItem
          data={item}
          isLoading={reviewsLoading}
          key={`reviews-all-${index}`}
        />
      );
    });
  };

  const isCloseToBottom = ({
    layoutMeasurement,
    contentOffset,
    contentSize,
  }) => {
    const paddingBottomTop = 80;
    return (
      contentSize.height - layoutMeasurement.height - paddingBottomTop <=
      contentOffset.y
    );
  };

  const clickMarker = (title, address) => {
    setTitle(title);
    setAddress(address);

    if (_panel.show && typeof _panel.show === "function") _panel.show();
  };

  const getParkingAddress = () => parkingAddress[title];
  console.log("MMMMMMMMMAAAAAAAAAAAAPPPPPPPPPP: ", _draggedValue);

  const { top, bottom } = draggableRange;

  const reviewList = reviewsLoading
    ? getSizeSkeletonLoaders(100, 200)
    : !reviewsLoading && !requestFailed && reviewsData && !!reviewsData.length
    ? reviewsData
    : [];
  const reviewsMoreLoadingList = reviewsMoreLoading
    ? getSizeSkeletonLoaders(100, 300)
    : [];

  return (
    <View style={styles.container}>
      <SafeAreaView>
        <Header
          title={"Карта"}
          basketShow={true}
          backTitle
          routeName={route.name}
        />
      </SafeAreaView>

      <View
        style={{ flex: 1, justifyContent: "flex-end", alignItems: "center" }}
      >
        <MapView
          // provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          // minZoomLevel={10}
          style={styles.map}
          region={{
            latitude: 55.735355,
            longitude: 37.617423,
            latitudeDelta: 0.3,
            longitudeDelta: 0.3,
          }}
          ref={(mapView) => (_mapView = mapView)}
        >
          {isActive && markSudios && markSudios.length
            ? markSudios.map((marker, i) => (
                <Marker
                  key={`marker-studio-${
                    marker && marker.titile ? marker.titile : i
                  }`}
                  coordinate={marker.latlng}
                  title={marker.title}
                  description={marker.address}
                  onPress={() => clickMarker(marker.title, marker.address)}
                >
                  <Image
                    source={require("../../../assets/icons/studio-marker.png")}
                  />
                </Marker>
              ))
            : markParkings.map((marker, i) => {
                console.log("MARK coordinates: ", marker.latlng);

                return (
                  <Marker
                    key={`marker-parking-${
                      marker && marker.titile ? marker.titile : i
                    }`}
                    coordinate={marker.latlng}
                    title={marker.title}
                    description={marker.address}
                    onPress={() => clickMarker(marker.studio, marker.address)}
                  >
                    <Image
                      source={require("../../../assets/icons/parking-marker.png")}
                    />
                  </Marker>
                );
              })}
        </MapView>

        <LinearGradient
          pointerEvents={"none"}
          colors={["rgba(255, 255, 255, 0)", "#FFF"]}
          style={styles.linearGradient}
        />

        <SlidingUpPanel
          height={draggableRange.top}
          draggableRange={draggableRange}
          animatedValue={_draggedValue}
          backdropOpacity={0.5}
          snappingPoints={[360]}
          allowDragging={true}
          friction={0.2}
          ref={(c) => (_panel = c)}
        >
          {(dragHandler) => (
            <View style={styles.panel}>
              <View
                style={{
                  zIndex: 1,
                  paddingBottom: 35,
                  flexDirection: "row",
                  paddingHorizontal: 16,
                }}
                {...dragHandler}
              >
                <TouchableOpacity
                  onPress={() => setIsActive(true)}
                  activeOpacity={0.9}
                  style={[
                    {
                      backgroundColor: isActive ? colors.orange : colors.white,
                    },
                    styles.button,
                    styles.buttonLeft,
                  ]}
                >
                  <Text
                    style={[
                      { color: !isActive ? colors.orange : colors.white },
                      styles.textButton,
                    ]}
                  >
                    Студии
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => setIsActive(false)}
                  activeOpacity={0.9}
                  style={[
                    {
                      backgroundColor: !isActive ? colors.orange : colors.white,
                    },
                    styles.button,
                    styles.buttonRight,
                  ]}
                >
                  <Text
                    style={[
                      { color: isActive ? colors.orange : colors.white },
                      styles.textButton,
                    ]}
                  >
                    Парковки
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={{ flex: 1 }}>
                <ScrollView
                  contentContainerStyle={{ flexGrow: 1, paddingBottom: 50 }}
                  scrollEventThrottle={400}
                  onScroll={({ nativeEvent }) => {
                    if (isNotEndContentList(nativeEvent)) loadMore();
                  }}
                >
                  <View style={{ flex: 1 }}>
                    <Text
                      style={[
                        fonts.medium,
                        {
                          fontSize: 28,
                          paddingHorizontal: 16,
                          marginBottom: 16,
                        },
                      ]}
                    >
                      {title}
                    </Text>

                    <View
                      style={{
                        paddingVertical: 18,
                        backgroundColor: colors.white,
                        paddingHorizontal: 16,
                      }}
                    >
                      <Text
                        style={[
                          fonts.medium,
                          {
                            fontSize: 18,
                            color: "rgba(0,0,0,0.87)",
                          },
                        ]}
                      >
                        Адрес
                      </Text>

                      <Text
                        style={[
                          fonts.medium,
                          {
                            fontSize: 16,
                            opacity: 0.54,
                            marginTop: 16,
                          },
                        ]}
                      >
                        {isActive ? address : getParkingAddress()}
                      </Text>

                      <Text
                        style={[
                          fonts.medium,
                          {
                            fontSize: 16,
                            opacity: 0.54,
                            marginTop: 5,
                          },
                        ]}
                      >
                        Москва
                      </Text>
                    </View>

                    <View
                      style={{
                        padding: 16,
                        backgroundColor: colors.white,
                        borderColor: "rgba(0,0,0,0.12)",
                        borderTopWidth: 1,
                        borderBottomWidth: 1,
                      }}
                    >
                      <Text
                        style={[
                          fonts.medium,
                          { fontSize: 18, color: "rgba(0,0,0,0.87)" },
                        ]}
                      >
                        Время работы
                      </Text>

                      <View style={{ marginTop: 16 }}>
                        {workTime.map((item, i) => {
                          return (
                            <View
                              key={`time-item-${i}`}
                              style={{
                                flexDirection: "row",
                                justifyContent: "space-between",
                                marginBottom: 8,
                              }}
                            >
                              <Text
                                style={[
                                  fonts.medium,
                                  {
                                    color: "rgba(0, 0, 0, 0.54)",
                                    fontSize: 16,
                                  },
                                ]}
                              >
                                {item.day}
                              </Text>
                              <Text
                                style={[
                                  fonts.medium,
                                  {
                                    marginRight: "40%",
                                    fontSize: 16,
                                  },
                                ]}
                              >
                                {item.time}
                              </Text>
                            </View>
                          );
                        })}
                      </View>
                    </View>

                    <View style={{ marginRight: 15, marginTop: 16, flex: 1 }}>
                      <Text
                        style={[
                          fonts.medium,
                          {
                            paddingHorizontal: 16,
                            fontSize: 18,
                            color: "rgba(0,0,0,0.87)",
                          },
                        ]}
                      >
                        Отзывы
                      </Text>

                      {(!requestFailed && !isEmptyReviews) || reviewsLoading ? (
                        renderReviews(reviewList)
                      ) : !reviewsLoading &&
                        !requestFailed &&
                        isEmptyReviews ? (
                        <View style={{ flex: 1, marginTop: 25 }}>
                          <Text
                            style={{
                              width: "100%",
                              textAlign: "center",
                              color: "#000",
                            }}
                          >
                            Список пуст
                          </Text>
                        </View>
                      ) : !reviewsLoading && requestFailed ? (
                        <View style={{ flex: 1, marginTop: 25 }}>
                          <Text
                            style={{
                              width: "100%",
                              textAlign: "center",
                              color: "#000",
                            }}
                          >
                            Ошибка получения данных
                          </Text>
                        </View>
                      ) : null}

                      {reviewsMoreLoadingList.map((item, index) => {
                        return (
                          <MapReviewsItem
                            data={item}
                            isLoading={reviewsMoreLoading}
                            key={`reviews-more-${index}`}
                          />
                        );
                      })}
                    </View>
                  </View>
                </ScrollView>
              </View>
            </View>
          )}
        </SlidingUpPanel>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  reviewsLoading: state.ReviewsReducer.companyReviewsLoading,
  reviewsData: state.ReviewsReducer.companyReviews,
  requestFailed: state.ReviewsReducer.companyReviewsError,

  reviewsMoreLoading: state.ReviewsReducer.companyReviewsMoreLoading,
  reviewsMeta: state.ReviewsReducer.companyReviewsMeta,
});

const mapDispatchToProps = {
  getCompanyReviews,
  cleanCompanyReviews,
};

export default connect(mapStateToProps, mapDispatchToProps)(MapScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FAFAFA",
  },
  // header: {
  //   paddingLeft: 16,
  //   paddingTop: 25,
  //   paddingBottom: 10,
  //   flexDirection: "row",
  //   justifyContent: "space-between",
  //   alignItems: "center",
  // },
  title: {
    fontSize: sizes.h1,
    fontFamily: fonts.normal.fontFamily,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  linearGradient: {
    flex: 1,
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 321,
  },
  button: {
    width: "50%",
    paddingVertical: 9,
    borderColor: "#F46F22",
    borderWidth: 1,
  },
  buttonLeft: {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  buttonRight: {
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  textButton: {
    textAlign: "center",
    fontFamily: fonts.normal.fontFamily,
    fontSize: 18,
  },

  _container: {
    //flex: 1,
    backgroundColor: "#fff",
    position: "relative",
    marginBottom: 50,
  },
  panel: {
    paddingTop: 16,
    flex: 1,
    backgroundColor: "#FAFAFA",
    position: "relative",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  gradient: {
    marginBottom: 8,
    borderRadius: 10,
    marginHorizontal: 16,
  },
});
