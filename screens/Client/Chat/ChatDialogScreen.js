import React, {
  useRef,
  useState,
  useEffect,
  useCallback,
  createRef,
} from "react";
import {
  GiftedChat,
  Bubble,
  InputToolbar,
  Send,
} from "react-native-gifted-chat";
import { connect } from "react-redux";
import io from "socket.io-client";
import {
  Animated,
  Image,
  Keyboard,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  View,
  TouchableWithoutFeedback,
  Dimensions,
  ActivityIndicator,
  SafeAreaView,
} from "react-native";
import moment from "moment";

import { CustomHeader, CustomBottomButton } from "../../../components";
import { colors, dimensions } from "../../../styles";
import { fonts, sizes } from "../../../constants/theme";

import { ChatAPI } from "../../../api/chat";
import { setCurrentUser, getCurrentUser } from "../../../actions";
import { Background } from "../../../components/Background";
import { getDialogsByUserId } from "../../../actions/UserActions";
import { validURL } from "../../../utils";

import sendButton from "../../../assets/images/sendButton.png";
import noAvatar from "../../../assets/icons/noAvatar.png";

const socket = io.connect("https://back.pmp.aumagency.ru:3003", {
  transports: ["websocket"],
});

function wait(timeout) {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
}

const ChatDialogScreen = (props) => {
  const { navigation = {}, route = {}, currentUser = {} } = props;
  const { id: user_id = 14 } = currentUser;
  const { trainer = {}, getDialogsByUserId = () => {} } = props;
  const { trainer_id = null, img: avatar = "", name = "", login = "" } =
    (route && route.params && route.params.trainer) || {};

  const trainerName = name || login;
  const img = avatar && validURL(avatar) ? avatar : noAvatar;

  const hasNotch = dimensions.hasNotch;
  const animatedMarginTop = useRef(new Animated.Value(hasNotch ? 40 : 0))
    .current;
  const [offsetBottomInput, setOffsetBottomInput] = useState(
    -Math.ceil(Dimensions.get("screen").height / Platform.OS === "ios" ? 10 : 8)
  );
  const [_messages, _setMessages] = useState([]);
  const [_loading, _setLoading] = useState(true);
  const [_firstLoading, _setFirstLoading] = useState(true);
  const [_currentMessage, _setCurrentMessage] = useState("");
  const [_lastMessageTime, _setLastMessageTime] = useState("");
  const [_limit, _setLimit] = useState(10);
  const [_offset, _setOffset] = useState(0);
  const [_hasNextPage, _setHasNextPage] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [messageBody, setMessageBody] = useState("");

  console.log(
    "animatedMarginTop",
    offsetBottomInput,
    typeof offsetBottomInput,
    +offsetBottomInput,
    "route.paramsroute.params",
    route.params
  );

  console.log("imgimgimg", avatar);

  useCallback(() => {
    _setFirstLoading(false);
  }, [_messages]);

  const getSocketAllMessages = () => {
    socket.on("RESPONSE_GET_MESSAGES", function (data) {
      // let filteredMessages = [];
      console.log("alllllllllll", data);

      if (data.error == "Dialog does not exist") _setLoading(false);

      if (data && data.messages && data.messages.length) {
        _setMessages((prevState) => {
          // filteredMessages = prevState.filter((oldItem) => {
          //   for (let newMessageItem of data.messages) {
          //     return oldItem._id !== newMessageItem._id;
          //   }
          // });
          //
          // console.log(
          //   "filteredMessages: ",
          //   filteredMessages,
          //   "prevState: ",
          //   prevState
          // );
          // console.log("NEW NEW: ", GiftedChat.append([], filteredMessages));

          return GiftedChat.append([], data.messages);
        });

        _setLoading(false);
      }
    });
  };

  useEffect(() => {
    if (!_loading) _setFirstLoading(false);
  }, [_messages, _loading]);

  // const setNewMessage = () => {
  //   if (
  //     _currentMessage &&
  //     _currentMessage.trim() &&
  //     trainerName &&
  //     trainerName.trim()
  //   ) {
  //     const fastNewMessage = {
  //       _id: _messages && _messages.length ? _messages.length + 1 : +Date.now(),
  //       createdAt: new Date(),
  //       receiver_type: 0,
  //       sender_type: 1,
  //       text: _currentMessage.trim(),
  //       user: {
  //         _id: user_id,
  //         avatar:
  //           avatar &&
  //           typeof avatar === "string" &&
  //           avatar.trim() &&
  //           validURL(avatar)
  //             ? avatar
  //             : noAvatar,
  //         name: trainerName,
  //       },
  //     };
  //
  //     console.log("FAST ", fastNewMessage);
  //
  //     _setMessages((prevState) => GiftedChat.append(prevState, fastNewMessage));
  //   }
  // };

  let refreshingAuto = null;

  const onRefresh = useCallback(() => {
    setRefreshing(true);

    getDialogMessages();

    wait(2000).then(() => setRefreshing(false));
  }, [refreshing]);

  useEffect(() => {
    _setLoading(true);

    socket.on("RESPONSE_NEW_MESSAGES", function (data) {
      _setMessages((prevState) => GiftedChat.append(prevState, data));

      console.log("RESPONSE_NEW_MESSAGES", data);
    });

    Keyboard.addListener("keyboardWillShow", keyboardWillShow);
    Keyboard.addListener("keyboardWillHide", keyboardWillHide);

    getDialogMessages();

    return () => {
      _setLoading(true);

      _setOffset(0);
      _setLimit(10);
      _setMessages([]);

      socket.on("disconnects", {
        id: user_id,
      });
    };
  }, []);

  useEffect(() => {
    console.log("Increment", _offset);

    getDialogMessagesOffset(_offset);
  }, [_offset]);

  const incrementOffset = () => _setOffset(_messages.length + _limit);

  const getDialogMessagesOffset = (offset = 0) => {
    console.log("Get next offset: ", offset, ", limit: ", _limit);

    if (user_id !== null) {
      try {
        ChatAPI.getDialogMessages({
          user_id,
          trainer_id,
          limit: _limit,
          offset,
          order: "desc",
        }).then((res) => {
          console.log("SS----", res, res.countAll, _limit);

          if (parseInt(_limit + offset) < res.countAll) {
            _setHasNextPage(true);
          } else _setHasNextPage(false);

          if (offset) {
            _setMessages((prevState) => {
              // console.log('prevState', prevState.concat(res.messages));
              // console.log('res.messages', res.messages);

              // const slicedState = prevState.slice(-_limit);
              //
              // console.log("slicedState", slicedState);
              // const filteredState = slicedState.filter(
              //   (item, index) => item.id === res.messages[index].id
              // );
              // const freshMessages =
              //   filteredState.length === 0
              //     ? prevState.concat(res.messages)
              //     : prevState;

              // console.log("MEssages 15", [...prevState, ...freshMessages]);

              return GiftedChat.append(res.messages, prevState);
            });
          }
        });
      } catch (e) {
        console.log("---ChatAPI.getDialogMessages ERR - ", e);
      }
    }
  };

  const getDialogMessages = () => {
    if (user_id !== null && trainer_id !== null) {
      try {
        socket.emit("GET_MESSAGES", {
          i_am: user_id,
          user_id,
          trainer_id,
          limit: _limit,
          offset: 0,
        });

        getSocketAllMessages();
      } catch (e) {
        console.log("---ChatAPI.getDialogMessages ERR - ", e);
      }
    }
  };

  const onSubmitMessage = () => {
    try {
      Keyboard.dismiss();
      _setCurrentMessage("");

      const newMessage = {
        user_id,
        trainer_id,
        sender_type: 0,
        receiver_type: 1,
        message: _currentMessage.trim(),
        to_user: {
          id: trainer_id,
          avatar: img,
          name,
          login,
        },
      };

      socket.emit("SEND_MESSAGES", {
        ...newMessage,
        to_client: trainer_id,
      });

      setNewMessage();
    } catch (e) {
      console.log("---ChatAPI.sendMessage ERR - ", e);
    }
  };

  const keyboardWillShow = (event) => {
    const keyboardHeight = event.endCoordinates.height;

    Platform.OS === "ios" &&
      Animated.timing(animatedMarginTop, {
        duration: event.duration + 50,
        toValue:
          keyboardHeight - Math.ceil(Dimensions.get("screen").height / 2.9),
        useNativeDriver: true,
      }).start(() => {
        setOffsetBottomInput(
          keyboardHeight - Math.ceil(Dimensions.get("screen").height / 2.9)
        );
      });
  };

  const keyboardWillHide = (event) => {
    Platform.OS === "ios" &&
      Animated.timing(animatedMarginTop, {
        duration: event ? event.duration : 100,
        toValue: 40,
        useNativeDriver: true,
      }).start(() => {
        setOffsetBottomInput(
          -Math.ceil(
            Dimensions.get("screen").height / Platform.OS === "ios" ? 11 : 8
          )
        );
      });
  };

  const navigateBack = () => {
    navigation.goBack();
    getDialogsByUserId(user_id, 0, 0, "desc");
  };

  const renderBubble = (props) => {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: {
            flex: 1,
            backgroundColor: colors.mainOrange,
          },
        }}
      />
    );
  };

  const renderInputToolbar = (props) => (
    <View style={{ bottom: offsetBottomInput }}>
      <InputToolbar
        {...props}
        autoCorrect={false}
        containerStyle={{
          paddingTop: 13,
          paddingLeft: 15,
          paddingBottom: 13,
          backgroundColor: "#fff",
        }}
      />
    </View>
  );

  const renderSend = (props) => (
    <Send {...props}>
      <View
        style={{
          width: 21,
          height: 21,
          // margin: 16,
          marginRight: 16,
          alignItems: "center",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <Image
          style={{ position: "relative", top: 0, left: 0 }}
          source={sendButton}
        />
      </View>
    </Send>
  );

  const onAvatarPress = () => {
    navigation.navigate("ItemSpecialist", {
      fromChat: true,
      byExpert: false,
      tiffany: false,
      staff: {
        // avatar: img,
        // fullname: trainerName,
        // recomendations: trainer.recomendations,
        // about: trainer.about,
        id: trainer_id,
      },
    });
  };

  return (
    <Background>
      <SafeAreaView style={{ flex: 1 }}>
        <CustomHeader
          back={true}
          navigateToMenu={navigateBack}
          menu={false}
          title={trainerName}
          titleText={styles.titleText}
          titleTextStyle={{ width: "70%", textAlign: "center" }}
          style={{
            ...styles.header,
            paddingHorizontal: 16,
            height: 70,
            marginBottom: 0,
            backgroundColor: "#fff",
            borderBottomWidth: Platform.OS !== "android" ? 0.5 : 0,
          }}
          noAvatar={noAvatar}
          avatarSource={
            img && typeof img == "string" && img.trim()
              ? { uri: img }
              : noAvatar
          }
          avatarAction={onAvatarPress}
          isUriAvatarSourse={true}
        />

        {_firstLoading ? (
          <View
            style={{
              flex: 1,
              height: Dimensions.get("screen").height / 10,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <ActivityIndicator size="large" color={colors.mainOrange} />
          </View>
        ) : (
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={{ flex: 1, height: "100%" }}>
              <GiftedChat
                messages={_messages}
                onSend={onSubmitMessage}
                user={{
                  _id: user_id,
                }}
                placeholder="Введите текст сообщения"
                alwaysShowSend={true}
                onInputTextChanged={_setCurrentMessage}
                onLoadEarlier={incrementOffset}
                loadEarlier={true}
                isLoadingEarlier={true}
                infiniteScroll={true}
                renderLoadEarlier={() => {
                  if (_hasNextPage) {
                    return (
                      <TouchableOpacity
                        style={styles.loadEarlierBtn}
                        activeOpacity={0.8}
                        onPress={incrementOffset}
                      >
                        <Text style={styles.loadEarlierText}>
                          Загрузить предыдущие
                        </Text>
                      </TouchableOpacity>
                    );
                  } else return false;
                }}
                onPressAvatar={onAvatarPress}
                renderBubble={renderBubble}
                renderInputToolbar={renderInputToolbar}
                renderSend={renderSend}
                listViewProps={{
                  flex: 1,
                  paddingBottom: 65,
                  paddingTop: 55,
                  onRefresh,
                  refreshing,
                }}
              />
            </View>
          </TouchableWithoutFeedback>
        )}

        {Platform.OS === "android" && (
          <KeyboardAvoidingView behavior="padding" />
        )}
      </SafeAreaView>
    </Background>
  );
};

const mapStateToProps = (state) => ({
  currentUser: state.currentUser.currentUser,
});

const mapDispatchToProps = {
  getCurrentUser,
  setCurrentUser,

  getDialogsByUserId,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatDialogScreen);

const styles = StyleSheet.create({
  loadEarlierBtn: {
    marginTop: 50,
    marginLeft: "auto",
    marginRight: "auto",
    backgroundColor: "rgba(0,0,0,0.35)",
    justifyContent: "center",
    alignContent: "center",
    width: 190,
    paddingVertical: 3,
    borderRadius: 10,
  },
  loadEarlierText: {
    textAlign: "center",
    color: "#fff",
  },
  backgroundImage: {
    opacity: 0.9,
  },
  background: {
    width: "100%",
    height: "100%",
    position: "relative",
  },
  separator: {
    flex: 1,
  },
  header: {
    zIndex: 3,
    marginTop: 0,
    marginBottom: 10,
    paddingHorizontal: 9,
    paddingTop: 5,
    borderBottomColor: "rgba(0,0,0,0.35)",
  },
  titleText: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    lineHeight: 22,
    fontWeight: "500",
    textAlign: "center",
    color: colors.mainBlack,
  },
  linearGradient: {
    flex: 1,
    zIndex: 2,
    position: "absolute",
    top: 0,
    width: "100%",
    height: 173,
  },
});
