import React from "react";

import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
} from "react-native";
import moment from "moment";
import "moment/locale/ru";

import UserIcon from "../../../assets/icons/MaskGroup.png";
import { colors, fonts, sizes } from "../../../constants/theme";
import { validURL } from "../../../utils";
import { NoAvatarTrainerSvg } from "../../../assets/icons/NoAvatarTrainerSvg/NoAvatarTrainerSvg";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader";

const ChatItem = (props) => {
  const { loading = false, date = "", item = {} } = props;
  const { lastMessage = {} } = item;
  const { createdAt = "" } =
    lastMessage != null && lastMessage != undefined ? lastMessage : {};

  const lastMeessageDate =
    /*lastMessage && lastMessage.createdAt
      ? */ lastMessage.createdAt; /*
      : item.createdAt*/

  return (
    <TouchableOpacity style={styles.chatItemBody} onPress={props.goToDialog}>
      <SkeletonLoader
        layout={[{ height: 40, width: 40, borderRadius: 40 }]}
        containerStyle={styles.chatIcon}
        isLoading={loading}
      >
        {!props.avatar || !validURL(props.avatar) ? (
          <NoAvatarTrainerSvg width={40} height={40} />
        ) : (
          <Image source={{ uri: props.avatar }} style={styles.chatIconDetail} />
        )}
      </SkeletonLoader>

      <View style={styles.chatRight}>
        <SkeletonLoader
          layout={[{ height: 20, width: 175 }]}
          containerStyle={styles.userName}
          isLoading={loading}
        >
          <View style={{ width: "100%", justifyContent: "space-between" }}>
            <View
              style={{
                width: "107%",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Text
                numberOfLines={1}
                style={{
                  ...styles.userNameStyle,
                  width: "68%",
                  marginRight: 20,
                }}
              >
                {props.name || "Неизвестный тренер"}
              </Text>

              {lastMeessageDate ? (
                <Text
                  numberOfLines={1}
                  style={{
                    flex: 1,
                    width: "30%",
                    fontSize: 14,
                    color: "#151C26",
                    opacity: 0.54,
                    position: "absolute",
                    top: 3,
                    right: 0,
                  }}
                >
                  {moment(lastMeessageDate)
                    .add(3, "hours")
                    .startOf("minutes")
                    .fromNow()}
                </Text>
              ) : (
                <View />
              )}
            </View>

            {props.text ? (
              <View
                style={{
                  minHeight: 28,
                  maxWidth: Dimensions.get("screen").width / 1.4,
                  justifyContent: "flex-start",
                  alignItems: "center",
                  flexDirection: "row",
                }}
              >
                <Text
                  style={{
                    maxWidth: props.totalUnread
                      ? Dimensions.get("screen").width / 1.7
                      : "100%",
                    fontSize: 14,
                    color: "#151C26",
                    opacity: 0.54,
                    marginTop: 10,
                  }}
                  numberOfLines={1}
                >
                  {props.receiverType ? "Вы: " + props.text : props.text}
                </Text>

                {props.totalUnread ? (
                  <View
                    style={{
                      marginTop: 10,
                      marginLeft: 10,
                      height: 14,
                      width: 14,
                      borderRadius: 14,
                      backgroundColor: "#0FD8D8",
                      justifyContent: "center",
                      alignItem: "center",
                    }}
                  >
                    <Text
                      style={{
                        fontSize:
                          props.totalUnread < 10
                            ? 11
                            : props.totalUnread < 100 && props.totalUnread >= 10
                            ? 7
                            : 5,
                        textAlign: "center",
                        fontWeight: "bold",
                        color: "#fff",
                      }}
                    >
                      {props.totalUnread}
                    </Text>
                  </View>
                ) : (
                  <View />
                )}
              </View>
            ) : (
              <View style={{ minHeight: 28 }} />
            )}
          </View>
        </SkeletonLoader>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  chatItemBody: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignContent: "center",
    alignItems: "center",
    paddingTop: 15,
    maxHeight: 72,
  },
  chatRight: {
    position: "relative",
    display: "flex",
    flexDirection: "row",
    width: "100%",

    borderColor: "rgba(21, 28, 38, 0.12)",
    borderBottomWidth: 1,
    paddingTop: 20,
    paddingBottom: 10,
  },
  chatIcon: {
    width: 56,
  },
  chatIconDetail: {
    width: 40,
    height: 40,
    borderRadius: 40,
  },
  chatText: {
    display: "flex",
    flexDirection: "column",
  },
  chatDate: {
    paddingRight: 45,
  },
  userTextStyle: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: sizes.font,
    color: "#A6A9AD",
  },
  userNameStyle: {
    justifyContent: "flex-end",
    fontFamily: fonts.medium.fontFamily,
    fontSize: sizes.headerName,
    color: colors.orange,
  },
});

export default ChatItem;
