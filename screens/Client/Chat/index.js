import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Platform,
  AsyncStorage,
} from "react-native";
import * as Animatable from "react-native-animatable";

import Header from "../../../components/Headers/Header";
import { sizes } from "../../../constants/theme";
import ChatItem from "./ChatItem";
import { Background } from "../../../components/Background";
import { setCurrentUser, getCurrentUser } from "../../../actions";
import { getDialogsByUserId } from "../../../actions/UserActions";
import { getSizeSkeletonLoaders } from "../../../utils";

const ChatScreen = (props) => {
  const {
    navigation,
    route,
    currentUser,
    setCurrentUser,

    getDialogsByUserId = () => {},
    dialogsLoading = false,
    dialogs = [],
    dialogsCountItems = 0,
    dialogsError = null,
  } = props;
  const loading = dialogsLoading;

  const [_trainers, _setTrainers] = useState([]);
  const [_isFetching, _setIsFetching] = useState(true);

  const [_userId, _setUserId] = useState(null);

  const [_offset, _setOffset] = useState(0);
  const [_limit, _setLimit] = useState(15);
  const [_orderData, _setOrderData] = useState("desc");

  const goToDialog = (trainer) =>
    navigation.navigate("ChatDialog", { trainer });

  const getUserData = async () => {
    let dataUser = await AsyncStorage.getItem("user");

    if (
      dataUser &&
      JSON.parse(dataUser) &&
      JSON.parse(dataUser).id &&
      JSON.parse(dataUser).id !== false
    ) {
      console.log("dataUserdataUser", dataUser, JSON.parse(dataUser).id);

      _setUserId(JSON.parse(dataUser).id);
    }
    console.log("UUUUUUSSSERRRRR", JSON.parse(dataUser));
  };

  useEffect(() => {
    getUserData();
  }, []);

  useEffect(() => {
    if (_userId) getDialogsByUserId(_userId, _offset, _limit, _orderData);
  }, [_userId]);

  useEffect(() => {
    if (!dialogsLoading) _setIsFetching();
  }, [dialogsLoading]);

  const trainersList = dialogsLoading
    ? getSizeSkeletonLoaders(75, 100)
    : !dialogsLoading && dialogs && dialogs.length
    ? dialogs
    : [];

  return (
    <Background>
      <Header
        title="Чаты"
        style={{
          paddingHorizontal: 9,
          borderBottomColor: "rgba(0,0,0,0.35)",
          borderBottomWidth: Platform.OS !== "android" ? 0.5 : 0,
        }}
      />

      <View style={styles.body}>
        {!dialogsError ? (
          dialogs.map((trainer, index) => {
            const Wrap = !dialogsLoading ? Animatable.View : View;

            return (
              <Wrap
                duration={250}
                key={`trainer-${index}`}
                delay={index < 11 ? index * 250 : 400}
                animation="fadeInUp"
              >
                <ChatItem
                  loading={loading}
                  name={trainer.name || trainer.login}
                  avatar={trainer.img}
                  item={trainer}
                  totalUnread={trainer.totalUnread}
                  text={
                    trainer &&
                    trainer.lastMessage &&
                    trainer.lastMessage.message &&
                    trainer.lastMessage.message.trim()
                      ? trainer.lastMessage.message
                      : ""
                  }
                  receiverType={
                    trainer.lastMessage != undefined &&
                    trainer.lastMessage.receiver_type != undefined
                      ? trainer.lastMessage.receiver_type
                      : ""
                  }
                  goToDialog={() => goToDialog(trainer)}
                />
              </Wrap>
            );
          })
        ) : !dialogsLoading && !dialogs.length && !_isFetching ? (
          <View
            style={{
              flex: 1,
              minHeight: Dimensions.get("screen").height / 1.2,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Список пуст
            </Text>
          </View>
        ) : !dialogsLoading && dialogsError && !_isFetching ? (
          <View
            style={{
              flex: 1,
              minHeight: Dimensions.get("screen").height / 1.2,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Ошибка получения данных
            </Text>
          </View>
        ) : null}
      </View>
    </Background>
  );
};

const mapStateToProps = (state) => ({
  currentUser: state.currentUser.currentUser,

  loading: state.TrainersReducer.loading,
  trainers: state.TrainersReducer.trainers,
  error: state.TrainersReducer.error,

  dialogsLoading: state.currentUser.dialogsLoading,
  dialogs: state.currentUser.dialogs,
  dialogsCountItems: state.currentUser.dialogsCountItems,
  dialogsError: state.currentUser.dialogsError,
});

const mapDispatchToProps = {
  getDialogsByUserId,

  getCurrentUser,
  setCurrentUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);

const styles = StyleSheet.create({
  headerWrapperStyle: {
    borderBottomColor: "rgba(21, 28, 38, 0.12)",
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  body: {
    margin: sizes.margin,
    marginTop: sizes.margin - 25,
    marginRight: 15,
  },
});
