import React, { useState } from "react";
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import Constants from "expo-constants";
import { Notifications } from "expo";

import Colors from "../../../constants/Colors";
import { ToastView } from "../../../components";
import { Header } from "../../../components";
import AuthInput from "../../../components/input/AuthInput";
import { fontSize16, fonts } from "../../../constants/theme";
import { UserAPI } from "../../../api/user/index";
import ButtonInput from "../../../components/button/ButtonInput";

export const ConfirmPasswordScreen = ({ navigation, route }) => {
  const { code = "", login = "" } = route && route.params ? route.params : {};

  const [password, setPassword] = useState("");
  const [repass, setRepass] = useState("");
  const [isValidatePassword, setIsValidatePassword] = useState(true);
  const [isEqualityPassword, setIsEqualityPassword] = useState(true);
  const [isValidateRepass, setIsValidateRepass] = useState(true);

  const save = async () => {
    const isCheckPassword = checkPassword(password);
    const isCheckRepass = checkPassword(repass);

    setIsValidatePassword(isCheckPassword);
    setIsEqualityPassword(repass === password);
    setIsValidateRepass(isCheckRepass);

    if (!isCheckPassword || !isCheckRepass || repass !== password) return null;

    try {
      let device_id = Constants.deviceId;
      let push_token = "";
      let experienceId = "";

      if (!Constants.manifest) {
        // Absence of the manifest means we're in bare workflow
        experienceId = "@c.amazon/pmpApp";
      }

      push_token = await Notifications.getExpoPushTokenAsync({
        experienceId,
      });

      const responseConfirm = await UserAPI.getPasswordRecovery({
        code,
        login,
        password,
        push_token,
        device_id,
      });
      // console.log("responseConfirm", responseConfirm);

      if (responseConfirm && responseConfirm.status) {
        if (responseConfirm.msg) {
          let messageToClient =
            responseConfirm.msg +
            (responseConfirm.password ? ": " + responseConfirm.password : "");

          ToastView(messageToClient, responseConfirm.password ? 6000 : 3000);

          if (responseConfirm.password) navigation.navigate("Login");
          return;
        }

        if (responseConfirm.status === "success") {
          ToastView("Ваш пароль успешно обновлен");

          navigation.navigate("Login");
        }
      }
    } catch (e) {
      console.log(
        "Error Notifications.getExpoPushTokenAsync || UserAPI.getPasswordRecovery: ",
        e
      );

      ToastView(
        "Произошла ошибка.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27"
      );
    }
  };

  const checkPassword = (password) => /(.{6,})/.test(password);

  return (
    <ImageBackground
      style={styles.container}
      source={require("../../../assets/images/smsbg.png")}
    >
      <Header
        title={"Создание пароля"}
        style={{ paddingTop: 30 }}
        isSubmit={false}
      />
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={styles.confirmForm}>
          <Text style={styles.comment}>
            Создайте, пожалуйста, новый пароль.
          </Text>

          <View style={{ marginTop: 20 }}>
            <AuthInput
              isValidate={isValidatePassword && isEqualityPassword}
              validateText={
                !isEqualityPassword
                  ? "Пароли должны совпадать"
                  : "Введите корректный пароль"
              }
              secure
              title="Введите, пожалуйста, пароль"
              style={{ flex: 1 }}
              onChangeText={(password) => setPassword(password)}
            />
          </View>

          <View style={{ marginTop: 20 }}>
            <AuthInput
              isValidate={isValidateRepass && isEqualityPassword}
              validateText={
                !isEqualityPassword
                  ? "Пароли должны совпадать"
                  : "Введите корректный пароль"
              }
              secure
              title="Повторите, пожалуйста, пароль"
              style={{ flex: 1 }}
              onChangeText={(password) => setRepass(password)}
            />
          </View>

          <View style={{ marginTop: 30 }}>
            <ButtonInput title="Сохранить" onClick={save} />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  confirmForm: {
    height: "100%",
    paddingTop: 37,
    paddingLeft: 24,
    paddingRight: 24,
    borderRadius: 22,
    backgroundColor: "white",
  },
  comment: {
    fontSize: fontSize16,
    fontFamily: fonts.normal.fontFamily,
    lineHeight: 25,
  },
});
