import React, { useState } from 'react';
import { View, ImageBackground,  } from 'react-native';
import { Header } from '../../../components';
import { fontSize16, fonts } from '../../../constants/theme';
import AuthInput from '../../../components/input/AuthInput';
import ButtonInput from '../../../components/button/ButtonInput';
import {Background} from "../../../components/Background";

export const ConfirmPhoneScreen = ({navigation}) => {
    const [phoneNumber, setPhoneNumber] = useState('');
    const [isValidatePhoneNumber, setIsValidatePhoneNumber] = useState(true);

    const continueScreen = () => {
        const isCheckPhoneNumber = checkPhoneNumber(phoneNumber);

        setIsValidatePhoneNumber(isCheckPhoneNumber);

        if (!isCheckPhoneNumber) return null;

        navigation.navigate('ConfirmSmsCode', { phoneNumber })
    };

    const checkPhoneNumber = (phone) => /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/.test(phone);

    return (
        <ImageBackground style={styles.container} source={require('../../../assets/images/smsbg.png')}>
            <Header
                title={'Восстановление пароля'}
                isSubmit={false}
            />

            <View style={styles.confirmForm}>
                <View style={{marginTop: 0}}>
                    <AuthInput
                        isValidate={isValidatePhoneNumber}
                        validateText="Введите корректный номер телефона"
                        inputType="numeric"
                        title="Введите, пожалуйста, номер телефона"
                        onChangeText={phone => setPhoneNumber(phone)}
                    >
                    </AuthInput>
                </View>

                <View style={{marginTop: 30}}>
                    <ButtonInput title="Продолжить" onClick={() => { continueScreen() }} />
                </View>
            </View>
        </ImageBackground>
    )
};

const styles = {
    container: {
        flex: 1,
    },
    confirmForm: {
        height: '100%',
        paddingTop: 37,
        paddingLeft: 24,
        paddingRight: 24,
        borderRadius: 22,
        backgroundColor: 'white',
    },
    comment: {
        fontSize: fontSize16,
        fontFamily: fonts.normal.fontFamily,
        lineHeight: 25
    }
};
