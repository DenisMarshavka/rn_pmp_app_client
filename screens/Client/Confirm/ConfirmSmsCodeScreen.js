import React, { useState, useEffect } from "react";
import {
  View,
  ImageBackground,
  Text,
  TouchableOpacity,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import { Notifications } from "expo";
import moment from "moment";

import { SmsModule } from "../../../api/sms";
import { Header } from "../../../components";
import { fontSize16, fonts } from "../../../constants/theme";
import AuthInput from "../../../components/input/AuthInput";
import ButtonInput from "../../../components/button/ButtonInput";
import { registerPushNotificationsAsync } from "../../../utils";
import { modeSmsCode } from "./config";
import { UserAPI } from "../../../api/user";
import { MyNotificationAPI } from "../../../api/myNotification";
import { getConfirmPushNotification } from "../../../actions";

const ConfirmSmsCodeScreen = ({ navigation, route }) => {
  const [clientCode, setClientCode] = useState("");
  const [smsCode, setSmsCode] = useState("");
  const [isValidateCode, setIsValidateCode] = useState(true);
  const [isRepeatCode, setIsRepeatCode] = useState(false);
  const [isSmsNotification, setIsSmsNotification] = useState(true);

  const [timerSeconds, setTimerSeconds] = useState(0);
  const [timer, setTimer] = useState(null);

  const phoneNumber = route.params.phone;
  const mode = route.params.mode;

  const handleSmsSend = async () => {
    const currentSmsCode = SmsModule.generateRandomSmsCode();
    const notificationRandCode = Math.ceil(Math.random() * 10000);
    // console.log(currentSmsCode);
    // setSmsCode(isNaN(+currentSmsCode) ? notificationRandCode : currentSmsCode);
    setSmsCode(currentSmsCode);

    console.log("currentSmsCode", currentSmsCode);

    //Push Notification
    if (!isNaN(+currentSmsCode)) {
      // const userData = await
      //
      //
      // if () {
      //   await MyNotificationAPI.setNotificationApi({
      //       "user_id": 31,
      //       "title": "Код подтвержения",
      //       "body": "klik me",
      //       // "service_id": 1,
      //       // "image": "img"
      //   })
      // } // TODO: Wiil finish this method
      //

      // const expoPushToken = await Notifications.getExpoPushTokenAsync({
      //   experienceId: "@c.amazon/pmpApp",
      // });
      //
      const expoPushToken = await registerPushNotificationsAsync();

      console.log(
        "expoPushToken",
        expoPushToken,
        "currentSmsCode",
        currentSmsCode
      );

      if (expoPushToken) {
        MyNotificationAPI.showNotificationNow({
          to: expoPushToken,
          sound: "default",
          title: "Подтверждение",
          body: `Ваш код для подтверждения: ${currentSmsCode}`,
          data: {},
        });
      }

      // getConfirmPushNotification(
      //   "Подтверждение",
      //   `Ваш код для подтверждения: ${notificationRandCode}`
      // ); //ToDO: Will active this method
    } else await SmsModule.smsRequest(phoneNumber, currentSmsCode);
  };

  const setTimeOut = () => {
    const timer = setInterval(() => {
      setTimerSeconds((timerSeconds) => {
        if (timerSeconds === 0) {
          setIsRepeatCode(false);
          clearInterval(timer);

          return timerSeconds;
        }

        return timerSeconds - 1;
      });
    }, 1000);
  };

  const continueScreen = () => {
    console.log(
      "smsCode",
      +smsCode,
      "clientCode",
      +clientCode,
      "phoneNumber",
      phoneNumber
    );

    if (+smsCode === +clientCode) {
      navigation.navigate("ConfirmPassword", {
        code: +smsCode,
        login: phoneNumber ? phoneNumber.replace(/ /g, "") : "",
      });
      setIsValidateCode(true);
    } else setIsValidateCode(false);
  };

  const repeatCode = () => {
    handleSmsSend().then(() => {
      if (!timerSeconds) setTimerSeconds(12);

      setTimeOut();
    });
  };

  useEffect(() => {
    repeatCode();

    return () => {
      clearInterval(timer);
    };
  }, []);

  useEffect(() => {
    console.log("!!route.params.phoneNumber", !!route.params.phoneNumber);

    setIsSmsNotification(route.params.phoneNumber !== undefined);
  }, []);

  const headerText = () => {
    let text = "";

    switch (mode) {
      case modeSmsCode.authorization:
        if (isSmsNotification) {
          text =
            "Введите, пожалуйста, СМС-код, высланный на Ваш номер телефона для входа в приложение.";
        } else
          text =
            "Введите, пожалуйста, код, высланный уведомлением в приложение.";
        break;

      case modeSmsCode.registration:
        if (isSmsNotification) {
          text =
            "Введите, пожалуйста, СМС-код, высланный на Ваш номер телефона для завершения регистрации.";
        } else
          text =
            "Введите, пожалуйста, код, высланный уведомлением в приложение.";
        break;

      default:
        text =
          "Введите, пожалуйста, СМС-код, высланный на Ваш номер телефона для восстановления пароля.";
        break;
    }
    return text;
  };

  const titleText = () => {
    let text = "";

    switch (mode) {
      case modeSmsCode.authorization:
        text = "Авторизация";
        break;

      case modeSmsCode.registration:
        text = "Регистрация";
        break;

      default:
        text = "Восстановление пароля";
        break;
    }

    return text;
  };

  return (
    <ImageBackground
      style={styles.container}
      source={require("../../../assets/images/smsbg.png")}
    >
      <SafeAreaView style={{ flex: 1 }}>
        <Header
          style={{ paddingTop: 15 }}
          styleHeadTitle={{ fontSize: 16 }}
          title={titleText()}
          isSubmit={false}
        />

        <View style={styles.confirmForm}>
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <View style={{ flex: 1 }}>
              <Text style={styles.comment}>{headerText()}</Text>

              <View style={{ marginTop: 40 }}>
                <AuthInput
                  isValidate={isValidateCode}
                  validateText={"Неверный код подтверждения"}
                  inputType="numeric"
                  title={
                    isSmsNotification !== undefined
                      ? "Введите, пожалуйста СМС-код"
                      : "Введите, пожалуйста код"
                  }
                  onChangeText={setClientCode}
                />
              </View>

              <View style={{ marginTop: 30 }}>
                <ButtonInput title="Продолжить" onClick={continueScreen} />
              </View>

              <View style={{ marginTop: 15 }}>
                {/*<ButtonInput
                        title="Отправить код повторно"
                        onClick={() => { repeatCode() }}
                        disabled={isRepeatCode}
                    />*/}
                <TouchableOpacity
                  activeOpacity={!timerSeconds ? 0.5 : 1}
                  onPress={() => {
                    !timerSeconds ? repeatCode() : null;
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#151C26",
                      opacity: !timerSeconds ? 0.5 : 0.2,
                      fontSize: 17,
                      marginTop: 10,
                      marginBottom: 5,
                    }}
                  >
                    ОТПРАВИТЬ КОД ЗАНОВО
                  </Text>
                </TouchableOpacity>

                <Text style={{ textAlign: "center", color: "#0FD8D8" }}>
                  {moment()
                    .startOf("day")
                    .seconds(timerSeconds)
                    .format("mm:ss")}
                </Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </SafeAreaView>
    </ImageBackground>
  );
};

export default ConfirmSmsCodeScreen;

const styles = {
  container: {
    flex: 1,
  },
  confirmForm: {
    height: "100%",
    paddingTop: 37,
    paddingLeft: 24,
    paddingRight: 24,
    borderRadius: 22,
    backgroundColor: "white",
    // marginTop: 20,
  },
  comment: {
    fontSize: fontSize16,
    fontFamily: fonts.normal.fontFamily,
    lineHeight: 25,
  },
};
