import React, { useEffect, useState } from "react";
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Linking,
} from "react-native";
import { connect } from "react-redux";
import HTMLView from "react-native-htmlview";

import { ToastView } from "../../components";
import { Background } from "../../components/Background";
import { colors, fonts, sizes } from "../../constants/theme";
import Header from "../../components/Headers/Header";
import ButtonInput from "../../components/button/ButtonInput";
import { UserAPI } from "../../api/user";
import {
  gerProgramById,
  getProgramStepInfo,
} from "../../actions/ProgramsActions";
import {
  addBasketProductById,
  getCurrentBasketState,
} from "../../actions/BasketActions";
import { getUserStorageData } from "../../utils";
import { Tabs } from "../../components/Tabs/Tabs";
import {
  getSizeSkeletonLoaders,
  showAlert,
  formattingPrice,
} from "../../utils";
import CheckBoxInput from "../../components/checkboxinput/CheckBoxInput.tsx";
import SkeletonLoader from "../../components/loaders/SkeletonLoader";
import CarouselWithVideos from "../../components/carouselWithVideos";
import { getFastCheckout } from "../../actions";

const Program = ({
  addBasketProductById = () => {},

  getFastCheckout = () => {},

  navigation,
  route,

  gerProgramById = () => {},
  programLoading = false,
  programData = {},
  programError = false,

  getProgramStepInfo = () => {},
  programStepInfoLoading = false,
  programStepInfoData = {},
  programStepInfoError = false,
}) => {
  const { programId = null } =
    route && route.params && route.params ? route.params : {};
  const {
    description = "",
    points = 0,
    price = 0,
    img: image = "",
    title: programName = "",
  } = programData.program || {};

  const { program: stepInfo = {}, section = {} } = programStepInfoData;
  const { videos = [] } = section;
  const { id: stepId = null, description: stepDescription = "" } = stepInfo;

  console.log("stepInfo", stepInfo);

  const { steps: programTypes = [] } = programData;

  const [purchasedList, setPurchasedList] = useState([]);
  const [programTypesTabs, setProgramTypesTabs] = useState(programTypes);
  const [activeTab, setActiveTab] = useState(
    programTypes && programTypes[0] && programTypes[0].id
      ? programTypes[0].id
      : 0
  );
  const [activeRadio, setActiveRadio] = useState("");
  const [selectedProgramId, setSelectedProgramId] = useState(programId);
  const [dataForSendActions, setDataForSendActions] = useState({
    id: selectedProgramId,
    type: "program",
  });
  const [videosStep, setVideosStep] = useState([]);
  const [upproveToPayment, setUpproveToPayment] = useState(true);
  const [stepPrice, setStepPrice] = useState(0);

  const getPuchasedList = async () => {
    let response = null;
    const userData = await getUserStorageData();

    const purchsedData = [];

    if (userData && userData.id)
      response = await UserAPI.getUserDataPurchasedPrograms(userData.id);

    if (
      response &&
      response.data &&
      response.status &&
      response.status === "success"
    ) {
      response.data.forEach((item, index) => {
        if (item && item.step_id) purchsedData.push(item.step_id);
      });
    }

    if (purchsedData && purchsedData.length) setPurchasedList(purchsedData);
  };

  useEffect(() => {
    if (
      !programLoading &&
      programTypes &&
      programTypes.length &&
      programTypes[0] &&
      programTypes[0].name
    ) {
      setActiveRadio(programTypes[0].name);
      setActiveTab(programTypes[0].id);
    }

    if (!programLoading) {
      getFastCheckout(dataForSendActions);

      getPuchasedList();
    }
  }, [programLoading]);

  useEffect(() => {
    gerProgramById(programId);

    if (activeTab) getProgramStepInfo(activeTab);
  }, []);

  useEffect(() => {
    // TODO: Replaced this massing selected program id
    if (programId) getFastCheckout(dataForSendActions);
  }, [selectedProgramId]);

  useEffect(() => {
    setProgramTypesTabs(programTypes);
  }, [programData]);

  useEffect(() => {
    const sections = [{ videos: [], name: "Видеоролики" }];

    if (activeTab && !programStepInfoLoading) getProgramStepInfo(activeTab);

    if (
      !programLoading &&
      programData &&
      programData.steps &&
      programData.steps.length
    ) {
      const selectedStep = programData.steps.filter((item) =>
        activeTab && item && item.id ? item.id === activeTab : false
      );

      if (selectedStep && selectedStep.length) {
        for (let step of selectedStep) {
          if (step && step.price !== false) setStepPrice(step.price);

          // console.log("STEPPP", step);

          if (step.program_sections && step.program_sections.length) {
            for (let section of step.program_sections) {
              // console.log(
              //   "STEPPP SECTIONS",
              //   section,
              //   "section.videos",
              //   section.videos
              // );

              if (section.videos && section.videos.length)
                sections[0].videos = [...sections[0].videos, ...section.videos];
            }
          }
        }
      }
    }

    // console.log("VIDDDEOOSS", sections);
    if (
      sections &&
      sections[0] &&
      sections[0].videos &&
      sections[0].videos.length
    )
      setVideosStep(sections);
  }, [activeTab, programLoading, programData]);

  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      {/*<BackHeader backTitle offsetTopIconBack={15} />*/}
      <Header basketShow={true} backTitle routeName={route.name} />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ paddingHorizontal: sizes.margin, flex: 1 }}>
          <SkeletonLoader
            layout={[
              { marginTop: 26, width: 266, height: 50, marginBottom: 25 },
            ]}
            isLoading={programLoading}
          >
            <Text
              style={[
                styles.title,
                { color: colors.title, maxWidth: 266, marginTop: 26 },
                fonts.normal,
                fonts.title,
              ]}
            >
              {programName || "Программа"}
            </Text>
          </SkeletonLoader>
        </View>

        <Tabs
          style={styles.periodSelection}
          elementStyle={{ maxWidth: 113, flex: 1, minHeight: 33 }}
          onTabActiveIndexSet={(activeIndex, tabStepId) => {
            setActiveTab(tabStepId);
          }}
          errorContainerStyle={{
            marginLeft: 16,
            marginTop: 25,
            marginBottom: 25,
          }}
          listData={programTypes}
          loading={programLoading}
          requestFailed={programError}
        />

        <View style={{ flex: 1 }}>
          {!programError ? (
            <>
              {programLoading ? (
                <View
                  style={{
                    flex: 1,
                    minHeight: Dimensions.get("screen").height / 2.7,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <ActivityIndicator color={colors.orange} />
                </View>
              ) : (
                <>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginTop: 22,
                      paddingHorizontal: sizes.margin,
                    }}
                  >
                    <Text
                      style={[
                        { color: colors.orange, fontSize: 24 },
                        fonts.normal,
                      ]}
                    >
                      {formattingPrice(stepPrice ? stepPrice : price)} ₽
                    </Text>

                    <Text
                      style={[
                        { color: colors.title, fontSize: 24 },
                        fonts.normal,
                      ]}
                    >
                      {points} баллов
                    </Text>
                  </View>

                  <View
                    style={{ marginTop: 25, paddingHorizontal: sizes.margin }}
                  >
                    <Image
                      style={{ borderRadius: 8, width: "100%", height: 207 }}
                      source={
                        image.trim()
                          ? { uri: image }
                          : require("../../assets/images/notProductImage.png")
                      }
                      resizeMode="cover"
                    />
                  </View>

                  <View
                    style={{ marginTop: 24, paddingHorizontal: sizes.margin }}
                  >
                    <Text
                      style={[{ color: colors.title }, fonts.h2, fonts.normal]}
                    >
                      О программе
                    </Text>

                    <HTMLView
                      value={
                        description ||
                        "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet architecto doloribus eum ex illum, impedit laboriosam magnam magni minus modi molestias nesciunt nihil non nulla officiis optio praesentium quas quasi reiciendis rem repellendus reprehenderit sed, similique temporibus veritatis voluptate. Adipisci ea eligendi enim id illo ipsam libero minima mollitia nam nemo, nulla numquam quasi recusandae rem sapiente sequi voluptas?"
                      }
                      style={{ marginTop: 10 }}
                      stylesheet={{
                        ul: styles.list,
                        addLineBreaks: false,
                        paragraphBreak: styles.text,
                      }}
                    />
                  </View>

                  {programStepInfoLoading ? (
                    <View
                      style={{
                        flex: 1,
                        marginTop: 24,
                        alignItems: "left",
                        justifyContent: "left",
                      }}
                    >
                      <ActivityIndicator color={colors.mainOrange} />
                    </View>
                  ) : !programStepInfoError &&
                    stepDescription &&
                    stepDescription.trim() ? (
                    <View
                      style={{ marginTop: 24, paddingHorizontal: sizes.margin }}
                    >
                      <Text
                        style={[
                          { color: colors.title },
                          fonts.h2,
                          fonts.normal,
                        ]}
                      >
                        + Дополнительно (Выбранный степ)
                      </Text>

                      <HTMLView
                        value={stepDescription}
                        style={{ marginTop: 10 }}
                        stylesheet={{
                          ul: styles.list,
                          addLineBreaks: false,
                          paragraphBreak: styles.text,
                        }}
                      />
                    </View>
                  ) : null}

                  <View style={{ flex: 1, marginTop: 10 }}>
                    {videosStep &&
                    videosStep[0] &&
                    videosStep[0].videos &&
                    videosStep[0].videos ? (
                      <CarouselWithVideos
                        sections={videosStep}
                        itemSize={{ width: 110, height: 110 }}
                      />
                    ) : null}
                  </View>

                  <View style={styles.separator} />

                  {programTypes.map((item, index) => {
                    let title = item.name;
                    let purchased = false;

                    if (purchasedList && purchasedList.length)
                      purchased = purchasedList.filter(
                        (pItem, index) => pItem === item.id
                      );

                    console.log(
                      "purchasedpurchasedpurchasedpurchased",
                      purchased,
                      "item",
                      item
                    );

                    if (purchased && purchased.length)
                      title += " (Приобретено)";

                    return (
                      <View
                        style={{ paddingHorizontal: sizes.margin }}
                        key={`type-${item && item.name ? item.name : index}`}
                      >
                        <CheckBoxInput
                          title={title}
                          style={styles.checkBoxInputText}
                          disabled={!item.active}
                          checked={activeRadio === item.name}
                          notPressWhenDisabled={false}
                          onPress={() => {
                            if (item.active && item.name && item.id !== false) {
                              setActiveRadio(item.name);
                              setSelectedProgramId(item.id);
                            } else if (!item.active)
                              ToastView(
                                "Надо, приобрести и пройти предыдущий степ",
                                2500
                              );
                          }}
                        />

                        <View style={styles.checkBoxInputTextBottomLine} />
                      </View>
                    );
                  })}
                </>
              )}
            </>
          ) : (
            <View
              style={{
                flex: 1,
                minHeight: Dimensions.get("screen").height / 1.4,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  width: "100%",
                  textAlign: "center",
                  color: "#000",
                }}
              >
                Ошибка получения данных
              </Text>
            </View>
          )}
        </View>
      </ScrollView>

      <View style={{ padding: 16, marginBottom: 25 }}>
        {programLoading ? (
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              height: 85,
              paddingBottom: 23,
            }}
          >
            <ActivityIndicator color={colors.orange} />
          </View>
        ) : (
          <ButtonInput
            disabled={
              /*!(
                !programLoading &&
                !paymentLinkError &&
                paymentLink &&
                typeof paymentLink !== "object"
              )*/ !selectedProgramId
            }
            title="Приобрести курс"
            onClick={async () => {
              const responseAdd = await addBasketProductById({
                id: selectedProgramId,
                type: "PROGRAM",
              });

              console.log("responseAdd", responseAdd);

              if (
                responseAdd &&
                responseAdd.response &&
                responseAdd.response.data &&
                responseAdd.response.status &&
                responseAdd.response.status === "success"
              ) {
                ToastView("Добавлено в корзину", 2000);
              } else
                ToastView(
                  "Ооо нет :( Случилось что-то непридвиденное.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27.",
                  3500
                );
              // handleLinkOpen(paymentLink)
            }}
          />
        )}
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  // basketLoading: state.BasketReducer.basketLoading,
  // basket: state.BasketReducer.basket,
  // basketFailed: state.BasketReducer.basketFailed,

  // paymentLinkLoading: state.mainReducer.paymentLinkLoading,
  // paymentLink: state.mainReducer.paymentLink,
  // paymentLinkError: state.mainReducer.paymentLinkError,

  programLoading: state.ProgramsReducer.programLoading,
  programData: state.ProgramsReducer.programData,
  programError: state.ProgramsReducer.programError,

  programStepInfoLoading: state.ProgramsReducer.programStepInfoLoading,
  programStepInfoData: state.ProgramsReducer.programStepInfoData,
  programStepInfoError: state.ProgramsReducer.programStepInfoError,
});

const mapDispatchToProps = {
  addBasketProductById,

  gerProgramById,
  getFastCheckout,

  getProgramStepInfo,
};

export default connect(mapStateToProps, mapDispatchToProps)(Program);

const styles = StyleSheet.create({
  periodSelection: {
    flexDirection: "row",
    marginBottom: 24,
    marginTop: 25,
    height: 33,
  },
  subtitle: {
    fontSize: 22,
    color: "#151C26",
    paddingTop: 16,
    ...fonts.normal,
    paddingRight: 50,
  },
  text: {
    color: colors.title,
    fontSize: 15,
    fontFamily: fonts.normal.fontFamily,
    fontWeight: fonts.normal.fontStyle,
  },
  list: {
    justifyContent: "flex-start",
    alignItems: "flex-start",

    marginLeft: 15,
  },
  listItemMarked: {
    height: 4,
    width: 4,
    borderRadius: 4,

    backgroundColor: "red",
  },

  separator: {
    height: 1,
    width: "100%",
    marginTop: 25,
    marginBottom: 20,

    opacity: 0.12,
    backgroundColor: colors.swiper_inactive,
  },
  checkBoxInput: {
    marginRight: 2,
  },
  checkBoxInputText: {
    marginLeft: 34,
  },
  checkBoxInputTextBottomLine: {
    width: "100%",
    marginLeft: 60,
    height: 1,
    marginTop: 18,
    marginBottom: 18,
    opacity: 0.12,
    backgroundColor: colors.swiper_inactive,
  },
});
