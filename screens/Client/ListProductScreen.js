import React, { useState, useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from "react-native";

import { Background } from "../../components/Background";
import { Header } from "../../components/Header";
import { ListItem } from "../../components/ListItem";
import { mocks } from "../../constants";
import { colors, fonts, sizes } from "../../constants/theme";
import MainHeader from "../../components/Headers/MainHeader";

import { ServicesAPI } from "../../api/services";

export const ListProductScreen = ({ navigation, route }) => {
  const { exercises } = mocks;
  const companyId =
    route.params !== undefined && route.params.companyId !== undefined
      ? route.params.companyId
      : false;

  const [_serivces, _setServices] = useState([]);

  console.log("<<<ListProductScreen -- ", route);

  useEffect(() => {
    if (companyId) {
      try {
        ServicesAPI.getServicesByCompanyId(companyId).then((services) => {
          _setServices(services);
        });
      } catch (e) {
        console.log("Error ServicesAPI.getServicesByCompanyId --- ", e);
      }
    }
  }, [navigation]);

  return (
    <Background>
      <MainHeader textColor={"dark"} back={true} navigation={navigation} />

      <View style={styles.container}>
        <Text style={[styles.title, fonts.normal, fonts.h1]}>Выбор пакета</Text>
      </View>

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ marginHorizontal: 16 }}>
          {_serivces.map((item) => (
            <TouchableOpacity
              key={`item-${item.id}`}
              onPress={() =>
                navigation.navigate("ItemService", { service: item, companyId })
              }
              activeOpacity={0.7}
            >
              <ListItem title={item.title} price={item.price_min}></ListItem>
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: { paddingHorizontal: sizes.margin },
  title: { color: colors.title, marginBottom: 25 },
});
