import React, { useState } from "react";
import { Text, View, StyleSheet, ScrollView, Dimensions } from "react-native";

import { Background } from "../../components/Background";
import HeaderBack from "../../components/Headers/BackHeader";
import { colors, fonts } from "../../constants/theme";
import ProfileCard from "../../components/ProfileCard";
import { connect } from "react-redux";
import { getAllStaff, clearStaffData } from "../../actions/StaffActions";
import { getSizeSkeletonLoaders } from "../../utils";
import Header from "../../components/Headers/Header";
import ListWithInfo from "../../components/ListWithInfo";

const Specialists = (props) => {
  const [limit, setLimit] = useState(10);

  const {
    getAllStaff = () => {},
    clearStaffData = () => {},

    loading = false,
    moreStaffloading = false,
    staffListMeta = {},
    staff = [],
    requestFailed = false,

    route = {},
  } = props;
  const { isSpecialists = false } = route && route.params ? route.params : {};

  const renderListItem = (
    item = {},
    index = 0,
    list = [],
    isLoading = false
  ) => (
    <ProfileCard
      {...item}
      index={index}
      item={item}
      tiffany={isSpecialists}
      isExpert={isSpecialists}
      isLoading={isLoading}
      navigation={props.navigation}
    />
  );

  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <Header
        title={isSpecialists ? "Специалисты" : "Тренеры"}
        backTitle={true}
        tiffany={isSpecialists}
        routeName={route.name}
      />

      <View style={styles.container}>
        <ListWithInfo
          requestDidMountCopmponent={() => getAllStaff(isSpecialists, 0, limit)}
          requestWillUnmountComponent={clearStaffData}
          mocsDataOffsetTop={60}
          mocsDataListElementSize={102}
          heightDimensionsContainer={1.4}
          data={staff}
          moreLoading={moreStaffloading}
          listMeta={staffListMeta}
          error={requestFailed}
          loading={loading}
          wrapListParams={{
            showsVerticalScrollIndicator: false,
            style: { flex: 1 },
            contentContainerStyle: {
              paddingBottom: 45,
            },
          }}
          elementKeyName={`${isSpecialists ? "expert" : "trainer"}-item-`}
          renderListItem={renderListItem}
          onMoreDataList={(offset = 0) =>
            getAllStaff(isSpecialists, offset, limit, true)
          }
        />
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  loading: state.StaffReducer.staffLoading,
  staff: state.StaffReducer.staff,
  requestFailed: state.StaffReducer.staffError,
  moreStaffloading: state.StaffReducer.moreStaffloading,
  staffListMeta: state.StaffReducer.staffListMeta,
});

const mapDispatchToProps = {
  getAllStaff,
  clearStaffData,
};

export default connect(mapStateToProps, mapDispatchToProps)(Specialists);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
    paddingTop: 15,
  },
  title: {
    color: colors.title,
    ...fonts.normal,
    ...fonts.title,
    paddingVertical: 16,
  },
});
