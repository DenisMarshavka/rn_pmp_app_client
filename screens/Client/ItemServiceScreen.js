import React, { useState, useEffect, useRef, createRef } from "react";
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  AsyncStorage,
  TouchableOpacity,
  View,
} from "react-native";
import Carousel from "react-native-snap-carousel";
import Lightbox from "react-native-lightbox";
import { connect } from "react-redux";

import Header from "../../components/Headers/Header";
import { Background } from "../../components/Background";
import { mocks } from "../../constants";
import { colors, fonts, sizes } from "../../constants/theme";
import SwipeModal from "./swipeModal";
import BackHeader from "../../components/Headers/BackHeader";
import CheckBoxInput from "../../components/checkboxinput/CheckBoxInput";
import ButtonInput from "../../components/button/ButtonInput";
import { getTrainersByServiceId } from "../../actions/TrainersActions";
import { addBasketProductById } from "../../actions/BasketActions";
import { ToastView } from "../../components";
import {
  validURL,
  addToBlackListProductCountChanging,
  formattingPrice,
} from "../../utils";

let deviceWidth = Dimensions.get("window").width;

const gallery = [
  {
    uri: require("../../assets/images/fitness-studio2.png"),
  },
  {
    uri: require("../../assets/images/fitness-studio2.png"),
  },
  {
    uri: require("../../assets/images/fitness-studio2.png"),
  },
];

const icons = {
  up: require("../../assets/icons/drop_down_arrow_up.png"),
  down: require("../../assets/icons/drop_down_arrow_down.png"),
};

const ItemServiceScreen = ({
  navigation,
  route,
  trainersLoading = false,
  trainers = [],
  requestFailed = false,
  getTrainersByServiceId = () => {},
  addBasketProductById = () => {},
}) => {
  const [visibleModal, setVisibleModal] = useState(false);
  const [expandedDropdown, setExpandedDropdown] = useState(false);
  const [selectedPackagesCount, setSelectedPackagesCount] = useState(1);
  const [packages, setPackages] = useState([]);
  const [disableSelectionPackages, setDisableSelectionPackages] = useState(
    false
  );

  let swipeModalRef = useRef(null);

  const { byExpert = false, trainType = 0 } = route.params;
  const service = route.params.service ? route.params.service : {};
  const { companyId = false, quantity = 0 } =
    route.params && route.params ? route.params : {};

  const { packages: existingPackages = false } = service || {};

  const description =
    service && service.service && service.service.comment
      ? service.service.comment
      : service && service.description
      ? service.description
      : "";

  console.log("SERVICE: ", service);

  useEffect(() => {
    if (service && service.price) {
      setPackages([
        {
          id: 0,
          count: 1,
          price: service.price,
          disabled: disableSelectionPackages && selectedPackagesCount !== 1,
        },
        {
          id: 1,
          count: 5,
          price: 5 * service.price,
          disabled: disableSelectionPackages && selectedPackagesCount !== 5,
        },
        {
          id: 2,
          count: 10,
          price: 10 * service.price,
          disabled: disableSelectionPackages && selectedPackagesCount !== 10,
        },
        {
          id: 3,
          count: 20,
          price: 20 * service.price,
          disabled: disableSelectionPackages && selectedPackagesCount !== 20,
        },
        {
          id: 4,
          count: 50,
          price: 50 * service.price,
          disabled: disableSelectionPackages && selectedPackagesCount !== 50,
        },
      ]);
    }
  }, [service, route, disableSelectionPackages, selectedPackagesCount]);

  useEffect(() => {
    getTrainersByServiceId(
      /*byExpert
        ? 5
        : */ service &&
        service.service &&
        service.service.id !== false
        ? service.service.id
        : service && service.id !== false
        ? service.id
        : null,
      byExpert /* false,*/,
      companyId
    );

    if (service.title && service.title.indexOf("тренировок") >= 0) {
      let count = NaN;
      const chunksTitle = service.title.split(" тренировок");

      if (chunksTitle && chunksTitle.length && chunksTitle[0]) {
        const twoChunks = chunksTitle[0].split(" ");

        for (let chunk of twoChunks) {
          if (chunk && !isNaN(+chunk)) {
            count = +chunk;
            setSelectedPackagesCount(+chunk);

            setDisableSelectionPackages(true);
          }
        }
      }

      console.log("COUNT PACKAGES: ", count);
    }

    return () => {
      setDisableSelectionPackages(false);
    };
  }, [route]);

  console.log("COMPANYYY ID", companyId, route.params, "service", service);

  const isRecord =
    !route ||
    !route.params ||
    route.params.tiffany ||
    (route.params.isOnline && !route.params.isPay) ||
    !route.params.isPay;

  const renderSliderItem = ({ item, index }) => (
    <View style={styles.slide} key={`video-${index}`}>
      <Lightbox>
        <Image style={styles.slidePhoto} source={item.uri} />
      </Lightbox>
    </View>
  );

  const handleDropDown = () => {
    setExpandedDropdown(!expandedDropdown);
  };

  console.log("ADD SERVICE TO BAsKET", {
    id: service.id,
    type: "trainer_service",
    count: selectedPackagesCount,
  });

  return (
    <Background>
      {/*// <BackHeader tiffany={route.params.tiffany} */}
      <Header
        tiffany={route.params.tiffany}
        backTitle
        basketShow
        basketIconStyle={{ top: 0 }}
        routeName={route.name}
      />

      <View style={{ flex: 1 }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ flex: 1, paddingHorizontal: sizes.margin }}>
            {/*<Text
              style={[
                styles.title,
                { color: colors.title },
                fonts.normal,
                fonts.title,
              ]}
            >
              Услуга
            </Text>*/}

            <Text style={styles.subtitle}>{service.title}</Text>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginTop: 22,
              }}
            >
              <Text
                style={[
                  { color: colors.orange, fontSize: 24 },
                  fonts.normal,
                  route.params.tiffany && { color: "#0FD8D8" },
                ]}
              >
                {formattingPrice(service.price) || 0} ₽
              </Text>

              <Text
                style={[{ color: colors.title, fontSize: 24 }, fonts.normal]}
              >
                {service.point || 0} баллов
              </Text>
            </View>

            <View style={{ marginTop: 25 }}>
              <Image
                style={{ borderRadius: 8, width: "100%", height: 207 }}
                source={
                  service &&
                  service.image &&
                  service.image.trim() &&
                  validURL(service.image)
                    ? { uri: service.image }
                    : require("../../assets/images/notProductImage.png")
                }
                resizeMode="cover"
              />
            </View>

            {quantity ? (
              <View
                style={{
                  flex: 1,
                  marginTop: 26,

                  flexDirection: "row",
                }}
              >
                <Text style={{ ...styles.dropDownTitle, marginRight: 10 }}>
                  В наличии:
                </Text>

                <Text style={{ ...styles.dropDownTitle, color: colors.orange }}>
                  {quantity} шт
                </Text>
              </View>
            ) : null}

            {/*description && description.trim() ? (*/}
            <View style={{ marginTop: 24, marginBottom: 10 }}>
              <Text style={[{ color: colors.title }, fonts.h2, fonts.normal]}>
                Об услуге
              </Text>

              <Text
                style={[
                  {
                    color: colors.title,
                    fontSize: 15,
                    lineHeight: 22,
                    paddingTop: 7,
                  },

                  fonts.normal,
                ]}
              >
                {description && description.trim()
                  ? description.trim()
                  : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}
              </Text>
            </View>
            {/*) : null}*/}
          </View>

          {service && service.video && service.video.length ? (
            <View style={{ marginTop: 15 }}>
              <Carousel
                layout={"default"}
                //ref = {(c) => {console.log(c)}}
                renderItem={renderSliderItem}
                sliderWidth={deviceWidth}
                itemWidth={deviceWidth - 45}
                contentContainerCustomStyle={{
                  marginBottom: 15,
                  marginTop: 16,
                  paddingLeft: 16,
                  alignSelf: "flex-start",
                }}
                inactiveSlideOpacity={1}
                data={gallery}
                inactiveSlideScale={1.6}
                layoutCardOffset={1}
                activeSlideAlignment={"start"}
              />
            </View>
          ) : null}

          {!isRecord &&
          service &&
          service.price &&
          packages &&
          packages.length &&
          !isNaN(+existingPackages) &&
          +existingPackages ? (
            <View
              style={{ flex: 1, marginTop: 20, marginHorizontal: sizes.margin }}
            >
              <View style={{ marginVertical: 16 }}>
                <TouchableOpacity
                  onPress={() => handleDropDown()}
                  style={styles.dropDownContainer}
                >
                  <Text style={styles.dropDownTitle}>Выберите пакет</Text>

                  <Image
                    source={expandedDropdown ? icons["up"] : icons["down"]}
                  />
                </TouchableOpacity>
              </View>

              {expandedDropdown &&
                packages.map((item, i) => {
                  const title = disableSelectionPackages
                    ? `Пакет из ${item.count} шт.`
                    : `Пакет из ${item.count} шт. ${
                        item.count * service.price
                      } ₽`;

                  if (item.id !== false && item.count)
                    return (
                      <View key={`dropDown-${item && item.id ? item.id : i}`}>
                        <CheckBoxInput
                          title={title}
                          style={styles.checkBoxInputText}
                          checked={selectedPackagesCount === item.count}
                          onPress={() => {
                            setSelectedPackagesCount(
                              disableSelectionPackages ? 1 : item.count
                            );
                          }}
                          disabled={item.disabled}
                          notPressWhenDisabled
                        />

                        <View style={styles.checkBoxInputTextBottomLine} />
                      </View>
                    );
                })}
            </View>
          ) : null}
        </ScrollView>
      </View>

      {service && service.price ? (
        <View
          style={{
            padding: 16,
            marginBottom: 15,
            position: "relative",
            zIndex: -1,
          }}
        >
          <ButtonInput
            tiffany={route.params.tiffany}
            title={isRecord ? "ВЫБРАТЬ СПЕЦИАЛИСТА" : "ДОБАВИТЬ В КОРЗИНУ"}
            onClick={async () => {
              if (isRecord) {
                if (swipeModalRef && swipeModalRef.current)
                  swipeModalRef.current.show();
              } else {
                if (service && service.id !== false) {
                  await addBasketProductById({
                    id: service.id,
                    type: "trainer_service",
                    count: disableSelectionPackages ? 1 : selectedPackagesCount,
                  });

                  await addToBlackListProductCountChanging(service.id);

                  // console.log("disableSliderForProducts", service.id);

                  ToastView("Добавлено в корзину", 2000);
                } else
                  ToastView(
                    "Ооо нет :( Случилось что-то непридвиденное.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27.",
                    3500
                  );
              }
            }}
          />
        </View>
      ) : null}

      {isRecord ? (
        <SwipeModal
          serviceTitle={service.title}
          byExpert={byExpert}
          tiffany={route.params.tiffany}
          staffs={trainers || []}
          loading={trainersLoading}
          service={service}
          navigation={navigation}
          companyId={companyId}
          swipeModalRef={swipeModalRef}
          requestFailed={requestFailed}
          fromService={true}
          trainType={trainType}
          isOnlineService={route && route.params && route.params.isOnline}
        />
      ) : null}
    </Background>
  );
};

const mapStateToProps = (state) => ({
  companyIdAndStaffIdData: {},
  companyIdAndStaffIdError: null,

  trainersLoading: state.TrainersReducer.loading,
  trainers: state.TrainersReducer.trainers,
  requestFailed: state.TrainersReducer.error,
});

const mapDispatchToProps = {
  getTrainersByServiceId,
  addBasketProductById,
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemServiceScreen);

const styles = StyleSheet.create({
  subtitle: {
    fontSize: 22,
    color: "#151C26",
    paddingTop: 16,
    ...fonts.normal,
    paddingRight: 50,
  },

  slide: {
    maxHeight: 196,
    width: "100%",
    maxWidth: deviceWidth - 55,
    backgroundColor: "#dedede",
    borderRadius: 10,
    overflow: "hidden",
    marginRight: 10,
  },
  slidePhoto: {
    width: "100%",
    resizeMode: "cover",
  },

  checkBoxInputText: {
    marginLeft: 34,
    color: "rgba(21, 28, 38, 0.87)",
  },
  checkBoxInputTextBottomLine: {
    width: "100%",
    marginLeft: 60,
    height: 1,
    marginTop: 18,
    marginBottom: 18,
    opacity: 0.12,
    backgroundColor: colors.swiper_inactive,
  },
  dropDownContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 10,
  },
  dropDownTitle: {
    ...fonts.medium,
    fontSize: 18,
  },
});
