import React, { useEffect, useState } from "react";
import { StyleSheet, ScrollView, Text, View, Dimensions } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import * as Animatable from "react-native-animatable";
import { connect } from "react-redux";

import { ToastView } from "../../../components";
import TitleHeader from "../../../components/Headers/TitleHeader";
import { sizes, fonts } from "../../../constants/theme";
import ListWithInfo from "../../../components/ListWithInfo";
import NotificationItem from "./NotificationItem";
import { Background } from "../../../components/Background";
import {
  deleteUserNotification,
  getUserNotifications,
  clearUserNotifications,
} from "../../../actions/UserActions";
import { getSizeSkeletonLoaders, getUserStorageData } from "../../../utils";
import Header from "../../../components/Headers/Header";

const NotificationScreen = ({
  navigation,
  route = {},
  getUserNotifications = () => {},
  notificationsLoading = true,
  listMeta = {},
  notifications = [],
  notificationsError = false,
  notificationsMoreLoading = false,
  deleteUserNotification = () => {},
}) => {
  const [fetchingStatus, setFetchingStatus] = useState(true);
  const [limit, setLimit] = useState(3);

  useEffect(() => {
    setFetchingStatus(true);

    getUserStorageData().then((userData) => {
      if (userData && !userData.notifications)
        ToastView(
          "Внимание! У вас отключены уведомления\nПожалуйста, включите их в Меню Приложения, чтобы не пропускать новые события в фоновом режиме",
          10000
        );
    });

    return () => {
      setFetchingStatus(true);
    };
  }, []);

  const notificationsList = notificationsLoading
    ? getSizeSkeletonLoaders(100, 100)
    : !notificationsLoading && notifications && notifications.length
    ? notifications
    : [];

  console.log("notificationsList", notificationsList);

  useEffect(() => {
    setFetchingStatus(notificationsLoading);
  }, [notificationsLoading]);

  let delay = 60;

  const renderListItem = (
    item = {},
    index = 0,
    list = [],
    isLoading = false
  ) => (
    <NotificationItem
      isLoading={isLoading}
      onDeleteNotification={deleteUserNotification}
      requestFailed={notificationsError}
      {...item}
      body={item && item.data !== null && item.data}
      description={item.body}
    />
  );

  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <Header title={"Уведомления"} backTitle routeName={route.name} />

      <View style={{ flex: 1 }}>
        <ListWithInfo
          titleEmpty={"Список уведомлений пуст"}
          requestDidMountCopmponent={getUserNotifications}
          requestWillUnmountComponent={clearUserNotifications}
          mocsDataOffsetTop={50}
          mocsDataListElementSize={75}
          heightDimensionsContainer={1.3}
          data={notifications}
          moreLoading={notificationsMoreLoading}
          listMeta={listMeta}
          error={notificationsError}
          loading={notificationsLoading || fetchingStatus}
          wrapListParams={{
            showsVerticalScrollIndicator: false,
            style: { flex: 1 },
            contentContainerStyle: {
              paddingBottom: 30,
            },
          }}
          elementKeyName={"notification-item-"}
          renderListItem={renderListItem}
          onMoreDataList={(offset = 0) =>
            getUserNotifications(offset, limit, true)
          }
        />
      </View>

      <LinearGradient
        pointerEvents={"none"}
        colors={["rgba(255, 255, 255, 0)", "#FFF"]}
        style={styles.linearGradient}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  notificationsLoading: state.currentUser.notificationsLoading,
  notifications: state.currentUser.notifications,
  notificationsError: state.currentUser.notificationsError,
  listMeta: state.currentUser.notificationsListMeta,
  notificationsMoreLoading: state.currentUser.notificationsMoreLoading,
});

const mapDispatchToProps = {
  getUserNotifications,
  deleteUserNotification,
  clearUserNotifications,
};

export default connect(mapStateToProps, mapDispatchToProps)(NotificationScreen);

const styles = StyleSheet.create({
  header: {
    paddingLeft: 16,
    paddingRight: 25,
    paddingTop: 52,
    paddingBottom: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  title: {
    fontSize: sizes.h1,
    fontFamily: fonts.normal.fontFamily,
  },
  body: {
    // backgroundColor: "#FAFAFA"
  },
  dateTitle: { margin: sizes.margin, marginTop: 25, marginBottom: 20 },
  dateTitleStyle: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: sizes.headerName,
  },
  linearGradient: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 130,
  },
});
