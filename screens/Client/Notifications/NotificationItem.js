import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import Swipeable from "react-native-swipeable";
import { AntDesign } from "@expo/vector-icons/build/Icons";
import moment from "moment";

import { colors, fonts, sizes } from "../../../constants/theme";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader";
import { validURL } from "../../../utils";
import { NoAvatarTrainerSvg } from "../../../assets/icons/NoAvatarTrainerSvg/NoAvatarTrainerSvg";
import { handleNotificationOpen } from "../../../utils";

// import { RectButton } from 'react-native-gesture-handler';

export default class FeedbackItem extends Component {
  swipeable = null;

  state = {
    isSwiping: false,
  };

  close = () => {
    this.swipeable.recenter();
  };

  pressHandler = (id) => {
    this.close();

    if (id) this.props.onDeleteNotification(id);
  };

  render() {
    const {
      image = "",
      title = "",
      send_date = "",
      description = "",
      id = null,
      isLoading = false,
      body = false,
    } = this.props;

    const receivedData = body ? JSON.parse(body) : false;
    const approveToNavigationScreen = receivedData && receivedData.type;

    const Wrapper = isLoading ? View : Swipeable;

    console.log(
      "Notification BODY Type: ",
      receivedData.type,
      "bodybodybody",
      body,
      "description",
      description
    );

    return (
      <Wrapper
        onRef={(ref) => (this.swipeable = ref)}
        onSwipeStart={() => this.setState({ isSwiping: true })}
        onSwipeRelease={() => this.setState({ isSwiping: false })}
        rightActionActivationDistance={300}
        rightButtonWidth={130}
        onRightActionRelease={() => this.pressHandler(id)}
        rightButtons={[
          <TouchableOpacity
            activeOpacity={0.6}
            style={{
              height: "100%",
              paddingRight: 15,
              justifyContent: "center",
              backgroundColor: "#E02323",
            }}
            onPress={() => this.pressHandler(id)}
          >
            <AntDesign
              style={{ marginLeft: "25%" }}
              name="delete"
              size={18}
              color="white"
            />
          </TouchableOpacity>,
        ]}
      >
        <TouchableOpacity
          onPress={() =>
            receivedData
              ? handleNotificationOpen(
                  JSON.parse(this.props.body).type,
                  this.props,
                  true,
                  () => this.pressHandler(id)
                )
              : null
          }
          activeOpacity={
            this.props.isLoading || !approveToNavigationScreen ? 1 : 0.5
          }
          style={styles.itemBody}
        >
          <SkeletonLoader
            isLoading={isLoading}
            containerStyle={styles.icon}
            layout={[{ width: 44, height: 44, borderRadius: 44 }]}
          >
            {!image || !image.trim() || !validURL(image) ? (
              <NoAvatarTrainerSvg width={44} height={44} />
            ) : (
              <Image source={{ uri: image }} style={styles.iconDetail} />
            )}
          </SkeletonLoader>

          <View style={styles.rightView}>
            <View
              style={{
                width: "100%",
                flexDirection: "row",
                alignItem: "space-between",
              }}
            >
              <SkeletonLoader
                isLoading={isLoading}
                containerStyle={styles.userName}
                layout={[{ width: 130, height: 18 }]}
              >
                <Text numberOfLines={1} style={styles.userNameStyle}>
                  {title.trim() || "Без названия"}
                </Text>
              </SkeletonLoader>

              <SkeletonLoader
                isLoading={isLoading}
                containerStyle={{
                  flex: 1,
                  width: "100%",
                  paddingRight: 16,
                  alignSelf: "flex-end",
                }}
                layout={[{ width: 55, height: 20, alignSelf: "flex-end" }]}
              >
                <Text
                  numberOfLines={1}
                  style={[styles.date, { textAlign: "right" }]}
                >
                  {send_date && send_date.trim()
                    ? moment(send_date).format("MMM Do")[0].toUpperCase() +
                      moment(send_date)
                        .format("MMM Do")
                        .slice(1, moment(send_date).format("MMM Do").length)
                    : ""}
                </Text>
              </SkeletonLoader>
            </View>

            <SkeletonLoader
              isLoading={isLoading}
              containerStyle={styles.userText}
              layout={[{ width: 220, height: 25 }]}
            >
              <Text style={styles.userTextStyle} numberOfLines={2}>
                {description}
              </Text>
            </SkeletonLoader>
          </View>
        </TouchableOpacity>
      </Wrapper>
    );
  }
}

const styles = StyleSheet.create({
  itemBody: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignContent: "center",
    paddingTop: 15,
    paddingLeft: 16,
    height: 72,
    // backgroundColor: '#FAFAFA',
  },
  rightView: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    flexWrap: "wrap",
    borderColor: "#d3d8dc",
    borderBottomWidth: 1,
  },
  icon: {
    width: 56,
  },
  iconDetail: {
    width: 40,
    height: 40,
  },
  userText: {
    paddingTop: 6,
  },
  userTextStyle: {
    ...fonts.normal,
    fontSize: sizes.font,
    maxWidth: "90%",

    color: "#000",
    opacity: 0.54,
  },
  userNameStyle: {
    //height: 18,
    fontSize: sizes.headerName,
    color: colors.title,
    width: Dimensions.get("screen").width / 2,
    ...fonts.normal,
  },

  rightAction: {
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "flex-end",
    paddingRight: 41,
    width: 150,
    height: "100%",
  },
  actionText: {
    marginRight: 36,
  },
  date: {
    ...fonts.normal,
    fontSize: sizes.font,
    color: "#000",
    opacity: 0.54,
  },
});
