import React, { useState, useCallback, useEffect, useRef } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  ScrollView,
  Alert,
  Linking,
  Keyboard,
  Dimensions,
  TouchableWithoutFeedback,
} from "react-native";
import qs from "qs";

import { Background } from "../../../../components/Background";
import Header from "../../../../components/Header";
import { UserAPI } from "../../../../api/user";
import sendButton from "../../../../assets/images/sendButton.png";

const supportMail = "denis-marshavka@mail.ru";

const ContactUsScreen = ({ navigation, userName }) => {
  let [message, setMessage] = useState("");
  let [name, setName] = useState("");
  const refTextInput = useRef(null);

  const changeMessage = useCallback((text) => setMessage(text), [setMessage]);

  useEffect(() => {
    UserAPI.getUserByToken()
      .then((response) => {
        setName(response.data.firstname);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  const finishSendMail = () => {
    setTimeout(() => {
      showAlert(
        "Информация",
        `${
          name.trim() || "Уважаемый пользователь"
        }, спасибо большое за ваше обращение!\nМы обязательно его рассмотрим в ближайшеё время.\n\nС уважением: Администрация "PMP"😉`,
        [
          {
            text: "Не за что",
            onPress: () => {},
            style: "success",
          },
        ],
        {
          cancelable: false,
        }
      );
    }, 1500);
  };

  const handleSenderToSupport = () => {
    const isNotEmpty = !!message.trim(),
      minLength = 15,
      containsMinLength = message.trim().length >= minLength;

    try {
      if (isNotEmpty && containsMinLength) {
        sendMail(supportMail);
      } else {
        showAlert(
          "Информация",
          !isNotEmpty
            ? "Сообжение не может быть пустым!\nЗаполните пожалуйста и повторите попытку"
            : `Минимальная длинна сообщения - ${minLength} символов!`,
          [
            {
              text: "Хорошо",
              onPress: () => refTextInput.current.focus(),
              style: "success",
            },
          ],
          {
            cancelable: false,
          }
        );
      }
    } catch (e) {
      console.log("Error send message to Support Mail: ", e);
    }
  };

  const sendMail = async (to, options = {}) => {
    const { cc, bcc } = options;

    let url = `mailto:${to}`;

    const canOpen = await Linking.canOpenURL(url);

    // Create email link query
    const query = qs.stringify({
      subject: 'Message from the application "PMP"',
      body: message,
      cc: cc,
      bcc: bcc,
    });

    if (query.length) url += `?${query}`;
    console.log("Generated message to send: ", url);

    if (!canOpen) throw new Error("Provided URL can not be handled");

    showAlert(
      "Подтверждение",
      `Ваше сообщение: "${message}",\n всё верно?`,
      [
        {
          text: "Да",
          onPress: () => {
            Keyboard.dismiss();
            setMessage("");

            finishSendMail();
            return Linking.openURL(url);
          },
          style: "success",
        },
        {
          text: "Нет",
          onPress: () => refTextInput.current.focus(),
          style: "cancel",
        },
      ],
      {
        cancelable: false,
      }
    );
  };

  const showAlert = (title = "", message = "", buttons = [], options = {}) => {
    try {
      if (title.trim() && message.trim() && buttons.length) {
        Alert.alert(title, message, [...buttons], {
          cancelable: false,
          ...options,
        });
      }
    } catch (e) {
      console.log("Error showed a Alert: ", e);
    }
  };

  return (
    <Background>
      <Header
        style={{ marginTop: 10 }}
        title="Техподдержка"
        goBack={() => navigation.goBack()}
        disabledBtn={!message.trim()}
        isSubmit={true}
        imageSubmit={sendButton}
        submitPress={handleSenderToSupport}
        styleSubmit={{
          paddingTop: 15,
          height: 20,
          width: 30,
        }}
      />

      <TouchableWithoutFeedback onPressOut={() => Keyboard.dismiss()}>
        <View style={styles.contentWrapper}>
          <ScrollView style={styles.textInputWrapper}>
            <TextInput
              style={styles.textInput}
              onChangeText={changeMessage}
              value={message}
              ref={refTextInput}
              multiline
              numberOfLines={20}
              placeholder="Какие сложности возникли у вас при работе с приложением, чего не хватает?"
              placeholderTextColor={"#ccc"}
              textAlignVertical="top"
            />
          </ScrollView>
        </View>
      </TouchableWithoutFeedback>
    </Background>
  );
};

export default ContactUsScreen;

const styles = StyleSheet.create({
  contentWrapper: {
    flex: 1,
    width: "100%",
    marginTop: 20,
  },
  textInputWrapper: {
    flex: 1,
    paddingHorizontal: 15,
  },
  textInput: {
    width: "100%",
    height: Dimensions.get("screen").height / 2.2,
    borderColor: "#F46F22",
    borderWidth: 1,
    color: "#000",
    borderRadius: 8,
    fontSize: 16,
    paddingHorizontal: 15,
    paddingVertical: 20,
    marginBottom: "10%",
  },
});
