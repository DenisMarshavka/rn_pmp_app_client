import React, { useState, useEffect } from "react";
import {
  Text,
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
  AsyncStorage,
  Share,
  Platform,
} from "react-native";
import { connect } from "react-redux";
import Switch from "react-native-switch-pro";

import { colors, fonts } from "../../../constants/theme";
import TitleHeader from "../../../components/Headers/TitleHeader";
import { Background } from "../../../components/Background";
import { setCurrentUser } from "../../../actions";
import { Logout } from "../../../components/logout/logout";
import Header from "../../../components/Headers/Header";

const dataButtons = [
  { title: "Поделиться", isShare: true },
  { title: "Поддержка", navigate: "Support" },
  { title: "Политика конфиденциальности", navigate: "Privacy" },
  { title: "Выйти" },
];

const SettingsScreen = ({ setCurrentUser, navigation, route = {} }) => {
  const [logoutModal, setLogoutModal] = useState(false);
  const [approveNotification, setApproveNotification] = useState(false);

  const checkApproveNotifications = async () => {
    const data = await AsyncStorage.getItem("user");

    return JSON.parse(data).notifications || false;
  };

  const handleShareOn = async () => {
    try {
      await Share.share({
        message:
          Platform.OS !== "ios"
            ? "https://play.google.com/store/apps/details?id=com.app.pmp"
            : "https://apps.apple.com/ua/app/app-pmp-studios/id1525725913?l=ru",
      });
    } catch (e) {
      console.log('Error the share of the application "PMP": ', e);
    }
  };

  useEffect(() => {
    checkApproveNotifications().then(setApproveNotification);
  }, []);

  const handleNotificationsSwitchOnPress = async () => {
    await AsyncStorage.mergeItem(
      "user",
      JSON.stringify({ notifications: !approveNotification })
    );

    setApproveNotification(!approveNotification);
  };

  return (
    <Background>
      <Header title={"Настройки"} backTitle routeName={route.name} />

      <View style={{ marginTop: 17 }}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.inputButton}
          onPress={handleNotificationsSwitchOnPress}
        >
          <Text style={[styles.title, fonts.light]}>Оповещения</Text>

          <Switch
            width={48}
            height={28}
            value={approveNotification}
            onSyncPress={handleNotificationsSwitchOnPress}
            backgroundActive={colors.orange}
          />
        </TouchableOpacity>

        {dataButtons.map((item, i) => (
          <TouchableOpacity
            activeOpacity={0.7}
            key={`item-${item.title ? item.title : i}`}
            style={styles.inputButton}
            onPress={() => {
              item.navigate
                ? navigation.navigate(item.navigate)
                : item.isShare
                ? handleShareOn()
                : setLogoutModal(true);
            }}
          >
            <Text style={[styles.title, fonts.light]}>{item.title}</Text>

            <Image
              style={{ width: 5, height: 10 }}
              source={require("../../../assets/images/icons/arrow_right.png")}
            />
          </TouchableOpacity>
        ))}
      </View>

      {logoutModal && (
        <Logout
          yesHander={() => {
            AsyncStorage.clear();

            setCurrentUser(null);
          }}
          noHandler={() => setLogoutModal(false)}
        />
      )}
    </Background>
  );
};

const mapDispatchToProps = {
  setCurrentUser,
};

export default connect(null, mapDispatchToProps)(SettingsScreen);

const styles = StyleSheet.create({
  inputButton: {
    borderBottomWidth: 1,
    borderColor: "rgba(65, 57, 57, 0.12)",
    backgroundColor: colors.white,
    padding: 18,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  title: {
    fontSize: 16,
    color: "rgba(21, 28, 38, 0.87)",
  },
});
