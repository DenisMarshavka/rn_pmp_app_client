import React, { useEffect, useState } from "react";
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Linking,
} from "react-native";
import { connect } from "react-redux";
import HTMLView from "react-native-htmlview";

import { ToastView } from "../../components";
import { Background } from "../../components/Background";
import { colors, fonts, sizes } from "../../constants/theme";
import Header from "../../components/Headers/Header";
import ButtonInput from "../../components/button/ButtonInput";
import { UserAPI } from "../../api/user";
import { gerProgramById } from "../../actions/ProgramsActions";
import {
  addBasketProductById,
  getCurrentBasketState,
} from "../../actions/BasketActions";
import { getUserStorageData } from "../../utils";
import {
  getSizeSkeletonLoaders,
  showAlert /*, handleLinkOpen*/,
  formattingPrice,
} from "../../utils";
import CheckBoxInput from "../../components/checkboxinput/CheckBoxInput.tsx";
import SkeletonLoader from "../../components/loaders/SkeletonLoader";
import CarouselWithVideos from "../../components/carouselWithVideos";
import { getFastCheckout } from "../../actions";

const Program = ({
  addBasketProductById = () => {},

  getFastCheckout = () => {},

  navigation,
  route,

  gerProgramById = () => {},
  programLoading = false,
  programData = {},
  programError = false,
}) => {
  const { programId = null } =
    route && route.params && route.params ? route.params : {};
  const {
    description = "",
    points = 0,
    price = 0,
    img: image = "",
    title: programName = "",
  } = programData.program || {};

  console.log();

  const [purchasedList, setPurchasedList] = useState([]);

  const [selectedProgramId, setSelectedProgramId] = useState(programId);
  const [dataForSendActions, setDataForSendActions] = useState({
    id: selectedProgramId,
    type: "program",
  });
  const [upproveToPayment, setUpproveToPayment] = useState(true);

  useEffect(() => {
    if (programId) gerProgramById(programId);
  }, []);

  // const getPuchasedList = async () => {
  //   let response = null;
  //   const userData = await getUserStorageData();
  //
  //   const purchsedData = [];
  //
  //   if (userData && userData.id)
  //     response = await UserAPI.getUserDataPurchasedPrograms(userData.id);
  //
  //   if (
  //     response &&
  //     response.data &&
  //     response.status &&
  //     response.status === "success"
  //   ) {
  //     response.data.forEach((item, index) => {
  //       if (item && item.step_id) purchsedData.push(item.step_id);
  //     });
  //   }
  //
  //   if (purchsedData && purchsedData.length) setPurchasedList(purchsedData);
  //
  //   console.log(
  //     "getPuchasedList RESPONSE: ",
  //     response,
  //     "userData: ",
  //     userData,
  //     "purchsedData: ",
  //     purchsedData
  //   );
  // };

  console.log(
    "PROGRAM ITEM ID: ",
    programData.program_types,
    programId,
    route,
    "programData",
    programData,
    "programTitle",
    programData.title
  );

  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      {/*<BackHeader backTitle offsetTopIconBack={15} />*/}
      <Header basketShow={true} backTitle routeName={route.name} />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ paddingHorizontal: sizes.margin, flex: 1 }}>
          <SkeletonLoader
            layout={[
              { marginTop: 26, width: 266, height: 50, marginBottom: 25 },
            ]}
            isLoading={programLoading}
          >
            <Text
              style={[
                styles.title,
                { color: colors.title, maxWidth: 266, marginTop: 26 },
                fonts.normal,
                fonts.title,
              ]}
            >
              {programName || "Программа"}
            </Text>
          </SkeletonLoader>
        </View>

        <View style={{ flex: 1 }}>
          {!programError ? (
            <>
              {programLoading ? (
                <View
                  style={{
                    flex: 1,
                    minHeight: Dimensions.get("screen").height / 2.7,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <ActivityIndicator color={colors.orange} />
                </View>
              ) : (
                <>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginTop: 22,
                      paddingHorizontal: sizes.margin,
                    }}
                  >
                    <Text
                      style={[
                        { color: colors.orange, fontSize: 24 },
                        fonts.normal,
                      ]}
                    >
                      {formattingPrice(price)} ₽
                    </Text>

                    <Text
                      style={[
                        { color: colors.title, fontSize: 24 },
                        fonts.normal,
                      ]}
                    >
                      {points} баллов
                    </Text>
                  </View>

                  <CarouselWithVideos
                    sections={[{ images: [image, image] }]}
                    itemSize={{
                      width: Math.round(Dimensions.get("screen").width / 1.1),
                      height: Math.round(Dimensions.get("screen").width / 2),
                    }}
                    isModalWithImages={true}
                  />

                  <View
                    style={{ marginTop: 24, paddingHorizontal: sizes.margin }}
                  >
                    <Text
                      style={[{ color: colors.title }, fonts.h2, fonts.normal]}
                    >
                      О программе
                    </Text>

                    <HTMLView
                      value={
                        description ||
                        "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet architecto doloribus eum ex illum, impedit laboriosam magnam magni minus modi molestias nesciunt nihil non nulla officiis optio praesentium quas quasi reiciendis rem repellendus reprehenderit sed, similique temporibus veritatis voluptate. Adipisci ea eligendi enim id illo ipsam libero minima mollitia nam nemo, nulla numquam quasi recusandae rem sapiente sequi voluptas?"
                      }
                      style={{ marginTop: 10 }}
                      stylesheet={{
                        ul: styles.list,
                        addLineBreaks: false,
                        paragraphBreak: styles.text,
                      }}
                    />
                  </View>

                  {/*<View style={{ flex: 1, marginTop: 10 }}>
                    {videosStep &&
                    videosStep[0] &&
                    videosStep[0].videos &&
                    videosStep[0].videos ? (
                      <CarouselWithVideos
                        sections={videosStep}
                        itemSize={{ width: 110, height: 110 }}
                      />
                    ) : null}
                  </View>

                  <View style={styles.separator} />*/}
                </>
              )}
            </>
          ) : (
            <View
              style={{
                flex: 1,
                minHeight: Dimensions.get("screen").height / 1.4,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  width: "100%",
                  textAlign: "center",
                  color: "#000",
                }}
              >
                Ошибка получения данных
              </Text>
            </View>
          )}
        </View>
      </ScrollView>

      <View style={{ padding: 16, marginBottom: 25 }}>
        {programLoading ? (
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              height: 85,
              paddingBottom: 23,
            }}
          >
            <ActivityIndicator color={colors.orange} />
          </View>
        ) : (
          <>
            <ButtonInput
              disabled={!selectedProgramId}
              title="Приобрести курс"
              onClick={async () => {
                const responseAdd = await addBasketProductById({
                  id: selectedProgramId,
                  type: "PROGRAM",
                });

                console.log("responseAdd", responseAdd);

                if (
                  responseAdd &&
                  responseAdd.response &&
                  responseAdd.response.data &&
                  responseAdd.response.status &&
                  responseAdd.response.status === "success"
                ) {
                  ToastView("Добавлено в корзину", 2000);
                } else
                  ToastView(
                    "Ооо нет :( Случилось что-то непридвиденное.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27.",
                    3500
                  );
                // handleLinkOpen(paymentLink)
              }}
            />

            <ButtonInput
              disabled={!selectedProgramId}
              style={{ marginTop: 10 }}
              title="Написать куратору"
              onClick={async () => {
                const responseAdd = await addBasketProductById({
                  id: selectedProgramId,
                  type: "PROGRAM",
                });

                console.log("responseAdd", responseAdd);

                if (
                  responseAdd &&
                  responseAdd.response &&
                  responseAdd.response.data &&
                  responseAdd.response.status &&
                  responseAdd.response.status === "success"
                ) {
                  ToastView("Чат скуратором создан", 2000);
                } else
                  ToastView(
                    "Ооо нет :( Случилось что-то непридвиденное.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27.",
                    3500
                  );
                // handleLinkOpen(paymentLink)
              }}
            />
          </>
        )}
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  programLoading: state.ProgramsReducer.programLoading,
  programData: state.ProgramsReducer.programData,
  programError: state.ProgramsReducer.programError,
});

const mapDispatchToProps = {
  addBasketProductById,

  gerProgramById,
  getFastCheckout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Program);

const styles = StyleSheet.create({
  periodSelection: {
    flexDirection: "row",
    marginBottom: 24,
    marginTop: 25,
    height: 33,
  },
  subtitle: {
    fontSize: 22,
    color: "#151C26",
    paddingTop: 16,
    ...fonts.normal,
    paddingRight: 50,
  },
  text: {
    color: colors.title,
    fontSize: 15,
    fontFamily: fonts.normal.fontFamily,
    fontWeight: fonts.normal.fontStyle,
  },
  list: {
    justifyContent: "flex-start",
    alignItems: "flex-start",

    marginLeft: 15,
  },
  listItemMarked: {
    height: 4,
    width: 4,
    borderRadius: 4,

    backgroundColor: "red",
  },

  separator: {
    height: 1,
    width: "100%",
    marginTop: 25,
    marginBottom: 20,

    opacity: 0.12,
    backgroundColor: colors.swiper_inactive,
  },
  checkBoxInput: {
    marginRight: 2,
  },
  checkBoxInputText: {
    marginLeft: 34,
  },
  checkBoxInputTextBottomLine: {
    width: "100%",
    marginLeft: 60,
    height: 1,
    marginTop: 18,
    marginBottom: 18,
    opacity: 0.12,
    backgroundColor: colors.swiper_inactive,
  },
});
