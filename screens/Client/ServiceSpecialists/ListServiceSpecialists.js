import React, { useEffect, useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { connect } from "react-redux";
import * as Animatable from "react-native-animatable";

import { Background } from "../../../components/Background";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader";
import HeaderBack from "../../../components/Headers/BackHeader";
import { colors, fonts, sizes } from "../../../constants/theme";
import { Service } from "./Service";
import { getExpertServices } from "../../../actions/ServicesActions";
import { getSizeSkeletonLoaders } from "../../../utils";
import Header from "../../../components/Headers/Header";

const ListServiceSpecialists = (props) => {
  const {
    route = {},
    getExpertServices = () => {},
    expertServicesLoading = true,
    expertServices = [],
    expertServicesError = false,
  } = props;

  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(10);
  const [isEmptyList, setIsEmptyList] = useState(false);

  useEffect(() => {
    getExpertServices(offset, limit);
  }, []);

  useEffect(() => {
    if (!expertServicesLoading) checkIsEmptyServices();
  }, [expertServicesLoading, expertServices]);

  const checkIsEmptyServices = () => {
    let isEmpty = true;

    if (!expertServicesLoading && expertServices && expertServices.length) {
      for (let item of expertServices) {
        if (
          item &&
          item.expert_services &&
          item.expert_services.length &&
          item.expert_services[0] &&
          item.expert_services[0].title &&
          item.expert_services[0].expert_child_services &&
          item.expert_services[0].expert_child_services.length
        )
          isEmpty = false;

        item.isEmpty =
          item &&
          item.expert_services &&
          item.expert_services.length &&
          item.expert_services[0] &&
          item.expert_services[0].title &&
          item.expert_services[0].expert_child_services &&
          item.expert_services[0].expert_child_services.length
            ? false
            : true;
      }
    }

    setIsEmptyList(isEmpty);
  };

  const mockData = [
    {
      id: 1,
      type: "Услуги стилистов",
      createdAt: "2020-07-07T09:40:36.000Z",
      updatedAt: "2020-07-07T09:40:36.000Z",
      expert_services: [
        {
          id: 1,
          service_id: 698,
          expert_service_trainer_id: 1,
          title: "Укладки",
          type_id: 1,
          studio_id: 2,
          trainer_id: 3,
          points: 0,
          video: null,
          image: null,
          isNew: false,
          isSpecial: false,
          createdAt: "2020-07-07T10:03:56.000Z",
          updatedAt: "2020-07-07T10:03:56.000Z",
          expert_child_services: [
            {
              id: 2,
              expert_service_id: 1,
              title: "Повседневная укладка ",
              description: "Уxcvb",
              price: 3000,
              createdAt: "2020-07-07T10:35:16.000Z",
              updatedAt: "2020-07-16T10:12:52.000Z",
            },
          ],
        },
        {
          id: 1,
          service_id: 698,
          expert_service_trainer_id: 1,
          title: "Укладки",
          type_id: 1,
          studio_id: 2,
          trainer_id: 3,
          points: 0,
          video: null,
          image: null,
          isNew: false,
          isSpecial: false,
          createdAt: "2020-07-07T10:03:56.000Z",
          updatedAt: "2020-07-07T10:03:56.000Z",
          expert_child_services: [
            {
              id: 34,
              expert_service_id: 1,
              title: "Повседневная укладка ",
              description: "Уxcvgfhb",
              price: 3000,
              createdAt: "2020-07-07T10:35:16.000Z",
              updatedAt: "2020-07-16T10:12:52.000Z",
            },
          ],
        },
      ],
    },
  ];

  const servicesList =
    expertServicesLoading && !expertServicesError
      ? getSizeSkeletonLoaders(100, 275, false, false, mockData)
      : !expertServicesLoading && expertServices && expertServices.length
      ? expertServices
      : [];

  const renderItem = (item = {}, index = 0) => {
    const isLoading = expertServicesLoading || (item && item.isTest);
    const Wrap = !isLoading ? Animatable.View : View;

    console.log("EXPERT ITEM", item);

    return (
      <Wrap duration={350} delay={index * 350} animation="fadeInUp">
        {item && item.expert_services && item.expert_services.length ? (
          <SkeletonLoader
            isLoading={isLoading}
            containerStyle={{ flex: 1 }}
            layout={[
              { ...styles.category, flex: 1, width: 100, height: 25 },
              { flex: 1 },
            ]}
          >
            <Text style={styles.category}>{item.type || "Категория"}</Text>

            <ScrollView
              showsVerticalScrollIndicator={false}
              style={{ borderRadius: 10 }}
              contentContainerStyle={{ paddingBottom: 50 }}
            >
              {item && item.expert_services && item.expert_services.length ? (
                item.expert_services.map((item, index) => (
                  <Service
                    key={`item-${index + (item.title || "")}`}
                    item={item}
                    navigation={props.navigation}
                    isLoading={isLoading}
                  />
                ))
              ) : (
                <View
                  style={{
                    flex: 1,
                    minHeight: 75,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Text style={{ textAlign: "center" }}>Список пуст</Text>
                </View>
              )}
            </ScrollView>
          </SkeletonLoader>
        ) : null}
      </Wrap>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <Header
        title={"Выбор услуги"}
        tiffany={true}
        backTitle
        routeName={route.name}
        basketIconStyle={{ top: 0 }}
        basketShow
      />

      <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
        {!expertServicesError ? (
          <ScrollView
            style={{ flex: 1, borderRadius: 10 }}
            showsVerticalScrollIndicator={false}
          >
            {servicesList.length ? (
              servicesList.map((item, index) => (
                <React.Fragment
                  key={`parent-item-${item && item.id ? item.id : index}`}
                >
                  {renderItem(item, index)}
                </React.Fragment>
              ))
            ) : servicesList && !servicesList.length ? (
              <View style={styles.fullFill}>
                <Text
                  style={{
                    width: "100%",
                    textAlign: "center",
                    color: "#000",
                  }}
                >
                  Список пуст
                </Text>
              </View>
            ) : null}
          </ScrollView>
        ) : (
          <View style={styles.fullFill}>
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Ошибка получения данных
            </Text>
          </View>
        )}
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state) => ({
  expertServicesLoading: state.ServicesReducer.expertServicesLoading,
  expertServices: state.ServicesReducer.expertServices,
  expertServicesError: state.ServicesReducer.expertServicesError,
});

const mapDispatchToProps = {
  getExpertServices,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListServiceSpecialists);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  title: {
    color: colors.title,
    ...fonts.normal,
    ...fonts.title,
    paddingVertical: 16,
  },
  fullFill: {
    flex: 1,
    minHeight: Dimensions.get("screen").height / 1.3,
    alignItems: "center",
    justifyContent: "center",
  },
  category: {
    ...fonts.normal,
    ...fonts.h3,
    paddingBottom: 13,
    paddingTop: 5,
    paddingHorizontal: 5,
  },
});
