import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

import { colors, fonts, sizes } from "../../../constants/theme";
import { formattingPrice } from "../../../utils";

export const Price = (props) => {
  const [isDescription, setIsDescription] = useState(false);

  const openService = (service) => {
    console.log("service DAta: ", service);

    // props.navigation.navigate("StudioSelection", {
    //   service,
    //   byExpert: true,
    //   tiffany: true
    // });

    props.navigation.navigate("ItemService", {
      service,
      byExpert: true,
      tiffany: true,
    });
  };

  return (
    <TouchableOpacity onPress={() => openService(props.item)}>
      <View
        activeOpacity={0.8}
        style={{
          paddingTop: 24,
          paddingBottom: 8,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Text
          numberOfLines={2}
          style={[
            { width: "75%", color: colors.title },
            fonts.normal,
            fonts.h3,
          ]}
        >
          {props.item.title}
        </Text>

        <Text style={[{ color: "#0FD8D8" }, fonts.normal, fonts.h2]}>
          {formattingPrice(props.item.price)} ₽
        </Text>
      </View>

      <View style={{ paddingBottom: 0 }}>
        <Text
          numberOfLines={4}
          style={[
            fonts.normal,
            fonts.description,
            { width: "100%", color: "rgba(0, 0, 0, 0.54)", marginBottom: 10 },
          ]}
        >
          {props.item.description}
        </Text>
      </View>
    </TouchableOpacity>
  );
};
