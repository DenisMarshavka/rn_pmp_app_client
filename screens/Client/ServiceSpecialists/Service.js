import { LinearGradient } from "expo-linear-gradient";
import React, { useState } from "react";
import { Icon } from "react-native-elements";

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { colors, fonts, sizes } from "../../../constants/theme";
import { Price } from "./Price";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader";

export const Service = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      {props.item &&
      props.item.expert_child_services &&
      props.item.expert_child_services.length ? (
        <LinearGradient
          start={{ x: 1.2, y: 1.2 }}
          end={{ x: 0.1, y: 0.1 }}
          colors={[colors.white, colors.gray]}
          style={{ marginBottom: 11, borderRadius: 10 }}
        >
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => setIsOpen(!isOpen)}
          >
            <View style={styles.container_line}>
              <SkeletonLoader
                isLoading={props.isLoading}
                containerStyle={{ flex: 1, paddingRight: 10 }}
                layout={[{ width: 80, height: 25 }]}
              >
                <Text
                  style={[
                    { color: colors.text, paddingTop: isOpen ? 5 : 0 },
                    fonts.normal,
                    fonts.h3,
                  ]}
                  numberOfLines={!isOpen ? 2 : 10}
                  ellipsizeMode="tail"
                >
                  {props.item.title || "Услуга"}
                </Text>
              </SkeletonLoader>

              <View>
                <Icon
                  name={isOpen ? "chevron-small-up" : "chevron-small-down"}
                  type="entypo"
                  color="#151C26"
                  size={28}
                />
              </View>
            </View>
          </TouchableOpacity>

          {isOpen && !props.isLoading && (
            <View
              style={{
                paddingHorizontal: sizes.margin,
                paddingBottom: 24,
                flex: 1,
              }}
            >
              <View style={{ backgroundColor: "#0FD8D8", height: 1 }} />

              {props &&
              props.item &&
              props.item.expert_child_services &&
              props.item.expert_child_services.length ? (
                props.item.expert_child_services.map((item, index) => (
                  <Price
                    key={`item--${item && item.id ? item.id : index}`}
                    item={item}
                    navigation={props.navigation}
                  />
                ))
              ) : (
                <View
                  style={{
                    flex: 1,
                    minHeight: 75,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Text style={{ textAlign: "center" }}>Список пуст</Text>
                </View>
              )}
            </View>
          )}
        </LinearGradient>
      ) : null}
    </>
  );
};

const styles = StyleSheet.create({
  container_line: {
    width: "100%",
    height: 64,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: sizes.margin,
    alignItems: "center",
  },
});
