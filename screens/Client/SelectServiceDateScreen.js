import React, { Component, useEffect } from "react";
import moment from "moment";
import {
  ActivityIndicator,
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
} from "react-native";
import { Calendar, LocaleConfig } from "react-native-calendars";
import { Icon } from "react-native-elements";
import { LinearGradient } from "expo-linear-gradient";

import { ToastView } from "../../components";
import Header from "../../components/Headers/Header";
import { Background } from "../../components/Background";
import {
  fontSize16,
  fontSize28,
  WINDOW_HEIGHT,
  fonts,
  colors,
} from "../../constants/theme";
import CheckBoxInput from "../../components/checkboxinput/CheckBoxInput";
import { TimetableAPI } from "../../api/timetable";
import { connect } from "react-redux";
import { getStaffScheduleByToday } from "../../actions/StaffActions";

LocaleConfig.locales["ru"] = {
  monthNames: [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь",
  ],
  monthNamesShort: [
    "Янв.",
    "Фев.",
    "Мрт.",
    "Апр.",
    "Май",
    "Ин.",
    "Ил.",
    "Авг.",
    "Снт.",
    "Окт.",
    "Нб.",
    "Дек.",
  ],
  dayNames: [
    "Понедельник",
    "Вторник",
    "Среда",
    "Четверг",
    "Пятница",
    "Суббота",
    "Воскресенье ",
  ],
  dayNamesShort: ["ВС", "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ"],
};

LocaleConfig.defaultLocale = "ru";

class SelectServiceDateScreen extends Component {
  constructor(props) {
    super(props);
    this.dateCurrent = new Date();

    const staffSlots =
      this.props.schedule.is_working &&
      this.props.schedule &&
      this.props.schedule.slots &&
      this.props.schedule.slots[0];

    const startWorkStaff =
      (staffSlots && this.props.schedule.slots[0].from) || "";
    const endWorkStaff = (staffSlots && this.props.schedule.slots[0].to) || "";

    const today = `${this.dateCurrent.getFullYear()}-${(
      "0" +
      (this.dateCurrent.getMonth() + 1)
    ).slice(-2)}-${("0" + this.dateCurrent.getDate()).slice(-2)}`;

    console.log("staffSlots", this.props.schedule);

    this.state = {
      isOnlineService:
        this.route && this.route.params && this.route.params.isOnlineService,
      dateSelected: { [today]: { selected: true } },
      dateSelectedString: today,
      staffId: props.route.params.staff.yid,
      trainerId: props.route.params.staff.id,
      seanceLength: /*props.route.params.service.staff[0].seance_length*/ 900,
      companyId: props.route.params.companyId,
      byExpert: props.route.params.byExpert,
      freeTimes: [],
      selectedDate: false,
      activeRadio: "",
      anyFree: false,
      scheduleLoading: props.scheduleLoading,
      startWorkStaffMinutes: startWorkStaff.trim()
        ? moment.duration(startWorkStaff).asMinutes()
        : false,
      endWorkStaffMinutes: startWorkStaff.trim()
        ? moment.duration(endWorkStaff).asSeconds()
        : false,
    };
  }

  handleRadioPress = (time) => {
    this.setState({ activeRadio: time, selectedDate: true });
  };

  checkSelectedDay = async (day = "") => {
    const { dateSelectedString } = this.state;
    const { getStaffScheduleByToday } = this.props;

    if (day) {
      const currentDate = moment(day).format("YYYY-MM-DD");

      getStaffScheduleByToday(
        this.state.trainerId,
        this.state.staffId,
        this.state.companyId || 57919,
        currentDate,
        currentDate,
        this.state.byExpert
      );

      await this.getTimetable(dateSelectedString);
    }
  };

  async componentDidMount() {
    const { dateSelectedString } = this.state;
    const { getStaffScheduleByToday } = this.props;

    // console.log(
    //   "Received Staff Props: ",
    //   this.props.route.params.staff,
    //   ", all",
    //   this.props.route.params,
    //   "COMPANY IDDDD2",
    //   this.state.companyId
    // );

    this.checkSelectedDay(new Date());
  }

  async getTimetable(date) {
    const { companyId, staffId } = this.state;

    if (staffId && date) {
      try {
        this.setState({ scheduleLoading: true, selectedDate: false });

        const freeTimes = await TimetableAPI.getTimetableByCompanyIdAndStaffId(
          this.state.companyId || 57919,
          this.state.staffId,
          date
        );
        let anyFree = [...freeTimes].filter((time) => time.is_free);

        console.log("freeTimes", freeTimes);
        console.log("AAANYYYY1: ", anyFree);

        this.setState({
          freeTimes,
          anyFree: (anyFree && anyFree.length) || anyFree > -1,
        });

        // this.checkAnyFreeTime();

        this.setState({ scheduleLoading: false });
      } catch (e) {
        console.log(
          "Error TimetableAPI.getTimetableByCompanyIdAndStaffId --- ",
          e
        );

        this.setState({ scheduleLoading: false });
      }
    }
  }

  onSubmitPress = () => {
    if (this.state.selectedDate) {
      this.props.navigation.navigate("SubmitService", {
        ...this.props.route.params,
        staffId: this.state.staffId,
        submitDate: this.state.dateSelectedString,
        submitTime: this.state.activeRadio,
        staffId: this.staffId,
        seanceLength: this.state.seanceLength,
        isOnlineService: this.state.isOnlineService,
        trainType: this.props.route.params.trainType,
      });
    } else ToastView("Выберите время");
  };

  // checkAnyFreeTime = () => {
  //   let anyFree = false;
  //
  //   if (this.state.freeTimes && this.state.freeTimes.length)
  //     for (const item of this.state.freeTimes) if (item.is_free) anyFree = true;
  //
  //   this.setState({ anyFree: anyFree > -1 });
  // };

  checkApproveMomentWork(time = false) {
    const { dateSelectedString } = this.state;
    const dateUnixNow = moment().add(3, "hours").unix();

    console.log("time && this.state.anyFree", time, this.state.anyFree);

    if (time && this.state.anyFree && time.is_free) {
      const dateUnixSelectFromCalendar = moment(
        dateSelectedString + " " + time.time
      ).unix();
      const minutes = moment.duration(time.time).asMinutes();

      const hour =
        time.time && time.time.includes(":") ? +time.time.split(":")[0] : false;
      const currentMinutes =
        time && time.time ? +time.time.split(":")[1] : false;

      console.log(
        "RESULT",
        time.is_free,
        "minutes",
        minutes,
        "currentMinutes",
        currentMinutes,
        hour,
        hour >= 9,
        dateUnixSelectFromCalendar >= dateUnixNow,
        this.state.startWorkStaffMinutes,
        this.state.startWorkStaffMinutes <= minutes,
        this.state.endWorkStaffMinutes >= minutes + this.state.seanceLength,

        "this.state.endWorkStaffMinutes",
        this.state.endWorkStaffMinutes,
        "minutes + this.state.seanceLength",
        minutes + this.state.seanceLength
      );

      return (
        time.is_free &&
        hour &&
        hour >= 9 &&
        dateUnixSelectFromCalendar >= dateUnixNow &&
        this.state.startWorkStaffMinutes &&
        !currentMinutes &&
        this.state.startWorkStaffMinutes <= minutes &&
        this.state.endWorkStaffMinutes >= minutes + this.state.seanceLength
      );
    }

    return false;
  }

  getCorrectTimeList = (tiffany) => {
    const checkBoxes = [];
    const listTimes = [];

    console.log(
      "STATE FREE TIMES",
      this.state.freeTimes,
      "ANNYYY",
      this.state.anyFree
    );

    if (this.state.freeTimes && this.state.freeTimes.length) {
      this.state.freeTimes.map((time) => {
        if (this.checkApproveMomentWork(time)) {
          console.log("timeeeee", time);

          const freeTime = time.time.includes(":")
            ? time.time.split(":")[0] + ":" + time.time.split(":")[1]
            : titme.time;

          if (!listTimes.find((time) => time === freeTime)) {
            checkBoxes.push(
              <View key={freeTime}>
                <CheckBoxInput
                  tiffany={tiffany}
                  title={freeTime}
                  style={styles.checkBoxInputText}
                  checked={this.state.activeRadio === freeTime}
                  onPress={() => this.handleRadioPress(freeTime)}
                />

                <View style={styles.checkBoxInputTextBottomLine} />
              </View>
            );

            listTimes.push(freeTime);
          }
        }
      });
    }

    if (checkBoxes.length) {
      return checkBoxes;
    } else
      return (
        <Text style={fonts.normal} color={{ color: "#000" }}>
          В данный день нет свободного времени
        </Text>
      );
  };

  render() {
    const tiffany = this.props.route.params.tiffany;

    const { scheduleError = false } = this.props;

    return (
      <View style={{ flex: 1 }}>
        <SafeAreaView>
          <Header
            title={"Дата и время"}
            tick={true}
            tiffany={tiffany}
            handleclick={this.onSubmitPress}
            disabled={!this.state.selectedDate}
          />
        </SafeAreaView>

        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            <LinearGradient
              useAngle
              angle={267}
              start={[0.8, 0.8]}
              end={[0, 0]}
              colors={["rgba(240, 240, 240, 1);", "#DADADA"]}
              style={styles.calendarGradient}
            >
              <Calendar
                style={styles.calendar}
                markedDates={this.state.dateSelected}
                renderArrow={(direction) => customIcons(direction)}
                theme={{
                  ...calendarTheme,
                  selectedDayBackgroundColor: tiffany
                    ? "rgba(15, 216, 216, 0.12)"
                    : "rgba(244, 111, 34, 0.12)",
                  selectedDayTextColor: tiffany ? "#0FD8D8" : "#F46F22",
                  todayTextColor: tiffany ? "#0FD8D8" : "#F46F22",
                }}
                onDayPress={async (day) => {
                  console.log("DAY PRESS - ", day);

                  await this.checkSelectedDay(day.dateString);

                  this.setState({
                    dateSelected: {
                      [day.dateString]: {
                        selected: true,
                      },
                    },
                    dateSelectedString: day.dateString,
                    activeRadio: "",
                  });
                }}
                monthFormat={"MMMM yyyy"}
                hideExtraDays={true}
                firstDay={1}
              />
            </LinearGradient>

            <View style={{ marginLeft: 16 }}>
              <Text
                style={[
                  fonts.h2,
                  { color: colors.title, marginTop: 16, fontWeight: "normal" },
                ]}
              >
                Выберите время
              </Text>

              <View style={[styles.checkBoxInput, { marginTop: 35 }]}>
                {scheduleError ? (
                  <Text style={{ color: "#000" }}>Ошибка получения данных</Text>
                ) : this.state.scheduleLoading ? (
                  <View style={styles.fullFill}>
                    <ActivityIndicator color={colors.mainOrange} />
                  </View>
                ) : !this.state.scheduleLoading &&
                  this.state.freeTimes.length === 0 &&
                  !this.state.anyFree ? (
                  <Text style={fonts.normal} color={{ color: "#000" }}>
                    В данный день нет свободного времени
                  </Text>
                ) : (
                  <View>{this.getCorrectTimeList(tiffany)}</View>
                )}
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  scheduleLoading: state.StaffReducer.scheduleLoading,
  schedule: state.StaffReducer.schedule,
  scheduleError: state.StaffReducer.scheduleError,
});

const mapDispatchToProps = {
  getStaffScheduleByToday,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectServiceDateScreen);

const styles = StyleSheet.create({
  container: { flex: 1, marginBottom: 64, paddingLeft: 10, paddingRight: 10 },
  calendarGradient: {
    marginVertical: 24,
    width: "100%",
    borderRadius: 10,
    alignItems: "center",
  },
  calendar: {
    width: Dimensions.get("screen").width / 1.1,
    borderRadius: 10,
    backgroundColor: "transparent",
  },
  header: {
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0, 0, 0, 0.12)",
  },
  gradient: {
    marginTop: 8,
    paddingVertical: 18,
    paddingHorizontal: 8,
    borderRadius: 10,
    justifyContent: "space-between",
    flexDirection: "row",
  },
  listDish: {
    marginBottom: WINDOW_HEIGHT / 11.5,
    backgroundColor: "#FAFAFA",
    width: "100%",
    paddingHorizontal: 16,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  containerListFood: {
    backgroundColor: "#FAFAFA",
    width: "100%",
    flex: 1,
    borderRadius: 16,
  },
  checkBoxInput: {
    marginRight: 2,
  },
  checkBoxInputText: {
    marginLeft: 34,
  },
  checkBoxInputTextBottomLine: {
    width: "100%",
    marginLeft: 60,
    height: 1,
    marginTop: 18,
    marginBottom: 18,
    opacity: 0.12,
    backgroundColor: colors.swiper_inactive,
  },
  fullFill: {
    flex: 1,
    minHeight: Dimensions.get("window").height / 4,
    alignItems: "center",
    justifyContent: "center",
  },
});

const calendarTheme = {
  calendarBackground: "transparent",
  textDayFontFamily: fonts.normal.fontFamily,
  textMonthFontFamily: fonts.bold.fontFamily,
  textDayHeaderFontFamily: fonts.bold.fontFamily,
  dayTextColor: "#151C26",
  textSectionTitleColor: "#151C26",
  monthTextColor: "#151C26",
  textDayFontSize: fontSize16,
  "stylesheet.day.basic": {
    text: {
      marginTop: 6,
    },
  },
};

const customIcons = (direction) => {
  {
    if (direction === "right") {
      return (
        <Icon
          name="chevron-small-right"
          size={fontSize28}
          color="rgba(21, 28, 38, 0.5)"
          type="entypo"
        />
      );
    } else {
      return (
        <Icon
          name="chevron-small-left"
          size={fontSize28}
          color="rgba(21, 28, 38, 0.5)"
          type="entypo"
        />
      );
    }
  }
};
