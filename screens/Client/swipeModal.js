import React from "react";
import {
  Text,
  View,
  Dimensions,
  ActivityIndicator,
  StyleSheet,
} from "react-native";
import { ListSpecialistItem } from "../../components/ListSpecialistItem";

import { colors, fonts, sizes } from "../../constants/theme";
import PropTypes from "prop-types";
import SwipeBottomSheet from "../../components/SwipeBottomSheet";

const { height } = Dimensions.get("window");

class BottomSheet extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log("SPECIALISDT COMPANY ID", this.props.companyId);
  }

  render() {
    const service = this.props.service;
    const { byExpert = false } = this.props;

    return (
      <SwipeBottomSheet
        title={"Выбрать специалиста"}
        swipeModalRef={this.props.swipeModalRef}
        heightSheet={height / 1.5}
      >
        {this.props.loading ? (
          <View style={styles.fullFill}>
            <ActivityIndicator color={colors.mainOrange} />
          </View>
        ) : this.props.staffs &&
          this.props.staffs.length &&
          !this.props.requestFailed ? (
          this.props.staffs.map((staff) => {
            const btPress = () => {
              this.props.navigation.navigate("ItemSpecialist", {
                staff,
                service,
                byExpert,
                trainType: this.props.trainType,
                companyId: this.props.companyId,
                tiffany: this.props.tiffany,
                serviceTitle: this.props.serviceTitle,
                fromService: this.props.fromService,
                fromOnlineService: this.props.isOnlineService,
              });
            };

            return (
              <View key={`specialist-${staff.id}`}>
                <ListSpecialistItem
                  key={`item-${staff.id}`}
                  name={staff.fullname}
                  profile={staff.specialization}
                  iconImg={staff.avatar}
                  tiffany={this.props.tiffany}
                  btPress={btPress}
                />
              </View>
            );
          })
        ) : !this.props.requestFailed &&
          !this.props.loading &&
          !this.props.staffs.length ? (
          <View style={styles.fullFill}>
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Список пуст
            </Text>
          </View>
        ) : (
          <View style={styles.fullFill}>
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Ошибка получения данных
            </Text>
          </View>
        )}
      </SwipeBottomSheet>
    );
  }
}

BottomSheet.propTypes = {
  swipeModalRef: PropTypes.object,
};

export default BottomSheet;

const styles = StyleSheet.create({
  fullFill: {
    flex: 1,
    minHeight: Dimensions.get("window").height / 1.9,
    alignItems: "center",
    justifyContent: "center",
  },
  gradient: {
    marginBottom: 8,
    borderRadius: 10,
    marginHorizontal: 16,
  },
});
