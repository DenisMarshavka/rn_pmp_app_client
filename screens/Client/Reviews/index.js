import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  Dimensions,
} from "react-native";
import { connect } from "react-redux";
import moment from "moment";

import { Background } from "../../../components/Background";
import Header from "../../../components/Headers/Header";
import { sizes, fonts, colors } from "../../../constants/theme";
import ReviewsItem from "./ReviewsItem";
import { getAllReviews } from "../../../actions/ReviewsActions";
import PlusSvgIcon from "../../../components/Svg/PlusSvg";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader";
import { getSizeSkeletonLoaders } from "../../../utils";

let sortedReviewsData = {};

const sortByPeriod = (
  sortedData = {},
  momentNormalFormat = "",
  momentData = "",
  reviewData = false
) => {
  const sortedReviews = { ...sortedData };
  const newData = { ...reviewData };

  if (momentData.trim() && reviewData && momentNormalFormat.trim()) {
    const chunksDate = momentNormalFormat.includes("-")
      ? momentNormalFormat.split("-")
      : [];
    const createdReviewDateDay =
      chunksDate.length && chunksDate[2].includes("T")
        ? chunksDate[2].trim("T")
        : [];

    if (reviewData && createdReviewDateDay)
      newData.day = createdReviewDateDay[1] || null;

    if (sortedReviews[momentData] && sortedReviews[momentData].data) {
      let approveToPush = true;

      for (let item of sortedReviews[momentData].data) {
        if (item && item.date) {
          if (item.date === newData.date) approveToPush = false;
        } else approveToPush = false;
      }

      if (approveToPush) sortedReviews[momentData].data.push(newData);
    } else
      sortedReviews[momentData] = {
        data: [newData],
      };
  }

  return sortedReviews;
};

const ReviewsScreen = ({
  navigation,
  getAllReviews = () => {},
  loading = false,
  reviewsData = [],
  countTrainersReviewsItems = 0,
  requestFailed = false,
}) => {
  const [reviewsList, setReviewsList] = useState([]);
  const [reviewsListFinish, setReviewsListFinish] = useState([]);
  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(100);
  const [finishedLoader, setFinishedLoader] = useState(false);

  useEffect(() => {
    getAllReviews(limit, offset);

    return () => {
      setFinishedLoader(false);
    };
  }, []);

  useEffect(() => {
    setReviewsListFinish([]);

    if (!loading && reviewsData) {
      console.log("Current reviews gor the filtering: ", reviewsData);

      for (let review of reviewsData) {
        console.log("Review item", review);

        if (review) {
          sortedReviewsData = sortByPeriod(
            sortedReviewsData,
            review.date,
            moment(review.date).format("MMMM YYYY"),
            review
          );
        }
      }
    } else sortedReviewsData = [];

    console.log(
      "reviewsData",
      reviewsData,
      "sortedReviewsData",
      sortedReviewsData
    );

    setReviewsList(sortedReviewsData);
  }, [loading, reviewsData]);

  useEffect(() => {
    setReviewsListFinish(
      loading
        ? getSizeSkeletonLoaders(100, 550, false, false, {
            [`period-${Date.now() + 12}`]: {
              data: [
                {
                  id: 1,
                  trainer: { fullname: "...." },
                  text: "...",
                  trainer_id: 1,
                },
                {
                  id: 2,
                  trainer: { fullname: "...." },
                  text: "...",
                  trainer_id: 1,
                },
                {
                  id: 3,
                  trainer: { fullname: "...." },
                  text: "...",
                  trainer_id: 1,
                },
                {
                  id: 4,
                  trainer: { fullname: "...." },
                  text: "...",
                  trainer_id: 1,
                },
              ],
            },
          })[0]
        : !loading && reviewsList
        ? reviewsList
        : []
    );

    if (!loading) setFinishedLoader(true);
  }, [reviewsList, loading]);

  return (
    <Background>
      <Header
        title="Мои отзывы"
        backTitle
        tick={true}
        isSvgTickIcon={true}
        SvgIcon={<PlusSvgIcon color={colors.orange} />}
        handleclick={() => navigation.navigate("CreateReview")}
      />

      {!requestFailed &&
      reviewsListFinish &&
      Object.keys(reviewsListFinish).length ? (
        <ScrollView showsVerticalScrollIndicator={false}>
          {Object.keys(reviewsListFinish).map((item, i) => {
            const currentPeriod = item;

            if (
              item &&
              reviewsListFinish[item] &&
              reviewsListFinish[item].data &&
              reviewsListFinish[item].data.length
            ) {
              return (
                <React.Fragment key={`review-${item && item.id ? item.id : i}`}>
                  <View style={{ flex: 1, paddingBottom: 20 }}>
                    <SkeletonLoader
                      isLoading={reviewsListFinish.isTest}
                      containerStyle={styles.dateTitle}
                      layout={[{ flex: 1, height: 25, width: 150 }]}
                    >
                      <Text style={styles.dateTitleStyle}>
                        {item[0].toUpperCase() + item.slice(1, item.length)}
                      </Text>
                    </SkeletonLoader>

                    {reviewsListFinish[item].data.map((item, i) => (
                      <ReviewsItem
                        isLoading={reviewsListFinish.isTest}
                        onReviewRemove={() => setFinishedLoader(false)}
                        item={item}
                        key={`item-${item && item.id ? item.id : i}`}
                        {...item}
                      />
                    ))}
                  </View>
                </React.Fragment>
              );
            }
          })}
        </ScrollView>
      ) : !loading &&
        !requestFailed &&
        reviewsListFinish &&
        !reviewsListFinish.length &&
        finishedLoader ? (
        <View style={styles.fullFill}>
          <Text
            style={{
              width: "100%",
              textAlign: "center",
              color: "#000",
            }}
          >
            Список пуст
          </Text>
        </View>
      ) : !loading && requestFailed && finishedLoader ? (
        <View style={styles.fullFill}>
          <Text
            style={{
              width: "100%",
              textAlign: "center",
              color: "#000",
            }}
          >
            Ошибка получения данных
          </Text>
        </View>
      ) : null}
    </Background>
  );
};

const mapStateToProps = (state) => ({
  loading: state.ReviewsReducer.reviewsLoading,
  reviewsData: state.ReviewsReducer.reviews,
  countTrainersReviewsItems: state.ReviewsReducer.countTrainersReviewsItems,
  requestFailed: state.ReviewsReducer.reviewsError,
});

const mapDispatchToProps = {
  getAllReviews,
};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewsScreen);

const styles = StyleSheet.create({
  fullFill: {
    flex: 1,
    minHeight: Dimensions.get("screen").height / 2.4,
    alignItems: "center",
    justifyContent: "center",
  },
  headerWrapperStyle: {
    borderBottomColor: "rgba(21, 28, 38, 0.12)",
    borderBottomWidth: 1,
  },
  dateTitle: { margin: sizes.margin, marginTop: 25, marginBottom: 20 },
  dateTitleStyle: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: sizes.headerName,
  },
});
