import React, { useState, useRef, useEffect } from "react";
import {
  View,
  Text,
  Platform,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Dimensions,
  ActivityIndicator,
} from "react-native";
import { connect } from "react-redux";
import moment from "moment";

import { Background } from "../../../components/Background";
import { Header } from "../../../components";
import { fonts, colors } from "../../../constants/theme";
import { DropDown } from "../Profile/addDish/components/DropDown";
import { getAllStaff } from "../../../actions/StaffActions";
import { showAlert } from "../../../utils";
import { createReview } from "../../../actions/ReviewsActions";

const CreateReview = ({
  getAllStaff = () => {},
  staffLoading = false,
  staff = [],
  staffError = false,
  createReviewLoading = false,
  createReviewError = false,
  createReview = () => {},
}) => {
  const [trainer, setTrainer] = useState("");
  const [trainers, setTrainers] = useState(staff);
  const [reviewText, setReviewText] = useState("");
  const [trainerId, setTrainerId] = useState(null);
  const [createdReview, setCreatedReview] = useState(false);

  const refTextInput = useRef(null);

  useEffect(() => {
    if (!createReviewLoading && createdReview) {
      showAlert(
        !createReviewError ? "Готово" : "Ошибка",
        !createReviewError
          ? `Ваш отзыв был \n успешно создан`
          : "Извините, случилась непредвиденная ишибка,\n пожалуйта Обратитесь в техподдержку или попробуйте снова",
        [
          {
            text: !createReviewError ? "Oк" : "Попробовать ещё раз",
            onPress: () => {
              if (!createReviewError) {
                Keyboard.dismiss();

                setTrainer("");
                setReviewText("");
              } else
                refTextInput && refTextInput.current
                  ? refTextInput.current.focus()
                  : null;

              setCreatedReview(false);
            },
            style: !createReviewError ? "success" : "error",
          },
        ]
      );
    }
  }, [createReviewLoading, createReviewError, createdReview]);

  const onSubmit = () => {
    const reviewError = !reviewText.trim() || reviewText.trim().length < 15;
    const trainerError = !trainer.trim();

    console.log("trainerId", trainerId);

    if (reviewError || trainerError || isNaN(+trainerId)) {
      showAlert(
        "Ошибка",
        trainerError
          ? "Тренер не выбран"
          : reviewError
          ? `Зполните пожалйуста поле\n с отзывом (Минимум 15 символов, сейчас - ${
              reviewText.trim().length
            })`
          : "Неизвестная ошибка",
        [
          {
            title: "Ок",
            onPress: () => {
              if (
                !trainerError &&
                reviewError &&
                refTextInput &&
                refTextInput.current
              ) {
                refTextInput.current.focus();
              }

              setCreatedReview(false);
            },
            style: "error",
          },
        ]
      );
    } else {
      const localTime = moment().format("YYYY-MM-DD");
      const currentMoment = new Date();
      const currentHours = currentMoment.getHours().toString();
      const currentMinutes = currentMoment.getMinutes().toString();

      const currentDate =
        localTime +
        `T${currentHours.length > 1 ? currentHours : "0" + currentHours}:${
          currentMinutes.length > 1 ? currentMinutes : "0" + currentMinutes
        }:00.000Z`;

      createReview(reviewText.trim(), currentDate, +trainerId);
      setCreatedReview(true);
    }
  };

  const onDropDownClosed = () =>
    setTrainers(
      staff.filter((trainerItem) => trainerItem.fullname !== trainer)
    );

  useEffect(() => {
    getAllStaff();
  }, []);

  useEffect(() => {
    if (!staffLoading && staff.length && !staffError) {
      const trainersList = staff.map((item, i) => ({
        key: `staff-${item && item.fullname ? item.fullname : i}`,
        fullname: item.fullname,
        id: item.id,
      }));

      setTrainers(trainersList);
    }
  }, [staff, staffLoading, staffError]);

  return (
    <Background>
      <SafeAreaView>
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "position" : "height"}
            style={{ paddingTop: 10 }}
          >
            <Header
              title={"Добавить отзыв"}
              isSubmit={true}
              submitPress={() => (!createReviewLoading ? onSubmit() : null)}
              disabledBtn={createReviewLoading}
            />

            <View style={{ paddingHorizontal: 16 }}>
              <Text style={[styles.title, fonts.text, { marginTop: 25 }]}>
                Выберите специалиста
              </Text>

              <View style={{ paddingHorizontal: 5 }}>
                {staffLoading ? (
                  <View
                    style={{
                      flex: 1,
                      alignItems: "flex-start",
                      justifyContent: "center",
                      minHeight: 47,
                    }}
                  >
                    <ActivityIndicator color={colors.mainOrange} />
                  </View>
                ) : !staffError && !staffLoading ? (
                  <DropDown
                    parentCallback={onDropDownClosed}
                    title="Специалист"
                    heightTitle={20}
                    value={trainer}
                    boldText={false}
                    isLoading={staffLoading}
                  >
                    {trainers.map((trainerItem, i) => {
                      if (trainerItem.fullname) {
                        return (
                          <TouchableOpacity
                            style={{ marginTop: 5 }}
                            onPress={() => {
                              !staffLoading
                                ? setTrainer(trainerItem.fullname)
                                : null;

                              console.log(
                                "TRAINER ITEM",
                                trainerItem,
                                "trainers",
                                trainers
                              );

                              setTrainerId(
                                !staffLoading &&
                                  trainerItem &&
                                  trainerItem.id !== false
                                  ? trainerItem.id
                                  : null
                              );
                            }}
                            activeOpacity={staffLoading ? 1 : 0.8}
                            key={`trainer-${
                              trainerItem && trainerItem.id ? trainerItem.id : i
                            }`}
                          >
                            <Text
                              numberOfLines={1}
                              style={{
                                width: "100%",
                                fontWeight: "normal",
                                fontSize: 18,
                                color: "#1E2022",
                              }}
                            >
                              {trainerItem.fullname}
                            </Text>
                          </TouchableOpacity>
                        );
                      }
                    })}
                  </DropDown>
                ) : (
                  <View
                    style={{
                      flex: 1,
                      marginTop: 10,
                      alignItems: "flex-start",
                      justifyContent: "center",
                    }}
                  >
                    <Text
                      style={{
                        width: "100%",
                        textAlign: "left",
                        color: "#000",
                      }}
                    >
                      Ошибка получения данных
                    </Text>
                  </View>
                )}

                <View style={{ marginTop: 35 }}>
                  <Text style={[styles.title, fonts.text]}>Ваш отзыв</Text>

                  <TextInput
                    style={{
                      borderColor: colors.orange,
                      borderWidth: 1,
                      borderRadius: 10,
                      padding: 10,
                      height: Dimensions.get("screen").height / 3.6,
                    }}
                    onEndEditing={() => Keyboard.dismiss()}
                    onChangeText={setReviewText}
                    value={reviewText}
                    ref={refTextInput}
                    multiline
                    numberOfLines={20}
                    onBlur={() => Keyboard.dismiss()}
                    placeholder="Ваше мнение о выбранном специалисте"
                    placeholderTextColor={"#858585"}
                    textAlignVertical="top"
                  />
                </View>
              </View>
            </View>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    </Background>
  );
};

const mapStateToProps = (state) => ({
  staffLoading: state.StaffReducer.staffLoading,
  staff: state.StaffReducer.staff,
  staffError: state.StaffReducer.staffError,

  createReviewLoading: state.ReviewsReducer.createReviewLoading,
  createReviewError: state.ReviewsReducer.createReviewError,
});

const mapDispatchToProps = {
  getAllStaff,
  createReview,
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateReview);

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    color: "#1E2022",
    marginBottom: 18,
  },
});
