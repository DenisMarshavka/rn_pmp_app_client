import React from "react";
import {
  Animated,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from "react-native";
import Swipeable from "react-native-swipeable";
import { AntDesign } from "@expo/vector-icons";

// import { RectButton } from 'react-native-gesture-handler'
import { colors, fonts, sizes } from "../../../constants/theme";
import { NoAvatarTrainerSvg } from "../../../assets/icons/NoAvatarTrainerSvg/NoAvatarTrainerSvg";
import { validURL } from "../../../utils";
import { connect } from "react-redux";
import { deleteReview } from "../../../actions/ReviewsActions";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader";

class ReviewsItem extends React.Component {
  constructor(props) {
    super(props);

    this._swipeableRow = React.createRef();
  }

  state = {
    isSwiping: false,
  };

  close = () => {
    if (this._swipeableRow) this._swipeableRow.recenter();
    this.props.onReviewRemove();
    // this.props.onReviewRemove()
  };

  pressHandler = (id) => {
    this.close();

    if (id) this.props.deleteReview(id);
  };

  render() {
    const {
      text = "",
      trainer_avatar = "",
      trainer_fullname = "",
      trainer_id = null,
      id = null,
    } = this.props;

    if (text.trim() && trainer_fullname && trainer_id)
      return (
        <Swipeable
          onRef={(ref) => (this._swipeableRow = ref)}
          onSwipeStart={() => this.setState({ isSwiping: true })}
          onSwipeRelease={() => this.setState({ isSwiping: false })}
          rightActionActivationDistance={300}
          rightButtonWidth={130}
          onRightActionRelease={() => this.pressHandler(id)}
          rightButtons={[
            <TouchableOpacity
              activeOpacity={0.6}
              style={{
                height: "100%",
                paddingRight: 15,
                justifyContent: "center",
                backgroundColor: "#E02323",
              }}
              onPress={() => this.pressHandler(id)}
            >
              <AntDesign
                style={{ marginLeft: "25%" }}
                name="delete"
                size={18}
                color="white"
              />
            </TouchableOpacity>,
          ]}
        >
          <View style={styles.chatItemBody}>
            <SkeletonLoader
              isLoading={this.props.isLoading}
              layout={[{ width: 40, height: 40, borderRadius: 40 }]}
            >
              <View style={styles.chatIcon}>
                {!trainer_avatar || !validURL(trainer_avatar) ? (
                  <NoAvatarTrainerSvg />
                ) : (
                  <Image
                    source={{ uri: trainer_avatar }}
                    style={styles.chatIconDetail}
                  />
                )}
              </View>
            </SkeletonLoader>

            <SkeletonLoader
              isLoading={this.props.isLoading}
              containerStyle={{ ...styles.chatRight, width: "85%" }}
              layout={[
                { flex: 1, height: 20, width: 90, marginBottom: 15 },
                { flex: 1, height: 60, width: "100%" },
              ]}
            >
              <Text style={styles.userNameStyle}>{trainer_fullname}</Text>

              <Text style={styles.userTextStyle}>{text}</Text>
            </SkeletonLoader>
          </View>
        </Swipeable>
      );

    return false;
  }
}

const mapDispatchToProps = {
  deleteReview,
};

export default connect(null, mapDispatchToProps)(ReviewsItem);

const styles = StyleSheet.create({
  // leftAction: {
  //   flex: 1,
  //   justifyContent: 'center',
  // },
  // rightAction: {
  //   alignItems: 'flex-end',
  //   flex: 1,
  //   justifyContent: 'center',
  // },
  chatItemBody: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignContent: "center",
    paddingTop: 15,
    paddingBottom: 29,
    paddingRight: 50,
    marginLeft: 16,
  },
  chatRight: {
    marginLeft: 16,
  },
  actionText: {
    color: "white",
    fontSize: 16,
    backgroundColor: "transparent",
    padding: 10,
  },
  chatIconDetail: {
    width: 40,
    height: 40,
    borderRadius: 40,
  },
  userTextStyle: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: sizes.font,
    color: "#A6A9AD",
    marginTop: 13,
  },
  userNameStyle: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: sizes.headerName,
    color: colors.orange,
  },
});
