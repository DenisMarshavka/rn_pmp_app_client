import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Dimensions,
  SafeAreaView,
} from "react-native";
import { connect } from "react-redux";

import { Background } from "../../components/Background";
import { ListItem } from "../../components/ListItem";
import { colors, fonts, sizes } from "../../constants/theme";
import BackHeader from "../../components/Headers/BackHeader";
import {
  getServicesByTypeName,
  getServicesDataReset,
  getServicesTypes,
} from "../../actions/ServicesActions";
import { getSizeSkeletonLoaders, formattingPrice } from "../../utils";
import { Tabs } from "../../components/Tabs/Tabs";
import Header from "../../components/Headers/Header";

const ListServiceScreen = (props) => {
  const {
    navigation,
    route = {},
    getServicesDataReset = () => {},
    serviceTypesLoading = false,
    serviceTypesError = false,
    servicesTypes = [],
    getServicesTypes = () => {},
    getServicesByTypeName = () => {},
    serviceLoading = true,
    services = [],
    serviceError = false,
  } = props;

  const typeName =
    route && route.params && route.params.servicesTypeName
      ? route.params.servicesTypeName
      : "Pilates";

  const selectedTypeName = typeName.includes("Online")
    ? typeName.replace(" Online", "")
    : typeName;

  const [tabTypeNameState, setTabTypeNameState] = useState(selectedTypeName);
  const { isOnline = false, isPay = false, isAll = false, tiffany = false } =
    (route && route.params) || {};

  console.log("isAll", isAll, "isPay", isPay);

  const [tabActive, setTabActive] = useState(0);
  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(15);

  console.log("SERVICES", services);

  const openStudio = (serviceData) => {
    // const studio = serviceData.studio;
    console.log("serviceDataserviceData", serviceData);

    navigation.navigate(isOnline || isPay ? "ItemService" : "StudioSelection", {
      // studios: studio,
      quantity: (serviceData && serviceData.quantity) || 0,
      service:
        serviceData && serviceData.trainer_service
          ? serviceData.trainer_service
          : serviceData,
      trainType: tabTypeNameState === "Pilates" ? 0 : 1,
      byExpert: tiffany,
      tiffany,
      isOnline,
      isPay,
    });
  };

  useEffect(() => {
    getServicesTypes();

    return () => {
      getServicesDataReset();
    };
  }, []);

  const handleTabClick = (tabName = tabTypeNameState) =>
    setTabTypeNameState(tabName);

  useEffect(() => {
    if (!serviceTypesLoading && tabTypeNameState)
      getServicesByTypeName(
        tabTypeNameState,
        offset,
        limit,
        isOnline,
        !isPay,
        isAll
      );
  }, [tabTypeNameState, serviceTypesLoading, offset, limit]);

  const renderServices = (item = {}, index = Date.now()) => {
    console.log("SER: ", item, "ITEM INDEX: ", index);

    const key =
      item && item.id !== false && item.title && item.price
        ? item.id +
          Date.now() * Math.ceil(Math.random() * 120000) +
          item.title +
          item.price
        : Date.now() * Math.ceil(Math.random() * 120000) + index;

    const firstStepPrice =
      item &&
      item.steps &&
      item.steps.length &&
      item.steps[0] &&
      item.steps[0].price !== false
        ? item.steps[0].price
        : item && item.price
        ? item.price
        : item && item.trainer_service && item.trainer_service.price
        ? item.trainer_service.price
        : null;

    return (
      <TouchableOpacity
        key={`item-${key}`}
        activeOpacity={serviceLoading ? 1 : 0.7}
        onPress={() => (!serviceLoading ? openStudio(item) : null)}
      >
        <ListItem
          index={index}
          title={item.title}
          price={firstStepPrice}
          isLoading={serviceLoading || (item && item.isTest)}
        />
      </TouchableOpacity>
    );
  };

  console.log(
    "FIRST SERVICES: ",
    services[0],
    isAll
      ? services &&
          services.length &&
          services[0] &&
          services[0].trainer_services &&
          services[0].trainer_services.length
      : false
  );

  console.log("ISSS AAAALLLL", isAll, "RESDSSSSSULT", services, "isPay", isPay);

  const getCheckExistingServicesList = () => {
    if (!isAll) {
      return (
        services &&
        services.length &&
        services[0] &&
        services[0].trainer_service &&
        Object.keys(services[0].trainer_service).length
      );
    } else
      return (
        services &&
        services.length &&
        services[0] &&
        services[0].trainer_services &&
        services[0].trainer_services.length
      );
  };

  const getCurrentList = () => {
    if (serviceLoading) {
      return [];
    } else
      return getCheckExistingServicesList() && !isAll
        ? services
        : getCheckExistingServicesList()
        ? services[0].trainer_services
        : [];
  };

  const servicesList = serviceLoading
    ? getSizeSkeletonLoaders(375, 74, false)
    : !serviceLoading && getCheckExistingServicesList()
    ? getCurrentList()
    : [];

  console.log(
    "SERVIIICESSS LIST",
    serviceError,
    servicesList,
    servicesList && servicesList.length,
    "getCheckExistingServicesList()",
    getCheckExistingServicesList()
  );

  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <SafeAreaView>
        <Header
          title={"Выбор услуги"}
          backTitle
          routeName={route.name}
          basketIconStyle={{ top: 0 }}
          basketShow
        />
      </SafeAreaView>

      <Tabs
        listData={servicesTypes}
        getDataByTabActive={handleTabClick}
        onTabActiveIndexSet={setTabActive}
        startActiveTabByName={tabTypeNameState}
        loading={serviceTypesLoading}
        requestFailed={serviceTypesError}
      />

      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 45 }}
        style={{ flex: 1, marginHorizontal: 16, paddingBottom: 25 }}
      >
        {(!serviceError && servicesList && servicesList.length) ||
        serviceLoading ? (
          servicesList.map(renderServices)
        ) : !serviceLoading &&
          !serviceError &&
          servicesList &&
          !servicesList.length ? (
          <View style={styles.fullFill}>
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              {!isPay
                ? `Список пуст, или же Вы уже записывались,\nна всё возможные сервисы`
                : `Список пуст, или же Вы уже приобрели,\nвсё возможные сервисы`}
            </Text>
          </View>
        ) : (
          <View style={styles.fullFill}>
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Ошибка получения данных
            </Text>
          </View>
        )}
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state) => ({
  serviceTypesLoading: state.ServicesReducer.serviceTypesLoading,
  servicesTypes: state.ServicesReducer.serviceTypes,
  serviceTypesError: state.ServicesReducer.serviceTypesError,

  serviceLoading: state.ServicesReducer.serviceLoading,
  services: state.ServicesReducer.services,
  serviceError: state.ServicesReducer.serviceError,
});

const mapDispatchToProps = {
  getServicesDataReset,
  getServicesTypes,
  getServicesByTypeName,
};

export default connect(mapStateToProps, mapDispatchToProps)(ListServiceScreen);

const styles = StyleSheet.create({
  container: { paddingHorizontal: sizes.margin },
  title: { color: colors.title },
  typesLoading: {
    flex: 1,
    height: "100%",
    maxHeight: 35,
    marginLeft: 18,
    alignItems: "flex-start",
    justifyContent: "center",
  },
  fullFill: {
    flex: 1,
    height: Dimensions.get("window").height / 1.7,
    alignItems: "center",
    justifyContent: "center",
  },
});
