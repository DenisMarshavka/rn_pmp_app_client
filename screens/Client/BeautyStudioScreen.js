import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

import { Background } from '../../components/Background';
import { Header } from '../../components/Header';
import { colors, fonts, sizes } from '../../constants/theme';

export const BeautyStudioScreen = ({ navigation }) => {
  return (
    <Background>
      <Header
        title="beauty"
        deg="0"
        navigation={navigation}
        pageName="ListServiceScreen"
      ></Header>
      <View style={styles.container}>
        <Text style={[styles.title, fonts.normal, fonts.h1]}>Выбор студии</Text>
        <Image
          style={styles.img}
          source={require("../../assets/images/beauty-studio.png")}
          resizeMode="cover"
        />
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: { paddingHorizontal: sizes.margin },
  title: { color: colors.title },
  img: { borderRadius: 8, width: "100%", marginTop: 25 }
});
