import React, { useEffect, useState } from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  SafeAreaView,
} from "react-native";
import { connect } from "react-redux";
import * as Animatable from "react-native-animatable";

import { Background } from "../../components/Background";
import BackHeader from "../../components/Headers/BackHeader";
import { colors, fonts } from "../../constants/theme";
import {
  getStudiosByServiceId,
  cleanStudiosByServiceId,
} from "../../actions/StudiosActions";
import SkeletonLoader from "../../components/loaders/SkeletonLoader";
import { BoxShadow } from "react-native-shadow";
import { Card } from "react-native-paper";
// import { getSizeSkeletonLoaders } from "../../utils";
import Header from "../../components/Headers/Header";
import ListWithInfo from "../../components/ListWithInfo";

const shadowOpt = {
  width: Dimensions.get("screen").width - 32,
  height: 246,
  color: "#000",
  border: 8,
  radius: 18,
  opacity: 0.06,
  x: 0,
  y: 0,
  style: {
    marginBottom: 10,
  },
};

const StudioSelection = (props) => {
  const {
    route,
    getStudiosByServiceId = () => {},
    cleanStudiosByServiceId = () => {},
    loading = true,
    studios = [],
    count = 0,
    requestFailed = false,
  } = props;
  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(15);

  const [firstRequestFinished, setFirstRequestFinished] = useState(false);

  const params = route.params;
  const {
    service = {},
    byExpert = false,
    isPay = false,
    trainType = 0,
  } = params;

  console.log("SERVICEservice", service, "isPayyyy", isPay);

  useEffect(() => {
    getStudiosByServiceId(
      service && service.service_id
        ? service.service_id
        : service && service.service && service.service.id !== false
        ? service.service.id
        : service.id !== false
        ? service.id
        : null,
      byExpert,
      offset,
      limit
    );

    return () => {
      setFirstRequestFinished(false);
    };
  }, []);

  useEffect(() => {
    if (!loading) setFirstRequestFinished(true);
  }, [loading]);

  const openService = (id, companyId) => {
    props.navigation.navigate("ItemService", {
      service,
      byExpert,
      trainType,
      tiffany: params.tiffany,
      companyId,
      isPay,
    });
  };

  const renderListItem = (item = {}, index = 0) => {
    const isLoading = loading || (item && item.isTest);
    const Wrap = !isLoading ? Animatable.View : View;

    return (
      <Wrap duration={250} delay={index * 250} animation="fadeInUp">
        <TouchableOpacity
          key={(item.id || Date.now()) + Date.now()}
          style={{
            flex: 1,
            paddingHorizontal: 16,
            borderRadius: 10,
            minHeight: 260,
          }}
          activeOpacity={isLoading ? 1 : 0.6}
          onPress={() => {
            !isLoading ? openService(item.id, item.companyId) : null;
          }}
        >
          <BoxShadow setting={shadowOpt}>
            <View
              style={{
                flex: 1,
                width: "100%",
                position: "relative",
                borderRadius: 10,
              }}
            >
              <SkeletonLoader
                isLoading={isLoading}
                containerStyle={{
                  flex: 1,
                  height: 184,
                  width: "100%",
                  backgroundColor: "grey",
                  borderTopLeftRadius: 10,
                  borderTopRightRadius: 10,
                }}
                layout={[{ flex: 1 }]}
              >
                <Image
                  source={
                    item && item.image
                      ? { uri: item.image }
                      : require("../../assets/images/fitness-studio2.png")
                  }
                  style={styles.img}
                />
              </SkeletonLoader>

              <View style={styles.infoSlide}>
                <SkeletonLoader
                  isLoading={isLoading}
                  containerStyle={styles.new}
                  layout={[
                    { flex: 1, width: 148, height: 11, marginBottom: 10 },
                  ]}
                >
                  <Text style={styles.titleSlide}>
                    {item.name || "Неизвестная студия"}
                  </Text>
                </SkeletonLoader>
              </View>
            </View>
          </BoxShadow>
        </TouchableOpacity>
      </Wrap>
    );
  };

  // const studiosData = loading
  //   ? getSizeSkeletonLoaders(350, 255)
  //   : studios && studios.length
  //   ? studios
  //   : [];
  //
  console.log("STUDDDIOS", studios, "SERVICE", service);

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView>
        <Header
          title={"Выбор студии"}
          backTitle
          routeName={route.name}
          tiffany={byExpert}
          basketIconStyle={{ top: 0 }}
          basketShow
        />
      </SafeAreaView>

      <ListWithInfo
        titleEmpty={"Список студий пуст"}
        mocsDataOffsetTop={350}
        mocsDataListElementSize={255}
        heightSizeFromDimensionsScreenSize={true}
        componentWillUnmount={cleanStudiosByServiceId}
        heightDimensionsContainer={1.7}
        data={studios}
        error={requestFailed}
        loading={loading}
        wrapListParams={{
          style: styles.container,
          showsVerticalScrollIndicator: false,
          contentContainerStyle: { paddingBottom: 55 },
        }}
        elementKeyName={"studio-item-"}
        renderListItem={renderListItem}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  loading: state.StudiosReducer.loading,
  count: state.StudiosReducer.count,
  studios: state.StudiosReducer.studios,
  requestFailed: state.StudiosReducer.error,
});

const mapDispatchToProps = {
  getStudiosByServiceId,
  cleanStudiosByServiceId,
};

export default connect(mapStateToProps, mapDispatchToProps)(StudioSelection);

const styles = StyleSheet.create({
  fullFill: {
    flex: 1,
    minHeight: Dimensions.get("screen").height / 1.6,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    color: colors.title,
    marginVertical: 10,
  },
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
  },
  slideGradient: {
    width: "100%",
    height: 72,
  },
  infoSlide: {
    height: 65,
    backgroundColor: "#EAEAEA",
    width: "100%",
    justifyContent: "center",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    padding: 16,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  titleSlide: {
    color: "#000000",
    ...fonts.normal,
    ...fonts.h3,
  },
  priceSlide: {
    fontSize: 18,
    color: "#F46F22",
    fontWeight: "500",
    textTransform: "uppercase",
    fontFamily: "PFDin500",
  },
  img: {
    width: "100%",
    height: 181,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    resizeMode: "cover",
  },
});
