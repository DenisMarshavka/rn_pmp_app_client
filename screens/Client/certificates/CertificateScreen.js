import React, { useState, useEffect } from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  Dimensions,
} from "react-native";
import { connect } from "react-redux";
import HTMLView from "react-native-htmlview";

import { fontSize24, fonts, colors } from "../../../constants/theme";
import { Background } from "../../../components/Background";
import CheckBoxInput from "../../../components/checkboxinput/CheckBoxInput";
import { CustomBottomButton } from "../../../components";
import Header from "../../../components/Headers/Header";
import { getCertificateById } from "../../../actions/CertificatesAction";
import { addBasketProductById } from "../../../actions/BasketActions";
import { validURL, formattingPrice } from "../../../utils";

const icons = {
  up: require("../../../assets/icons/drop_down_arrow_up.png"),
  down: require("../../../assets/icons/drop_down_arrow_down.png"),
};

const CertificateScreen = ({
  navigation,
  route = {},
  getCertificateById = () => {},
  certificateLoading = false,
  certificate = {},
  certificateFailed = null,

  addBasketProductById = () => {},
}) => {
  const { item = {}, id = null } =
    (route && route.params && route.params.item) || {};
  const {
    certificate_nominales: nominals = [],
    img = "",
    text = "",
    title = "",
  } = certificate;

  console.log(
    "NOMInALS",
    route.params,
    "SERTIFICATE ITEM ",
    certificate,
    "validURL(img)",
    validURL(img)
  );

  const [activeRadio, setActiveRadio] = useState(false);
  const [expandedDropdown, setExpandedDropdown] = useState(false);
  const [selectedNominal, setSelectedNominal] = useState(null);
  const [isExistingNominals, setIsExistingNominals] = useState(false);

  useEffect(() => {
    if (
      certificate &&
      Object.keys(certificate).length &&
      certificate.certificate_nominales &&
      certificate.certificate_nominales.length
    ) {
      if (certificate.certificate_nominales[0]) {
        if (certificate.certificate_nominales[0].price)
          setActiveRadio(certificate.certificate_nominales[0].price);

        if (certificate.certificate_nominales[0].id)
          setSelectedNominal(certificate.certificate_nominales[0].id);
      }

      setIsExistingNominals(true);
    }
  }, [certificate]);

  useEffect(() => {
    getCertificateById(id);
    // console.log("selectedNominal", selectedNominal);

    return () => {
      setActiveRadio(false);
      setSelectedNominal(false);
      setIsExistingNominals(false);
    };
  }, []);

  const handleDropDown = () => {
    setExpandedDropdown(!expandedDropdown);
  };

  return (
    <>
      <Background>
        <Header title={"Сертификаты"} backTitle routeName={route.name} />

        <View style={{ flex: 1, marginBottom: expandedDropdown ? 85 : 25 }}>
          <ScrollView>
            {certificateLoading ? (
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  minHeight: Dimensions.get("screen").height / 1.2,
                  paddingBottom: 23,
                }}
              >
                <ActivityIndicator size={"large"} color={colors.orange} />
              </View>
            ) : !certificateFailed ? (
              <View style={styles.container}>
                <>
                  <Image
                    source={
                      !img || !img.trim() || !validURL(img)
                        ? require("../../../assets/images/notProductImage.png")
                        : { uri: img }
                    }
                    style={styles.bowImage}
                  />

                  {title && title.trim() ? (
                    <View style={{ flex: 1, alignItems: "center" }}>
                      <Text
                        style={{
                          ...styles.cardTitle,
                          color:
                            !img || !img.trim() || !validURL(img)
                              ? "#000"
                              : "#FAFAFA",
                        }}
                      >
                        {title}
                      </Text>
                    </View>
                  ) : null}
                </>

                {title && title.trim() ? (
                  <Text style={[fonts.medium, styles.title]}>{title}</Text>
                ) : null}

                {text && text.trim() ? (
                  <HTMLView
                    value={text}
                    stylesheet={[fonts.light, styles.description, stylesHtml]}
                  />
                ) : null}

                {isExistingNominals ? (
                  <View style={{ marginVertical: 16 }}>
                    <TouchableOpacity
                      onPress={() => handleDropDown()}
                      style={styles.dropDownContainer}
                    >
                      <Text style={styles.dropDownTitle}>Выберите номинал</Text>

                      <Image
                        source={expandedDropdown ? icons["up"] : icons["down"]}
                      />
                    </TouchableOpacity>
                  </View>
                ) : null}

                {expandedDropdown &&
                  nominals.map((item, i) => {
                    if (item.id !== false && item.price)
                      return (
                        <View key={`dropDown-${item && item.id ? item.id : i}`}>
                          <CheckBoxInput
                            title={formattingPrice(item.price) + " ₽"}
                            style={styles.checkBoxInputText}
                            checked={activeRadio === item.price}
                            onPress={() => {
                              setSelectedNominal(item.id);
                              setActiveRadio(item.price);
                            }}
                          />
                          <View style={styles.checkBoxInputTextBottomLine} />
                        </View>
                      );
                  })}
              </View>
            ) : (
              <View
                style={{
                  flex: 1,
                  height: Dimensions.get("screen").height / 1.2,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    flex: 1,
                    width: "100%",
                    textAlign: "center",
                    color: "#000",
                  }}
                >
                  Ошибка получения данных
                </Text>
              </View>
            )}
          </ScrollView>
        </View>
      </Background>

      {expandedDropdown ? (
        <CustomBottomButton
          label={"В КОРЗИНУ"}
          disabled={!selectedNominal || !activeRadio}
          onPress={async () => {
            await addBasketProductById({
              id: selectedNominal,
              count: 1,
              type: "nominal",
            });
            navigation.navigate("Basket");
          }}
        />
      ) : null}
    </>
  );
};

const mapStateToProps = (state) => ({
  certificateLoading: state.CertificatesReducer.certificateLoading,
  certificate: state.CertificatesReducer.certificate,
  certificateFailed: state.CertificatesReducer.certificateFailed,
});

export default connect(mapStateToProps, {
  getCertificateById,
  addBasketProductById,
})(CertificateScreen);

const stylesHtml = StyleSheet.create({
  a: {
    fontWeight: "300",
    color: "#FF3366", // make links coloured pink
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  bowImage: {
    flex: 1,
    height: 190,
    width: "100%",
    borderRadius: 8,
  },
  cardTitle: {
    position: "absolute",
    paddingHorizontal: "10%",
    bottom: 30,
    letterSpacing: 3,
    fontFamily: "BadScript400",
    fontSize: fontSize24,
    textAlign: "center",
    color: "#FAFAFA",
  },
  title: {
    fontSize: 18,
    marginTop: 24,
  },
  description: {
    fontSize: 14,
    marginTop: 27,
    lineHeight: 24,
  },
  checkBoxInputText: {
    marginLeft: 34,
    color: "rgba(21, 28, 38, 0.87)",
  },
  checkBoxInputTextBottomLine: {
    width: "100%",
    marginLeft: 60,
    height: 1,
    marginTop: 18,
    marginBottom: 18,
    opacity: 0.12,
    backgroundColor: colors.swiper_inactive,
  },
  dropDownContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 10,
  },
  dropDownTitle: {
    ...fonts.medium,
    fontSize: 18,
  },
});
