import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

import { fontSize18, WINDOW_HEIGHT, fonts } from "../../../../constants/theme";
import { formattingPrice } from "./../../../../utils";

import { Icon } from "react-native-elements";

export default class GiftPriceList extends Component {
  render() {
    const giftPrices = [
      { price: 5000 },
      { price: 10000 },
      { price: 15000 },
      { price: 20000 },
      { price: 30000 },
      { price: 50000 },
    ];
    const selectedPrice = undefined;
    showPricesList = false;

    const selectPrice = (item) => {
      this.selectedPrice = item;
    };

    const showPricesListFunction = () => {
      this.showPricesList = !this.showPricesList;
    };

    const addToBasket = () => {
      alert(
        this.selectedPrice ? `${this.selectedPrice}` : `Вы ничего не выбрали.`
      );
    };

    return (
      <View style={{ paddingBottom: 32 }}>
        <TouchableOpacity
          onPress={() => {
            // showPricesListFunction();
          }}
          style={styles.selectPriceTitleContainer}
        >
          <Text style={styles.selectPriceTitle}>Выберите </Text>

          <Icon
            color={"rgba(0,0,0,.5)"}
            name={showPricesList ? "up" : "down"}
            size={fontSize18}
            type="antdesign"
          />
        </TouchableOpacity>
        {showPricesList ? (
          <View style={{ paddingBottom: 64 }}>{listPrices}</View>
        ) : null}
      </View>
    );
  }
}

const listPrices = giftPrices.map((item, index) => {
  return (
    <View key={index}>
      {() => (
        <TouchableOpacity
          onPress={() => {
            selectPrice(item.price);
          }}
          style={styles.renderItemContainer}
        >
          <View key={item.price} style={styles.iconContainer}>
            {selectedPrice === item.price ? (
              <Icon
                type="material-community"
                name={"radiobox-marked"}
                size={fontSize18}
                color={"#FF9D04"}
              />
            ) : (
              <Icon
                type="material-community"
                name={"radiobox-blank"}
                size={fontSize18}
                color={"rgba(6,6,12,0.12)"}
              />
            )}
          </View>

          <View style={styles.priceContainer}>
            <Text style={styles.price}>
              {formattingPrice(item.price) + " ₽"}
            </Text>
          </View>
        </TouchableOpacity>
      )}
    </View>
  );
});

const styles = StyleSheet.create({
  renderItemContainer: {
    paddingTop: 32,
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  priceContainer: {
    borderBottomColor: "rgba(0,0,0,0.12)",
    borderBottomWidth: 1,
    flex: 6,
    height: WINDOW_HEIGHT / 15,
  },
  price: {
    fontSize: fontSize18,
    fontFamily: fonts.normal.fontFamily,
  },
  iconContainer: {
    flex: 1,
    height: WINDOW_HEIGHT / 15,
    paddingTop: 1,
  },
  selectPriceTitleContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 36,
  },
  selectPriceTitle: {
    fontSize: fontSize18,
    fontFamily: fonts.normal.fontFamily,
    color: "#000000",
    lineHeight: 22,
  },
});
