import React, { useEffect } from 'react'
import { connect } from "react-redux"

import DefaultRowsList from "../../DefaultRowsListScreen"
import {getCertificates} from "../../../actions/CertificatesAction"

const CertificatesScreen = ({ navigation, certificatesLoading = false, certificates = [], certificatesFailed = null, getCertificates = () => {} }) => {
    useEffect(() => {
        getCertificates();
    }, []);

    return (
        <DefaultRowsList
            navigation={navigation}
            title={'Сертификаты и карты'}
            isLoading={certificatesLoading}
            dataList={certificates}
            itemToScreenNameMove={"Certificate"}

            requestFailed={certificatesFailed}
        />
    );
};

const mapStateToProps = state => ({
    certificatesLoading: state.CertificatesReducer.certificatesLoading,
    certificates: state.CertificatesReducer.certificates,
    certificatesFailed: state.CertificatesReducer.certificatesFailed,
});

const mapDispatchToProps = {
    getCertificates,
};

export default connect(mapStateToProps, mapDispatchToProps)(CertificatesScreen);
