import React from "react";
import {
  Text,
  View,
  ImageBackground,
  FlatList,
  Image,
  SafeAreaView,
  Dimensions,
  AsyncStorage,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import { AntDesign } from "@expo/vector-icons";
import * as Animatable from "react-native-animatable";

import { Logout } from "../../components/logout/logout";
import Colors from "../../constants/Colors";
import { fonts, colors } from "../../constants/theme";
import { setCurrentUser } from "../../actions";
import {
  checkAsyncStorageUserByData,
  renderCountNotification,
  clearUserDataAndAccountExit,
} from "../../utils";
import { AboutUs } from "./../../assets/icons/AboutUsSvg";

const { width, height } = Dimensions.get("window");

class MainMenuScreen extends React.Component {
  constructor(props) {
    super(props);

    let img =
      props.activeTab === 0 || props.activeTab === 1
        ? require(`../../assets/images/mainmenu/trainer.png`)
        : props.activeTab === 2
        ? require(`../../assets/images/mainmenu/specialist.png`)
        : require(`../../assets/images/mainmenu/bar.png`);

    this.state = {
      logoutModal: false,

      MainMenu: [
        {
          title: "Главная",
          file: require("../../assets/images/mainmenu/home.png"),
          navigate: "Home",
        },
        {
          title: "Профиль",
          file: require("../../assets/images/mainmenu/profile.png"),
          navigate: "Profile",
        },
        {
          title: "PMP News",
          file: require("../../assets/images/mainmenu/news.png"),
          navigate: "News",
        },
        {
          title: "Карта",
          file: require("../../assets/images/mainmenu/map.png"),
          navigate: "Map",
        },
        {
          title:
            props.activeTab === 0 || props.activeTab === 1
              ? "Тренеры"
              : props.activeTab === 2
              ? "Специалисты"
              : "Меню бара",
          file: img,
          navigate: props.activeTab !== 3 ? "Specialists" : "MenuList",
          params: { isSpecialists: props.activeTab === 2 },
        },
        {
          title: "Подарочные сертификаты \n и карты",
          file: require("../../assets/images/mainmenu/gift.png"),
          navigate: "CertificatesList",
        },
        {
          title: "Уведомления",
          file: require("../../assets/images/mainmenu/notification.png"),
          navigate: "Notification",
        },
        {
          title: "О PMP",
          isSvg: true,
          file: <AboutUs />,
          navigate: "About",
        },
      ],

      BottomMenu: [
        {
          title: "bucket",
          file: require("../../assets/images/mainmenu/bucket.png"),
          navigate: "Basket",
        },
        {
          title: "setting",
          file: require("../../assets/images/mainmenu/setting.png"),
          navigate: "Settings",
        },
        {
          title: "logout",
          file: require("../../assets/images/mainmenu/logout.png"),
        },
      ],

      basket:
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.basket
          ? this.props.route.params.basket
          : [],
      notifications: this.props.notifications || [],
    };
  }

  // checkUserBasket = () => {
  //     checkAsyncStorageUserByData('basket', null, null, null, false, false)
  //         .then(basket => {
  //             this.setState({
  //                 basket
  //             });
  //         });
  // };

  // componentDidMount() {
  // this.checkUserBasket()
  // }

  render() {
    let delay = 0;

    const routeName =
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.routeName
        ? this.props.route.params.routeName
        : "Home";
    const receivedParams =
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.receivedParams
        ? this.props.route.params.receivedParams
        : {};

    return (
      <ImageBackground
        style={styles.container}
        source={require("../../assets/images/background/mainmenu.png")}
      >
        <SafeAreaView style={{ flex: 1 }}>
          <TouchableOpacity
            style={{
              width: 55,
              marginTop: 20,
              marginLeft: 20,
              marginBottom: 15,
            }}
            activeOpacity={0.5}
            onPress={() =>
              this.props.navigation.navigate(routeName, { ...receivedParams })
            }
          >
            <AntDesign name="close" size={25} color={"#151C26"} />
          </TouchableOpacity>

          <View style={styles.menuPad}>
            <FlatList
              data={this.state.MainMenu}
              numColumns={2}
              keyExtractor={(item) => item.title}
              showsVerticalScrollIndicator={false}
              scrollEnabled={false}
              renderItem={({ item, index }) => {
                delay += 100;

                return (
                  <Animatable.View
                    duration={400}
                    delay={delay}
                    animation="fadeInUp"
                    style={[
                      styles.menuItem,
                      {
                        borderBottomWidth:
                          index >= this.state.MainMenu.length - 2 ? 0 : 0.5,
                      },
                    ]}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate(
                          item.navigate,
                          item.params ? { ...item.params } : {}
                        )
                      }
                    >
                      {this.props.notificationsCount &&
                      item.title === "Уведомления"
                        ? renderCountNotification(
                            this.props.notificationsCount,
                            { right: 10 }
                          )
                        : null}

                      <View style={styles.circleIcon}>
                        {!item.isSvg ? (
                          <Image
                            source={item.file}
                            style={styles.menuIcon}
                            resizeMode={"contain"}
                          />
                        ) : (
                          <View
                            style={{
                              flex: 1,
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            {item.file}
                          </View>
                        )}
                      </View>

                      <Text
                        style={[
                          fonts.light,
                          { fontSize: 14, textAlign: "center", paddingTop: 10 },
                        ]}
                      >
                        {item.title}
                      </Text>
                    </TouchableOpacity>
                  </Animatable.View>
                );
              }}
            />
          </View>

          <View style={styles.bottom}>
            <FlatList
              data={this.state.BottomMenu}
              numColumns={3}
              keyExtractor={(item) => item.title}
              alignItems={"center"}
              justifyContent={"center"}
              renderItem={({ item }) => (
                <Animatable.View
                  duration={400}
                  delay={delay}
                  animation="fadeInUp"
                >
                  <TouchableOpacity
                    style={{ marginHorizontal: 10, position: "relative" }}
                    onPress={() => {
                      item.navigate
                        ? +this.props.navigation.navigate(item.navigate)
                        : this.setState({ logoutModal: true });
                    }}
                  >
                    <Image source={item.file} style={styles.bottomMenuItem} />

                    {this.props.basketCount && item.title === "bucket"
                      ? renderCountNotification(this.props.basketCount)
                      : null}
                  </TouchableOpacity>
                </Animatable.View>
              )}
            />
          </View>

          {this.state.logoutModal && (
            <Logout
              yesHander={async () => {
                await clearUserDataAndAccountExit(this.props.setCurrentUser);
              }}
              noHandler={() => this.setState({ logoutModal: false })}
            />
          )}
        </SafeAreaView>
      </ImageBackground>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentUser: (user) => {
      dispatch(setCurrentUser(user));
    },
  };
};

const mapStateToProps = (state) => ({
  activeTab: state.mainReducer.activeTab,

  notifications: state.currentUser.notifications,
  notificationsCount: state.currentUser.notificationsCount,
  basketCount: state.currentUser.basketCount,
});

export default connect(mapStateToProps, mapDispatchToProps)(MainMenuScreen);

const styles = {
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    opacity: 0.7,
  },
  title: {
    height: 55,
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
    flexDirection: "row",
    textAlign: "center",
  },
  menuPad: {
    flex: 1,
    width: "100%",
  },
  menuItem: {
    position: "relative",

    paddingTop: "5.5%",
    width: "50%",
    height: (height - 80 - 100) / 4,
    justifyContent: "flex-start",
    alignItems: "center",
    borderWidth: 0.5,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    borderColor: colors.auth_gray,
    zIndex: 10,
  },
  bottomMenuItem: {
    position: "relative",
    zIndex: 2,
    width: 50,
    height: 50,
  },
  circleIcon: {
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 25,
    width: width / 8,
    height: width / 8,
    borderWidth: 1,
    borderColor: Colors.orange,
  },
  menuIcon: {
    width: 22,
    height: 22,
    alignSelf: "center",
  },
  bottom: {
    width: width,
    height: 80,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "center",
  },
};
