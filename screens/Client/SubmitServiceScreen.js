import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import moment from "moment";
import {
  AsyncStorage,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from "react-native";
import { Icon } from "react-native-elements";
import Switch from "react-native-switch-pro";

import { Header, ToastView } from "../../components";
import { Background3 } from "../../components/Background3";
import {
  WINDOW_WIDTH,
  WINDOW_HEIGHT,
  fonts,
  colors,
} from "../../constants/theme";
import { RecordsAPI } from "../../api/records";
import { ApplicationsAPI } from "../../api/applications";
import { ChatAPI } from "../../api/chat";
import { MyNotificationAPI } from "../../api/myNotification";
import { getUserStorageData } from "../../utils";
import ButtonInput from "../../components/button/ButtonInput.tsx";
import axios from "axios";

const SubmitServiceScreen = ({ navigation, route, currentUser }) => {
  const {
    submitDate: _submitDate = "",
    pushData,
    service = {},
    companyId,
    submitTime,
    trainType,
    staffId,
    byExpert = false,
    /*staffId,*/ staff = {},
    isOnlineService = false,
  } = route.params;

  const serviceTitle = isOnlineService
    ? service.title.trim() + " Online"
    : service.title.trim();

  console.log(
    "Service",
    service,
    "currentStaff",
    staff,
    "route.paramsroute.params",
    route.params
  );

  const submitDate = _submitDate ? _submitDate : "";
  const propsDate = new Date(submitDate);
  const minutes = +submitTime.split(":")[1];
  const hours = +submitTime.split(":")[0];

  console.log(hours, minutes);

  const submitDateISO = new Date(
    propsDate.getFullYear(),
    propsDate.getMonth(),
    propsDate.getDate(),
    hours + 3,
    minutes
  ).toISOString(); //Todo
  // const submitDateISO = new Date(propsDate.getFullYear(), propsDate.getMonth(), propsDate.getDate(), 9, 25).toISOString(); //Todo
  const dateSelected = `${("0" + propsDate.getDate()).slice(-2)}.${(
    "0" +
    (propsDate.getMonth() + 1)
  ).slice(-2)}.${propsDate.getFullYear()}`;
  const [_switches, _setSwitches] = useState({ day: true, three: false });
  const [_smsRemain, _setSmsRemain] = useState(0);
  const [_remainDay, _setRemainDay] = useState(true);
  const [_remainThree, _setRemainThree] = useState(false);
  const [_recordSuccess, _setRecordSuccess] = useState(false);

  const normalRecordDate =
    moment(route.params.submitDate).format("YYYY.DD.MM") +
    " " +
    hours +
    ":" +
    submitTime.split(":")[1];

  const successMessage =
    route && route.params && route.params.submitDate
      ? `Здравствуйте. Моя запись к Вам: ${
          isOnlineService
            ? service.title.trim() + " Online"
            : service.title.trim()
        }. Дата: ${normalRecordDate}`
      : "";

  const seanceMinutes = 20; /*parseInt(service.staff[0].seance_length / 60)*/
  const endTimeMinutes =
    moment.duration(submitTime).asMinutes() + seanceMinutes;

  console.log(
    "route.params",
    route.params,
    "saervice",
    service,
    "staff",
    staff,
    "staffId",
    staffId,
    "successMessage",
    successMessage
  );

  const sendPushNotification = async () => {
    const userData = await getUserStorageData();

    console.log("userDatadddddd", userData);

    if (userData && userData.id !== false) {
      const notificationBody = {
        user_id: userData.id,
        title: "Вы успешно записались на сеанс!",
        body: `Вы записаны на услугу: ${
          isOnlineService
            ? service.title.trim() + " Online"
            : service.title.trim()
        }, Пожалуйста не пропустите, запись была создана! Датa: ${normalRecordDate}`,
        // day_to_event_time: true, // TODO: For send push back 3 hours to servise notification
        data: JSON.stringify({
          qwe: "rty",
        }),
        service_id: service.id,
        send_date:
          moment(route.params.submitDate).format("YYYY-MM-DD") +
          " " +
          hours +
          ":" +
          submitTime.split(":")[1],
        data_push_response: JSON.stringify({
          qwe: "rty",
        }),
        body_push_response: `Вы записаны на услугу: ${
          isOnlineService ? service.title + " Online" : service.title
        }, Пожалуйста не пропустите, запись была создана! Датa: ${normalRecordDate}`,
        title_push_response: `Вы успешно записались на сеанс!`,
        warning_to_event_time: _switches && _switches.three,
      };

      const resultNotification = await MyNotificationAPI.setNotificationApi(
        notificationBody
      );

      // if (_switches && _switches.three)
      //   await MyNotificationAPI.setNotificationBeforeThreeHoursApi(
      //     notificationBody
      //   );

      // console.log("resultNotification", resultNotification);
      // console.log("currentService", service, "PushHBody", notificationBody);
    }
  };

  // useEffect(() => {
  //   sendPushNotification();
  //
  //   ApplicationsAPI.addApplication({
  //     isOnline: isOnlineService,
  //     staff_id: staff.yid,
  //     companyId: isOnlineService ? 57919 : companyId,
  //     train_type_id: byExpert ? 2 : trainType,
  //     datetime: data.datetime,
  //     service_id: service.id,
  //     service_type: byExpert ? "expert_service" : "trainer_service",
  //     title: isOnlineService
  //       ? service.title.trim() + " Online"
  //       : service.title.trim(),
  //     remain_day: _remainDay,
  //     remain_three: _remainThree,
  //   }).then((res) => {
  //     if (!res || (res.status && res.status === "error")) {
  //       ToastView(
  //         "Системная ошибка: Уведомление не создано, Обратитесь пожалуйста в техподдержку!"
  //       );
  //
  //       console.log("Error ApplicationsAPI.addApplication --- 1 ", res);
  //     }
  //
  //     AsyncStorage.setItem(
  //       "paymentsOperation",
  //       JSON.stringify({ hasPaymentsOperation: true })
  //     );
  //
  //     // console.log("<<CREATE APPLICATION --- ", res);
  //   });
  // }, []);

  useEffect(() => {
    if (_switches.day) {
      _setSmsRemain(24);
      _setRemainDay(true);
    } else if (_switches.three) {
      _setSmsRemain(3);
      _setRemainThree(true);
    }
  }, [_switches]);

  const primaryColor = !byExpert ? colors.orange : colors.swiper_active;

  const sendFirtsMessageToTrainer = async () => {
    const userData = await getUserStorageData();

    if (
      userData &&
      userData.id !== false &&
      staff &&
      staff.id !== false &&
      successMessage
    ) {
      const newMessage = {
        user_id: userData.id,
        trainer_id: staff.id,
        sender_type: 0,
        receiver_type: 1,
        message: successMessage,
      };

      try {
        await ChatAPI.sendMessage(newMessage);

        ToastView("Чат со специалистом PMP создан", 1500);
      } catch (e) {
        ToastView(
          "Системная ошибка: Сообщение специалисту не создано.\nПроизошла ошибка.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27",
          2000
        );
      }
    }

    console.log("userDatauserData", userData);
  };

  useEffect(() => {
    if (_recordSuccess) {
      try {
        console.log("currentService", service);

        ApplicationsAPI.addApplication({
          isOnline: isOnlineService,
          staff_id: staff.yid,
          companyId: isOnlineService ? 57919 : companyId,
          train_type_id: byExpert ? 2 : trainType,
          datetime: data.datetime,
          service_id: service.id,
          service_type: byExpert ? "expert_service" : "trainer_service",
          title: isOnlineService
            ? service.title.trim() + " Online"
            : service.title.trim(),
          remain_day: _remainDay,
          remain_three: _remainThree,

          // staff_id: staff.yid,
          // companyId: isOnlineService ? 57919 : companyId,
          // title: isOnlineService
          //   ? service.title.trim() + " Online"
          //   : service.title.trim(),
          // train_type: trainType,
          // datetime: data.datetime,
          // seance_length: data.seance_length,
          // service_id: service.id,
          // trainer_service: byExpert ? 2 : 1 service.id, //TODO:
          // cost:
          //   service.price ||
          //   service.service.price_min ||
          //   service.service.price_max,
          // remain_day: _remainDay,
          // remain_three: _remainThree,
          // service_type: byExpert ? 'expert_service' : 'trainer_service',
          // isOnline: isOnlineService,
        }).then((res) => {
          if (!res || (res.status && res.status === "error"))
            ToastView(
              "Системная ошибка: Уведомление не создано.\nПроизошла ошибка.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27"
            );

          console.log("Error ApplicationsAPI.addApplication --- 1 ", res);

          AsyncStorage.setItem(
            "paymentsOperation",
            JSON.stringify({ hasPaymentsOperation: true })
          );

          // console.log("<<CREATE APPLICATION --- ", res);
        });

        sendFirtsMessageToTrainer();
        sendPushNotification();
      } catch (e) {
        console.log("Error ApplicationsAPI.addApplication --- ", e);

        ToastView(
          "Системная ошибка: Уведомление не создано.\nПроизошла ошибка.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27",
          3000
        );
      }
    }

    return () => {
      _setRecordSuccess(false);
    };
  }, [_recordSuccess]);

  console.log(
    "Submit Services onSubmit: companyId -- ",
    companyId,
    "DATAAAA -- ",
    data,
    " companyId  -- ",
    companyId,
    "service",
    service
  );

  const data = {
    staff_id: staff.yid,
    services: [
      {
        id: /*service.id*/ 491893, //TODO: Will add this paropertie to the table with Services Data
        first_cost: service.price,
        discount: service.discount || 0,
        cost: service.price,
      },
    ],
    client: {
      phone: currentUser.login.split("+")[1],
      name:
        currentUser.firstname && currentUser.firstname.trim()
          ? currentUser.firstname
          : "Неизвестный пользователь приложения", //Todo
      email:
        currentUser.email && currentUser.email.trim() ? currentUser.email : "",
    },
    save_if_busy: false,
    datetime: submitDateISO,
    seance_length: route.params.seanceLength, //TODO: Will add this paropertie to the table with Services Data
    send_sms: true,
    comment: `ЗАПИСЬ С ПРИЛОЖЕНИЯ! На услугу: ${serviceTitle}`,
    sms_remain_hours: _smsRemain,
    email_remain_hours: 24,
    attendance: 1,
    api_id: "777",
    custom_color: primaryColor,
  };

  const onSubmit = async () => {
    data.category_id = 491893; //TODO: Will add this paropertie to the table with Services Data

    try {
      // let data12 = { // EXAMPLE VALID Data for create record
      //   staff_id: 134623,
      //   services: [
      //     {
      //       id: 491893,
      //       first_cost: 9000,
      //       discount: 50,
      //       cost: 4500,
      //     },
      //   ],
      //   client: {
      //     phone: "79235748071",
      //     name: "Дмитрий",
      //     email: "d@yclients.com",
      //   },
      //   save_if_busy: false,
      //   datetime: "2020-07-16T9:00:00.000Z",
      //   seance_length: 3600,
      //   send_sms: true,
      //   comment: "тестовая запись!",
      //   sms_remain_hours: 15,
      //   email_remain_hours: 24,
      //   attendance: 1,
      //   api_id: "777",
      //   color: "#f44336",
      // };

      RecordsAPI.createRecord({
        companyId,
        data,
      }).then((res) => {
        console.log("<<<CREATE RECORD --- ", res);

        if (res && (!res.status || res.status !== "error")) {
          ToastView("Благодарим. Ваша запись создана", 500);

          _setRecordSuccess(true);

          navigation.navigate("Home");
        } else {
          if (res.error && res.error.toString().includes("409")) {
            ToastView(
              `Ошибка: Указанное врем уже занято или является нерабочим!\n Вернитесь пожалуйста на шаг назад и Выберите другое`,
              1000
            );
          } else {
            ToastView(
              "Системная ошибка: Запись не создана.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27",
              1000
            );
          }

          _setRecordSuccess(false);
        }
      });
    } catch (e) {
      console.log("Error RecordsAPI.createRecord --- ", e);

      ToastView(
        "Системная ошибка: Запись не создана.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27",
        2000
      );
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <Background3>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Header
            style={{ ...styles.header, bottomTitileColor: "transparent" }}
            backTitle={"Назад"}
          />

          <View style={styles.container}>
            <Text
              style={[
                {
                  color: colors.title,
                  marginTop: 16,
                  marginBottom: 35,
                  width: "80%",
                  textAlign: "center",
                },
                fonts.h1,
                fonts.normal,
              ]}
            >
              Подтверждение записи
            </Text>

            <Icon
              name="check-circle"
              type="font-awesome"
              color={primaryColor}
              size={100}
            />

            <Text
              style={[
                { color: colors.title, marginTop: 30, marginBottom: 15 },
                fonts.h2,
                fonts.light,
              ]}
            >
              Вы подтверждаете запись на:
            </Text>

            <View style={{ ...styles.timeContainer, marginBottom: 45 }}>
              <Text
                style={[
                  { color: colors.title, marginBottom: 2, letterSpacing: 0.22 },
                  fonts.h2,
                  fonts.medium,
                ]}
              >
                {serviceTitle}
              </Text>

              <Text
                style={[
                  { color: colors.title, marginBottom: 1, letterSpacing: 0.22 },
                  fonts.h2,
                  fonts.medium,
                ]}
              >
                {service.booking_title}
              </Text>

              <Text
                style={[
                  { color: colors.title, letterSpacing: 0.22 },
                  fonts.h2,
                  fonts.medium,
                ]}
              >
                {/*{seanceMinutes} минут*/}Дата записи: {dateSelected}{" "}
                {submitTime}
                {/*// -{" "}
                // {Math.floor(endTimeMinutes / 60)}:
                // {endTimeMinutes % 60 < 10
                //   ? "0" + (endTimeMinutes % 60)
                //   : endTimeMinutes % 60}*/}
              </Text>
            </View>

            {/*<View style={styles.checkboxContainer}>
              <Text style={{ flex: 1, color: "#000", fontWeight: "normal" }}>
                Перед самой записью за несколько часов, с Вами свяжуться наши
                сотрудники. {"\n\n\n"}Хорошего вам время суток! 😉
              </Text>
            </View>*/}

            <View style={styles.checkboxContainer}>
              <View>
                <Text style={[{ color: colors.title }, fonts.h3, fonts.normal]}>
                  Напоминание о записи будет выслано Вам за день до визита.
                </Text>
                {/*<Text style={[{ color: colors.title }, fonts.h3, fonts.normal]}>
                  Предупредить за 1 день
                </Text>*/}
              </View>

              {/*<View>
                <Switch
                  value={_switches && _switches.day}
                  onSyncPress={(value) =>
                    _setSwitches((prevState) => ({ ...prevState, day: true }))
                  }
                  width={48}
                  height={28}
                  backgroundActive={primaryColor}
                />
              </View>*/}
            </View>

            <View style={styles.checkboxContainer}>
              <View>
                <Text style={[{ color: colors.title }, fonts.h3, fonts.normal]}>
                  Предупредить за 3 часа
                </Text>
              </View>

              <View>
                <Switch
                  value={_switches && _switches.three}
                  onSyncPress={(value) => {
                    _setSwitches((prevState) => ({
                      ...prevState,
                      three: value,
                    }));

                    _setRemainThree(value);

                    console.log("REMAIN THREE: ", value);
                  }}
                  width={48}
                  height={28}
                  backgroundActive={primaryColor}
                />
              </View>
            </View>

            <TouchableOpacity
              activeOpacity={0.7}
              style={{
                width: WINDOW_WIDTH - 30,
                marginTop: 20,
              }}
            >
              <ButtonInput
                tiffany={byExpert}
                title="ПОДТВЕРДИТЬ"
                onClick={() => onSubmit()}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Background3>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    currentUser: state.currentUser.currentUser,
  };
};

export default connect(mapStateToProps)(SubmitServiceScreen);

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: "center", marginBottom: 64 },
  header: {
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0, 0, 0, 0)",
  },
  timeContainer: {
    width: WINDOW_WIDTH - 32,
    backgroundColor: "#fff",
    borderRadius: 10,
    paddingVertical: 14,
    paddingHorizontal: 16,
    marginBottom: WINDOW_HEIGHT / 10,
  },
  checkboxContainer: {
    width: WINDOW_WIDTH - 35,
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 20,
  },
});
