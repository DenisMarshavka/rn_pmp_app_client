import React from "react";
import { Background } from "../../components/Background";
import HeaderBack from "../../components/Headers/BackHeader";
import { colors, fonts, sizes } from "../../constants/theme";
import { Text, View, StyleSheet, FlatList } from "react-native";
import ProfileCard from "../../components/ProfileCard";

const Specialists = (props) => {
  const list = [
    {
      name: "Денис Сычёв",
      position: "Владелец и основатель бренда",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.",
      photo: "",
    },
    {
      name: "Юрий Гавриш",
      position: "Сертифицированный инструктор Pilate...",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.",
      photo: require("../../assets/images/yogaPic.png"),
    },
  ];

  return (
    <Background>
      <HeaderBack title={"Тренеры"} />
      <View style={styles.container}>
        <FlatList
          style={{ flex: 1 }}
          data={list}
          renderItem={({ item }) => (
            <ProfileCard {...item} navigation={props.navigation} />
          )}
          keyExtractor={(item, index) => String(index)}
        />
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  title: {
    color: colors.title,
    ...fonts.normal,
    ...fonts.title,
  },
});
export default Specialists;
