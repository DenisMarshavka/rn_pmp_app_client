import React, { useState, useEffect } from 'react';
import { Image, ImageBackground, StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';

import { Background } from '../../components/Background';
import { Header } from '../../components/Header';
import { colors, fonts, sizes, fontSize28 } from '../../constants/theme';
import MainHeader from '../../components/Headers/MainHeader';
import { LinearGradient } from 'expo-linear-gradient';

import { CompaniesAPI } from '../../api/companies';

export const FitnessStudioScreen = ({ navigation, route }) => {
  const [_companies, _setCompanies] = useState([]);
  const network = route.params.network;
  const list = route.params.list;
  useEffect(() => {
    try {
      CompaniesAPI.getCompaniesByNetworkId({ group_id: network })
      .then(data => {
        _setCompanies(data);
      })
    } catch (e) {
      console.log('Error CompaniesAPI.getCompaniesByNetworkId -- ', e);
    }
  }, [navigation])

  return (
    <Background>
      <MainHeader textColor={'dark'}
                  back={true}
                  navigation={navigation}/>

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <Text style={[styles.title, fonts.normal, fonts.h1]}>Выбор студии</Text>
          {
            _companies.map((company, index) => (
              <TouchableOpacity style={{marginTop: 16, height: 295, borderRadius: 8}} key={index} onPress={() => navigation.navigate('ListProducts', { companyId: company.id })} activeOpacity={0.8}>
               <ImageBackground
                  style={[styles.imgBg]}
                  imageStyle={{ borderRadius: 8 }}
                  source={{ uri: company.logo }}
                  resizeMode="cover"
                >
                 <LinearGradient
                  colors={['#000', 'transparent']}
                  start={{x: 0.1, y: 1}}
                  end={{x: 0.1, y: 0.1}}
                  locations={[0, 0.9]}
                  style={{width: '100%', height: 295, zIndex: 1, borderRadius: 8}}>
                  <Text style={[fontSize28, fonts.bold, styles.companyTitle]}>{ company.title }</Text>
                </LinearGradient>
                </ImageBackground>
              </TouchableOpacity>
            ))
          }

          {/*<TouchableOpacity onPress={() => navigation.navigate('ListProducts')} activeOpacity={0.8}>
            <Image
              style={[styles.img, { marginTop: 16 }]}
              source={require("../../assets/images/fitness-studio2.png")}
              resizeMode="cover"
            />
          </TouchableOpacity>*/}
        </View>
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: { paddingHorizontal: sizes.margin, paddingBottom: 30 },
  title: { color: colors.title },
  img: {
    borderRadius: 8,
    width: "100%",
  },
  imgBg: {
    borderRadius: 8,
    width: "100%",
    maxHeight: 295,
  },
  companyTitle: {
    width: '100%',
    color: '#fff',
    textAlign: 'center',
    bottom: 17,
    position: 'absolute',
  }
});
