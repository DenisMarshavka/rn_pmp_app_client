import React, { useState } from "react";
import { Background } from "../../components/Background";
import HeaderBack from "../../components/Headers/BackHeader";
import { colors, fonts, sizes } from "../../constants/theme";
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  ScrollView,
  Image,
  Dimensions,
} from "react-native";
import ProfileCard from "../../components/ProfileCard";
import ButtonInput from "../../components/button/ButtonInput";
import Carousel from "react-native-snap-carousel";
let deviceWidth = Dimensions.get("window").width;
import Lightbox from "react-native-lightbox";

const Specialists = (props) => {
  const [visible, setIsVisible] = useState(false);

  const params = props.route.params;

  // const list = [
  //     {
  //         name: 'Денис Сычёв',
  //         position: 'Владелец и основатель бренда',
  //         description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.',
  //         photo: ''
  //     },
  //     {
  //         name: 'Юрий Гавриш',
  //         position: 'Сертифицированный инструктор Pilate...',
  //         description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.',
  //         photo: require('../../assets/images/yogaPic.png')
  //     }
  // ];

  const galery = [
    {
      uri: require("../../assets/images/yogaPic.png"),
    },
    {
      uri: require("../../assets/images/yogaPic.png"),
    },
    {
      uri: require("../../assets/images/yogaPic.png"),
    },
  ];

  const renderItem = ({ item, index }) => {
    return (
      <View style={styles.slide} key={index}>
        <Lightbox>
          <Image style={styles.slidePhoto} source={item.uri} />
        </Lightbox>
      </View>
    );
  };

  return (
    <Background>
      <HeaderBack tiffany={params.tiffany} />
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.headerContainer}>
            <View style={styles.avatarContainer}>
              <Image
                source={
                  props.photo
                    ? props.photo
                    : require("../../assets/images/avatarIcon.png")
                }
                style={[styles.avatar, !props.photo && { width: 32 }]}
              />
            </View>
            <Text style={styles.name}>Денис Сычёв</Text>
            <View style={styles.item}>
              {/*<Text style={styles.title}>Описание</Text>*/}
              <Text style={styles.description}>
                Владелец и основатель бренда, профессиональный хореограф,
                сертифицированный тренер, ученик американской школы, открытой
                музой и ученицей Йозефа Пилатеса. Именно Денис собрал вместе
                профессионалов в области пилатеса, функционального тренинга и
                бьюти-индустрии, которые ждут гостей в сети моно студий PMP.
                Денис работал в пилатес студиях по всему миру — США, Испания,
                Турция, Франция, Голландия, Сингапур. Его интервью популярным
                изданиям помогают понять идею и цели метода пилатеса.
              </Text>
            </View>
            <View style={styles.item}>
              <Text style={styles.title}>Рекомендации</Text>
              <Text style={styles.description}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </Text>
            </View>
            <View style={styles.item}>
              <Text style={styles.title}>Видеоуроки</Text>
            </View>
            <Carousel
              layout={"default"}
              //ref = {(c) => {console.log(c)}}
              renderItem={renderItem}
              sliderWidth={deviceWidth}
              itemWidth={deviceWidth - 32}
              contentContainerCustomStyle={{ marginBottom: 15 }}
              inactiveSlideOpacity={1}
              data={galery}
              inactiveSlideScale={1}
              layoutCardOffset={2}
              activeSlideAlignment={"center"}
            />
          </View>
        </ScrollView>
      </View>
      <View style={{ padding: 16 }}>
        <ButtonInput tiffany={params.tiffany} title="записаться" />
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    ...fonts.normal,
    ...fonts.h2,
    color: "#151C26",
    paddingBottom: 6,
  },
  description: {
    ...fonts.normal,
    fontSize: 14,
    color: "#151C26",
  },
  headerContainer: {
    alignItems: "center",
  },
  avatarContainer: {
    width: 82,
    height: 82,
    borderRadius: 50,
    backgroundColor: "#C4C4C4",
    marginVertical: 26,
    alignItems: "center",
    justifyContent: "center",
  },
  avatar: {
    width: 82,
    height: 82,
    borderRadius: 50,
    resizeMode: "contain",
  },
  name: {
    color: "#151C26",
    ...fonts.normal,
    ...fonts.title,
    paddingBottom: 23,
  },
  item: {
    paddingBottom: 21,
    paddingHorizontal: 16,
    flex: 1,
    width: "100%",
  },
  slide: {
    height: 196,
    backgroundColor: "#dedede",
    borderRadius: 10,
    overflow: "hidden",
    marginRight: 5,
  },
  slidePhoto: {
    width: "100%",
    resizeMode: "cover",
    borderRadius: 10,
  },
});
export default Specialists;
