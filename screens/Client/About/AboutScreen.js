import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  ScrollView,
  ActivityIndicator,
  Dimensions,
  Image,
  Linking,
  TouchableOpacity,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import Carousel from "react-native-snap-carousel";
import Lightbox from "react-native-lightbox";
import HTMLView from "react-native-htmlview";
import { connect } from "react-redux";
import { useNavigation } from "@react-navigation/native";

import { Background } from "../../../components/Background";
import { colors, fonts, sizes } from "../../../constants/theme";
import TitleHeader from "../../../components/Headers/TitleHeader";
import { getAboutUsInfo } from "../../../actions";
import { showAlert } from "../../../utils";
import Header from "../../../components/Headers/Header";

let { width: deviceWidth, height: deviceHeight } = Dimensions.get("window");

const HTMLSectionText = ({ content = "" }) => (
  <>
    {content && typeof content === "string" ? (
      <HTMLView
        value={content}
        style={{ marginTop: 5 }}
        stylesheet={{
          ul: styles.list,
          addLineBreaks: false,
          paragraphBreak: styles.text,
        }}
      />
    ) : null}
  </>
);

const AboutScreen = ({
  route = {},

  getAboutUsInfo = () => {},
  aboutUsLoading = false,
  aboutUsInfo = {},
  aboutUsError = false,
}) => {
  const navigation = useNavigation();

  const {
    about = "",
    contact = [],
    address = "",
    img = [],
    takeForTraining = "",
  } = aboutUsInfo;

  let swipeModalRef = React.useRef(null);

  console.log("About img", img, "aboutUsInfo", aboutUsInfo);

  React.useEffect(() => {
    getAboutUsInfo();
  }, []);

  const renderSliderItem = ({ item, index }) => (
    <View style={{ flex: 1 }}>
      {item.trim() ? (
        <View style={styles.slide} key={`image-${index}`}>
          <Image style={styles.img} source={{ uri: item }} />
        </View>
      ) : null}
    </View>
  );

  return (
    <Background>
      <Header title={"О PMP"} backTitle routeName={route.name} />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.body}>
          {aboutUsLoading && !aboutUsError ? (
            <View
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                minHeight: deviceHeight / 1.4,
                paddingBottom: 23,
              }}
            >
              <ActivityIndicator size={"large"} color={colors.mainOrange} />
            </View>
          ) : (
            <View style={{ flex: 1 }}>
              {img && img.length ? (
                <View
                  style={{
                    flex: 1,
                    marginTop: 15,
                    minHeight: 190,
                  }}
                >
                  <Carousel
                    layout={"default"}
                    //ref = {(c) => {console.log(c)}}
                    renderItem={renderSliderItem}
                    sliderWidth={deviceWidth}
                    itemWidth={deviceWidth - 45}
                    contentContainerCustomStyle={{
                      marginTop: 16,
                      paddingLeft: 16,
                      alignSelf: "flex-start",
                      height: 190,
                    }}
                    inactiveSlideOpacity={1}
                    data={img}
                    inactiveSlideScale={1.6}
                    layoutCardOffset={1}
                    activeSlideAlignment={"start"}
                  />
                </View>
              ) : null}

              {about.trim() ? (
                <View style={{ paddingHorizontal: 16 }}>
                  {/*<Text style={[{ marginTop: 25 }, styles.textFont]}>
                    О бренде
                  </Text>*/}

                  <View
                    style={{
                      marginTop: 25,
                    }}
                  >
                    <HTMLSectionText content={about.trim()} />
                  </View>
                </View>
              ) : null}

              {contact && contact.length ? (
                <View style={{ paddingHorizontal: 16 }}>
                  <Text style={[{ marginTop: 20 }, styles.textFont]}>
                    Контакты
                  </Text>

                  {contact.map((item, index) => {
                    if (item && item.trim()) {
                      return (
                        <TouchableOpacity
                          key={`contact-${item + index}`}
                          style={{
                            maxWidth: 90,
                          }}
                          onPress={async () => {
                            const supported = await Linking.canOpenURL(
                              `tel: ${item}`
                            );

                            if (supported) {
                              await Linking.openURL(`tel: ${item}`);
                            } else {
                              alert(`Упс, не могу вызвать этот номер: ${item}`);
                            }
                          }}
                          activeOpacity={0.6}
                        >
                          <Text
                            style={[
                              { marginTop: 8, textDecorationLine: "underline" },
                              fonts.light,
                            ]}
                          >
                            {item}
                          </Text>
                        </TouchableOpacity>
                      );
                    }
                  })}
                </View>
              ) : null}

              {address.trim() ? (
                <View style={{ paddingHorizontal: 16 }}>
                  <Text style={[{ marginTop: 20 }, styles.textFont]}>
                    Как проехать
                  </Text>

                  <HTMLSectionText content={address.trim()} />
                </View>
              ) : null}

              {takeForTraining.trim() ? (
                <View style={{ paddingHorizontal: 16 }}>
                  <Text style={[{ marginTop: 30 }, styles.textFont]}>
                    Что взять на тренировку
                  </Text>

                  <HTMLSectionText content={takeForTraining.trim()} />
                </View>
              ) : null}

              <View style={{ paddingHorizontal: 16, flex: 1 }}>
                <TouchableOpacity
                  activeOpacity={0.5}
                  style={[
                    { paddingBottom: 50, marginTop: 35 },
                    styles.textFont,
                  ]}
                  onPress={() =>
                    navigation.navigate("OnBoardings", {
                      moveEndScreen: "About",
                    })
                  }
                >
                  <Text
                    style={{
                      color: colors.orange,
                      fontWeight: "bold",
                    }}
                  >
                    Инструкция использования приложения
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>

        <LinearGradient
          pointerEvents={"none"}
          colors={["rgba(255, 255, 255, 0)", "#FFF"]}
          style={styles.linearGradient}
        />
      </ScrollView>
    </Background>
  );
};

const mapStateToProps = (state) => ({
  aboutUsLoading: state.mainReducer.aboutUsLoading,
  aboutUsInfo: state.mainReducer.aboutUsInfo,
  aboutUsError: state.mainReducer.aboutUsError,
});

export default connect(mapStateToProps, { getAboutUsInfo })(AboutScreen);

const styles = StyleSheet.create({
  imgBg: {
    flex: 1,
    borderRadius: 8,
    width: "100%",
    height: 196,

    resizeMode: "cover",
  },
  body: {
    paddingBottom: 20,
  },
  slide: {
    flex: 1,

    backgroundColor: "#dedede",
    borderRadius: 10,
    marginRight: 10,
  },

  viewImage: {
    width: "100%",
  },
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  articleHeaderIcon: {
    display: "flex",
    justifyContent: "flex-end",
    width: 28,
    height: 28,
  },
  headerTitle: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: sizes.headerTitle,
  },
  headerSubTitle: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: sizes.headerSubTitle,
  },
  textFont: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: sizes.headerName,
  },
  flat_list: {
    marginTop: 26,
    marginBottom: 26,
  },
  flat_list_item: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: sizes.text,
  },
  img: {
    flex: 1,
    borderRadius: 8,
    width: "100%",
  },
  companyTitle: {
    width: "100%",
    color: "#fff",
    textAlign: "center",
    bottom: 17,
    position: "absolute",
  },
  linearGradient: {
    flex: 1,
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 80,
  },
  list: {
    justifyContent: "flex-start",
    alignItems: "flex-start",

    marginLeft: 15,
  },
});
