import React, { useState, useEffect } from "react";
import {
  Animated,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  AsyncStorage,
  LayoutAnimation,
  SafeAreaView,
} from "react-native";
import { ProgressCircle } from "react-native-svg-charts";
import { connect, useDispatch, useSelector } from "react-redux";
import { BoxShadow } from "react-native-shadow";
import moment from "moment";
import * as Animatable from "react-native-animatable";

import { ToastView } from "../../components";
import { Background } from "../../components/Background";
import { colors, fonts, sizes } from "../../constants/theme";
import { SalonsAPI } from "../../api/salons";
import ButtonInput from "../../components/button/ButtonInput";
import { menuListData } from "../../constants/mocks";
import { dimensions } from "../../styles";
import { updateTab } from "../../reducers/MainReducer";
import { SectionWithSlider } from "../../components/SectionWithSlider/SectionWithSlider";
import SkeletonProductItem from "../../components/loaders/SkeletonProductItem";
import {
  getNewPrograms,
  getSpecialPrograms,
} from "../../actions/ProgramsActions";
import { setCurrentUser } from "../../actions";
import {
  addBasketProductById,
  getCurrentBasketState,
} from "../../actions/BasketActions";
import { Tabs } from "../../components/Tabs/Tabs";
import {
  getStatusToConstantlyClient,
  // getTrainingsBalanceOnline,
  getTrainingsBalance,
} from "../../actions";
import SkeletonLoader from "../../components/loaders/SkeletonLoader";
import ListWithInfo from "../../components/ListWithInfo";
import InfoCardWithGraphicsIndicator from "../../components/InfoCardWithGraphicsIndicator";
import { getHistoryState } from "../../actions/BasketActions";
import {
  registerPushNotificationsAsync,
  askPermissionNotifications,
  getUserStorageData,
  getSizeSkeletonLoaders,
  handleLinkOpen,
  clearUserDataAndAccountExit,
  formattingPrice,
} from "../../utils";
import { BasketSvg } from "../../assets/icons/profile/BasketSvg";
import { UserAPI } from "../../api/user";

import {
  getTopProducts,
  getProductsCategories,
} from "../../actions/StoreActions.js";
import { getUserNotifications } from "../../actions/UserActions";
import { getGeneratePaymentLink } from "../../actions";
import BasketState from "../../components/basketState";
// import { sendPushNotification } from "../../utils/notifications/expoNotifications";

const { width } = Dimensions.get("window");

const shadowOpt = {
  width: (width - 48) / 3,
  height: 244,
  color: "#000",
  border: 8,
  radius: 18,
  opacity: 0.07,
  x: 0,
  y: 0.3,
  style: {
    borderRadius: 3,
    marginBottom: 8,
    marginRight: 8,
  },
};

const HomeScreen = (props) => {
  const {
    navigation,
    route,

    setCurrentUser = () => {},

    getCurrentBasketState = () => {},
    basketCount = 0,

    getUserNotifications = () => {},
    addBasketProductById = () => {},

    getTrainingsBalance = () => {},
    trainingsBalanceLoading = false,
    trainingsBalanceData = [],
    trainingsBalanceFailed = false,

    // getTrainingsBalanceOnline = () => {},
    // trainingsBalanceOnlineLoading = false,
    // trainingsBalanceOnlineData = [],
    // trainingsBalanceOnlineFailed = false,

    getStatusToConstantlyClient = () => {},
    constantlyStatusLoading = false,
    constantlyStatus = 0,
    maxValueToConstantly = 0,
    constantlyStatusFailed = false,

    // getHistoryPaymentsProducts = () => {},
    // historyLoading = false,
    // historyProducts = [],
    // historyFailed = null,

    getHistoryState = () => {},
    historyLoading = false,
    history = [],
    countHistoryItems = 0,
    countItems = 0,
    historyFailed = null,

    getProductsCategories = () => {},
    productsCategoriesLoading = false,
    productsCategories = [],
    productsCategoriesError = false,

    getTopProducts = () => {},
    topProductsLoading = true,
    topProductsError = null,
    topProducts = [],

    getGeneratePaymentLink = () => {},

    getSpecialPrograms = () => {},
    specialProgramsLoading = false,
    specialProgramsData = [],
    specialProgramsError = false,

    getNewPrograms = () => {},
    newProgramsLoading = false,
    newProgramsData = [],
    newProgramsError = false,

    basket = [],

    // existingNotificationOpened = null,
  } = props;

  const [activeInviteSlideIndex, setActiveInviteSlideIndex] = useState(0);
  const [activeProductSlideIndex, setActiveProductSlideIndex] = useState(0);
  const [currentUserData, setCurrentUserData] = useState({});

  const [offsetHistory, setOffsetHistory] = useState(0);
  const [limitHistory, setLimitHistory] = useState(15);
  const [orderDataHistory, seOrderDataHistory] = useState("desc");
  const [historyPaymets, setHistoryPaymets] = useState([]);

  const activeTab = useSelector((state) => state.mainReducer.activeTab);
  const dispatch = useDispatch();

  const getScreenData = () => {
    getCurrentBasketState(true, 0, 15, "desc", false, false);
    getUserNotifications(0, 0);

    getSpecialPrograms(false, 3);
    getNewPrograms(false, 3);

    getTrainingsBalance();
    // getTrainingsBalanceOnline();

    getStatusToConstantlyClient();
    getHistoryState(true);

    getProductsCategories();
    getTopProducts();
  };

  const setPushToken = async (pushToken = "") => {
    if (pushToken && pushToken.trim()) {
      await AsyncStorage.mergeItem(
        "user",
        JSON.stringify({
          push_token: pushToken,
        })
      );
    }
  };

  // useEffect(() => {
  //   const foodHistory = [];
  //
  //   console.log("HISTORY_HISTORY: ", history);
  //
  //   if (history && history.length && activeTab === 3) {
  //     history.forEach((item, index) => {
  //       if (
  //         item &&
  //         item.items &&
  //         Object.kesy(item.items).length &&
  //         item.items.product &&
  //         item.items.product.length
  //       )
  //         foodHistory = [...item.items.product];
  //     });
  //   }
  // }, [history]);

  useEffect(() => {
    askPermissionNotifications().then(() => {
      // console.log(
      //   "existingNotificationOpenedexistingNotificationOpened",
      //   existingNotificationOpened
      // );

      getUserStorageData().then((userData) => {
        if (userData && userData.notifications && !userData.push_token) {
          setCurrentUserData(userData);

          registerPushNotificationsAsync().then((token) => {
            console.log("PushToken Home Screen: ", token);

            setPushToken(token);
          });
        }

        if (userData && Object.keys(userData).length) {
          console.warn("Home Screen userData: ", userData);

          if (!userData.token) {
            ToastView(
              "Требуеться срочно повторная авторизация!\nЕсли это случаеться очень часто, Пожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27."
            );

            clearUserDataAndAccountExit(setCurrentUser);
          }
        } else
          ToastView(
            "Ооо нет :( Случилось что-то непридвиденное.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27."
          );
      });
    });

    navigation.addListener("focus", () => {
      let hasPaymentsOperation = false;

      getUserNotifications(0, 0);

      AsyncStorage.getItem("paymentsOperation").then((result) => {
        if (result) hasPaymentsOperation = JSON.parse(result);

        if (hasPaymentsOperation) {
          getTrainingsBalance();
          getStatusToConstantlyClient();

          AsyncStorage.removeItem("paymentsOperation");
        }
      });
    });

    getScreenData();

    // sendPushNotification();

    return () => {
      chooseTab(0);
    };
  }, []);

  useEffect(() => {
    if (!historyLoading) generateHistoryPaymentsState();
  }, [historyLoading, history]);

  // useEffect(() => {
  //   if (!historyLoading) getHistoryState();
  // }, [offsetHistory, limitHistory, orderDataHistory]);

  const chooseTab = (tab) => {
    dispatch(updateTab(tab));
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  const historyOrdersData = historyLoading
    ? getSizeSkeletonLoaders(175, 200)
    : !historyLoading && history && history.length
    ? history
    : [];

  const topProdcusData = topProductsLoading
    ? getSizeSkeletonLoaders(175, 200, true)
    : !topProductsLoading && topProducts && topProducts.length
    ? topProducts
    : [];

  const generateHistoryPaymentsState = () => {
    let currentState = [];

    if (history && history.length) {
      for (let product of history) {
        if (product.items && Object.keys(product.items).length) {
          for (let itemProductType of Object.keys(product.items)) {
            if (
              itemProductType &&
              itemProductType === "product" &&
              product.items[itemProductType] &&
              product.items[itemProductType].length
            ) {
              const currentProductsTypeItems = [
                ...product.items[itemProductType],
              ];

              for (let productItem of currentProductsTypeItems)
                productItem.type = itemProductType;

              if (currentProductsTypeItems && currentProductsTypeItems.length)
                currentState = [...currentState, ...currentProductsTypeItems];
            }
          }
        }
      }
    }

    if (currentState && currentState.length)
      setHistoryPaymets(currentState.slice(0, 14));
    // console.log("HOSTORY currentState", currentState);
  };

  const basketItem = (
    item = {},
    index = 0,
    dataList = [],
    isLoading = true,
    isTop = false
  ) => {
    const {
      id = null,
      img = "",
      image = "",
      title = "",
      name = "",
      text,
      price = 0,
      category_product_id = null,
      count = null,
      type = "",
    } = item;
    let category = "";

    const handleItemPress = () => {
      if (!isLoading && isTopProduct)
        navigation.setOptions({ addBasketProductById });

      isTopProduct
        ? navigation.navigate("ItemBasketProduct", {
            item,
          })
        : null;
    };

    const isTopProduct = !(
      count !== undefined &&
      count !== null &&
      type &&
      type.trim()
    );

    if (
      item &&
      category_product_id &&
      !isNaN(+category_product_id) &&
      !isLoading &&
      !productsCategoriesLoading &&
      productsCategories.length
    ) {
      category = productsCategories.filter(
        (item) => +item.id === +category_product_id
      );

      category =
        (category && category.length && category[0] && category[0].title) || "";
    }

    // console.log(
    //   "HOME BASKET: ",
    //   basket,
    //   ", HISTORY STATE: ",
    //   history,
    //   "count history: ",
    //   countHistoryItems
    // );

    // const isLoading = loading || (item && item.isTest);
    // const Wrap = !isLoading ? Animatable.View : View;

    console.log(" HISTORY ITEM", item);

    const layoutsSkeletonDescription = !isTopProduct
      ? [
          {
            height: (category && category.trim()) || isLoading ? 15 : 0,
            marginBottom: (category && category.trim()) || isLoading ? 5 : 0,
            width: 45,
          },
          { height: 15, width: 55 },
        ]
      : [
          {
            height: 15,
            width: 45,
          },
        ];

    const elementsSkeletonDescription = !isTopProduct ? (
      <>
        {category && category.trim() && count && !isNaN(+count) ? (
          <>
            <Text numberOfLines={1} style={styles.sectionName}>
              {category}
            </Text>

            <Text numberOfLines={1} style={styles.sectionName}>
              Количество: {count}
            </Text>
          </>
        ) : !(count && !isNaN(+count)) ? (
          <Text numberOfLines={1} style={styles.sectionName}>
            {category}
          </Text>
        ) : (
          <Text numberOfLines={1} style={styles.sectionName}>
            Количество: {count}
          </Text>
        )}
      </>
    ) : (
      <Text numberOfLines={1} style={styles.sectionName}>
        {category}
      </Text>
    );

    return (
      <View
        // duration={400}
        // delay={index * 200}
        // animation="fadeInUp"
        style={{
          flex: 1,
          marginLeft: index === 0 ? 16 : 8,
          marginRight:
            dataList &&
            dataList.length - 1 === index &&
            !isLoading &&
            !item.isTest
              ? 16
              : 0,
        }}
      >
        <BoxShadow setting={shadowOpt}>
          <TouchableOpacity
            activeOpacity={isLoading || !isTopProduct ? 1 : 0.8}
            style={styles.item}
            onPress={handleItemPress}
          >
            <SkeletonLoader
              layout={[{ ...styles.pictureListItem }]}
              isLoading={isLoading}
            >
              <Image
                source={
                  img.trim()
                    ? { uri: img }
                    : item.image && item.image.trim()
                    ? { uri: image }
                    : require("../../assets/images/notProductImage.png")
                }
                style={styles.pictureListItem}
              />
            </SkeletonLoader>

            <View
              style={{
                flex: 1,
                height: "100%",
                padding: 8,
                justifyContent: "flex-start",
                position: "relative",
              }}
            >
              <SkeletonLoader
                layout={[{ height: 35, width: "90%", marginBottom: 5 }]}
                isLoading={isLoading}
                containerStyle={{ flex: 1 }}
              >
                <Text numberOfLines={2} style={styles.itemName}>
                  {title && title.trim()
                    ? title
                    : name && name.trim()
                    ? name
                    : "Неизвестный продукт"}
                </Text>
              </SkeletonLoader>

              <SkeletonLoader
                layout={layoutsSkeletonDescription}
                isLoading={isLoading}
                containerStyle={{
                  position: isLoading ? "relative" : "absolute",
                  top: isLoading ? 0 : "40%",
                  left: isLoading ? 0 : 8,
                }}
              >
                {elementsSkeletonDescription}
              </SkeletonLoader>

              <SkeletonLoader
                layout={[
                  { height: 15, width: 50, marginLeft: "auto", marginTop: 5 },
                  {
                    marginTop: 7,

                    width: 75,
                    height: 30,

                    borderRadius: 30,
                    display: !isLoading || isTopProduct ? "flex" : "none",
                  },
                ]}
                containerStyle={{
                  alignItems: "center",
                  justifyContent: "flex-start",
                }}
                isLoading={isLoading}
              >
                {count && !isNaN(+count) && price && !isNaN(+price) ? (
                  <Text style={styles.priceText}>
                    {formattingPrice(count * price)} ₽
                  </Text>
                ) : (
                  <View />
                )}

                <TouchableOpacity
                  activeOpacity={0.7}
                  style={{
                    position: "absolute",
                    bottom: 0,
                    left: 0,
                    borderRadius: 30,
                    backgroundColor: colors.orange,
                    paddingVertical: 8,
                    width: "100%",
                    alignItems: "center",
                  }}
                  onPress={async () => {
                    if (!isTopProduct) {
                      if (
                        !isLoading &&
                        count &&
                        !isNaN(+count) &&
                        price &&
                        !isNaN(+price) &&
                        type &&
                        type.trim() &&
                        id &&
                        !isNaN(+id)
                      ) {
                        // handleLinkOpen(item.linkPaymentAgain);

                        const result = await addBasketProductById({
                          id,
                          count,
                          type,
                        });

                        // console.log("TRY AGAIN ADD TO BASKET response: ", result);

                        if (
                          result &&
                          result.response &&
                          result.response.status &&
                          result.response.status === "success"
                        ) {
                          ToastView(
                            `Повторный заказ успешно добавлен в корзину`,
                            1000
                          );

                          navigation.navigate("Basket");
                        } else
                          ToastView(
                            `К сожалению, произошла ошибка.\nОбратитесь в поддержку по телефону +7 495 797 94 27.`,
                            2500
                          );
                      }
                    } else handleItemPress();
                  }}
                >
                  <Text
                    style={[
                      fonts.normal,
                      {
                        fontSize: width < 400 ? 11 : 13,
                        color: colors.white,
                      },
                    ]}
                  >
                    {!isTopProduct ? "Повторить" : "Заказать"}
                  </Text>
                </TouchableOpacity>
              </SkeletonLoader>
            </View>
          </TouchableOpacity>
        </BoxShadow>
      </View>
    );
  };

  const headerSectionTabs = [
    { id: 1, title: "PMP Sport" },
    { id: 2, title: "PMP Sport Online" },
    { id: 3, title: "PMP Beauty" },
    {
      id: 4,
      title: "PMP Food",
    },
  ];

  const renderSlide = (
    slide = {},
    onSlidePress = () => {},
    isLoading = true
  ) => (
    <SkeletonProductItem
      isLoading={isLoading}
      product={slide}
      onProductPress={onSlidePress}
    />
  );

  const getConstantlyBottomChildren = (item = {}) => [
    {
      skeletonLayoutStyle: [
        {
          marginTop: 20,
          width: 100,
          height: 50,
        },
      ],
      content: (
        <Text
          style={[
            fonts.normal,
            {
              marginHorizontal: 13,
              textAlign: "center",
              fontSize: 14,
              color: colors.title,
              opacity: 0.5,
              marginTop: 18,
            },
          ]}
        >
          До статуса постоянного клиента
        </Text>
      ),
    },
  ];

  const getTrainingsBalanceBottomChildren = (item = {}) => [
    {
      skeletonLayoutStyle: [{ marginTop: 20, width: 85, height: 25 }],
      content: (
        <Text
          style={[
            fonts.normal,
            {
              fontSize: 20,
              color: colors.title,
              marginTop: 20,
              textAlign: "center",
            },
          ]}
        >
          {item && item.title ? item.title : "Неизвестное название"}
        </Text>
      ),
    },
    {
      skeletonLayoutStyle: [{ marginTop: 7, width: 80, height: 16 }],
      content: (
        <Text style={[styles.card_title, fonts.normal]}>
          {item.balance
            ? "До " +
              moment()
                .add(10 - (item.balance || 0), "days")
                .calendar()
            : "Не активно"}
        </Text>
      ),
    },
    {
      skeletonLayoutStyle: [
        {
          marginTop: 15,
          width: 100,
          height: 30,
          borderRadius: 30,
        },
      ],
      content: (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{
            backgroundColor: colors.orange,
            borderRadius: 30,
            paddingVertical: 8,
            paddingHorizontal: 20,
            marginTop: 15,
          }}
          onPress={() =>
            navigation.navigate("ListServices", {
              servicesTypeName: item.title,
              activeTypesNameTabIndex: item.trainerServiceTypeId || 0,
              isPay: !item.balance,
              isAll: !item.balance,
            })
          }
        >
          <Text
            style={[
              fonts.normal,
              {
                fontSize: 13,
                color: colors.white,
              },
            ]}
          >
            {item.balace === 0 ? "Купить" : "Записаться"}
          </Text>
        </TouchableOpacity>
      ),
    },
  ];

  const renderTrainingsBalanceItem = (
    item = {},
    i = 0,
    dataList = [],
    loading = true,
    settingsAndRestProps = {}
  ) => {
    const { isConstantly = false } = settingsAndRestProps;

    const currentStatus = isConstantly
      ? 100 - constantlyStatus
      : item && item.balance
      ? item.balance
      : 0;
    const toNumberText = isConstantly
      ? constantlyStatus + "%"
      : item && item.balance
      ? item.balance
      : 0;

    return (
      <InfoCardWithGraphicsIndicator
        key={`card-${i}`}
        index={i}
        item={item}
        currentStatus={currentStatus}
        toNumberText={toNumberText}
        loading={loading}
        {...settingsAndRestProps}
        fillIndicatorColor={"#0FD8D8"}
        indicatorColor={"#cff7f7"}
        bottomChildren={
          !isConstantly
            ? getTrainingsBalanceBottomChildren(item)
            : getConstantlyBottomChildren(item)
        }
      />
    );
  };

  const renderCurrentSectionTabActive = () => {
    switch (activeTab) {
      // case 0:
      //   return (
      //     <ListWithInfo
      //       mocsDataOffsetTop={175}
      //       mocsDataListElementSize={200}
      //       defaultHeight={250}
      //       isHorizontalList={true}
      //       titleEmpty={"Нет данных"}
      //       heightSizeFromDimensionsScreenSize={false}
      //       data={trainingsBalanceData}
      //       error={trainingsBalanceFailed}
      //       loading={trainingsBalanceLoading}
      //       wrapListParams={{
      //         showsHorizontalScrollIndicator: false,
      //         horizontal: true,
      //         contentContainerStyle: styles.diagramCard,
      //       }}
      //       restRenderListItemProps={{
      //         duration: 450,
      //         delay: 500,
      //         title: "Баланс тренировок",
      //         animation: "fadeInUp",
      //         endValue: 10,
      //       }}
      //       elementKeyName={"trainings-card-item-"}
      //       renderListItem={renderTrainingsBalanceItem}
      //     />
      //   );
      //
      // case 1:
      //   return (
      //     <ListWithInfo
      //       mocsDataOffsetTop={175}
      //       mocsDataListElementSize={200}
      //       defaultHeight={250}
      //       isHorizontalList={true}
      //       titleEmpty={"Нет данных"}
      //       heightSizeFromDimensionsScreenSize={false}
      //       data={[{ id: 0 }]}
      //       error={constantlyStatusFailed}
      //       loading={constantlyStatusLoading}
      //       wrapListParams={{
      //         showsHorizontalScrollIndicator: false,
      //         horizontal: true,
      //         contentContainerStyle: styles.diagramCard,
      //       }}
      //       restRenderListItemProps={{
      //         duration: 450,
      //         delay: 500,
      //         title: "Программа лояльности",
      //         animation: "fadeInUp",
      //         endValue: 100,
      //         currentStatus: 100 - constantlyStatus,
      //         toNumberText: constantlyStatus + "%",
      //         toNumberStyle: { fontSize: 20, lineHeight: 19 },
      //         isConstantly: true,
      //       }}
      //       elementKeyName={"constantly-card-item-"}
      //       renderListItem={renderTrainingsBalanceItem}
      //     />
      //   );
      case 0:
        return (
          <View style={styles.diagramCard}>
            {!trainingsBalanceFailed ? (
              (
                trainingsBalanceData || [
                  { id: 3, title: "..." },
                  { id: 4, title: "..." },
                ]
              ).map((item, index) => {
                console.log(
                  "Balance item",
                  item.title,
                  "delay",
                  index * 500,
                  "trainingsBalanceData",
                  trainingsBalanceData
                );

                return (
                  <InfoCardWithGraphicsIndicator
                    key={`card-${item && item.id ? item.id : index}`}
                    index={index}
                    item={item}
                    loading={trainingsBalanceLoading}
                    duration={450}
                    delay={index * 450}
                    title={"Баланс тренировок"}
                    animation={"fadeInUp"}
                    endValue={10}
                    currentStatus={/*item.balance || 0*/ 10}
                    toNumberText={item.balance}
                    fillIndicatorColor="#F46F22"
                    indicatorColor="#cff7f7"
                    bottomChildren={[
                      {
                        skeletonLayoutStyle: [
                          { marginTop: 20, width: 85, height: 25 },
                        ],
                        content: (
                          <Text
                            style={[
                              fonts.normal,
                              {
                                fontSize: 20,
                                color: colors.title,
                                marginTop: 20,
                                textAlign: "center",
                              },
                            ]}
                          >
                            {item.title || "Неизвестное название"}
                          </Text>
                        ),
                      },

                      {
                        skeletonLayoutStyle: [
                          { marginTop: 7, width: 80, height: 16 },
                        ],
                        content: (
                          <Text style={[styles.card_title, fonts.normal]}>
                            {item.balance
                              ? "До " + moment().add(62, "days").calendar()
                              : "Не активно"}
                          </Text>
                        ),
                      },

                      {
                        skeletonLayoutStyle: [
                          {
                            marginTop: 15,
                            width: 100,
                            height: 30,
                            borderRadius: 30,
                          },
                        ],
                        content: (
                          <TouchableOpacity
                            activeOpacity={0.7}
                            style={{
                              backgroundColor: colors.orange,
                              borderRadius: 30,
                              paddingVertical: 8,
                              paddingHorizontal: 20,
                              marginTop: 15,
                            }}
                            onPress={() =>
                              navigation.navigate("ListServices", {
                                servicesTypeName: item.title,
                                isPay: !item.balance,
                                isAll: !item.balance,
                                activeTypesNameTabIndex:
                                  item.trainerServiceTypeId || 0,
                              })
                            }
                          >
                            <Text
                              style={[
                                fonts.normal,
                                {
                                  fontSize: 13,
                                  color: colors.white,
                                },
                              ]}
                            >
                              {!item.balance ? "Купить" : "Записаться"}
                            </Text>
                          </TouchableOpacity>
                        ),
                      },
                    ]}
                  />
                );
              })
            ) : !trainingsBalanceLoading &&
              !trainingsBalanceFailed &&
              trainingsBalanceData &&
              !trainingsBalanceData.length ? (
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    width: "100%",
                    textAlign: "center",
                    color: "#000",
                  }}
                >
                  Нет данных
                </Text>
              </View>
            ) : !trainingsBalanceLoading && trainingsBalanceFailed ? (
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    width: "100%",
                    textAlign: "center",
                    color: "#000",
                  }}
                >
                  Ошибка получения данных
                </Text>
              </View>
            ) : null}
          </View>
        );

      case 1:
        return (
          <View style={styles.diagramCard}>
            {!trainingsBalanceFailed ? (
              (
                trainingsBalanceData || [
                  { id: 0, title: "..." },
                  { id: 1, title: "..." },
                ]
              ).map((item, index) => {
                console.log(
                  "BalanceOnline item",
                  item.title,
                  "delay",
                  index * 500,
                  "trainingsBalanceOnlineData",
                  trainingsBalanceData
                );

                return (
                  <InfoCardWithGraphicsIndicator
                    key={`card-${item && item.id ? item.id : index}`}
                    index={index}
                    item={item}
                    loading={trainingsBalanceLoading}
                    duration={450}
                    delay={index * 450}
                    title={"Баланс тренировок"}
                    animation={"fadeInUp"}
                    endValue={10}
                    currentStatus={/*item.balance || 0*/ 10}
                    toNumberText={item.balance}
                    fillIndicatorColor="#F46F22"
                    indicatorColor="#cff7f7"
                    bottomChildren={[
                      {
                        skeletonLayoutStyle: [
                          { marginTop: 20, width: 85, height: 25 },
                        ],
                        content: (
                          <Text
                            style={[
                              fonts.normal,
                              {
                                fontSize: item.title > 30 ? 17 : 18,
                                color: colors.title,
                                marginTop: 20,
                                textAlign: "center",
                              },
                            ]}
                          >
                            {item.title + " Online" || "Неизвестное название"}
                          </Text>
                        ),
                      },

                      {
                        skeletonLayoutStyle: [
                          { marginTop: 7, width: 80, height: 16 },
                        ],
                        content: (
                          <Text style={[styles.card_title, fonts.normal]}>
                            {item.balance
                              ? "До " + moment().add(62, "days").calendar()
                              : "Не активно"}
                          </Text>
                        ),
                      },

                      {
                        skeletonLayoutStyle: [
                          {
                            marginTop: 15,
                            width: 100,
                            height: 30,
                            borderRadius: 30,
                          },
                        ],
                        content: (
                          <TouchableOpacity
                            activeOpacity={0.7}
                            style={{
                              backgroundColor: colors.orange,
                              borderRadius: 30,
                              paddingVertical: 8,
                              paddingHorizontal: 20,
                              marginTop: 15,
                            }}
                            onPress={() =>
                              navigation.navigate("ListServices", {
                                servicesTypeName: item.title,
                                isOnline: true,
                                isAll: !item.balance,
                                isPay: !item.balance,
                                activeTypesNameTabIndex:
                                  item.trainerServiceTypeId || 0,
                              })
                            }
                          >
                            <Text
                              style={[
                                fonts.normal,
                                {
                                  fontSize: 13,
                                  color: colors.white,
                                },
                              ]}
                            >
                              {!item.balance ? "Купить" : "Записаться"}
                            </Text>
                          </TouchableOpacity>
                        ),
                      },
                    ]}
                  />
                );
              })
            ) : !trainingsBalanceLoading &&
              !trainingsBalanceFailed &&
              trainingsBalanceData &&
              !trainingsBalanceData.length ? (
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    width: "100%",
                    textAlign: "center",
                    color: "#000",
                  }}
                >
                  Нет данных
                </Text>
              </View>
            ) : !trainingsBalanceLoading && trainingsBalanceFailed ? (
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    width: "100%",
                    textAlign: "center",
                    color: "#000",
                  }}
                >
                  Ошибка получения данных
                </Text>
              </View>
            ) : null}
          </View>
        );

      case 2:
        return (
          <View style={styles.diagramCard}>
            {!constantlyStatusFailed ? (
              <InfoCardWithGraphicsIndicator
                loading={constantlyStatusLoading}
                duration={450}
                delay={500}
                title={"Программа лояльности"}
                animation={"fadeInUp"}
                endValue={maxValueToConstantly}
                currentStatus={maxValueToConstantly - constantlyStatus}
                toNumberText={constantlyStatus}
                toNumberStyle={{ fontSize: 20, lineHeight: 19 }}
                fillIndicatorColor="#0FD8D8"
                indicatorColor="#cff7f7"
                doneStatus={true}
                bottomChildren={[
                  {
                    skeletonLayoutStyle: [
                      {
                        marginTop: 20,
                        width: 100,
                        height: 50,
                      },
                    ],
                    content: (
                      <Text
                        style={[
                          fonts.normal,
                          {
                            marginHorizontal: 13,
                            textAlign: "center",
                            fontSize: 14,
                            color: colors.title,
                            opacity: 0.5,
                            marginTop: 18,
                          },
                        ]}
                      >
                        До статуса постоянного клиента
                      </Text>
                    ),
                  },
                ]}
              />
            ) : (
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    width: "100%",
                    textAlign: "center",
                    color: "#000",
                  }}
                >
                  Ошибка получения данных
                </Text>
              </View>
            )}
          </View>
        );

      case 3:
        return (
          <View style={{ flex: 1, minHeight: 250 }}>
            {historyPaymets && historyPaymets.length
              ? renderTabProducsData(
                  "История заказов",
                  "Заказов пока не было",
                  "Ошибка получения данных",
                  historyLoading,
                  historyFailed,
                  historyPaymets
                )
              : renderTabProducsData(
                  "Рекомендации",
                  "Рекомендаций пока нет,\n но наша система активно обновляеться",
                  "Ошибка получения данных",
                  topProductsLoading,
                  topProductsError,
                  topProducts,
                  true
                )}
          </View>
        );
    }
  };

  const renderTabProducsData = (
    title = "",
    titleEmpty = "",
    titleError = "",
    loading = true,
    error = null,
    data = [],
    isTop = false
  ) => {
    if (
      title &&
      title.trim() &&
      titleError &&
      titleError.trim() &&
      titleEmpty &&
      titleEmpty.trim()
    ) {
      const isLoadingData = loading || productsCategoriesLoading;
      const isErrorData = error || productsCategoriesError;
      const isExistingData =
        data && data.length && productsCategories && productsCategories.length;

      console.log("data", data, "titleEmpty", titleEmpty);

      return (
        <View style={{ flex: 1, minHeight: 250 }}>
          <Text
            style={[
              fonts.normal,
              { fontSize: 18, color: colors.title, marginLeft: 16 },
            ]}
          >
            {title}
          </Text>

          <ListWithInfo
            mocsDataOffsetTop={175}
            mocsDataListElementSize={200}
            heightSizeFromDimensionsScreenSize={true}
            heightDimensionsContainer={3.2}
            isHorizontalList={true}
            titleEmpty={titleEmpty}
            titleError={titleError}
            existLikeKeys={true}
            containerInfoStyle={{
              height: 250,
              maxHeight: 250,
            }}
            data={data}
            error={isErrorData}
            loading={isLoadingData}
            wrapListParams={{
              showsHorizontalScrollIndicator: false,
              horizontal: true,
              style: styles.picturesContainer,
              justifyContent: "flex-start",
            }}
            elementKeyName={"product-item-"}
            renderListItem={basketItem}
            restRenderListItemProps={isTop}
          />
        </View>
      );
    }
  };

  const SectionSliders = (
    <View style={{ paddingBottom: 25 }}>
      <SectionWithSlider
        activeSlideIndex={activeProductSlideIndex}
        handleSlideRender={renderSlide}
        isLoading={newProgramsLoading}
        data={newProgramsData}
        requestFailed={newProgramsError}
        title={"Новые продукты"}
        handleActiveInviteSlideIndexSet={setActiveProductSlideIndex}
        buttonHeadTitle={"Все"}
        onButtonHeadPress={() =>
          navigation.navigate("ProgramsScreen", { headTitle: "Новые продукты" })
        }
      />

      <SectionWithSlider
        activeSlideIndex={activeInviteSlideIndex}
        handleSlideRender={renderSlide}
        isLoading={specialProgramsLoading}
        data={specialProgramsData}
        requestFailed={specialProgramsError}
        title={"Особые предложения"}
        handleActiveInviteSlideIndexSet={setActiveInviteSlideIndex}
        buttonHeadTitle={"Все"}
        onButtonHeadPress={() =>
          navigation.navigate("ProgramsScreen", {
            isInvites: true,
            headTitle: "Особые предложения",
          })
        }
      />
    </View>
  );

  return (
    <View style={{ flex: 1, backgroundColor: "#FFF", position: "relative" }}>
      <SafeAreaView>
        <View style={styles.header}>
          <Text style={styles.title}>Главная</Text>

          <View style={{ flexDirection: "row" }}>
            <BasketState
              count={basketCount}
              style={{ top: 2 }}
              navigation={navigation}
            />

            <TouchableOpacity
              onPress={() => navigation.navigate("MainMenu", { basket })}
              activeOpacity={0.5}
              style={{ padding: 20 }}
            >
              <Image source={require("../../assets/images/menuIcon.png")} />
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>

      <ScrollView showsVerticalScrollIndicator={false}>
        <Tabs
          firstTabTitle={"PMP Sport"}
          offsetHorizontal={16}
          style={{
            flex: 1,
            marginTop: 15,
            marginBottom: 20,
            maxHeight: "auto",
          }}
          onTabActiveIndexSet={chooseTab}
          listData={headerSectionTabs}
        />

        <View style={{ minHeight: 220 }}>
          {renderCurrentSectionTabActive()}

          <Animatable.View
            duration={400}
            delay={600}
            animation="fadeIn"
            style={{ marginTop: 20, paddingHorizontal: 16 }}
          >
            {activeTab === 3 && (
              <ButtonInput
                title="ПЕРЕЙТИ В МЕНЮ"
                onClick={() => navigation.navigate("MenuList")}
              />
            )}

            {activeTab !== 3 && (
              <ButtonInput
                title="ПЕРЕЙТИ В УСЛУГИ"
                onClick={() =>
                  navigation.navigate(
                    activeTab === 0 || activeTab === 1
                      ? "ListServices"
                      : "ListServiceSpecialists",
                    {
                      isOnline: activeTab === 1,
                      isPay: activeTab === 0 || activeTab === 1,
                      isAll: true,
                    }
                  )
                }
              />
            )}
          </Animatable.View>

          {SectionSliders}
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state) => ({
  basket: state.BasketReducer.basket,
  basketCount: state.currentUser.basketCount,

  topProductsLoading: state.StoreReducer.topProductsLoading,
  topProductsError: state.StoreReducer.topProductsError,
  topProducts: state.StoreReducer.topProducts,

  productsCategoriesLoading: state.StoreReducer.productsCategoriesLoading,
  productsCategories: state.StoreReducer.productsCategories,
  productsCategoriesError: state.StoreReducer.productsCategoriesError,

  historyLoading: state.BasketReducer.historyLoading,
  history: state.BasketReducer.history,
  countHistoryItems: state.BasketReducer.countHistoryItems,
  countItems: state.BasketReducer.countItems,
  historyFailed: state.BasketReducer.historyFailed,

  trainingsBalanceLoading: state.mainReducer.trainingsBalanceLoading,
  trainingsBalanceData: state.mainReducer.trainingsBalanceData,
  trainingsBalanceFailed: state.mainReducer.trainingsBalanceFailed,

  // trainingsBalanceOnlineLoading:
  //   state.mainReducer.trainingsBalanceOnlineLoading,
  // trainingsBalanceOnlineData: state.mainReducer.trainingsBalanceOnlineData,
  // trainingsBalanceOnlineFailed: state.mainReducer.trainingsBalanceOnlineFailed,

  constantlyStatusLoading: state.mainReducer.constantlyStatusLoading,
  constantlyStatus: state.mainReducer.constantlyStatus,
  maxValueToConstantly: state.mainReducer.maxValue,
  constantlyStatusFailed: state.mainReducer.constantlyStatusFailed,

  specialProgramsLoading: state.ProgramsReducer.specialProgramsLoading,
  specialProgramsData: state.ProgramsReducer.specialProgramsData,
  specialProgramsError: state.ProgramsReducer.specialProgramsError,

  newProgramsLoading: state.ProgramsReducer.newProgramsLoading,
  newProgramsData: state.ProgramsReducer.newProgramsData,
  newProgramsError: state.ProgramsReducer.newProgramsError,
  // existingNotificationOpened: state.currentUser.existingNotificationOpened,
});

const mapDispatchToProps = {
  setCurrentUser,

  getUserNotifications,

  getCurrentBasketState,

  addBasketProductById,

  getTrainingsBalance,
  // getTrainingsBalanceOnline,

  getStatusToConstantlyClient,
  getTopProducts,
  getProductsCategories,

  getHistoryState,

  getGeneratePaymentLink,

  getSpecialPrograms,
  getNewPrograms,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const styles = StyleSheet.create({
  header: {
    paddingLeft: 16,
    paddingTop: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  title: {
    fontSize: sizes.h1,
    fontFamily: fonts.normal.fontFamily,
  },
  headerCarousel: {
    height: "100%",
    maxHeight: 40,

    flexDirection: "row",
    marginBottom: 25,
    marginTop: 16,
  },
  contentTitle: {
    backgroundColor: colors.orange,
    height: 120,
    justifyContent: "space-around",
  },
  diagramCard: {
    minHeight: 250,
    marginHorizontal: sizes.margin,
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1,
    width: width - 16 * 2,
  },
  card: {
    width: width / 2 - 22,
    backgroundColor: colors.white,
    borderWidth: 1,
    borderColor: "rgba(21, 28, 38, 0.12)",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "space-between",
    paddingTop: 19,
    paddingBottom: 61,
    position: "relative",
  },
  card_container: {
    height: 70,
    width: 70,
    marginTop: 19,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  pie_value: {
    position: "absolute",
    color: colors.title,
    fontSize: 28,
  },
  card_title: {
    fontSize: 14,
    color: colors.title,
    opacity: 0.5,
    marginTop: 7,
  },
  fullFill: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    minHeight: 250,
  },
  containerSlide: {
    width: width - 32,
    position: "relative",
  },
  slideGradient: {
    width: width - 32,
    height: "65%",
    position: "absolute",
    bottom: 0,
  },
  infoSlide: {
    flex: 1,
    height: 72,
    backgroundColor: "#EAEAEA",
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 16,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  img: {
    width: width - 32,
    height: 184,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    resizeMode: "cover",
  },
  titleSlide: {
    width: "100%",
    maxWidth: "98%",
    fontSize: 18,
    lineHeight: 18,
    fontWeight: "500",
    color: "#000000",
    fontFamily: "PFDin500",

    alignSelf: "flex-start",
  },
  priceSlide: {
    fontSize: 18,
    color: "#F46F22",
    fontWeight: "500",
    textTransform: "uppercase",
    fontFamily: "PFDin500",

    alignSelf: "flex-end",
  },
  picturesContainer: {
    marginTop: 15,
    paddingBottom: 5,
  },
  pictureListItem: {
    width: (dimensions.width - 30 - 16) / 3,
    borderTopRightRadius: 3,
    borderTopLeftRadius: 3,
    height: 116,
    resizeMode: "cover",
  },
  itemName: {
    fontSize: 14,
    lineHeight: 16,
    opacity: 0.87,
    fontFamily: fonts.normal.fontFamily,
  },
  item: {
    width: (dimensions.width - 48) / 3,
    //height: width < 400 ? 225 : 244,
    flex: 1,
    height: "100%",
    display: "flex",
    backgroundColor: "#FFF",
    flexWrap: "wrap",
  },
  sectionName: {
    fontSize: 12,
    color: "rgba(0, 0, 0, 0.87)",
    opacity: 0.54,
    fontFamily: fonts.normal.fontFamily,
  },
  priceText: {
    fontSize: 12,
    color: colors.orange,
    fontFamily: fonts.normal.fontFamily,
    textAlign: "right",
    //paddingVertical: 8,
    paddingBottom: 8,
    width: "100%",
    //position: 'absolute',
    //top: 10,
    //right: 8
  },
  columnWrapperStyle: {
    justifyContent: "center",
  },
});
