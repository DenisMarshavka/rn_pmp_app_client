import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
} from "react-native";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import { connect } from "react-redux";
import { MaterialIcons } from "@expo/vector-icons";

import { Background2 } from "../../../components/Background2";
import Header from "../../../components/Headers/Header";
import { colors, fonts, sizes } from "../../../constants/theme";
import {
  getCurrentPointsByUserId,
  getMaxPointsByUser,
} from "../../../actions/UserActions";

const Points = ({
  getCurrentPointsByUserId = () => {},
  getMaxPointsByUser = () => {},

  currentPointsLoading = false,
  currentPoints = 0,
  currentPointsFailed = null,

  maxPointsLoading = false,
  maxPoints = 0,
  maxPointsFailed = null,

  currentUser = null,
}) => {
  const [currentFill, setCurrentFill] = useState(0);
  const [getPercentResultFinished, setGetPercentResultFinished] = useState(
    false
  );

  const getCurrentPercentFill = (
    currentPointsCount = 0,
    maxPointsCount = 0
  ) => {
    let fill = 0;

    if (maxPointsCount) {
      let onePercent = maxPointsCount / 100;

      console.log(
        "currentPointsCount",
        currentPointsCount,
        "maxPointsCount",
        maxPointsCount,
        "onePercent",
        onePercent,
        currentPointsCount / onePercent
      );

      if (onePercent && currentPointsCount) {
        fill = Math.ceil(currentPointsCount / onePercent);
      } else fill = 0;
    } else fill = 0;

    return fill;
  };

  useEffect(() => {
    getMaxPointsByUser();

    return () => {
      setGetPercentResultFinished(false);
    };
  }, []);

  useEffect(() => {
    if (!maxPointsLoading && currentUser && currentUser.id !== false)
      getCurrentPointsByUserId(currentUser.id);
  }, [maxPointsLoading, currentUser]);

  useEffect(() => {
    if (!currentPointsLoading) {
      setCurrentFill(getCurrentPercentFill(currentPoints, maxPoints));
    }
  }, [currentPoints, currentPointsLoading]);

  return (
    <Background2>
      <Header
        title="Мои баллы"
        backText
        backTitle
        backStyle={{ paddingTop: 7 }}
      />

      {currentPointsLoading || maxPointsLoading ? (
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            minHeight: Dimensions.get("screen").height / 1.2,
            paddingBottom: 23,
          }}
        >
          <ActivityIndicator size={"large"} color={colors.mainOrange} />
        </View>
      ) : !currentPointsLoading &&
        !currentPointsFailed &&
        !maxPointsLoading &&
        !maxPointsFailed ? (
        <>
          <View style={styles.points}>
            <View style={styles.progressBody}>
              <AnimatedCircularProgress
                size={264}
                width={25}
                fill={currentFill}
                tintColor="#F69359"
                onAnimationComplete={() => setGetPercentResultFinished(true)}
                backgroundColor="#CBCBCB"
                rotation={0}
                lineCap={"round"}
              >
                {(currentFill) => (
                  <>
                    {Math.ceil(currentFill) !== 100 ? (
                      <Text
                        style={{
                          fontSize: 58,
                          fontFamily: fonts.thin.fontFamily,
                          opacity: getPercentResultFinished ? 1 : 0,
                          lineHeight: 60,
                        }}
                      >
                        {Math.ceil(currentFill)}%
                      </Text>
                    ) : (
                      <MaterialIcons
                        name="done"
                        size={120}
                        color={colors.orange}
                      />
                    )}
                  </>
                )}
              </AnimatedCircularProgress>
            </View>

            <View style={styles.pointsTextView}>
              {Math.ceil(currentFill) !== 100 ? (
                <Text style={styles.pointsText}>{`${currentPoints || 0}/${
                  maxPoints || 0
                } баллов`}</Text>
              ) : (
                <>
                  <Text
                    style={{
                      ...styles.pointsText,
                      fontSize: 21,
                      marginBottom: 15,
                      ...fonts.h1,
                      textAlign: "center",
                    }}
                  >
                    Поздравляем!
                  </Text>

                  <Text
                    style={{
                      ...styles.pointsText,
                      fontSize: 17,

                      textAlign: "center",
                    }}
                  >{`Цель достигнута!\nЗаберите ваш подарок на рецепшине`}</Text>
                </>
              )}
            </View>

            {Math.ceil(currentFill) !== 100 ? (
              <View style={styles.pointsDescription}>
                <Text style={styles.pointsDescription}>
                  До получения подарка
                </Text>
              </View>
            ) : null}
          </View>
        </>
      ) : (!currentPointsLoading && currentPointsFailed) ||
        (!maxPointsLoading && maxPointsFailed) ? (
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            minHeight: Dimensions.get("screen").height / 1.3,
          }}
        >
          <Text
            style={{
              width: "100%",
              textAlign: "center",
              color: "#000",
            }}
          >
            Ошибка получения данных
          </Text>
        </View>
      ) : null}
    </Background2>
  );
};

const mapStateToProps = (state) => ({
  currentPointsLoading: state.currentUser.currentPointsLoading,
  currentPoints: state.currentUser.currentPoints,
  currentPointsFailed: state.currentUser.currentPointsFailed,

  maxPointsLoading: state.currentUser.maxPointsLoading,
  maxPoints: state.currentUser.maxPoints,
  maxPointsFailed: state.currentUser.maxPointsFailed,

  currentUser: state.currentUser.currentUser,
});

export default connect(mapStateToProps, {
  getCurrentPointsByUserId,
  getMaxPointsByUser,
})(Points);

const styles = StyleSheet.create({
  points: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  progressBody: {
    paddingTop: "20%",
    width: 264,
  },
  pointsTextView: {
    paddingTop: 44,
  },
  pointsText: {
    fontSize: 40,
    fontFamily: fonts.thin.fontFamily,
  },
  pointsDescription: { fontSize: 16, fontFamily: fonts.light.fontFamily },
});
