import React from 'react';
import { Animated, Dimensions, FlatList, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ProgressCircle } from 'react-native-svg-charts';

import { Background } from '../../components/Background';
import { Header } from '../../components/Header';
import { colors, fonts, sizes } from '../../constants/theme';

const { width } = Dimensions.get("window");

export const FitnessScreen = ({ navigation }) => {
  const leftValuePie = 5;
  const rightValuePie = 3;
  const scrollX = new Animated.Value(0);
  const illustrations = [
    { id: 1, source: require("../../assets/images/carousel1.png") },
    { id: 2, source: require("../../assets/images/carousel1.png") },
    { id: 3, source: require("../../assets/images/carousel1.png") }
  ];

  return (
    <Background>
      <Header
        title="fitness"
        deg="180"
        navigation={navigation}
        pageName="ProductScreen"
      ></Header>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.contentTitle}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Image
              source={require("../../assets/icons/fitness.png")}
              style={{ marginHorizontal: 21 }}
            />
            <TouchableOpacity
              onPress={() => navigation.navigate("FitnessStudio")}
            >
              <Text
                style={[{ color: colors.white }, fonts.normal, fonts.upper]}
              >
                fitness
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Image
              source={require("../../assets/icons/beauty.png")}
              style={{ marginHorizontal: 21, opacity: sizes.opcity }}
            />
            <TouchableOpacity
              onPress={() => navigation.navigate("BeautyStudio")}
            >
              <Text
                style={[
                  {
                    color: colors.black,
                    opacity: sizes.opcity
                  },
                  fonts.normal,
                  fonts.upper
                ]}
              >
                beauty
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.diagramCard}>
          <View style={styles.card}>
            <View style={styles.card_container}>
              <Text style={[styles.pie_value, fonts.bold]}>{leftValuePie}</Text>
              <ProgressCircle
                style={{ width: "100%", height: "100%" }}
                progress={leftValuePie / 10}
                progressColor="#F46F22"
                backgroundColor="#fde2d3"
                startAngle={-Math.PI * 1}
                endAngle={Math.PI * 1}
              />
            </View>

            <Text style={[styles.card_title, fonts.normal]}>До 11.11.2019</Text>
          </View>
          <View style={styles.card}>
            <View style={styles.card_container}>
              <Text style={[styles.pie_value, fonts.bold]}>
                {rightValuePie}
              </Text>
              <ProgressCircle
                style={{ width: "100%", height: "100%" }}
                progress={rightValuePie / 10}
                progressColor="#0FD8D8"
                backgroundColor="#cff7f7"
                startAngle={-Math.PI * 1}
                endAngle={Math.PI * 1}
              />
            </View>
            <Text
              style={[
                styles.card_title,
                {
                  marginHorizontal: 32,
                  textAlign: "center"
                },
                fonts.normal
              ]}
            >
              До статуса постоянного клиента
            </Text>
          </View>
        </View>
        <View style={{ marginTop: 27, paddingHorizontal: sizes.margin }}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <Text
              style={[
                {
                  color: "#1E2022"
                },
                fonts.bold,
                fonts.h2
              ]}
            >
              Особые предложения
            </Text>
            <Text
              style={[
                {
                  color: "#F46F22",
                  fontSize: 15
                },
                fonts.upper,
                fonts.bold
              ]}
            >
              Все
            </Text>
          </View>
          <View
            style={{
              marginTop: 23,
              width: "100%"
            }}
          >
            <FlatList
              horizontal
              pagingEnabled
              scrollEnabled
              showsHorizontalScrollIndicator={false}
              scrollEventThrottle={16}
              snapToAlignment="center"
              data={illustrations}
              extraDate={false}
              keyExtractor={(item, index) => `${item.id}`}
              renderItem={({ item }) => (
                <Image
                  key={`carousel-image=${item.id}`}
                  source={item.source}
                  style={{
                    width: width - 32,
                    height: 257,
                    overflow: "visible"
                  }}
                />
              )}
              onScroll={Animated.event([
                {
                  nativeEvent: { contentOffset: { x: scrollX } }
                }
              ])}
            />
            <View
              style={{
                marginVertical: 34,
                flexDirection: "row",
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  width: "100%",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              >
                {illustrations.map((item, index) => {
                  const stepPosition = Animated.divide(scrollX, width);
                  const color = stepPosition.interpolate({
                    inputRange: [index - 1, index, index + 1],
                    outputRange: [
                      "rgba(0, 0, 0, 0.2)",
                      "#0FD8D8",
                      "rgba(0, 0, 0, 0.2)"
                    ],
                    extrapolate: "clamp"
                  });
                  return (
                    <Animated.View
                      key={`animated-point-${index}`}
                      style={{
                        width: 10,
                        height: 10,
                        borderRadius: 5,
                        marginHorizontal: 5,
                        backgroundColor: color
                      }}
                    ></Animated.View>
                  );
                })}
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  contentTitle: {
    backgroundColor: colors.orange,
    height: 120,
    justifyContent: "space-around"
  },
  diagramCard: {
    marginHorizontal: sizes.margin,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  card: {
    height: 150,
    width: width / 2 - 22,
    backgroundColor: colors.white,
    borderWidth: 1,
    borderColor: "rgba(21, 28, 38, 0.12)",
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    alignItems: "center"
  },
  card_container: {
    position: "relative",
    height: 70,
    width: 70,
    marginTop: 19,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  pie_value: {
    position: "absolute",
    color: colors.title,
    fontSize: 28
  },
  card_title: {
    position: "absolute",
    fontSize: 14,
    color: colors.title,
    opacity: 0.5,
    bottom: 15
  }
});
