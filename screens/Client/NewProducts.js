import React from 'react';
import { Animated, Dimensions, FlatList, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ProgressCircle } from 'react-native-svg-charts';

import { Background } from '../../components/Background';
import  BackHeader  from '../../components/Headers/BackHeader';
import { colors, fonts, sizes } from '../../constants/theme';
import * as Animatable from 'react-native-animatable';

const { width } = Dimensions.get("window");

const NewProducts = ({ navigation }) => (
  <Background>
    <BackHeader/>
      <View style={styles.container}>
        <Text style={[styles.title, fonts.normal, fonts.title, {paddingBottom: 10}]}>Новые продукты</Text>

        <ScrollView showsVerticalScrollIndicator={false}>
          <TouchableOpacity onPress={() => navigation.navigate('ProgramScreen')} style={{width: '100%', position: 'relative', borderRadius: 10, marginBottom: 10,}}>
            <Image
              source={require("../../assets/images/fitness-studio1.png")}
              style={styles.img}
            />
              <View style={styles.infoSlide}>
                <Text style={styles.titleSlide}>Online программы</Text>
                <Text style={styles.priceSlide}>1000 ₽</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate('ProgramScreen')} style={{width: '100%', position: 'relative', borderRadius: 10, marginBottom: 10,}}>
              <Image
                source={require("../../assets/images/carousel1.png")}
                style={styles.img}
              />
                <View style={styles.infoSlide}>
                  <Text style={styles.titleSlide}>Online программы</Text>
                  <Text style={styles.priceSlide}>1000 ₽</Text>
                </View>
            </TouchableOpacity>
        </ScrollView>
      </View>
  </Background>
);

export default NewProducts;

const styles = StyleSheet.create({
  title: {
    color: colors.title,
    marginVertical: 10
},
container: {
    flex: 1,
    paddingHorizontal: 16
},
slideGradient: {
    width: '100%',
    height: 72,
  },
  infoSlide: {
    height: 72,
    backgroundColor: '#EAEAEA',
    width: '100%',
    padding: 16,
    justifyContent: 'space-between',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  titleSlide: {
    fontSize: 18,
    fontWeight: "500",
    color: '#000000',
    fontFamily: 'PFDin500'
  },
  priceSlide: {
    fontSize: 18,
    color: '#F46F22',
    fontWeight: "500",
    textTransform: 'uppercase',
    fontFamily: 'PFDin500',
  },
  img: {
    width: '100%',
    height: 184,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    resizeMode: 'cover'
  }
});
