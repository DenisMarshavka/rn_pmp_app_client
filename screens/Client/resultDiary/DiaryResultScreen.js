import React, { useState, useEffect } from "react";
import {
  ImageBackground,
  StyleSheet,
  View,
  Text,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ActivityIndicator,
} from "react-native";
import moment from "moment";
import { LineChart, Grid, YAxis, XAxis } from "react-native-svg-charts";
import { G, Line } from "react-native-svg";
import * as shape from "d3-shape";
import Carousel from "react-native-snap-carousel";

import { WeightAPI } from "../../../api/diary/weights";
import { BodyAPI } from "../../../api/diary/bodies";
import Header from "../../../components/Headers/Header";
import {
  CustomBottomButton,
  MeasurementDiaryListItem,
} from "../../../components";
import { colors } from "../../../styles";
import { fonts } from "../../../constants/theme";
import { getSizeSkeletonLoaders } from "../../../utils";
import { USES_DEFAULT_IMPLEMENTATION } from "react-native-maps/lib/components/decorateMapComponent";
import { UserAPI } from "../../../api/user";
import { connect } from "react-redux";

const { width: viewportWidth, height: viewportHeight } = Dimensions.get(
  "window"
);

let month = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
];

const axesSvg = { fontSize: 10, fill: "gray" };
const verticalContentInset = { top: 10, bottom: 20, left: 25 };
const verticalBodyContentInset = { top: 0, bottom: 390, left: 25 };
const currentOrderDate = new Date().getDate();
const XAxisData = [...Array(moment().daysInMonth())]
  .map((v, index) => index)
  .filter((date) => date < currentOrderDate);
const YAxisWeightData = [60, 70, 80, 90, 100, 110, 120];
const YAxisData = [70, 80, 90, 100, 110, 120];

const currentYuear = moment().format("YYYY");
const startMonth = moment().format("M");

const DiaryResultScreen = (props) => {
  const { isFemale = true } =
    (props.route && props.route.params && props.route.params) || {};

  const [currentTab, setCurrentTab] = useState(true);

  const [daysView, setDaysView] = useState([]);
  const [daysProp, setDaysProp] = useState([]);
  const [currentCarouselIndex, setCurrentCarouselIndex] = useState(0);
  const [bodies, setBodies] = useState([]);
  const [bodiesData, setBodiesData] = useState([]);

  const [weights, setWeights] = useState([]);
  const [weightsData, setWeightsData] = useState([]);

  const [currentMonth, setCurrentMonth] = useState(null);
  const [firstMonth, setFirsMonth] = useState(0);

  const [weightsLoading, setWeightsLoading] = useState(false);
  const [bodiesLoading, setBodiesLoading] = useState(false);

  useEffect(() => {
    getData();

    if (startMonth !== false) setCurrentMonth(startMonth - 1);
  }, []);

  const getData = async (index) => {
    await fetchWeightAPI(index);
    await fetchBodiesAPI(index);
  };

  async function fetchWeightAPI(index) {
    // let day = getDateData(index)

    const currentStartDefaultMonth =
      startMonth && startMonth < 10 ? "0" + startMonth : startMonth;
    const month =
      index && !isNaN(+index) && index < 10
        ? "0" + index + 1
        : index && !isNaN(+index)
        ? index + 1
        : currentStartDefaultMonth;

    console.log(
      "currentStartDefaultMonth",
      currentStartDefaultMonth,
      "month",
      month
    );

    setWeightsLoading(true);

    try {
      const _weights = await WeightAPI.getWeightsByDate(
        `${currentYuear}-${
          month !== false
            ? month
            : currentStartDefaultMonth !== false
            ? currentStartDefaultMonth
            : startMonth
        }-01`
      );

      setWeights(
        _weights.data.weights.map((_weight) => ({
          date: moment(_weight.createdAt).format("DD.MM.YYYY"),
          weight: _weight.weight,
        }))
      );

      setWeightsData(
        _weights &&
          _weights.data &&
          _weights.data.graphics &&
          _weights.data.graphics.weight
          ? _weights.data.graphics.weight
          : []
      );
      setWeightsLoading(false);
    } catch (e) {
      console.log("Error WeightAPI.getWeightsByDate --- ", e);

      setWeightsLoading(false);
    }
  }

  async function fetchBodiesAPI(index = 0) {
    // let day = getDateData(index);

    try {
      setBodiesLoading(true);

      const currentStartDefaultMonth =
        startMonth && startMonth < 10 ? "0" + startMonth : startMonth;
      const month =
        index && !isNaN(+index) && index < 10
          ? "0" + index + 1
          : index && !isNaN(+index)
          ? index + 1
          : currentStartDefaultMonth;

      console.log(
        "currentStartDefaultMonth",
        currentStartDefaultMonth,
        "month",
        month
      );

      const _bodies = await BodyAPI.getBodiesByDate(
        `${currentYuear}-${
          month
            ? month
            : currentStartDefaultMonth
            ? currentStartDefaultMonth
            : startMonth
        }-01`
      );

      setBodies(
        _bodies.data.bodies.map((_body) => ({
          date: moment(_body.createdAt).format("DD.MM.YYYY"),
          chest: _body.chest,
          waist: _body.waist,
          hips: _body.hips,
        }))
      );

      setBodiesData(
        _bodies && _bodies.data && _bodies.data.graphics
          ? _bodies.data.graphics
          : []
      );
      setBodiesLoading(false);
    } catch (e) {
      console.log("Error BodyAPI.getBodiesByDate --- ", e);

      setBodiesLoading(false);
    }
  }

  const _renderItem = ({ item, index }) => {
    console.log("IIIITTTEEMM", index, "currentMonth", currentMonth);

    return (
      <View
        style={{
          backgroundColor: index === currentMonth ? "#F46F22" : "transparent",
          borderRadius: 30,
          paddingVertical: 6,
        }}
      >
        <Text
          style={[
            fonts.medium,
            {
              fontSize: 14,
              color: index === currentMonth ? "#FAFAFA" : "#151C26",
              textAlign: "center",
            },
          ]}
        >
          {item}
        </Text>
      </View>
    );
  };

  const CustomGrid = ({ x, y, data, ticks }) => (
    <G>
      {
        // Horizontal grid
        ticks.map((tick) => (
          <Line
            key={tick}
            x1={"0%"}
            x2={"100%"}
            y1={y * (tick - (ticks.length - 1) * 10)}
            y2={y * (tick - (ticks.length - 1) * 10)}
            stroke={"rgba(0,0,0,0.2)"}
          />
        ))
      }
      {
        // Vertical grid
        data.map((value, index) => (
          <Line
            key={index}
            y1={"0%"}
            y2={"100%"}
            x1={x * value}
            x2={x * value}
            stroke={"red"}
          />
        ))
      }
    </G>
  );

  const renderLineChart = (
    style = {},
    data = [],
    contentInset = {},
    svg = {},
    gridSettings = {}
  ) => {
    console.log(
      "data: ",
      data,
      "contentInset: ",
      contentInset,
      "svg: ",
      svg,
      "gridSettings",
      gridSettings
    );

    return (
      <LineChart
        style={style}
        data={data}
        curve={shape.curveNatural}
        contentInset={contentInset}
        svg={svg}
      >
        {CustomGrid(gridSettings)}
      </LineChart>
    );
  };

  const hasData =
    (!currentTab &&
      bodiesData &&
      bodiesData.chest &&
      bodiesData.chest.length &&
      bodiesData.hips &&
      bodiesData.hips.length &&
      bodiesData.waist &&
      bodiesData.waist.length) ||
    (currentTab && weights && weights.length);

  console.log(
    "hasData",
    hasData,
    "bodiesData",
    bodiesData,
    "bodiesData.chest",
    bodiesData.chest,
    "weights",
    weights,
    "weights.lengt",
    weights.lengt
  );

  const dataWeightsList = weightsLoading
    ? getSizeSkeletonLoaders(420, 65)
    : !weightsLoading && weights && weights.length
    ? weights
    : [];
  const dataBodiesList = bodiesLoading
    ? getSizeSkeletonLoaders(420, 65)
    : !bodiesLoading && bodies && bodies.length
    ? bodies
    : [];

  const loadingIndicator = (
    <View
      style={{
        flex: 1,
        width: viewportWidth,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <ActivityIndicator size="large" color={colors.mainOrange} />
    </View>
  );

  return (
    <>
      <ImageBackground
        source={require("../../../assets/images/background/diaryResult.png")}
        resizeMode="stretch"
        style={{ width: "100%", height: viewportHeight }}
      >
        <SafeAreaView style={{ flex: 1 }}>
          <Header back={true} title={"Дневник результатов"} />

          <View style={styles.separator} />

          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ flex: 1, alignItems: "center" }}>
              <View style={{ position: "relative", marginBottom: 15 }}>
                {
                  <Carousel
                    activeSlideAlignment={"center"}
                    enableMomentum={true}
                    activeSlideOffset={0}
                    callbackOffsetMargin={0}
                    onBeforeSnapToItem={async (index) => {
                      setCurrentMonth(index);
                      getData(index);
                      console.log(index, "index");
                      // const day = daysProp[currentIndex];
                      // await this.getFoodsByDay(day);
                      // this.setState({currentCarouselIndex: currentIndex});
                    }}
                    inactiveSlideOpacity={0.6}
                    itemHeight={40}
                    data={month}
                    firstItem={startMonth - 1}
                    renderItem={_renderItem}
                    sliderWidth={viewportWidth - 30}
                    itemWidth={viewportWidth / 5}
                  />
                }
              </View>
            </View>

            <View style={{ flexDirection: "row", width: "100%", flex: 1 }}>
              <ScrollView
                horizontal={true}
                style={{ width: 700, height: 315, position: "relative" }}
              >
                {hasData && !weightsLoading && !bodiesLoading ? (
                  <YAxis
                    data={currentTab ? YAxisWeightData : YAxisData}
                    style={{
                      width: 50,
                      position: "absolute",
                      height: "105%",
                      left: 0,
                      bottom: 0,
                    }}
                    numberOfTicks={7}
                    contentInset={verticalContentInset}
                    svg={axesSvg}
                  />
                ) : null}

                {weightsLoading || bodiesLoading ? (
                  loadingIndicator
                ) : currentTab && weightsData && weightsData.length ? (
                  renderLineChart(
                    {
                      flex: 1,
                      height: "100%",
                      width: 850,
                      marginHorizontal: 15,
                    },
                    weightsData,
                    verticalContentInset,
                    { stroke: "rgb(134, 65, 244)", strokeWidth: 3 },
                    { x: 0, y: 5, data: [], ticks: YAxisWeightData }
                  )
                ) : !currentTab && hasData ? (
                  renderLineChart(
                    {
                      flex: 1,
                      position: "relative",
                      top: -60,
                      height: 750,
                      width: 850,
                      marginHorizontal: 15,
                    },
                    [
                      {
                        data: bodiesData.chest,
                        svg: { stroke: colors.mainOrange, strokeWidth: 3 },
                      },
                      {
                        data: bodiesData.hips,
                        svg: { stroke: colors.mainBlack, strokeWidth: 3 },
                      },
                      {
                        data: bodiesData.waist,
                        svg: { stroke: "#0FD8D8", strokeWidth: 3 },
                      },
                    ],
                    verticalBodyContentInset,
                    {},
                    { x: 0, y: 6, data: [], ticks: YAxisData }
                  )
                ) : (
                  <View
                    style={{
                      flex: 1,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Text
                      style={{
                        marginLeft: 18,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      Нет данных для отрисовки
                    </Text>
                  </View>
                )}

                {hasData && !weightsLoading && !bodiesLoading ? (
                  <XAxis
                    style={{
                      position: "absolute",
                      width: "94%",
                      bottom: -3,
                      left: 35,
                    }}
                    data={XAxisData}
                    formatLabel={(value) =>
                      value + 1 < 10 ? `0${value + 1}` : value + 1
                    }
                    contentInset={{ left: 10, right: 10 }}
                    svg={{ fontSize: 10, fill: "gray" }}
                  />
                ) : null}
              </ScrollView>
            </View>

            <View
              style={{
                flexDirection: "row",
              }}
            >
              <TouchableOpacity
                style={[
                  styles.measurementTab,
                  currentTab ? styles.activeMeasurementTab : null,
                ]}
                onPress={() => setCurrentTab(!currentTab)}
              >
                <Text
                  style={[
                    styles.measurementTabText,
                    currentTab ? styles.activeMeasurementTabText : null,
                  ]}
                >
                  Вес
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={[
                  styles.measurementTab,
                  currentTab ? null : styles.activeMeasurementTab,
                ]}
                onPress={() => setCurrentTab(!currentTab)}
              >
                <Text
                  style={[
                    styles.measurementTabText,
                    currentTab ? null : styles.activeMeasurementTabText,
                  ]}
                >
                  Замеры
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={
                !currentTab && hasData
                  ? styles.measurementsContainer
                  : { display: "none" }
              }
            >
              <View
                style={[styles.measurementsLabelContainer, { marginRight: 38 }]}
              >
                <View
                  style={[styles.smallCircle, { backgroundColor: "#0FD8D8" }]}
                />

                <Text style={styles.measurementsMentionText}>Талия</Text>
              </View>

              <View
                style={[styles.measurementsLabelContainer, { marginRight: 38 }]}
              >
                <View
                  style={[
                    styles.smallCircle,
                    { backgroundColor: colors.mainOrange },
                  ]}
                />

                <Text style={styles.measurementsMentionText}>Грудь</Text>
              </View>

              <View style={[styles.measurementsLabelContainer]}>
                <View
                  style={[
                    styles.smallCircle,
                    { backgroundColor: colors.mainBlack },
                  ]}
                />

                <Text style={styles.measurementsMentionText}>Бедра</Text>
              </View>
            </View>

            <View style={styles.measurementsOverview}>
              <View
                style={{
                  paddingTop: 20,
                  flex: 1,
                }}
              >
                <Text style={styles.measurementMonth}>
                  {month[currentMonth ? currentMonth : startMonth - 1]}{" "}
                  {currentYuear}
                </Text>

                <View style={{ marginTop: 18, borderRadius: 10 }}>
                  <ScrollView
                    contentContainerStyle={{ paddingBottom: 120 }}
                    showsVerticalScrollIndicator={false}
                  >
                    {(currentTab && dataWeightsList.length) ||
                    (!currentTab && dataBodiesList.length) ? (
                      (currentTab
                        ? dataWeightsList
                        : dataBodiesList
                      ).map((item, index) => (
                        <MeasurementDiaryListItem
                          key={`state-${index}`}
                          loading={currentTab ? weightsLoading : bodiesLoading}
                          prevWeight={
                            currentTab &&
                            weights &&
                            weights[index - 1] &&
                            weights[index - 1].weight
                              ? weights[index - 1].weight
                              : null
                          }
                          {...item}
                          currentTab={currentTab}
                        />
                      ))
                    ) : (
                      <View
                        style={{
                          flex: 1,
                          minHeight: Dimensions.get("screen").height / 2.8,
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <Text style={{ width: "100%", textAlign: "center" }}>
                          Нет данных
                        </Text>
                      </View>
                    )}
                  </ScrollView>
                </View>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </ImageBackground>

      <CustomBottomButton
        label={"ДОБАВИТЬ ЗАМЕР"}
        onPress={() =>
          props.navigation.navigate("AddWeightMeasurement", {
            isFemale,
            getBodiesData: fetchBodiesAPI,
            getWeightsData: fetchWeightAPI,
          })
        }
      />
    </>
  );
};

const mapStateToProps = (state) => ({
  currentUser: state.currentUser.currentUser,
});

export default connect(mapStateToProps)(DiaryResultScreen);

const styles = StyleSheet.create({
  backgroundImage: {
    opacity: 0.9,
    resizeMode: "cover",
  },
  background: {
    width: "100%",
    height: "100%",
    position: "relative",
  },
  separator: {
    // marginTop: dimensions.hasNotch ? 44 : 20,
    flex: 1,
  },
  header: {
    zIndex: 3,
    marginTop: 10,
    paddingHorizontal: 9,
  },
  titleText: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    lineHeight: 22,
    textAlign: "center",
    color: colors.mainBlack,
  },
  measurementsOverview: {
    flex: 1,
    paddingHorizontal: 16,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowColor: colors.absoluteBlack,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.12,
    shadowRadius: 50,
    elevation: 6,
    height: 400,
    width: "100%",
    backgroundColor: "#FFF",
  },
  measurementMonth: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    lineHeight: 22,
    color: colors.mainBlack,
    letterSpacing: 0.14,
  },
  smallCircle: {
    width: 7,
    height: 7,
    borderRadius: 50,
  },
  measurementsMentionText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 13,
    height: 13,
    marginLeft: 7,
    color: colors.mainBlack,
  },
  measurementsLabelContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  measurementsContainer: {
    maxWidth: 205,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 23,
  },
  activeMeasurementTab: {
    borderWidth: 1,
    borderColor: colors.mainOrange,
    borderRadius: 100,
  },
  measurementTab: {
    marginTop: 16,
    marginBottom: 23,
    marginLeft: 16,
    height: 25,
    paddingHorizontal: 12,
    alignItems: "center",
    justifyContent: "center",
  },
  measurementTabText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 13,
    lineHeight: 22,
    color: colors.mainBlack,
    opacity: 0.54,
  },
  activeMeasurementTabText: {
    opacity: 1,
  },
});
