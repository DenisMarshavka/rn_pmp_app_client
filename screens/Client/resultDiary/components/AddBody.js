import React, { useState, useEffect } from "react";
import {
  Platform,
  StyleSheet,
  KeyboardAvoidingView,
  View,
  Dimensions,
  Alert,
} from "react-native";
import { colors } from "../../../../styles";
import Dash from "react-native-dash";
import { fonts } from "../../../../constants/theme";
import PersonalDataInput from "../../../../components/input/personal_data_input/PersonalDataInput";
import ButtonInput from "../../../../components/button/ButtonInput";
import { BodyAPI } from "../../../../api/diary/bodies";
import { UserAPI } from "../../../../api/user";
import { useNavigation } from "@react-navigation/native";
import { ToastView } from "../../../../components";
import { LinearGradient } from "expo-linear-gradient/build/index";

const AddBody = ({ onBodiesDataUpdate = () => {} }) => {
  const [chest, setChest] = useState("");
  const [waist, setWaist] = useState("");
  const [hips, setHips] = useState("");
  const [activeInput, setActiveInput] = useState(null);

  const navigation = useNavigation();

  useEffect(() => {
    async function fetchBodiesAPI() {
      try {
        const user = await UserAPI.getUserByToken();
        // console.log('UUUUUUUSEEEEEEEERRRRRR', user);

        // if (user.gender === null) {
        //   Alert.alert('Укажите свой пол.');
        //   navigation.navigate('PersonalData')
        // }
      } catch (e) {
        console.log("Error UserAPI.getUserByToken --- ", e);
      }
    }

    fetchBodiesAPI();
  }, []);

  const save = async () => {
    try {
      await BodyAPI.addBody({ chest, waist, hips });
      await onBodiesDataUpdate();

      ToastView("Данные успешно добавлены!");
    } catch (e) {
      console.log("Error BodyAPI.addBody --- ", e);
      ToastView(
        "К сожалению, произошла ошибка.\nОбратитесь в поддержку по телефону +7 495 797 94 27"
      );
    }
  };

  return (
    <KeyboardAvoidingView
      style={{ flex: 1, minHeight: Dimensions.get("screen").height / 3 }}
      behavior="padding"
    >
      <View style={styles.bodyMeasurementContainer}>
        <Dash
          style={styles.bodyMeasurementDash}
          dashGap={4}
          dashLength={6}
          dashColor={activeInput !== 0 ? "#A1A4A7" : colors.mainOrange}
        />

        <View style={styles.bodyMeasurementInsideContainer}>
          <PersonalDataInput
            value={chest}
            inputType="numeric"
            id={"Грудь"}
            placeholder={"см"}
            label={"Грудь"}
            onChange={(value) => {
              setChest(value);
            }}
            onFocus={() => setActiveInput(0)}
            onBlur={() => setActiveInput(null)}
          />
        </View>
      </View>

      <View style={styles.bodyMeasurementContainer}>
        <Dash
          style={styles.bodyMeasurementDash}
          dashGap={4}
          dashLength={6}
          dashColor={activeInput !== 1 ? "#A1A4A7" : colors.mainOrange}
        />

        <View style={[styles.bodyMeasurementInsideContainer]}>
          <PersonalDataInput
            value={waist}
            inputType="numeric"
            id={"Талия"}
            placeholder={"см"}
            label={"Талия"}
            onChange={(value) => {
              setWaist(value);
            }}
            onFocus={() => setActiveInput(1)}
            onBlur={() => setActiveInput(null)}
          />
        </View>
      </View>

      <View style={styles.bodyMeasurementContainer}>
        <Dash
          style={styles.bodyMeasurementDash}
          dashGap={4}
          dashLength={6}
          dashColor={activeInput !== 2 ? "#A1A4A7" : colors.mainOrange}
        />

        <View style={styles.bodyMeasurementInsideContainer}>
          <PersonalDataInput
            value={hips}
            inputType="numeric"
            id={"Бедра"}
            placeholder={"см"}
            label={"Бедра"}
            onChange={(value) => {
              setHips(value);
            }}
            onFocus={() => setActiveInput(2)}
            onBlur={() => setActiveInput(null)}
          />
        </View>
      </View>

      <View style={{ marginTop: 65, marginBottom: 10 }}>
        <ButtonInput title="СОХРАНИТЬ" onClick={() => save()} />
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  backgroundImage: {
    opacity: 0.9,
    resizeMode: "cover",
  },
  nonActiveButtonText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: Platform.OS === "ios" ? 16 : 13,
    height: Platform.OS === "ios" ? 16 : 20,
    textAlign: "center",
    color: colors.mainWhite,
    width: 111,
  },
  activeButtonText: {
    color: colors.mainBlack,
    width: 29,
  },
  activeButton: {
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.mainOrange,
    paddingHorizontal: 12,
    height: 29,
    marginLeft: 12,
  },
  nonActiveButton: {
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "rgba(21, 28, 38, 0.12)",
    paddingHorizontal: 12,
    height: 29,
  },
  background: {
    width: "100%",
    height: "100%",
    position: "relative",
  },
  separator: {
    marginTop: 44,
  },
  titleText: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    lineHeight: 22,
    fontWeight: "500",
    textAlign: "center",
    marginLeft: 28,
    marginTop: 8,
    color: colors.mainBlack,
  },
  header: {
    zIndex: 3,
    marginTop: 14,
    paddingHorizontal: 9,
  },
  bodyMeasurementScreenContainer: {
    flex: 1,
  },
  bodyMeasurementContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
    paddingRight: 13,
    marginBottom: 24,
  },
  bodyMeasurementInsideContainer: {
    flex: 1,
    marginLeft: 24,
  },
  bodyMeasurementDash: {
    width: 95,
    height: 1,
    marginBottom: 14,
  },
  bodyMeasurementInput: {
    fontSize: 18,
    lineHeight: 24,
    marginBottom: 3,
    color: colors.absoluteBlack,
  },
  bodyMeasurementInputLabel: {
    fontSize: 13,
    lineHeight: 18,
    marginBottom: 8,
    color: colors.mainOrange,
    fontWeight: "500",
  },
});

export default AddBody;
