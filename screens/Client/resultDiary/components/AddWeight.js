import React, { useState, useEffect, useRef } from "react";
import {
  Platform,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Animated,
  KeyboardAvoidingView,
} from "react-native";
import { Slider } from "react-native-elements";
import { colors, dimensions } from "../../../../styles";
import { fonts } from "../../../../constants/theme";
import NumberFormat from "react-number-format";
import PersonalDataInput from "../../../../components/input/personal_data_input/PersonalDataInput";
import ButtonInput from "../../../../components/button/ButtonInput";
import { WeightAPI } from "../../../../api/diary/weights";
import { ToastView } from "../../../../components";

const AddWeight = ({ onWeightsDataUpdate = () => {} }) => {
  const [sliderValue, setSliderValue] = useState(10);

  const save = async () => {
    try {
      await WeightAPI.addWeight({ weight: sliderValue });
      await onWeightsDataUpdate();

      ToastView("Данные успешно добавлены!");
    } catch (e) {
      console.log("Error WeightAPI.addWeight --- ", e);
      ToastView(
        "К сожалению, произошла ошибка.\nОбратитесь в поддержку по телефону +7 495 797 94 27"
      );
    }
  };

  return (
    <KeyboardAvoidingView
      style={{ flex: 1, minHeight: 800 }}
      behavior="padding"
    >
      <Slider
        minimumValue={40}
        maximumValue={150}
        value={Number(sliderValue)}
        minimumTrackTintColor={colors.mainOrange}
        maximumTrackTintColor="#C7C7CC"
        onValueChange={(value) => setSliderValue(Math.floor(value))}
        thumbTintColor={colors.mainOrange}
      />

      <Text style={styles.weightText}>{sliderValue}&nbsp;кг</Text>

      <NumberFormat
        value={sliderValue}
        displayType={"text"}
        thousandSeparator={true}
        //suffix={' кг'}
        renderText={(value) => (
          <PersonalDataInput
            id={"weight"}
            placeholder={"Вес"}
            label={"Вес"}
            inputType="numeric"
            onChange={(value) => setSliderValue(value)}
            value={value}
          />
        )}
      />

      <View style={{ flex: 1, marginTop: 136, backgroundColor: "transparent" }}>
        <ButtonInput title="СОХРАНИТЬ" onClick={() => save()} />
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  background: {
    width: "100%",
    height: "100%",
    position: "relative",
  },
  separator: {
    marginTop: dimensions.hasNotch ? 44 : 20,
  },
  titleText: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    lineHeight: 22,
    textAlign: "center",
    color: colors.mainBlack,
  },
  header: {
    zIndex: 3,
    marginTop: 10,
    paddingHorizontal: 9,
  },
  weightText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 48,
    marginTop: 20,
    letterSpacing: -0.1,
    textAlign: "center",
    color: colors.mainBlack,
  },
  weightInputLabel: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 13,
    lineHeight: 18,
    fontWeight: "500",
    color: colors.mainOrange,
    marginTop: 34,
  },
  weightInput: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 18,
    lineHeight: 24,
    color: colors.absoluteBlack,
  },
});

export default AddWeight;
