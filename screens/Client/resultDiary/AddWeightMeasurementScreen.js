import React, { useState } from "react";
import {
  ImageBackground,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  Dimensions,
  Image,
  KeyboardAvoidingView,
} from "react-native";

import bgMen from "../../../assets/images/bgMen.png";
import bgWomen from "../../../assets/images/bgWomen.png";
import { colors } from "../../../styles";
import { fonts } from "../../../constants/theme";
import AddWeight from "./components/AddWeight";
import AddBody from "./components/AddBody";
import Header from "../../../components/Headers/Header";
import { Background } from "../../../components/Background";
import { LinearGradient } from "expo-linear-gradient/build/index";

const { height } = Dimensions.get("window");

const AddWeightMeasurementScreen = ({ navigation, route = {} }) => {
  const [currentTab, setCurrentTab] = useState(true);
  const {
    isFemale = false,
    getBodiesData = () => {},
    getWeightsData = () => {},
  } = route.params || {};

  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <SafeAreaView>
        <Header title={"Добавить замер"} back={true} />
      </SafeAreaView>

      <View style={styles.separator} />

      <ScrollView
        style={{ flex: 1 }}
        contentContainerStyle={{
          minHeight: 650,
        }}
        showsVerticalScrollIndicator={false}
      >
        <View
          style={{
            position: "relative",
            flex: 1,
            paddingHorizontal: 16,
            height: Dimensions.get("screen").height / 1.5,
            zIndex: 2,
          }}
        >
          <Image
            source={
              currentTab
                ? require(`../../../assets/images/background/addWeight.png`)
                : !currentTab && isFemale
                ? require(`../../../assets/images/background/addMeasurementFemale.png`)
                : require(`../../../assets/images/background/addMeasurementMale.png`)
            }
            resizeMode="contain"
            style={[
              {
                flex: 1,
                position: "absolute",
                top: "17%",

                width: 282,
                height: 398,
              },
              !currentTab ? { left: "-17%" } : { right: "-20%" },
            ]}
          />

          <View style={{ flexDirection: "row", marginTop: 24 }}>
            <TouchableOpacity
              style={[
                styles.tub,
                { marginRight: 12 },
                currentTab ? styles.activeButton : styles.nonActiveButton,
              ]}
              onPress={() => setCurrentTab(true)}
            >
              <Text
                style={[
                  styles.tubText,
                  !currentTab
                    ? styles.nonActiveButtonText
                    : styles.activeButtonText,
                ]}
              >
                Вес
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[
                styles.tub,
                !currentTab ? styles.activeButton : styles.nonActiveButton,
              ]}
              onPress={() => setCurrentTab(false)}
            >
              <Text
                style={[
                  styles.tubText,
                  currentTab
                    ? styles.nonActiveButtonText
                    : styles.activeButtonText,
                ]}
              >
                Параметры тела
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              position: "relative",
              minHeight: Dimensions.get("screen").height / 3,
              flex: 1,
              zIndex: 5,
            }}
          >
            {currentTab ? (
              <View style={{ marginTop: 230, backgroundColor: "transparent" }}>
                <AddWeight onWeightsDataUpdate={getWeightsData} />
              </View>
            ) : (
              <View style={{ marginTop: 200 }}>
                <AddBody onBodiesDataUpdate={getBodiesData} />
              </View>
            )}
          </View>

          <LinearGradient
            pointerEvents={"none"}
            colors={["rgba(255, 255, 255, 0)", "#FFF"]}
            style={styles.linearGradient}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  backgroundImage: {
    opacity: 0.9,
  },
  nonActiveButtonText: {
    color: "#151C26",
  },
  activeButtonText: {
    color: "#FFFFFF",
  },
  tub: {
    paddingVertical: 8,
    paddingHorizontal: 20,
    borderRadius: 30,
  },
  tubText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 16,
  },
  activeButton: {
    backgroundColor: "#F46F22",
  },
  nonActiveButton: {
    borderWidth: 1,
    borderColor: "rgba(21, 28, 38, 0.12)",
  },
  background: {
    flex: 1,
    height: 770,
  },
  titleText: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    lineHeight: 22,
    textAlign: "center",
    color: colors.mainBlack,
  },
  header: {
    zIndex: 3,
    marginTop: 10,
    paddingHorizontal: 9,
  },
  weightText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 48,
    marginTop: 20,
    letterSpacing: -0.1,
    textAlign: "center",
    color: colors.mainBlack,
  },
  weightInputLabel: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 13,
    lineHeight: 18,
    fontWeight: "500",
    color: colors.mainOrange,
    marginTop: 34,
  },
  weightInput: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 18,
    lineHeight: 24,
    color: colors.absoluteBlack,
  },

  linearGradient: {
    flex: 1,
    zIndex: 3,

    position: "absolute",
    bottom: 0,
    left: 0,
    width: "110%",
    height: Dimensions.get("screen").height / 1.1,
  },
});

export default AddWeightMeasurementScreen;
