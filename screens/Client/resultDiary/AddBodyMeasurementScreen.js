import React, { useState, useEffect, useRef } from "react";
import {
  ImageBackground,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Animated,
  Keyboard,
} from "react-native";
import bgMen from "../../../assets/images/bgMen.png";
import bgWomen from "../../../assets/images/bgWomen.png";
import { CustomHeader, CustomBottomButton } from "../../../components";
// import { navigateBackFromItem, navigateToChat } from '../../../rn-navigation/screens'
import ButtonWithShadow from "../../../components/ButtonWithShadow";
import { colors } from "../../../styles";
import { LinearGradient } from "expo-linear-gradient";
import { useKeyboard } from "@react-native-community/hooks";
import Dash from "react-native-dash";
import { color } from "react-native-reanimated";
import { fonts, sizes } from "../../../constants/theme";

const AddBodyMeasurementScreen = (props) => {
  const animatedMarginTop = useRef(new Animated.Value(150)).current;

  const bgImage = Math.random() >= 0.5 ? bgMen : bgWomen;

  useEffect(() => {
    Keyboard.addListener("keyboardWillShow", keyboardWillShow);

    Keyboard.addListener("keyboardWillHide", keyboardWillHide);
  }, []);

  const keyboardWillShow = (event) => {
    Animated.timing(animatedMarginTop, {
      duration: event.duration,
      toValue: 50,
      useNativeDriver: true,
    }).start();
  };

  const keyboardWillHide = (event) => {
    Animated.timing(animatedMarginTop, {
      duration: event ? event.duration : 100,
      toValue: 150,
      useNativeDriver: true,
    }).start();
  };

  const dashActiveColor = "#A1A4A7";
  const dashInactiveColor = colors.mainOrange;

  const activeBorderColor = colors.mainOrange;
  const inactiveBorderColor = "rgba(0, 0, 0, 0.12)";

  const navigateBack = () => {
    navigateBackFromItem(props.componentId);
  };

  return (
    <ImageBackground
      source={bgImage}
      style={styles.background}
      imageStyle={styles.backgroundImage}
      resizeMode="stretch"
    >
      {Platform.OS === "ios" ? <View style={styles.separator} /> : null}
      <CustomHeader
        back={true}
        navigateToMenu={navigateBack}
        menu={false}
        title={"Добавить замер"}
        style={styles.header}
        titleText={styles.titleText}
      />

      <View style={styles.bodyMeasurementScreenContainer}>
        <View style={{ flexDirection: "row", marginTop: 24, paddingLeft: 16 }}>
          <TouchableOpacity style={styles.nonActiveButton}>
            <Text style={[styles.nonActiveButtonText, styles.activeButtonText]}>
              Вес
            </Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.activeButton} onPress={() => {}}>
            <Text style={styles.nonActiveButtonText}>Параметры тела</Text>
          </TouchableOpacity>
        </View>

        <Animated.View style={{ marginTop: animatedMarginTop }}>
          <View style={styles.bodyMeasurementContainer}>
            <Dash
              style={styles.bodyMeasurementDash}
              dashGap={4}
              dashLength={6}
              dashColor={"#A1A4A7"}
            />

            <View style={styles.bodyMeasurementInsideContainer}>
              <Text style={styles.bodyMeasurementInputLabel}>Грудь</Text>
              <TextInput
                style={styles.bodyMeasurementInput}
                returnKeyType={"done"}
                keyboardType={"number-pad"}
              />
            </View>
          </View>

          <View style={styles.bodyMeasurementContainer}>
            <Dash
              style={styles.bodyMeasurementDash}
              dashGap={4}
              dashLength={6}
              dashColor={colors.mainOrange}
            />

            <View
              style={[
                styles.bodyMeasurementInsideContainer,
                { borderColor: activeBorderColor, borderBottomWidth: 2 },
              ]}
            >
              <Text style={styles.bodyMeasurementInputLabel}>Талия</Text>

              <TextInput
                style={styles.bodyMeasurementInput}
                returnKeyType={"done"}
                keyboardType={"number-pad"}
              />
            </View>
          </View>

          <View style={styles.bodyMeasurementContainer}>
            <Dash
              style={styles.bodyMeasurementDash}
              dashGap={4}
              dashLength={6}
              dashColor={"#A1A4A7"}
            />

            <View style={styles.bodyMeasurementInsideContainer}>
              <Text style={styles.bodyMeasurementInputLabel}>Бедра</Text>
              <TextInput
                style={styles.bodyMeasurementInput}
                returnKeyType={"done"}
                keyboardType={"number-pad"}
              />
            </View>
          </View>
        </Animated.View>
      </View>

      <ButtonWithShadow
        label={"Сохранить"}
        style={{ position: "absolute", bottom: 46, zIndex: 3, width: "90%" }}
        onPress={() => navigateToChat(props.componentId)}
      />

      <LinearGradient
        pointerEvents={"none"}
        colors={["rgba(255, 255, 255, 0)", "#FFF"]}
        style={styles.linearGradient}
      />
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  backgroundImage: {
    opacity: 0.9,
    resizeMode: "cover",
  },
  nonActiveButtonText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: Platform.OS === "ios" ? 16 : 13,
    height: Platform.OS === "ios" ? 16 : 20,
    textAlign: "center",
    color: colors.mainWhite,
    width: 111,
  },
  activeButtonText: {
    color: colors.mainBlack,
    width: 29,
  },
  activeButton: {
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.mainOrange,
    paddingHorizontal: 12,
    height: 29,
    marginLeft: 12,
  },
  nonActiveButton: {
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "rgba(21, 28, 38, 0.12)",
    paddingHorizontal: 12,
    height: 29,
  },
  background: {
    width: "100%",
    height: "100%",
    position: "relative",
    backgroundColor: "#fff",
  },
  separator: {
    marginTop: 44,
  },
  titleText: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    lineHeight: 22,
    fontWeight: "500",
    textAlign: "center",
    marginLeft: 28,
    marginTop: 8,
    color: colors.mainBlack,
  },
  header: {
    zIndex: 3,
    marginTop: 14,
    paddingHorizontal: 9,
    backgroundColor: "#fff",
  },
  linearGradient: {
    flex: 1,
    zIndex: 2,
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 466,
  },
  bodyMeasurementScreenContainer: {
    flex: 1,
    borderTopWidth: 1,
    borderColor: "rgba(21, 28, 38, 0.12)",
    marginTop: 8,
    paddingRight: 16,
    zIndex: 2,
    elevation: 2,
  },
  bodyMeasurementContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
    paddingRight: 13,
    marginBottom: 24,
  },
  bodyMeasurementInsideContainer: {
    borderColor: "rgba(0, 0, 0, 0.12)",
    borderBottomWidth: 1,
    flex: 1,
    marginLeft: 24,
  },
  bodyMeasurementDash: {
    width: 95,
    height: 1,
  },
  bodyMeasurementInput: {
    fontSize: 18,
    lineHeight: 24,
    marginBottom: 3,
    color: colors.absoluteBlack,
  },
  bodyMeasurementInputLabel: {
    fontSize: 13,
    lineHeight: 18,
    marginBottom: 8,
    color: colors.mainOrange,
    fontWeight: "500",
  },
});

export default AddBodyMeasurementScreen;
