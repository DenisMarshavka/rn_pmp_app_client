import React, { useState } from "react";
import {
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
} from "react-native";

import { Background } from "../../components/Background";
import BackHeader from "../../components/Headers/BackHeader";
import ListWithInfo from "../../components/ListWithInfo";
import { colors, fonts } from "../../constants/theme";
import { connect } from "react-redux";
import { getSizeSkeletonLoaders } from "../../utils";
import SkeletonProductItem from "../../components/loaders/SkeletonProductItem";
import {
  getNewPrograms,
  getSpecialPrograms,
  clearNewProgramsData,
  clearSpecialProgramsData,
} from "../../actions/ProgramsActions";
import Header from "../../components/Headers/Header";

const ProgramsScreen = ({
  navigation,
  route,

  getSpecialPrograms = () => {},
  getNewPrograms = () => {},

  specialProgramsLoading = false,
  specialProgramsData = [],
  specialProgramsError = false,
  specialProgramsListMeta = {},
  moreSpecialProgramsLoading = false,

  newProgramsLoading = false,
  newProgramsData = [],
  newProgramsError = false,
  newProgramsDataListMeta = {},
  moreNewProgramsLoading = false,
}) => {
  const { isInvites = false, headTitle = "" } =
    route && route.params ? route.params : {};

  const [maxListItemsCount, setMaxListItemsCount] = useState(15);

  const clearListData = isInvites
    ? clearSpecialProgramsData
    : clearNewProgramsData;
  const getListData = isInvites ? getSpecialPrograms : getNewPrograms;
  const loading = specialProgramsLoading || newProgramsLoading;

  const requestFailed = newProgramsError || specialProgramsError;
  const moreLoadingListData = isInvites
    ? moreSpecialProgramsLoading
    : moreNewProgramsLoading;
  const listData = isInvites ? specialProgramsData : newProgramsData;
  const listMeta = isInvites
    ? specialProgramsListMeta
    : newProgramsDataListMeta;

  console.log(
    "loading list data: ",
    loading,
    "more list data: ",
    moreLoadingListData
  );

  const renderProduct = (
    item = {},
    index = 0,
    list = [],
    isLoading = false
  ) => (
    <SkeletonProductItem
      style={{ marginBottom: 15 }}
      isLoading={isLoading}
      index={index}
      product={item}
      isListRendering={true}
      onSlidePress={() =>
        navigation.navigate("ProgramScreen", { programId: item.id })
      }
      onProductPress={() =>
        navigation.navigate("ProgramScreen", { programId: item.id })
      }
    />
  );

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView>
        <Header
          title={headTitle}
          backTitle
          basketShow
          routeName={route.name}
          receivedParams={route && route.params ? route.params : {}}
        />
      </SafeAreaView>

      <View style={styles.container}>
        <ListWithInfo
          requestDidMountCopmponent={() =>
            getListData(false, maxListItemsCount)
          }
          requestWillUnmountComponent={clearListData}
          mocsDataOffsetTop={35}
          mocsDataListElementSize={125}
          heightDimensionsContainer={1.6}
          data={listData}
          moreLoading={moreLoadingListData}
          listMeta={listMeta}
          error={requestFailed}
          loading={loading}
          wrapListParams={{
            showsVerticalScrollIndicator: false,
            contentContainerStyle: {
              paddingBottom: 30,
            },
          }}
          elementKeyName={`${isInvites ? "special" : "new"}-product-item-`}
          renderListItem={renderProduct}
          onMoreDataList={(offset = 0) =>
            getListData(false, maxListItemsCount, offset, true)
          }
        />
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  invitesLoading: state.StoreReducer.invitesLoading,
  invites: state.StoreReducer.invites,
  invitesError: state.StoreReducer.invitesError,

  productsLoading: state.StoreReducer.productsLoading,
  products: state.StoreReducer.products,
  productsError: state.StoreReducer.productsError,

  specialProgramsLoading: state.ProgramsReducer.specialProgramsLoading,
  specialProgramsData: state.ProgramsReducer.specialProgramsData,
  specialProgramsError: state.ProgramsReducer.specialProgramsError,
  specialProgramsListMeta: state.ProgramsReducer.specialProgramsListMeta,
  moreSpecialProgramsLoading: state.ProgramsReducer.moreSpecialProgramsLoading,

  newProgramsLoading: state.ProgramsReducer.newProgramsLoading,
  newProgramsData: state.ProgramsReducer.newProgramsData,
  newProgramsError: state.ProgramsReducer.newProgramsError,
  newProgramsDataListMeta: state.ProgramsReducer.newProgramsDataListMeta,
  moreNewProgramsLoading: state.ProgramsReducer.moreNewProgramsLoading,
});

const mapDispatchToProps = {
  getSpecialPrograms,
  getNewPrograms,
  clearNewProgramsData,
  clearSpecialProgramsData,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProgramsScreen);

const styles = StyleSheet.create({
  title: {
    color: colors.title,
    marginVertical: 10,
  },
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  slideGradient: {
    width: "100%",
    height: 72,
  },
  infoSlide: {
    height: 72,
    backgroundColor: "#EAEAEA",
    width: "100%",
    justifyContent: "space-between",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    padding: 16,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  titleSlide: {
    fontSize: 18,
    fontWeight: "500",
    color: "#000",
    fontFamily: "PFDin500",
  },
  priceSlide: {
    fontSize: 18,
    color: "#F46F22",
    fontWeight: "500",
    textTransform: "uppercase",
    fontFamily: "PFDin500",
  },
  img: {
    width: "100%",
    height: 184,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    resizeMode: "cover",
  },
});
