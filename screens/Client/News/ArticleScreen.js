import React from "react";
import {
  View,
  Text,
  TextInput,
  Image,
  ImageBackground,
  ScrollView,
  Dimensions,
  StyleSheet,
} from "react-native";
import { Background } from "../../../components/Background";
import Header from "../../../components/Headers/Header";
import HTMLView from "react-native-htmlview";
import { fonts, sizes, colors } from "../../../constants/theme";
import { LinearGradient } from "expo-linear-gradient";

const fitness = require("../../../assets/images/icons/fitness.png");
const beauty = require("../../../assets/images/icons/beauty.png");
const defaultImage = require("../../../assets/images/slideTest-1.png");

const ArticleScreen = (props) => {
  const article = props.route.params.article;

  return (
    <Background>
      <Header title={"Статья"} />

      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.titleContainer}>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Text
                style={[
                  fonts.normal,
                  { fontSize: sizes.headerTitle, color: "#151C26" },
                ]}
              >
                {article.title}
              </Text>

              <Image
                source={article.mode === "fitness" ? fitness : beauty}
                style={styles.articleMode}
              />
            </View>
          </View>

          <View style={{ flex: 1, marginBottom: 18 }}>
            <Text
              style={[
                fonts.normal,
                {
                  fontSize: sizes.headerSubTitle,
                  paddingVertical: 28,
                  color: "#151C26",
                  opacity: 0.87,
                },
              ]}
            >
              {article.comment ||
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto est illum laudantium placeat tempore. Beatae culpa cum enim itaque libero, neque officiis repellat sint ullam!"}
            </Text>

            <ImageBackground
              style={styles.articleImage}
              imageStyle={{ borderRadius: 10 }}
              source={article.img ? { uri: article.img } : defaultImage}
              resizeMode="cover"
            />

            <HTMLView
              value={
                article.description ||
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet architecto doloribus eum ex illum, impedit laboriosam magnam magni minus modi molestias nesciunt nihil non nulla officiis optio praesentium quas quasi reiciendis rem repellendus reprehenderit sed, similique temporibus veritatis voluptate. Adipisci ea eligendi enim id illo ipsam libero minima mollitia nam nemo, nulla numquam quasi recusandae rem sapiente sequi voluptas?"
              }
              stylesheet={stylesHtml}
            />
          </View>
        </ScrollView>
      </View>
    </Background>
  );
};

export default ArticleScreen;

const stylesHtml = StyleSheet.create({
  a: {
    fontWeight: "300",
    color: "#FF3366", // make links coloured pink
  },
});

const styles = {
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  titleContainer: {
    //paddingVertical: 24,
    width: "100%",
    flexDirection: "row",
  },
  articleMode: {
    width: 28,
    height: 28,
    resizeMode: "contain",
  },
  articleImage: {
    width: "100%",
    height: 174,
    marginBottom: 24,
  },
};
