import React from "react";
import { View, Text, StyleSheet, Image, FlatList } from "react-native";
import { Background } from "../../../components/Background";
import { Header } from "../../../components/Header";

import { colors, fonts, sizes } from "../../../constants/theme";
// import { FlatList } from "react-native-gesture-handler";

const DATA = [
  {
    id: "1",
    title: "First Item",
  },
  {
    id: "2",
    title: "Second Item",
  },
  {
    id: "3",
    title: "Third Item",
  },
];
const ArticleDetailsScreen = (props) => {
  return (
    <Background>
      <Header title="Статья" backimg="1" />
      <View style={styles.article_body}>
        <View style={styles.articleHeader}>
          <Text style={styles.headerTitle}>Первая тренировка</Text>
          <Image
            source={require("../../../assets/icons/fitness.png")}
            style={styles.articleHeaderIcon}
          />
        </View>
        <Text style={styles.headerSubTitle}>
          Полезная информация о подготовке
        </Text>
        <View style={styles.articleViewImage}>
          <Image
            style={styles.articeImage}
            source={require("../../../assets/images/sample/article_2.png")}
          />
        </View>

        {/*<Text style={styles.headerName}>Описание</Text>*/}

        <Text style={[fonts.normal]}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum. Lorem ipsum dolor sit
          amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
          labore et dolore
        </Text>
        <FlatList
          data={DATA}
          renderItem={({ item }) => (
            <Text style={styles.flat_list_item}>{item.title}</Text>
          )}
          style={styles.flat_list}
        />
        <Text style={[fonts.normal]}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum. Lorem ipsum dolor sit
          amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
          labore et dolore
        </Text>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  article_body: {
    margin: sizes.margin,
    marginTop: 37,
  },
  articeImage: {
    marginTop: 30,
    width: 343,
    height: 169,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  articleHeader: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  articleHeaderIcon: {
    display: "flex",
    justifyContent: "flex-end",
    width: 28,
    height: 28,
  },
  headerTitle: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: sizes.headerTitle,
  },
  headerSubTitle: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: sizes.headerSubTitle,
  },
  headerName: {
    marginTop: 23,
    fontFamily: fonts.normal.fontFamily,
    fontSize: sizes.headerName,
  },
  flat_list: {
    marginTop: 26,
    marginBottom: 26,
  },
  flat_list_item: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: sizes.text,
  },
});

export default ArticleDetailsScreen;
