import React, { useEffect, useState } from "react";
import {
  TouchableOpacity,
  Image,
  View,
  Text,
  Dimensions,
  ScrollView,
  FlatList,
  SafeAreaView,
} from "react-native";
import { BoxShadow } from "react-native-shadow";
import moment from "moment";
import { connect } from "react-redux";
import * as Animatable from "react-native-animatable";

import Constants from "../../../constants/Constants";
import TitleHeader from "../../../components/Headers/TitleHeader";
import { Background } from "../../../components/Background";
import { fonts, sizes } from "../../../constants/theme";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader";
import { getNews } from "../../../actions/news";
import { getSizeSkeletonLoaders } from "../../../utils";
import { Tabs } from "../../../components/Tabs/Tabs";
import Header from "../../../components/Headers/Header";

const { width, height } = Dimensions.get("window");

const fitness = require("../../../assets/images/icons/fitness.png");
const beauty = require("../../../assets/images/icons/beauty.png");

const shadowOpt = {
  width: width - 32,
  height: 246,
  color: "#000",
  border: 8,
  radius: 18,
  opacity: 0.06,
  x: 0,
  y: 0,
  style: {
    marginHorizontal: 16,
    marginBottom: 5,
  },
};

const headerPeriodTabs = [
  { id: 0, title: "Все" },
  { id: 1, title: "Сегодня" },
  { id: 2, title: "Hеделя" },
  { id: 3, title: "Mecяц" },
];

/*const getPeriod = (date, activeTab) => {
  return moment().format("L") === moment(date).format("L")
    ? "Сегодня"
    : moment().subtract(7, "days").format("L") <= moment(date).format("L")
    ? "Неделя"
    : moment().subtract(30, "days").format("L") <= moment(date).format("L")
    ? "Месяц"
    : "Более месяца";
};*/

const NewsScreen = (props) => {
  const {
    loading = false,
    news = [],
    errorRequest = false,
    getNews = () => {},
    route = {},
  } = props;

  const monthNews = moment().subtract(29, "days").format("YYYY-MM-DD");
  const allNews = moment().subtract(365, "days").format("YYYY-MM-DD");

  const [listNews, setListNews] = useState([]);

  const [isFetch, setIsFetch] = useState(true);
  const [activeTab, setActiveTab] = useState(0);
  const [endPeriod, setEndPeriod] = useState(allNews);
  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(10);
  const [firstRender, setFirstRender] = useState(true);
  const [showSceleton, setShowSceleton] = useState(loading);

  const onLoad = () => {
    if (news.length > 0) {
      setFirstRender(false);
      let newOffset = offset + 10;
      setOffset(newOffset);
      getNews(endPeriod, moment().format("YYYY-MM-DD"), newOffset, limit);
    }
  };
  useEffect(() => {
    if (firstRender) {
      setListNews(news);
    } else {
      setListNews(listNews.concat(news));
    }
    setShowSceleton(false);
  }, [news]);
  useEffect(() => {
    if (!loading) sortData();
  }, [activeTab]);

  useEffect(() => {
    if (!loading) setIsFetch(false);
  }, [loading]);

  useEffect(() => {
    setShowSceleton(true);
    getNews(endPeriod, moment().format("YYYY-MM-DD"), offset, limit);
  }, [endPeriod]);

  const sortData = () => {
    if (activeTab === Constants.TODAY_ARTICLES) {
      setListNews([]);
      setOffset(0);
      setFirstRender(true);
      setEndPeriod(moment().format("YYYY-MM-DD"));
    } else if (activeTab === Constants.WEEKLY_ARTICLES) {
      setListNews([]);
      setOffset(0);
      setFirstRender(true);
      setEndPeriod(moment().subtract(7, "days").format("YYYY-MM-DD"));
    } else if (activeTab === Constants.MONTHLY_ARTICLES) {
      setListNews([]);
      setOffset(0);
      setFirstRender(true);
      setEndPeriod(monthNews);
    } else {
      setOffset(0);
      setListNews([]);
      setFirstRender(true);
      setEndPeriod(allNews);
    }
  };

  const dataList =
    showSceleton && !errorRequest ? getSizeSkeletonLoaders(150, 300) : listNews;

  const renderNewsListItem = (item = {}, index = 0) => (
    <TouchableOpacity
      key={`item-${(item.id || Date.now()) + index}`}
      style={{ flex: 1, borderRadius: 10 }}
      activeOpacity={showSceleton ? 1 : 0.8}
      onPress={() => {
        !showSceleton
          ? props.navigation.navigate("Article", { article: item })
          : null;
      }}
    >
      <BoxShadow setting={shadowOpt}>
        <View
          style={{
            flex: 1,
            backgroundColor: "#fff",
            justifyContent: "space-between",
            position: "relative",
          }}
        >
          <SkeletonLoader
            isLoading={isFetch}
            containerStyle={{
              flex: 1,
              height: 130,
              width: "100%",
              backgroundColor: "grey",
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
            }}
            layout={[{ flex: 1 }]}
          >
            <Image
              style={styles.articleImage}
              source={
                item && item.img && item.img != undefined
                  ? { uri: item.img }
                  : require("../../../assets/images/notProductImage.png")
              }
            />

            <Image
              source={item.mode === "fitness" ? fitness : beauty}
              style={styles.articleMode}
            />
          </SkeletonLoader>

          <View style={styles.content}>
            <SkeletonLoader
              isLoading={showSceleton}
              containerStyle={styles.new}
              layout={[
                { flex: 1, width: 148, height: 11, marginBottom: 10 },
                { flex: 1, width: 190, height: 17 },
              ]}
            >
              <Text
                numberOfLines={1}
                ellipsizeMode="tail"
                style={styles.articleTitle}
              >
                {item.title || "Title"}
              </Text>

              <Text
                numberOfLines={1}
                ellipsizeMode="tail"
                style={styles.comment}
              >
                {item.comment || ""}
              </Text>
            </SkeletonLoader>

            <SkeletonLoader
              isLoading={showSceleton}
              containerStyle={{ alignSelf: "flex-end" }}
              layout={[{ flex: 1, width: 50, height: 17, maxHeight: 18 }]}
            >
              <Text style={styles.date}>
                {
                  /*getPeriod(item.updatedAt, activeTab) || ""*/ item.createdAt ||
                  item.updatedAt
                    ? moment(
                        item.updatedAt ? item.updatedAt : item.createdAt
                      ).format("DD.MM.YYYY")
                    : ""
                }
              </Text>
            </SkeletonLoader>
          </View>
        </View>
      </BoxShadow>
    </TouchableOpacity>
  );

  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }} showGradient={true}>
      <SafeAreaView>
        <Header title={"PMP News"} backTitle routeName={route.name} />
      </SafeAreaView>

      <Tabs
        startActiveIndex={activeTab}
        offsetHorizontal={16}
        style={styles.periodSelection}
        elementStyle={{
          minWidth: 75,
          borderColor: "transparent",
          backgroundColor: "#fff",
          borderWidth: 0,
        }}
        onTabActiveIndexSet={setActiveTab}
        listData={headerPeriodTabs}
      />

      <View style={{ flex: 1 }}>
        {!errorRequest && dataList.length ? (
          <FlatList
            data={dataList.length > 0 ? dataList : []}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item, index }) => (
              <Animatable.View
                duration={550}
                delay={index * 200}
                animation="fadeInUp"
              >
                {renderNewsListItem(item, index)}
              </Animatable.View>
            )}
            onEndReached={onLoad}
          />
        ) : !isFetch && !showSceleton && !errorRequest && !dataList.length ? (
          <View style={styles.loader}>
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Список новостей пуст
            </Text>
          </View>
        ) : !isFetch && !showSceleton && errorRequest ? (
          <View style={styles.loader}>
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Ошибка получения данных
            </Text>
          </View>
        ) : null}
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  loading: state.NewsReducer.loading,
  news: state.NewsReducer.news,
  errorRequest: state.NewsReducer.error,
});

const mapDispatchToProps = {
  getNews,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsScreen);

const styles = {
  container: {
    flex: 1,
    padding: 20,
  },
  title: {
    fontSize: 36,
    fontWeight: "900",
  },
  periodSelection: {
    height: "100%",
    maxHeight: 30,
    width: "auto",

    flexDirection: "row",
    marginBottom: 24,
    marginTop: 16,
  },
  period: {
    height: "100%",
    textTransform: "uppercase",
    marginRight: 12,
    borderWidth: 1,
    borderColor: "rgba(21, 28, 38, .12)",
    borderRadius: 30,

    justifyContent: "center",
    alignItems: "center",
  },
  titlePeriod: {
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  headerOptions: {
    justifyContent: "space-between",
    flexDirection: "row",
    marginTop: 40,
    width: "100%",
  },
  loader: {
    flex: 1,
    minHeight: height / 2.7,
    alignItems: "center",
    justifyContent: "center",
  },
  article: {
    borderRadius: 10,
    backgroundColor: "white",
    position: "relative",
  },
  articleImage: {
    width: "100%",
    height: 174,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  articleTitle: {
    ...fonts.h2,
    ...fonts.normal,
  },
  articleMode: {
    position: "absolute",
    right: 16,
    top: 16,
    width: 28,
    height: 28,
  },
  new: {
    flex: 1,
    paddingRight: 10,
  },
  content: {
    width: "100%",
    paddingHorizontal: 12,
    paddingVertical: 16,
    display: "flex",
    height: 72,
    flexDirection: "row",
    alignItems: "flex-end",
  },
  comment: {
    ...fonts.h3,
    ...fonts.description,
    color: "#151C26",
    opacity: 0.54,
    paddingTop: 3,
  },
  date: {
    ...sizes.description,
    ...fonts.normal,
    color: "#151C26",
    opacity: 0.54,
  },
};
