import React, { useEffect, useState, createRef, useCallback } from "react";
import {
  Dimensions,
  FlatList,
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import moment from "moment";
// import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import { connect } from "react-redux";

// import { navigateBackFromItem } from '../../../rn-navigation/screens'
import { ToastView } from "../../../components";
import { StoreAPI } from "../../../api/store";
import { colors, fonts, sizes } from "../../../constants/theme";
import { Background } from "../../../components/Background";
import Header from "../../../components/Headers/Header";
import SwipeWebView from "../../../components/swipeWebView";
import {
  getHistoryState,
  cleanHistoryState,
} from "../../../actions/BasketActions";
import {
  getSizeSkeletonLoaders,
  handleLinkOpen,
  validURL,
  formattingPrice,
} from "../../../utils";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader";
import ListWithInfo from "../../../components/ListWithInfo";

const HistoryScreen = ({
  getHistoryState = () => {},
  cleanHistoryState = () => {},

  navigation,

  historyLoading = true,
  history = [],
  countItems = 0,
  historyFailed = null,
  moreHistoryListDataLoading = false,
  historyListMeta = {},
}) => {
  const [limit, setLimit] = useState(10);
  const [orderData, setOrderData] = useState("desc");

  const [currentWebUrl, setCurrentWebUrl] = useState("");
  const [approveToPayment, setApproveToPayment] = useState(false);

  const [loadingNewOrderUrlPayment, setLoadingNewOrderUrlPayment] = useState(
    false
  );
  let paymentWebViewRef = createRef(null);

  useEffect(() => {
    if (approveToPayment && paymentWebViewRef && paymentWebViewRef.current) {
      paymentWebViewRef.current.show();
    }
  }, [approveToPayment]);

  useEffect(() => {
    if (!loadingNewOrderUrlPayment) {
      console.log("can New open", currentWebUrl);

      if (
        currentWebUrl &&
        currentWebUrl.trim() &&
        validURL(currentWebUrl.trim())
      )
        paymentWebViewRef.current.show();
    }
  }, [loadingNewOrderUrlPayment]);

  //
  const getShowWebView = async (orderId = null) => {
    try {
      if (orderId && !isNaN(+orderId)) {
        setLoadingNewOrderUrlPayment(true);

        const response = await StoreAPI.getNewUrlOrderPayment(orderId);

        if (
          response &&
          response.status &&
          response.status === "success" &&
          !response.error &&
          response.data &&
          response.data.url &&
          response.data.url.trim()
        ) {
          setCurrentWebUrl(response.data.url.trim());
          setApproveToPayment(true);

          setLoadingNewOrderUrlPayment(false);
        } else
          ToastView(
            "Упс, повторная оплата по этому номеру чека невозможнa.\nПроизошла ошибка.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27.",
            2500
          );

        console.log("NEW URL RESPONSE", response);
      } else
        ToastView(
          "Упс, неверный номер чека.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27.",
          2500
        );
    } catch (e) {
      console.log("Error: getNewOrderUrlPayment ---- ", e);

      ToastView(
        "Упс, что-то пощло не так.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27.",
        2500
      );
    }
  };

  const handleGetHistory = () => getHistoryState(false, 0, limit, orderData);

  console.log("MY HISTORY DATA: ", history, ", max Count: ", countItems);

  const listItem = (item = {}, index = 0, dataList = [], isLoading = false) => {
    // const isLoading = loading || (item && item.isTest);
    // const Wrap = !isLoading ? Animatable.View : View;

    const {
      createdAt: date = "",
      paymentUrl = "",
      updatedAt = "",
      price = 0,
      id: numbeOrder = Date.now().toString().slice(0, 2) +
        Date.now()
          .toString()
          .slice(
            Date.now().toString().length - 2,
            Date.now().toString().length
          ),
    } = item;

    const disabledPaymentBtn =
      !paymentUrl || !validURL(paymentUrl) || historyLoading;

    return (
      <View style={styles.item}>
        <View style={styles.numberAndCostPart}>
          <Text style={styles.orderTitleStyle}>Заказ №</Text>

          <SkeletonLoader
            layout={[
              { height: 20, width: 150 },
              { height: 20, width: 50 },
            ]}
            containerStyle={{
              marginTop: 5,
              flexDirection: "row",
              justifyContent: "space-between",
              paddingRight: 4,
            }}
            isLoading={isLoading}
          >
            <Text style={styles.orderNumberStyle}>{numbeOrder}</Text>

            {price !== false ? (
              <Text style={styles.orderCostStyle}>
                {formattingPrice(price)} ₽
              </Text>
            ) : null}
          </SkeletonLoader>
        </View>

        <View
          style={{
            padding: 15,

            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <SkeletonLoader
            layout={[{ height: 20, width: 60, paddingBottom: 15 }]}
            containerStyle={styles.dateAndRepeatPart}
            isLoading={isLoading}
          >
            <Text
              ellipsizeMode={"clip"}
              numberOfLines={2}
              style={styles.orderDate}
            >
              {(date && date.trim()) || (updatedAt && updatedAt.trim())
                ? moment(date && date.trim() ? date : updatedAt).format(
                    "DD/MM/YYYY"
                  )
                : null}
            </Text>
          </SkeletonLoader>

          <TouchableOpacity
            focusedOpacity={disabledPaymentBtn ? 1 : 0.5}
            onPress={() => {
              if (!disabledPaymentBtn) {
                setCurrentWebUrl("");
                getShowWebView(numbeOrder);
              }

              /*!disabledPaymentBtn ? handleLinkOpen(paymentUrl) : null;*/
            }}
          >
            <Text
              style={{
                ...styles.repeatButton,
                color: disabledPaymentBtn ? "#ccc" : colors.orange,
              }}
            >
              ПОВТОРИТЬ
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const datalist = historyLoading
    ? getSizeSkeletonLoaders(150, 120)
    : !historyLoading && history && history.length
    ? history
    : [];

  return (
    <View style={{ flex: 1, backgroundColor: "#FAFAFA" }}>
      <Header title={"История заказов"} />

      <View style={{ flex: 1 }}>
        <ListWithInfo
          requestDidMountCopmponent={handleGetHistory}
          requestWillUnmountComponent={cleanHistoryState}
          mocsDataOffsetTop={150}
          mocsDataListElementSize={140}
          heightDimensionsContainer={1.7}
          existLikeKeys={true}
          limitContentList={limit}
          data={history}
          moreLoading={moreHistoryListDataLoading}
          listMeta={historyListMeta}
          error={historyFailed}
          loading={historyLoading}
          wrapListParams={{
            showsVerticalScrollIndicator: false,
            style: styles.picturesContainer,
            contentContainerStyle: { paddingBottom: 70 },
          }}
          elementKeyName={"history-payments-item-"}
          renderListItem={listItem}
          onMoreDataList={(offset = 0) => {
            getHistoryState(false, offset, limit, "desc", true);
          }}
        />
      </View>

      <LinearGradient
        pointerEvents={"none"}
        colors={["rgba(255, 255, 255, 0)", "#FFF"]}
        style={styles.linearGradient}
      />

      <SwipeWebView
        uri={currentWebUrl}
        swipeModalRef={paymentWebViewRef}
        // contentContainerStyle={{
        //   minHeight: 1000,
        //   paddingBottom: 500,
        // }}
        setCurrentWebUrl={setCurrentWebUrl}
        heightSheet={Dimensions.get("screen").height - 75}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  historyLoading: state.BasketReducer.historyLoading,
  history: state.BasketReducer.history,
  moreHistoryListDataLoading: state.BasketReducer.moreHistoryListDataLoading,
  historyListMeta: state.BasketReducer.historyListMeta,
  historyFailed: state.BasketReducer.historyFailed,
});

export default connect(mapStateToProps, {
  getHistoryState,
  cleanHistoryState,
})(HistoryScreen);

const styles = StyleSheet.create({
  backgroundImage: {
    opacity: 0.9,
    resizeMode: "cover",
  },
  background: {
    width: "100%",
    height: "100%",
    position: "relative",
  },
  separator: {
    flex: 1,
  },
  header: {
    zIndex: 3,
    marginTop: 10,
    marginBottom: 14,
    paddingHorizontal: 9,
  },
  titleText: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    lineHeight: 22,
    fontWeight: "500",
    marginLeft: -8,
    textAlign: "center",
    color: colors.mainBlack,
    letterSpacing: -0.5,
  },
  linearGradient: {
    flex: 1,
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 251,
  },
  item: {
    paddingBottom: 20,
    height: 112,

    backgroundColor: "#FFF",
    borderRadius: 10,
    marginBottom: 8,
    marginHorizontal: 16,
    elevation: 1,
    shadowOpacity: 0.4,
    shadowRadius: 1,
    shadowColor: "#F5F5F5",
    shadowOffset: {
      height: 1,
      width: 0,
    },
  },
  numberAndCostPart: {
    paddingTop: 16,
    paddingBottom: 10,
    paddingHorizontal: 12,
    borderColor: "#E0E0E0",
    borderBottomWidth: 1,
  },
  orderTitleStyle: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 12,
    lineHeight: 14,
    color: colors.mainBlack,
    opacity: 0.54,
  },
  orderNumberStyle: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 16,
    lineHeight: 19,
    color: colors.mainOrange,
    opacity: 0.87,
  },
  orderCostStyle: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 16,
    lineHeight: 19,
    color: colors.mainOrange,
    textAlign: "right",
    fontWeight: "500",
  },
  // dateAndRepeatPart: {
  //   flex: 1,
  //   paddingLeft: 12,
  //   paddingRight: 16,
  //   flexDirection: "row",
  //   justifyContent: "flex-start",
  // },
  repeatButton: {
    color: colors.orange,
    textAlign: "right",
    fontSize: 14,
    lineHeight: 17,
    fontFamily: fonts.medium.fontFamily,
    // fontWeight: '500',
  },
  orderDate: {
    color: colors.mainBlack,
    opacity: 0.54,
    fontSize: 14,
    lineHeight: 17,
    fontFamily: fonts.normal.fontFamily,
  },
});
