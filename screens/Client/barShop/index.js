import {ItemBasketProductScreen} from './ItemBasketProductScreen.js';
import MenuListScreen from './MenuListScreen.js';
import BasketScreen from './BasketScreen.js';
import PaymentScreen from './PaymentScreen.js';
import HistoryScreen from './HistoryScreen.js';

export { ItemBasketProductScreen, MenuListScreen, BasketScreen, PaymentScreen, HistoryScreen };
