import React from "react";
// import Header from "./../components/header";
import {
  Container,
  Content,
  Item,
  Form,
  Input,
  Label,
  Button,
  Text
} from "native-base";
import styles from "../css/index";
import { Header } from "react-native-elements";
import CustomFooter from "../components/footer";

const searchRecord = () => {
  return (
    <Container style={[styles.container]}>
      <Header
        backgroundColor={"#fff"}
        leftComponent={{ icon: "chevron-left", color: "#000" }}
        centerComponent={{
          text: "Cоздать учетную запись",
          style: { color: "#000" }
        }}
      />
      <Content>
        <Form style={[styles.mt]}>
          <Item stackedLabel style={[styles.mt]}>
            <Label style={[styles.title]}>СМС-код</Label>
            <Input style={[styles.pl]} keyboardType="number-pad" />
          </Item>
          <Item stackedLabel style={[styles.mt]}>
            <Label style={[styles.title]}>Email</Label>
            <Input style={[styles.pl]} />
          </Item>
          <Item stackedLabel style={[styles.mt]}>
            <Label style={[styles.title]}>Пароль</Label>
            <Input style={[styles.pl]} />
          </Item>
          <Item stackedLabel style={[styles.mt]}>
            <Label style={[styles.title]}>Подтвердите пароль</Label>
            <Input style={[styles.pl]} keyboardType="number-pad" />
          </Item>
          <Button style={[styles.center, styles.btn]}>
            <Text>Создать пароль</Text>
          </Button>
        </Form>
      </Content>
      {/*<CustomFooter />*/}
    </Container>
  );
};

export default searchRecord;
