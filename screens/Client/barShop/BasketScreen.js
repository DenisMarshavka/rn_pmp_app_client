import React, { useState, useEffect, useRef, createRef } from "react";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  AsyncStorage,
  Dimensions,
  ScrollView,
  ActivityIndicator,
  Linking,
} from "react-native";
import { BoxShadow } from "react-native-shadow";
import { Slider } from "react-native-elements";
import { connect } from "react-redux";

import { Background } from "../../../components/Background";
import { colors, fonts } from "../../../constants/theme";
import Header from "../../../components/Headers/Header";
import ButtonInput from "../../../components/button/ButtonInput.tsx";
import SwipeWebView from "../../../components/swipeWebView";
import { getGeneratePaymentLink } from "../../../actions/index";
import ListWithInfo from "../../../components/ListWithInfo";
import {
  getSizeSkeletonLoaders,
  checkAsyncStorageUserByData,
  showAlert,
  handleLinkOpen,
  validURL,
  getUserStorageData,
  formattingPrice,
} from "../../../utils";
import { Tabs } from "../../../components/Tabs/Tabs";
import {
  getCurrentBasketState,
  getBasketStateDataReset,
  addBasketProductById,
  removeProductItemFromBasket,
} from "../../../actions/BasketActions";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader";
import BasketListItem from "../../../components/listsItem/BasketListItem";
import { MyNotificationAPI } from "../../../api/myNotification";
import { StoreAPI } from "../../../api/store";

const { width, height } = Dimensions.get("window");

const shadowOpt = {
  width: width - 32,
  height: 110,
  color: "#000",
  border: 10,
  radius: 15,
  opacity: 0.03,
  x: 0,
  y: 0.2,
  style: {
    borderRadius: 10,
    marginBottom: 15,
    margin: 0,
    marginLeft: "auto",
    marginRight: "auto",
  },
};

const BasketScreen = (props) => {
  const {
    getCurrentBasketState = () => {},
    getBasketStateDataReset = () => {},

    basketLoading = true,
    basket = [],
    countBasketItems = 0,
    basketFailed = null,
    basketListMeta = {},
    moreBasketListDataLoading = false,
    listProductsId = [],
    priceAll = 0,

    addBasketProductById = () => {},
    removeProductItemFromBasket = () => {},

    getGeneratePaymentLink = () => {},
    paymentLinkLoading = true,
    paymentLink = "",
    paymentLinkError = false,
  } = props;

  const [basketList, setBasketList] = useState(basket);
  const [sliderValue, setSliderValue] = useState(10);
  const [modalVisible, setModalVisible] = useState(false);
  const [currentBasketItem, setCurrentBasketItem] = useState(null);
  const [tabState, setTabState] = useState(100);
  const [paymentList, setPaymentList] = useState([]);
  const [productsIdList, setProductsIdList] = useState([]);

  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(3);
  const [orderData, seOrderData] = useState("desc");

  const [paymentTitle, setPaymentTitle] = useState("");
  const [showPaymentSheet, setShowPaymentSheet] = useState(false);
  const [approveToPayment, setApproveToPayment] = useState(true);
  const [preOrders, setPreOrders] = useState([]);
  const [isUpdate, setIsUpdate] = useState(false);
  const [currentWebUrl, setCurrentWebUrl] = useState("");
  const [allPreOrderSum, setAllPreOrderSum] = useState(0);
  const [changedURlWithParams, setChangedURlWithParams] = useState(false);

  const [timeOutUpdateBasket, setTimeOutUpdateBasket] = useState(null);

  const swipeModalRef = createRef(null);

  const handleBasketState = () =>
    getCurrentBasketState(true, 0, limit, "desc", false, false);

  console.log("listMetalistMeta Component: ", basketListMeta);

  console.log(
    "BASKETT: ",
    basket,
    "LINK PAYMENT: ",
    paymentLink,
    "tabState",
    tabState
  );

  const handleUnmountComponent = () => {
    setIsUpdate(false);

    getBasketStateDataReset();
    setTimeOutUpdateBasket(null);
    setPaymentTitle("");
  };

  // useEffect(() => {
  //   const run = `(function() {
  //     document.body.style.backgroundColor = 'blue'
  //
  //     if (document.body.innerText.toLowerCase().indexOf('не хватает денег') > -1) {
  //       window.location = window.location.href + '&native=' + JSON.stringify({
  //           functionName: "error", data: document.body.innerText
  //       })
  //     }
  //
  //     true
  //   })()`;
  //
  //   setTimeout(() => {
  //     console.log(
  //       "URL UPDATED",
  //       swipeModalRef,
  //       "swipeModalRef.injectJavaScript",
  //       swipeModalRef.injectJavaScript
  //     );
  //
  //     if (
  //       currentWebUrl &&
  //       currentWebUrl.trim() &&
  //       swipeModalRef &&
  //       swipeModalRef.injectJavaScript
  //     )
  //       swipeModalRef.injectJavaScript(run);
  //   }, 3000);
  // }, [currentWebUrl]);

  // useEffect(() => {
  //   if (!basketLoading) setFetchFinished(false);
  // }, [basketLoading]);

  useEffect(() => {
    setPaymentTitle(tabState !== 2 && tabState !== 3 ? "Оплатить" : "Заказать");

    if (!basketLoading && !moreBasketListDataLoading) onFilterByActiveTab();
  }, [tabState]);

  useEffect(() => {
    if (swipeModalRef && swipeModalRef.current) {
      if (showPaymentSheet) {
        swipeModalRef.current.show();
      } else swipeModalRef.current.hide();
    }
  }, [showPaymentSheet]);

  useEffect(() => {
    if (!basketLoading && basket && !moreBasketListDataLoading && !isUpdate)
      setBasketList(basket);
  }, [basketLoading, basket, moreBasketListDataLoading]);

  useEffect(() => {
    if (!moreBasketListDataLoading) {
      const newDataPreOrder = [];

      if (basketList.length) {
        for (let item of basketList)
          if (item.id) {
            // if (!item.isOrganicBeauty && !item.isOrganicFood) {
            //   newData.push({ id: item.id, count: item.count });
            // }

            if (item.isOrganicBeauty || item.isOrganicFood)
              newDataPreOrder.push({ id: item.id, count: item.count });
          }
      }

      // if (newData.length) setPaymentList(newData);
      if (newDataPreOrder.length) setPreOrders(newDataPreOrder);
    }
  }, [moreBasketListDataLoading]);

  useEffect(() => {
    if (!basketLoading && basket && basket.length)
      getGeneratePaymentLink("PRODUCT", { items: listProductsId });
  }, [basketList, basketLoading]);

  useEffect(() => {
    let summAllPreOrder = 0;

    if (preOrders && preOrders.length) {
      summAllPreOrder = formattingPrice(
        Object.values(basketData).reduce(
          (sum, item) =>
            item.isOrganicBeauty || item.isOrganicFood
              ? sum + item.price * item.count
              : sum,
          0
        )
      );
    }

    if (summAllPreOrder) setAllPreOrderSum(summAllPreOrder);
  }, [preOrders]);

  //For filter
  const onFilterByActiveTab = () => {
    if (tabState !== false && !isNaN(+tabState)) {
      console.log("tabStatetabStatetabState: ", tabState, basket);

      // setBasketList(
      //   [...basket].filter((item) => {
      //     if (tabState === 100) return true;
      //
      //     return item && headerSortTabs[tabState].category
      //       ? item[headerSortTabs[tabState].category]
      //       : item && item.category_product_id
      //       ? item.category_product_id === tabState
      //       : true;
      //   })
      // );

      getCurrentBasketState(
        false,
        0,
        limit,
        "desc",
        true,
        false,
        tabState === 2,
        tabState === 3,
        false,
        false,
        tabState === 1
      );
    }
  };

  const generateButtonPymentTitle = () => {
    if (
      basketList &&
      basketList.length &&
      tabState &&
      // tabState !== 2 &&
      tabState !== 3
    ) {
      console.log("preOrders", preOrders, "paymentList", paymentList);

      setPaymentTitle(
        (!preOrders || !preOrders.length) && paymentList && paymentList.length
          ? "Оплатить"
          : preOrders && preOrders.length && paymentList && paymentList.length
          ? "Оплатить и заказать"
          : "Заказать"
      );
    } else if (!basketList || !basketList.length) setPaymentTitle("");
  };

  useEffect(() => {
    if (!showPaymentSheet)
      getCurrentBasketState(false, 0, limit, "desc", false, false);
  }, [showPaymentSheet]);

  useEffect(() => {
    if (basket && basket.length) generateButtonPymentTitle();
  }, [basketList, tabState, paymentList, preOrders, basket]);

  useEffect(() => {
    if (offset) getCurrentBasketState(true, offset, limit, "desc", false, true);
  }, [offset]);

  console.log("BASKETT2222: ", basketList);

  const rangeModal = () => (
    <View style={styles.backgroundModal}>
      <View style={styles.blockModal}>
        <View style={{ width: "100%", paddingHorizontal: 16 }}>
          <Text style={{ fontFamily: fonts.normal.fontFamily, fontSize: 20 }}>
            {sliderValue}
          </Text>

          <Slider
            minimumValue={1}
            maximumValue={10}
            value={Number(sliderValue)}
            minimumTrackTintColor={"#F46F22"}
            maximumTrackTintColor="#C7C7CC"
            onValueChange={(value) => setSliderValue(Math.floor(value))}
            thumbTintColor={"#F46F22"}
          />

          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              marginTop: 16,
            }}
          >
            <TouchableOpacity onPress={() => setModalVisible(false)}>
              <Text
                style={{ fontFamily: fonts.normal.fontFamily, fontSize: 20 }}
              >
                Отмена
              </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => saveCountInBasketItem()}>
              <Text
                style={{ fontFamily: fonts.normal.fontFamily, fontSize: 20 }}
              >
                Сохранить
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );

  const saveCountInBasketItem = async () => {
    const indexBasket = basketList.indexOf(currentBasketItem);

    if (
      indexBasket !== -1 &&
      currentBasketItem &&
      currentBasketItem.id &&
      !isNaN(+currentBasketItem.id)
    ) {
      const oldBasketState = [...basketList];
      const newBasketState = (oldBasketState[indexBasket].count = sliderValue);

      // setIsUpdate(true);

      await addBasketProductById(
        {
          id: currentBasketItem.id,
          count: sliderValue,
          type: currentBasketItem.type,
        },
        false
      );

      // console.log("UPDATE PRODUCT: ", {
      //   id: currentBasketItem.id,
      //   count: sliderValue,
      //   type: currentBasketItem.type,
      // });
    }

    setModalVisible(false);
  };

  const removeItem = (item = {}, params = {}) => {
    let newBasket = [];

    if (
      !basketLoading &&
      basket &&
      basket.length &&
      Object.keys(item).length &&
      Object.keys(params).length
    ) {
      newBasket = basket.filter(
        (product) => product.id !== item.id && params.type === item.type
      );

      setBasketList([...newBasket]);
    }

    setIsUpdate(true);
    removeProductItemFromBasket(params);
    generateButtonPymentTitle();
  };

  const sliderOpenHandler = (item) => {
    setCurrentBasketItem(item);
    setSliderValue(item.count);
    setModalVisible(true);
  };

  const headerSortTabs = [
    { id: 100, title: "Всё", isTest: basketLoading },
    { id: 1, title: "PMP Sport", category: "isSport", isTest: basketLoading },
    {
      id: 2,
      title: "PMP Organic Beauty",
      category: "isOrganicBeauty",
      isTest: basketLoading,
    },
    {
      id: 3,
      title: "PMP Organic Food",
      category: "isOrganicFood",
      isTest: basketLoading,
    },
  ];

  // const getAllProductsSum = () =>
  //   formattingPrice(
  //     Object.values(basketData).reduce(
  //       (sum, item) => sum + item.price * item.count,
  //       0
  //     )
  //   );

  const renderListItem = (
    item = {},
    index = 0,
    list = [],
    isLoading = false
  ) => {
    console.log("BBBBASKET ITEM: ", item, "BBBBASKET ITEM INDEX: ", index);

    return (
      <BasketListItem
        item={item}
        index={index}
        removeProductItemFromBasket={removeItem}
        sliderOpenHandler={sliderOpenHandler}
        loading={isLoading}
        navigation={props.navigation}
      />
    );
  };

  const basketData = basketLoading ? [] : basketList;

  console.log(
    "(tabState !== 2 || tabState !== 3) && paymentList.length",
    (tabState !== 2 || tabState !== 3) && paymentList.length,
    "tabState === 2 || tabState === 3 && basketList && basketList.length",
    (tabState === 2 || tabState === 3) && basketData && basketData.length
  );

  const notEmptyBasket = basketData && basketData.length;

  const existingPaymentLinkOrPreOrder =
    (approveToPayment && validURL(paymentLink)) ||
    (preOrders && preOrders.length);

  const enabledPaymentBtn =
    !basketLoading &&
    notEmptyBasket &&
    !basketFailed &&
    existingPaymentLinkOrPreOrder;

  console.log("basketList", basketList);

  const handleUpdateBasketState = async () => {
    await getCurrentBasketState(false, offset, limit, "desc", false, false);
  };

  const onPushNotificationSet = async (
    isPreOrder = false,
    preOrderId = null
  ) => {
    const currentUser = await getUserStorageData();
    const body = !isPreOrder
      ? `Оплата прошла успешно!\nСума заказа составляет: ${
          /*getAllProductsSum()*/ priceAll || 0
        }₽`
      : isPreOrder && !isNaN(+preOrderId)
      ? `Мы начали готовить Ваш заказ. Оплату Вы сможете произвести на рецепции Студии PMP Olymp\nВсего к оплате: ${allPreOrderSum}₽\nНомер вашего чека: ${preOrderId}`
      : `При создании предзаказа, случилась ошибка! \nПожалуйста сообщите нам об этом в нашу тех. Поддержку...`;

    if (
      currentUser &&
      currentUser.push_token &&
      ((isPreOrder && allPreOrderSum) || !isPreOrder)
    ) {
      await MyNotificationAPI.showNotificationNow({
        to: currentUser.push_token,
        sound: "default",
        title:
          isPreOrder && !isNaN(+preOrderId)
            ? "Благодарим Вас!"
            : "Упс, что-то пощло не так :(",
        body,
        data: {
          image: "../../../pre_order_notify_image.png",
        },
      });
    }
  };

  const onCreatePreOrder = async () => {
    const response = await StoreAPI.createPreOrder();

    if (response && response.status && response.status === "success") {
      onPushNotificationSet(true, response.data.id);
    }

    console.log("RESPONSE PRE ORDER: ", response);
  };

  return (
    <View style={{ position: "relative", flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: "#fff" }}>
        <Header
          title={"Корзина"}
          customIcon={require("../../../assets/images/paymentHistory.png")}
          handleclick={() => props.navigation.navigate("HistoryOrders")}
          backTitle
        />

        <View
          style={{
            flex: 1,
            minHeight: Dimensions.get("screen").height / 2.9,
          }}
        >
          <Tabs
            onDataByCurrentActiveTab={setTabState}
            listData={headerSortTabs}
          />

          <ListWithInfo
            requestDidMountCopmponent={handleBasketState}
            requestWillUnmountComponent={handleUnmountComponent}
            mocsDataOffsetTop={100}
            mocsDataListElementSize={300}
            heightDimensionsContainer={1.7}
            listMeta={basketListMeta}
            moreLoading={moreBasketListDataLoading}
            data={basketData}
            error={basketFailed}
            loading={basketLoading}
            wrapListParams={{
              showsVerticalScrollIndicator: false,
              style: styles.picturesContainer,
            }}
            withWrapElementComponent={true}
            WrapElementComponent={BoxShadow}
            wrapElemntComponentProps={{
              setting: shadowOpt,
            }}
            elementKeyName={"basket-product-item-"}
            renderListItem={renderListItem}
            onMoreDataList={(offset = 0) => {
              setOffset(offset);
            }}
          />
        </View>

        <View style={styles.separator}>
          {
            <View style={styles.overviewPriceStyles}>
              <Text style={styles.totalCostText}>Всего: &nbsp;</Text>

              <Text style={styles.totalCostNumber}>
                {/*getAllProductsSum()*/ priceAll || 0} ₽
              </Text>
            </View>
          }
        </View>

        <View
          style={{
            padding: 16,
            marginBottom: Dimensions.get("screen").height / 21,
          }}
        >
          {/*<ButtonInput disabled={!basketList.length} title="Выбрать дату и время" onClick={ () => props.navigation ? props.navigation.navigate('Payment') : null } />*/}
          <ButtonInput
            disabled={!enabledPaymentBtn}
            title={paymentTitle}
            onClick={() => {
              if (paymentLink && paymentLink.trim())
                handleLinkOpen(paymentLink, true, () => {
                  if (paymentList && paymentList.length) {
                    setShowPaymentSheet(true);
                    setApproveToPayment(false);
                  }
                });

              if (preOrders && preOrders.length) {
                onCreatePreOrder();

                getCurrentBasketState(false, 0, limit, "desc", false, false);
              }
            }}
          />
        </View>
      </View>

      {showPaymentSheet ? (
        <SwipeWebView
          uri={paymentLink}
          // setSwipeModalRef={setSwipeModalRef}
          swipeModalRef={swipeModalRef}
          contentContainerStyle={{
            // minHeight: 1000,
            // paddingBottom: 500,
            flex: 1,
          }}
          onBackDropPress={() => {
            setShowPaymentSheet(false);
            setApproveToPayment(true);
          }}
          setCurrentWebUrl={setCurrentWebUrl}
          heightSheet={height - 75}
          // onSuccessPayment={onSuccessPayment}
          onUpdateBasketState={() => {
            setTimeOutUpdateBasket(
              setTimeout(() => {
                getCurrentBasketState(
                  false,
                  offset,
                  limit,
                  "desc",
                  false,
                  false
                );
              }, 2000)
            );
          }}
        />
      ) : null}

      {modalVisible ? rangeModal() : null}
    </View>
  );
};

const mapStateToProps = (state) => ({
  basketLoading: state.BasketReducer.basketLoading,
  basket: state.BasketReducer.basket,
  basketFailed: state.BasketReducer.basketFailed,
  basketListMeta: state.BasketReducer.basketListMeta,
  moreBasketListDataLoading: state.BasketReducer.moreBasketListDataLoading,
  listProductsId: state.BasketReducer.listProductsId,
  priceAll: state.BasketReducer.priceAll,

  paymentLinkLoading: state.mainReducer.paymentLinkLoading,
  paymentLink: state.mainReducer.paymentLink,
  paymentLinkError: state.mainReducer.paymentLinkError,
});

const mapDispatchToProps = {
  getCurrentBasketState,
  getBasketStateDataReset,

  addBasketProductById,
  removeProductItemFromBasket,

  getGeneratePaymentLink,
};

export default connect(mapStateToProps, mapDispatchToProps)(BasketScreen);

const styles = StyleSheet.create({
  periodSelection: {
    height: "100%",
    maxHeight: 30,
    marginBottom: 15,

    position: "relative",
    zIndex: 1,
    flexDirection: "row",
    marginTop: 5,
  },
  backgroundModal: {
    backgroundColor: "#323232",
    opacity: 0.9,
    position: "absolute",
    width: "100%",
    height: "100%",
    flex: 1,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: "center",
  },
  blockModal: {
    backgroundColor: "#f7f7f7",
    paddingVertical: 18,
    borderRadius: 14,
    alignItems: "center",
    zIndex: 10,
    marginHorizontal: 16,
  },
  backgroundImage: {
    resizeMode: "cover",
  },
  background: {
    width: "100%",
    height: "100%",
    position: "relative",
  },
  separator: {
    paddingHorizontal: 16,
    width: "100%",

    margin: 0,
    marginLeft: "auto",
    marginRight: "auto",
  },
  picturesContainer: {
    paddingVertical: 10,
    minHeight: 112,
    flex: 1,

    position: "relative",
    zIndex: 15,
  },
  pictureListItem: {
    width: 80,
    height: 80,
  },
  itemInfo: {
    flex: 1,
    marginLeft: 16,
    paddingVertical: 10,
    justifyContent: "space-between",
  },
  itemNameLine: {
    width: "100%",

    flexDirection: "row",
    justifyContent: "space-between",
  },
  listItemStyle: {
    borderRadius: 10,
    minHeight: 112,
    flex: 1,
    width: "100%",
    backgroundColor: "#FFF",
    padding: 16,
    marginBottom: 8,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",

    // shadowColor: "#000",
    // shadowOffset: {
    //   width: 0,
    //   height: 3,
    // },
    // shadowOpacity: 0.1,
    // shadowRadius: 4.65,
    //
    // elevation: 7,
  },
  itemCountAndPriceLine: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 0,
  },
  countStyle: {
    width: "100%",

    marginTop: 5,
    fontSize: 14,
    lineHeight: 17,
    color: colors.mainBlack,
    opacity: 0.54,
    fontFamily: fonts.normal.fontFamily,
  },
  itemName: {
    fontSize: 18,
    lineHeight: 18.5,
    marginTop: 4,
    opacity: 0.87,
    fontFamily: fonts.normal.fontFamily,
  },
  priceText: {
    fontSize: 16,
    lineHeight: 17,
    fontWeight: "500",
    alignSelf: "flex-end",
    color: colors.orange,
    textAlign: "right",
    opacity: 0.87,
    fontFamily: fonts.medium.fontFamily,
  },
  header: {
    zIndex: 3,
    marginTop: 2,
    marginBottom: 12,
    paddingHorizontal: 9,
  },
  overviewPriceStyles: {
    position: "relative",
    zIndex: 1,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 16,
  },
  totalCostText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 15,
    letterSpacing: 0.3,
    lineHeight: 16,
    color: colors.mainBlack,
    opacity: 0.54,
  },
  totalCostNumber: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 14,
    letterSpacing: 0.3,
    lineHeight: 16,
    color: colors.orange,
  },
  titleText: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    lineHeight: 22,
    fontWeight: "500",
    textAlign: "center",
    marginLeft: 28,
    marginTop: 8,
    color: colors.mainBlack,
  },
});
