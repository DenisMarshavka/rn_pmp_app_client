import React, { useEffect, useState } from "react";
import {
  FlatList,
  Image,
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { connect } from "react-redux";
import { LinearGradient } from "expo-linear-gradient";
import { BoxShadow } from "react-native-shadow";

import TitleHeader from "../../../components/Headers/TitleHeader";
import { dimensions } from "../../../styles";
import { colors, fonts } from "../../../constants/theme";
import { Background } from "../../../components/Background";
import {
  getProductsCategories,
  getProductsByParams,
  cleanProductsByParams,
} from "../../../actions/StoreActions";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader";
import { getSizeSkeletonLoaders, validURL } from "../../../utils";
import { Tabs } from "../../../components/Tabs/Tabs";
import Header from "../../../components/Headers/Header";
import ListWithInfo from "../../../components/ListWithInfo";
import { addBasketProductById } from "../../../actions/BasketActions";
import { formattingPrice } from "../../../utils";

const { width } = Dimensions.get("window");

const shadowOpt = {
  width: (dimensions.width - 48) / 3.1,
  height: 194,
  color: "#000",
  border: 11,
  radius: 8,
  opacity: 0.05,
  x: 0,
  y: 0.3,
  style: {
    borderRadius: 3,
    marginBottom: 8,
    marginRight: 8,
  },
};

const listItem = (
  item,
  navigation,
  isLoading = false,
  addBasketProductById = () => {}
) => {
  const { img = "", title = "", price = "", category_product = {} } = item;
  const { title: category } = category_product;

  console.log("PRODUCT IMG", img);

  return (
    <BoxShadow setting={shadowOpt}>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.item}
        onPress={() =>
          navigation.navigate("ItemBasketProduct", {
            item,
            addBasketProductById,
          })
        }
      >
        <SkeletonLoader
          item={item}
          layout={[
            {
              width: (dimensions.width - 48) / 3,
              height: 112,
              borderBottomRightRadius: 0,
              borderBottomLeftRadius: 0,
            },
          ]}
          isLoading={isLoading}
        >
          <Image
            source={
              img && img.trim()
                ? { uri: img }
                : require("../../../assets/images/notProductImage.png")
            }
            style={styles.pictureListItem}
          />
        </SkeletonLoader>

        <View style={{ flex: 1, padding: 8, justifyContent: "space-between" }}>
          <View>
            <SkeletonLoader
              item={item}
              layout={[{ width: "90%", height: 22 }]}
              isLoading={isLoading}
            >
              <Text
                ellipsizeMode={"clip"}
                numberOfLines={2}
                style={styles.itemName}
              >
                {title || "Неизвестный продукт"}
              </Text>
            </SkeletonLoader>

            <SkeletonLoader
              item={item}
              layout={[{ width: "75%", height: 16, marginTop: 5 }]}
              isLoading={isLoading}
            >
              <Text style={styles.sectionName}>{category}</Text>
            </SkeletonLoader>
          </View>

          <SkeletonLoader
            item={item}
            layout={[
              {
                flex: 1,
                width: "35%",
                alignSelf: "flex-end",
                height: 22,
                marginTop: 5,
              },
            ]}
            containerStyle={{ flex: 1, alignItems: "center" }}
            isLoading={isLoading}
          >
            <Text style={styles.priceText}>
              {price ? formattingPrice(price) + " ₽" : 0}
            </Text>
          </SkeletonLoader>
        </View>
      </TouchableOpacity>
    </BoxShadow>
  );
};

const MenuListScreen = (props) => {
  const {
    route = {},

    getProductsByParams = () => {},
    cleanProductsByParams = () => {},

    productsLoading = false,
    products = [],
    productsError = false,
    moreProductsLoading = false,
    productsListMeta = {},

    getProductsCategories = () => {},
    productsCategoriesLoading = false,
    productsCategories = [],
    productsCategoriesError = false,

    addBasketProductById = () => {},
  } = props;

  const maxHorizontalCount = Math.ceil(width / ((width - 48) / 2.5));
  const maxVerticalCount = Math.ceil(
    (Dimensions.get("screen").height - 100) / 250
  );
  const limit = maxHorizontalCount * maxVerticalCount;
  console.log("offsetoffset", limit);

  useEffect(() => {
    // getProductsByParams();
    getProductsCategories();
  }, []);

  const [categoryActive, setCategoryActive] = useState("Все");
  const [productsCategoriesData, setProductsCategoriesData] = useState([]);
  const [productsList, setProductList] = useState([]);

  const [fetchingStatus, setFetchingStatus] = useState(true);
  let timeOutProductsLoading = null;

  useEffect(() => {
    setFetchingStatus(true);

    return () => {
      setFetchingStatus(true);

      clearTimeout(timeOutProductsLoading);
    };
  }, []);

  useEffect(() => {
    if (!productsCategoriesLoading)
      setProductsCategoriesData([
        { id: 250, title: "Все" },
        ...productsCategories,
      ]);

    if (!productsCategoriesLoading) setFetchingStatus(false);
  }, [productsCategoriesLoading, productsCategories]);

  useEffect(() => {
    setProductList(
      productsLoading && !productsError
        ? getSizeSkeletonLoaders(100, 60)
        : !productsLoading && products && products.length
        ? products
        : []
    );

    if (!productsLoading && products.length)
      timeOutProductsLoading = setTimeout(setFetchingStatus, 500, false);
  }, [productsLoading, products]);

  const handleFilterProductsByCategory = (categoryName = "") => {
    if (categoryName.trim()) {
      setCategoryActive(categoryName);
      console.log("categoryName", categoryName);

      setProductList(
        products.filter((product) =>
          categoryName !== "Все"
            ? product &&
              product.category_product &&
              product.category_product.title === categoryName
            : true
        )
      );
    }
  };

  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <Header
        title={"Меню бара"}
        backTitle
        basketShow={true}
        routeName={route.name}
      />

      <Tabs
        firstTabTitle={0}
        offsetHorizontal={width / 23}
        style={{ marginTop: 15, marginBottom: 5 }}
        loading={productsCategoriesLoading}
        getDataByTabActive={handleFilterProductsByCategory}
        listData={productsCategoriesData}
        requestFailed={productsCategoriesError}
      />

      <ListWithInfo
        requestDidMountCopmponent={() =>
          getProductsByParams(false, 0, limit, false)
        }
        requestWillUnmountComponent={cleanProductsByParams}
        mocsDataOffsetTop={75}
        mocsDataListElementSize={100}
        heightDimensionsContainer={1.8}
        withRenderingAnimation={false}
        data={productsList}
        listMeta={productsListMeta}
        error={productsError}
        loading={productsLoading || fetchingStatus}
        moreLoading={moreProductsLoading}
        wrapListParams={{
          showsVerticalScrollIndicator: false,
          style: { ...styles.picturesContainer, width },

          contentContainerStyle: {
            ...styles.columnWrapperStyle,
            paddingBottom: 70,
            paddingTop: 10,
            flexDirection: "row",
            flexWrap: "wrap",
          },
        }}
        elementKeyName={"store-product-item-"}
        renderListItem={(item, index) =>
          listItem(item, props.navigation, fetchingStatus, addBasketProductById)
        }
        onMoreDataList={(offset = 0) =>
          getProductsByParams(false, offset, limit, true)
        }
      />

      <LinearGradient
        pointerEvents={"none"}
        colors={["rgba(255, 255, 255, 0)", "#FFF"]}
        style={styles.linearGradient}
      />
    </View>
  );
};

const mpStateToProps = (state) => ({
  productsLoading: state.StoreReducer.productsLoading,
  products: state.StoreReducer.products,
  productsError: state.StoreReducer.productsError,
  productsListMeta: state.StoreReducer.productsListMeta,
  moreProductsLoading: state.StoreReducer.moreProductsLoading,

  productsCategoriesLoading: state.StoreReducer.productsCategoriesLoading,
  productsCategories: state.StoreReducer.productsCategories,
  productsCategoriesError: state.StoreReducer.productsCategoriesError,
});

const mapDispatchToProps = {
  getProductsByParams,
  cleanProductsByParams,

  getProductsCategories,
  addBasketProductById,
};

export default connect(mpStateToProps, mapDispatchToProps)(MenuListScreen);

const styles = StyleSheet.create({
  separator: {
    marginTop: Platform.OS === "ios" ? 44 : 11,
  },
  picturesContainer: {
    flex: 1,
    marginTop: 15,
    paddingLeft: 15,
    paddingRight: 7,
  },

  pictureListItem: {
    width: (dimensions.width - 30 - 16) / 3.1,
    borderTopRightRadius: 3,
    borderTopLeftRadius: 3,
    height: 116,
    resizeMode: "cover",
  },
  itemName: {
    fontSize: 14,
    lineHeight: 16,
    opacity: 0.87,
    fontFamily: fonts.normal.fontFamily,
  },
  item: {
    borderRadius: 3,

    width: (dimensions.width - 48) / 3.1,
    //height: width < 400 ? 225 : 244,
    height: "100%",
    backgroundColor: "#FFF",
    flexWrap: "wrap",
  },
  sectionName: {
    fontSize: 12,
    color: "rgba(0, 0, 0, 0.87)",
    opacity: 0.54,
    fontFamily: fonts.normal.fontFamily,
  },
  priceText: {
    fontSize: 12,
    color: colors.orange,
    fontFamily: fonts.normal.fontFamily,
    textAlign: "right",
    //paddingVertical: 8,
    paddingBottom: 8,
    width: "100%",
    //position: 'absolute',
    //top: 10,
    //right: 8
  },
  columnWrapperStyle: {
    justifyContent: "space-between",
  },
  linearGradient: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 130,
  },
});
