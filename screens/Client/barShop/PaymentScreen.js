import React from 'react'
import {
  ImageBackground,
  Platform,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native'
import { TabView, SceneMap } from 'react-native-tab-view'
import { LinearGradient } from 'expo-linear-gradient'

import bgImage from '../../../assets/images/backgroundPayment.png'
import { CustomHeader, CardInputForm, CustomBottomButton } from '../../../components'
import applePayImage from '../../../assets/images/applePayImage.png'
import visaImage from '../../../assets/images/visaImage.png'
import payPalImage from '../../../assets/images/payPalImage.png'
import { dimensions } from '../../../styles'
import { colors, fonts, sizes } from "../../../constants/theme"

const CardInputFormFn = () => (
  <CardInputForm />
);

const handleSubmit = () => {};

const initialLayout = { width: dimensions.width };


const PaymentScreen = props => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'paypal', title: 'paypal' },
    { key: 'visa', title: 'visa' },
    { key: 'applePay', title: 'applePay' },
  ]);


  const renderScene = SceneMap({
    paypal: CardInputFormFn,
    visa: CardInputFormFn,
    applePay: CardInputFormFn,
  });

  const renderTabBar = props => {
      return (
        <View style={styles.tabBar}>
          {
              props.navigationState.routes.map((route, i) => {
                let tabImageSource;

                switch (route.key) {
                  case 'paypal':
                    tabImageSource = payPalImage;
                    break;

                  case 'visa':
                    tabImageSource = visaImage;
                    break;

                  case 'applePay':
                    tabImageSource = applePayImage;
                    break;
                }

              return (
                <TouchableOpacity
                  style={[styles.tabItem]}
                  onPress={() => setIndex(i)}>
                    <Image
                      source={tabImageSource}
                    />
                </TouchableOpacity>

              )
            }
          )}
        </View>
      );
  };

  return (
    <ImageBackground
      source={bgImage}
      style={styles.background}
      imageStyle={styles.backgroundImage}>
        <View style={styles.separator} />

        <CustomHeader
          back={true}
          navigateToMenu={() => props.navigation.goBack()}
          title={'Оплата'}
          style={styles.header}
          titleText={styles.titleText}
        />

        <LinearGradient
            start={{x: 1.0, y: 1.0}} end={{x: 0.0, y: 0.0}}
            colors={['#FFF', '#DADADA']} style={{ height: 395, marginTop: 10, paddingTop: 12, paddingBottom: 22, marginHorizontal: 16, borderRadius: 10}}
        >
          <TabView
            renderTabBar={(props) => renderTabBar(props)}
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            swipeEnabled={false}
            initialLayout={initialLayout}
          />
        </LinearGradient>

        {/*<CustomBottomButton style={{position: 'absolute', left: 0, bottom: 0}} label={'ОПЛАТИТЬ'} onPress={() => handleSubmit()} />*/}
    </ImageBackground>
    )
};

export default PaymentScreen

const styles = StyleSheet.create({
  backgroundImage: {
    resizeMode: 'cover'
  },
  background: {
    width: '100%',
    height: '100%',
  },
  separator: {
    marginTop: Platform.OS === 'ios' ? 44 : 11,
  },
  header: {
    zIndex: 3,
    marginTop: 2,
    marginBottom: 12,
    paddingHorizontal: 9,
  },
  titleText: {
    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    lineHeight: 22,
    fontWeight: '500',
    textAlign: 'center',
    marginTop: 9,
    marginRight: 4,
    color: colors.mainBlack,
    letterSpacing: -0.6,
  },
  tabBar: {
    flexDirection: 'row',
    height: 98,
    backgroundColor: 'transparent',
    borderColor: 'rgba(21,28,38, 0.1)',
    borderBottomWidth: 1,
  },
  tabItem: {
    flex: 1,
    zIndex: 2,
    alignItems: 'center',
    padding: 16,
  },
});
