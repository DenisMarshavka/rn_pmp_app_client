import React from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  AsyncStorage,
  SafeAreaView,
  ImageBackground,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import HTMLView from "react-native-htmlview";
import { connect } from "react-redux";

import { CustomBottomButton } from "../../../components";
import { colors, fonts } from "../../../constants/theme";
import BackHeader from "../../../components/Headers/BackHeader";
import { addBasketProductById } from "../../../actions/BasketActions";
import { formattingPrice } from "./../../../utils";

const ItemBasketProductScreen = (props) => {
  const { navigation, route, options, addBasketProductById = () => {} } = props;

  const {
    img = "",
    title = "",
    category = "",
    price = 0,
    ingredients = "",
    text = "",
    id = null,
    isOrganicBeauty = false,
    isOrganicFood = false,
    isSpecial = false,
    isSport = false,
  } = route && route.params && route.params.item ? route.params.item : {};

  const isProductValid = title.trim() && price !== false && id;

  {
    /*// const createBasketProduct = async () => {
    //     try {
    //         if (isProductValid) {
    //             let isDublicateProduct = false;
    //             let basket = [];
    //
    //             const userData = await AsyncStorage.getItem('user');
    //
    //             if (JSON.parse(userData).hasOwnProperty("basket")) {
                    basket = JSON.parse(userData).basket.map(item => {
    {/*                    if (item.id === id && item.title === title) {
    {/*                        item.count++;*/
  }
  {
    /*                        isDublicateProduct = true;*/
  }

  {
    /*                        item.isOrganicBeauty = isOrganicBeauty;
    {/*                        item.isOrganicFood = isOrganicFood;
    //                         item.isSpecial = isSpecial;
    //                         item.isSport = isSport;
    //                     }
    //
    //                     return item
    //                 });
    //
    //                 if (!isDublicateProduct)  basket.push({id, img, title, price, count: 1, isOrganicBeauty,
    //                     isOrganicFood,
    //                     isSpecial,
    //                     isSport});
    //             } else basket = [{id, img, title, price, count: 1, isOrganicBeauty,
    //                 isOrganicFood,
    //                 isSpecial,
    {/*                isSport}];*/
  }

  {
    /*            await AsyncStorage.mergeItem('user', JSON.stringify({basket}));*/
  }
  //
  //             navigation.navigate('Basket')
  //         }
  //     } catch (err) {
  //         console.log('\n ERROR to add product in Basket', err)
  //     }
  // };

  return (
    <View style={{ flex: 1, position: "relative", zIndex: 1 }}>
      <ImageBackground
        source={
          img.trim()
            ? { uri: img }
            : require("../../../assets/images/notProductImage.png")
        }
        style={styles.picture}
      >
        <LinearGradient
          pointerEvents={"none"}
          colors={["#FFF", "rgba(255, 255, 255, 0)"]}
          style={styles.linearGradient}
        />

        <SafeAreaView style={{ position: "relative", zIndex: 3 }}>
          <BackHeader hide={true} backTitle />
        </SafeAreaView>
      </ImageBackground>

      <View style={{ flex: 1, backgroundColor: "#fff", marginBottom: 35 }}>
        <View style={styles.separator} />

        <ScrollView
          contentContainerStyle={{ paddingHorizontal: 16, paddingBottom: 65 }}
          showsVerticalScrollIndicator={false}
        >
          <Text style={styles.name}>
            {title.trim() || "Неизвестный продукт"}
          </Text>

          <View style={styles.categoryAndPriceRow}>
            <Text style={styles.category}>{category.trim() || ""}</Text>

            <Text style={styles.price}>{formattingPrice(price)} ₽</Text>
          </View>

          <HTMLView
            value={
              text.trim() ||
              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A amet aspernatur autem consequuntur eveniet facilis necessitatibus obcaecati, officia optio qui reiciendis repellat sunt temporibus. Aliquid architecto aspernatur assumenda beatae blanditiis commodi doloremque dolores eaque earum, facilis id illum maiores nostrum officia pariatur quas qui quibusdam quo repellat saepe sapiente sit?"
            }
            stylesheet={{
              span: {
                color: colors.title,
                fontSize: 15,
                lineHeight: 22,
                fontFamily: fonts.normal.fontFamily,
                fontWeight: fonts.normal.fontStyle,
                ...styles.description,
              },
            }}
          />

          <Text style={styles.ingredientsTitle}>Ингредиенты</Text>

          <Text style={styles.description}>
            {ingredients.trim() ||
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."}
          </Text>
        </ScrollView>
      </View>

      <CustomBottomButton
        label={"В КОРЗИНУ"}
        onPress={async () => {
          await addBasketProductById({ id, type: "PRODUCT" });
          navigation.navigate("Basket");
        }}
      />
    </View>
  );
};

export default connect(null, { addBasketProductById })(ItemBasketProductScreen);

const styles = StyleSheet.create({
  backgroundImage: {
    opacity: 0.9,
    resizeMode: "cover",
  },
  background: {
    width: "100%",
    height: "100%",
    position: "relative",
  },
  separator: {
    marginTop: Platform.OS === "ios" ? 34 : 22,
  },
  picture: {
    //  position: 'absolute',
    position: "relative",
    resizeMode: "cover",
    height: 280,
    width: "100%",
    zIndex: 2,
  },
  header: {
    zIndex: 3,
    marginTop: 14,
    paddingHorizontal: 9,
  },
  category: {
    fontSize: 18,
    color: "rgba(0, 0, 0, 0.87)",
    opacity: 0.54,
    fontFamily: fonts.normal.fontFamily,
  },
  price: {
    fontSize: 18,
    color: colors.orange,
    textAlign: "right",
    ...fonts.normal,
  },
  name: {
    fontSize: 28,
    opacity: 0.87,
    color: "#151C26",
    ...fonts.normal,
  },
  description: {
    fontSize: 14,
    lineHeight: 17,
    opacity: 0.87,
    fontFamily: fonts.light.fontFamily,
  },
  ingredientsTitle: {
    marginTop: 22,
    fontSize: 18,
    lineHeight: 30,
    fontWeight: "500",
    color: colors.title,
    fontFamily: fonts.medium.fontFamily,
  },
  categoryAndPriceRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 36,
  },
  buttonStyle: {
    width: "100%",
    height: 75,
    justifyContent: "center",
    alignItems: "center",
    borderTopColor: colors.mainBlack,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    position: "absolute",
    bottom: 0,
  },
  buttonShadow: {
    shadowColor: colors.absoluteBlack,
    shadowOffset: {
      width: 0,
      height: -10,
    },
    shadowOpacity: 0.2,
    shadowRadius: 15,
    elevation: 32,
  },
  buttonText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 14,
    fontWeight: "500",
    letterSpacing: 0.16,
    color: colors.mainWhite,
  },
  overviewStyles: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 75,
    backgroundColor: colors.mainOrange,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  linearGradient: {
    width: "100%",
    height: 170,
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
  },
});
