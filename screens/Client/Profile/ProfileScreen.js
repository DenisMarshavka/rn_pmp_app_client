import React from "react";
import {
  FlatList,
  Image,
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
  ActivityIndicator,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import * as Animatable from "react-native-animatable";
import { connect } from "react-redux";

import { colors, sizes, fonts } from "../../../constants/theme";
import { UserAPI } from "../../../api/user";
// const sidemenu = require('../../../assets/images/menuIcon.png');
import hamburger from "../../../assets/images/menuIcon.png";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader";
import { PersonSvg } from "../../../assets/icons/profile/PersonSvg";
import { CalendarSvg } from "../../../assets/icons/profile/CalendarSvg";
import { PointsSvg } from "../../../assets/icons/profile/PointsSvg";
import { FoodSvg } from "../../../assets/icons/profile/FoodSvg";
import { ResultSvg } from "../../../assets/icons/profile/ResultSvg";
import { MyProgramSvg } from "../../../assets/icons/profile/MyProgramSvg";
import { ChatSvg } from "../../../assets/icons/profile/ChatSvg";
import { ReviewSvg } from "../../../assets/icons/profile/ReviewSvg";
import { HistorySvg } from "../../../assets/icons/profile/HistorySvg";
import { VisitingSvg } from "../../../assets/icons/profile/VisitingSvg";
import { validURL } from "../../../utils";
import moment from "moment";

const editAvatar = require("../../../assets/images/icons/edit_avatar.png");

const menuItemData = [
  {
    icon: <PersonSvg />,
    width: 16,
    height: 21,
    title: "Персональные данные",
    navigate: "PersonalData",
  },
  {
    icon: <CalendarSvg />,
    width: 20,
    height: 20,
    title: "Мое расписание",
    navigate: "Schedule",
  },
  {
    icon: <PointsSvg />,
    width: 20,
    height: 20,
    title: "Мои баллы",
    navigate: "Points",
  },
  {
    icon: <VisitingSvg />,
    width: 24,
    height: 24,
    title: "Статистика и история посещений",
    navigate: "VisitingHistory",
  },
  {
    icon: <FoodSvg />,
    width: 16,
    height: 19,
    title: "Дневник питания",
    navigate: "FoodDiary",
  },
  {
    icon: <ResultSvg />,
    width: 17,
    height: 20,
    title: "Дневник результатов",
    navigate: "DiaryResult",
  },
  {
    icon: <MyProgramSvg />,
    width: 18,
    height: 18,
    title: "Мои программы",
    navigate: "MyProgram",
  },
  { icon: <ChatSvg />, width: 20, height: 15, title: "Чаты", navigate: "Chat" },
  {
    icon: <ReviewSvg />,
    width: 20,
    height: 20,
    title: "Мои отзывы",
    navigate: "Reviews",
  },
  {
    icon: <HistorySvg />,
    width: 20,
    height: 20,
    title: "История заказов",
    navigate: "HistoryOrders",
  },
];

class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userInfo: {
        name: "Пользователь",
        middlename: null,
        firstname: null,
        img: require("../../../assets/images/sample/sample_avatar.png"),
      },
      female: true,
      image: "null",
      loadData: false,
      uploadingImage: false,
    };
  }

  handleUerDataUpdate = async () => {
    console.log("GOOOOO PROFILE ", this.props.currentUser);

    if (
      this.props &&
      this.props.currentUser &&
      this.props.currentUser.id !== false
    ) {
      await UserAPI.getUserById(this.props.currentUser.id)
        .then((response) => {
          console.log("RESPONSEEEEEESSSSESESE ", response);

          if (response) {
            this.setState({
              userInfo: {
                firstname: response.firstname,
                middlename: response.middlename,
                female: response.female,
                img: response.img,
              },
              loadData: false,
              image: response.img,
            });
          }
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };

  updateAvatar = async (avatarImgFile) => {
    let response = null;

    if (avatarImgFile) {
      this.setState({ loadData: true });

      response = await UserAPI.updateUserAvatar(avatarImgFile);

      this.handleUerDataUpdate();
      this.setState({ uploadingImage: false });
    }

    console.log("UPDATE AVATAR RESPONSE: ", response);
  };

  componentDidMount() {
    // console.log("FIURST: ", this.props.currentUser);
    let currentUserInfo = {};

    this.setState({ loadData: true });

    if (
      this.props &&
      this.props.currentUser &&
      this.props.currentUser.id !== false
    ) {
      currentUserInfo = this.props.currentUser;
    }

    // AsyncStorage.getItem("imageUri")
    //   .then((response) => this.setState({ image: response }))
    //   .catch((e) => console.log(e));

    this.handleUerDataUpdate();
    this.getPermissionAsync();
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  };

  chooseImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 3],
        base64: true,
        quality: 1,
        exif: true,
      });

      if (!result.cancelled && result.uri && result.uri.trim()) {
        this.setState({ image: result.uri, uploadingImage: true });

        const data = new FormData();
        data.append("img", {
          uri: result.uri,
          name: +new Date() + "-avatar.jpg",
          type: "image/jpeg",
        });

        // await UserAPI.update({
        //   data,
        // });

        console.log("SELECTED IMG: ", data);

        this.updateAvatar(data);
      }
    } catch (e) {
      console.log("Error: When changing the profile Avatar: ", e);
    }
  };

  render() {
    let {
      userInfo,
      image,

      female,
      routeName,
    } = this.state;

    const { middlename = "", firstname = "" } = userInfo || {};

    console.log("USER DATTTAAA: ", userInfo);

    let delay = 0;

    return (
      <ImageBackground
        style={styles.container}
        source={require("../../../assets/images/background/profile.png")}
      >
        <View style={styles.avatarPad}>
          <SkeletonLoader
            isLoading={this.state.loadData}
            layout={[
              { width: 82, height: 82, borderRadius: 82, marginBottom: 15 },
            ]}
          >
            <TouchableOpacity
              activeOpacity={0.8}
              style={{ position: "relative" }}
              onPress={() =>
                !this.state.uploadingImage ? this.chooseImage() : null
              }
            >
              {this.state.uploadingImage ? (
                <View
                  style={{
                    ...styles.userAvatar,
                    backgroundColor: "rgba(0, 0, 0, 0.2)",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <ActivityIndicator size="small" color={colors.orange} />
                </View>
              ) : (
                <>
                  <Image
                    source={
                      image && image.trim() && validURL(image)
                        ? { uri: image }
                        : userInfo.img
                    }
                    style={styles.userAvatar}
                  />

                  <Image source={editAvatar} style={styles.editAvatar} />
                </>
              )}
            </TouchableOpacity>
          </SkeletonLoader>

          <SkeletonLoader
            isLoading={this.state.loadData}
            layout={[{ width: 120, height: 25, marginTop: 25 }]}
          >
            <Text style={[fonts.medium, styles.userName, { marginTop: 30 }]}>
              {(middlename && middlename.trim()) ||
              (firstname && firstname.trim())
                ? `${firstname} ${middlename}`
                : "Пользователь"}
            </Text>
          </SkeletonLoader>

          <TouchableOpacity
            style={{
              position: "absolute",
              right: 17,
              top: 25,
              padding: 20,
              paddingRight: 0,
            }}
            onPress={() =>
              this.props.navigation.navigate("MainMenu", { routeName })
            }
          >
            <Image source={hamburger} />
          </TouchableOpacity>
        </View>

        <View style={styles.menuPad}>
          <FlatList
            data={menuItemData}
            renderItem={({ item }) => {
              delay += 60;

              return (
                <Animatable.View
                  duration={400}
                  delay={delay}
                  animation="fadeInUp"
                >
                  <TouchableOpacity
                    style={styles.menuItem}
                    onPress={() => {
                      this.props.navigation.navigate(
                        item.navigate,
                        item.title === "Дневник результатов"
                          ? { isFemale: female }
                          : item.title === "Персональные данные"
                          ? {
                              isFemale: female,
                              onUerDataUpdate: this.handleUerDataUpdate,
                              currentUser: this.props.currentUser,
                            }
                          : {}
                      );
                    }}
                  >
                    {item && !item.notSvg ? (
                      item.icon
                    ) : (
                      <Image
                        opacity={0.5}
                        source={item.icon}
                        style={{ height: item.height, width: item.width }}
                      />
                    )}

                    <Text
                      style={[
                        fonts.normal,
                        { fontSize: 16, width: "80%", textAlign: "left" },
                      ]}
                    >
                      {item.title}
                    </Text>

                    <Image
                      source={require("../../../assets/images/icons/arrow.png")}
                      style={{ width: 5, height: 10 }}
                    />
                  </TouchableOpacity>
                </Animatable.View>
              );
            }}
            keyExtractor={(item) => item.title}
          />
        </View>
      </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => ({
  currentUser: state.currentUser.currentUser,
});

export default connect(mapStateToProps)(ProfileScreen);

const styles = {
  container: {
    flex: 1,
    width: "100%",
    height: 298,
  },
  avatarPad: {
    width: "100%",
    height: 298,
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "center",
  },
  menuPad: {
    flex: 1,
    width: "100%",
    backgroundColor: "white",
    paddingTop: 5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  menuItem: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",

    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: "rgba(255, 255, 255, 0.92)",
    height: 52,
    width: "100%",
    borderColor: "rgba(65, 57, 57, 0.12)",
    borderBottomWidth: 1,
  },
  userAvatar: {
    width: 82,
    height: 82,
    borderRadius: 45,
  },
  editAvatar: {
    position: "absolute",
    right: 0,
    bottom: 0,

    width: 30,
    height: 30,
  },
  userName: {
    textAlign: "center",
    width: "100%",
    fontSize: 28,
    lineHeight: 33,
  },
};
