import React, { useState, useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
  Dimensions,
  SafeAreaView,
  ActiveOpacity,
  ActivityIndicator,
} from "react-native";
import moment from "moment";
import NumberFormat from "react-number-format";

import { Header } from "../../../../components";
import { fonts, colors } from "../../../../constants/theme";
import PersonalDataInput from "../../../../components/input/personal_data_input/PersonalDataInput.tsx";
import { DropDown } from "./components/DropDown";
import { DiaryFoodAPI } from "../../../../api/diary/food";
import DatePickerInput from "../../../../components/datepickerinput/DatePickerInput.tsx";
import { Background } from "../../../../components/Background";

export const AddDishScreen = ({ navigation, route = {} }) => {
  const {
    fromProgram = false,
    programName = "",
    UpdateDishes = () => {},
    daysProp,
    currentActiveIndex = 15,
  } = route.params || {};

  const categories = ["Завтрак", "Обед", "Полдник", "Ужин", ""];

  const [categoriesList, setCategoriesList] = useState(categories);
  const [currentCategory, setCurrentCategory] = useState("");

  const [calories, setCalories] = useState("");
  const [dish, setDish] = useState("");
  const [fats, setFats] = useState("");
  const [carbohydrates, setCarbohydrates] = useState("");
  const [weight, setWeight] = useState("");
  const [squirrels, setSquirrels] = useState("");
  const [date, setDate] = useState(new Date());
  const [fetchingStatus, setFetchingStatus] = useState(false);
  const [selection, setSelection] = useState({ start: 0, end: 0 });

  const [defaultFoods, setDefaultFoods] = useState([]);
  const [defaultFoodsFilteredList, setDefaultFoodsFilteredList] = useState([]);
  const [defaultFoodsPackages, setFoodsPackages] = useState([]);
  const [currentDefaultFood, setCurrentDefaultFood] = useState("");
  const [loadingDefaultFoods, setLoadingDefaultFoods] = useState(true);
  const [showDatePicker, setShowDatePicker] = useState(false);

  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(15);

  useEffect(() => {
    setCurrentCategory(categories[0]);
    getDefaultFoods(offset, limit);

    // console.log(
    //   "DATE: ",
    //   moment(date).format("YYYY-MM-DD"),
    //   currentActiveIndex
    // );
  }, []);

  useEffect(() => {
    if (defaultFoods && defaultFoods.length) {
      setDefaultFoodsFilteredList(defaultFoods);

      console.log("defaultFoods filtered", defaultFoods);
    }
  }, [defaultFoods]);

  const getDefaultFoods = (offset = 0, limit = 15) => {
    DiaryFoodAPI.getDefaultFoods(offset, limit).then((response) => {
      setLoadingDefaultFoods(false);

      const foodsListTitles = [];

      // console.log("Response Data Default Foods: ", response);

      if (response && response.dishes && response.dishes.length) {
        setFoodsPackages(response.dishes);

        for (let food of response.dishes) {
          if (food.dish && food.dish.trim()) foodsListTitles.push(food.dish);
        }
      }

      if (foodsListTitles && foodsListTitles.length)
        setDefaultFoods(foodsListTitles);
    });
  };

  const callbackFunction = () =>
    setCategoriesList(categories.filter((item) => item !== currentCategory));

  const onSubmit = async () => {
    console.log(
      "DATE: ",
      moment(date).format("YYYY-MM-DD"),
      "calories",
      calories
    );

    const _caloriesDomain =
      typeof calories === "string"
        ? +calories.replace(/[, кКал]/g, "")
        : !isNaN(+calories)
        ? +calories
        : 0;
    if (!dish || !_caloriesDomain) return;

    try {
      const Squirrels =
        squirrels && squirrels.length > 0 ? squirrels : undefined;
      const Weight = weight && weight.length > 0 ? weight : undefined;
      const Carbohydrates =
        carbohydrates && carbohydrates.length > 0 ? carbohydrates : undefined;
      const Fats = fats && fats.length > 0 ? fats : undefined;

      const data = {
        category: categories.indexOf(currentCategory),
        calories: _caloriesDomain,
        date: moment(date).format("YYYY-MM-DD"),
      };

      if (dish) data.dish = dish;
      if (weight) data.weight = weight;
      if (squirrels) data.squirrels = squirrels;
      if (carbohydrates) data.carbohydrates = carbohydrates;
      if (fats) data.fats = fats;

      console.log("DISH DATA: ", data, "carbohydrates", carbohydrates);

      Object.keys(data).forEach((key) =>
        data[key] === undefined ? delete data[key] : {}
      );

      setFetchingStatus(true);

      await DiaryFoodAPI.addDish(data);
      // await DiaryFoodAPI.getDiaryFoodsByDate(moment(date).format("YYYY-MM-DD"));

      // if (fromDiary) const currentDayIndex = daysProp.indexOf(data.date);

      await UpdateDishes(moment(date).format("YYYY-DD-MM"), currentActiveIndex);
      navigation.navigate(
        !fromProgram ? "FoodDiary" : "Program",
        !fromProgram ? { currDate: { data } } : { programName }
      );
    } catch (e) {
      console.log("Error DiaryFoodAPI.addDish --- ", e);
    }
  };

  useEffect(() => {
    let currentSelectedIndexFoodPackage;

    if (
      typeof currentDefaultFood === "string" &&
      currentDefaultFood &&
      currentDefaultFood.trim() &&
      defaultFoodsPackages &&
      defaultFoodsPackages.length
    ) {
      currentSelectedIndexFoodPackage = defaultFoodsPackages.findIndex((item) =>
        item && item.dish && item.dish.trim()
          ? item.dish === currentDefaultFood
          : false
      );

      if (currentSelectedIndexFoodPackage > -1)
        setSelectedFoodPackageParams(
          defaultFoodsPackages[currentSelectedIndexFoodPackage]
        );
    }
  }, [currentDefaultFood]);

  const setSelectedFoodPackageParams = (packageItem = {}) => {
    if (packageItem && Object.keys(packageItem).length) {
      console.log(
        "SELECTED Package ITEm",
        packageItem,
        packageItem.dish.trim(),
        "packageItem.carbohydrates",
        packageItem.carbohydrates
      );

      if (packageItem.dish && packageItem.dish.trim())
        setDish(packageItem.dish);

      if (packageItem.calories !== false)
        setCalories(!packageItem.calories ? "" : packageItem.calories);
      if (packageItem.fats !== false)
        setFats(!packageItem.fats ? "" : packageItem.fats);
      if (packageItem.carbohydrates !== false)
        setCarbohydrates(
          !packageItem.carbohydrates ? "" : +packageItem.carbohydrates
        );
      if (packageItem.squirrels !== false)
        setSquirrels(!packageItem.squirrels ? "" : packageItem.squirrels);
      if (packageItem.weight !== false)
        setWeight(!packageItem.weight ? "" : packageItem.weight);
    }
  };

  const filterDefaultFoodsList = (closedDropDown = false) => {
    if (closedDropDown && defaultFoods && defaultFoods.length) {
      setDefaultFoodsFilteredList(
        defaultFoods.filter((title) => title !== currentDefaultFood)
      );
    }
  };

  const getDropDown = (
    title = "",
    subtitle = "",
    callbackFunction = () => {},
    currentValue = "",
    list = [],
    setValue = () => {}
  ) => (
    <>
      {title &&
      title.trim() &&
      subtitle &&
      subtitle.trim() &&
      list &&
      list.length ? (
        <>
          <Text style={[styles.title, fonts.text]}>{title}</Text>

          <View style={{ paddingHorizontal: 10 }}>
            <DropDown
              parentCallback={callbackFunction}
              title={subtitle}
              heightTitle={20}
              boldText={false}
              value={currentValue}
            >
              {list.map((item, i) => (
                <TouchableOpacity
                  style={{ marginTop: 5 }}
                  onPress={() => setValue(item)}
                  activeOpacity={0.8}
                  key={`item-${item}-${i.toString()}`}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      color: "#1E2022",
                    }}
                  >
                    {item}
                  </Text>
                </TouchableOpacity>
              ))}
            </DropDown>
          </View>
        </>
      ) : null}
    </>
  );

  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="position">
      <View
        style={{
          flex: 1,
          width: Dimensions.get("screen").width,
          minHeight: Dimensions.get("screen").height,
        }}
      >
        <SafeAreaView>
          <Header
            title={"Добавить блюдо"}
            isSubmit={true}
            disabledBtn={fetchingStatus}
            submitPress={() => onSubmit()}
            style={{ paddingTop: 5 }}
          />
        </SafeAreaView>

        <ScrollView contentContainerStyle={{ paddingBottom: 35 }}>
          <View style={{ paddingHorizontal: 16 }}>
            <View style={{ marginTop: 29 }}>
              {getDropDown(
                "Выберите категорию",
                "Категория",
                callbackFunction,
                currentCategory,
                categoriesList,
                setCurrentCategory
              )}
            </View>

            {loadingDefaultFoods ? (
              <View style={{ marginTop: 35, marginLeft: 10 }}>
                <View
                  style={{
                    flex: 1,
                    alignItems: "left",
                    justifyContent: "center",
                  }}
                >
                  <ActivityIndicator color={colors.orange} />
                </View>
              </View>
            ) : defaultFoods && defaultFoods.length ? (
              <View style={{ marginTop: 29 }}>
                {getDropDown(
                  "Стандартные наборы",
                  "Набор",
                  filterDefaultFoodsList,
                  currentDefaultFood,
                  defaultFoodsFilteredList,
                  setCurrentDefaultFood
                )}
              </View>
            ) : null}

            <View style={{ marginTop: 29 }}>
              <Text style={[styles.title, fonts.text]}>Выберите дату</Text>

              <DatePickerInput
                show={showDatePicker}
                onPress={() => setShowDatePicker(!showDatePicker)}
                date={date}
                label={"Дата"}
                setDate={(date) => setDate(date)}
              />
            </View>

            <View style={{ marginTop: 29 }}>
              <View style={styles.blockLabelInput}>
                <Text style={[styles.title, fonts.text]}>Введите название</Text>
                {!dish ? (
                  <Text style={[fonts.medium, { color: "#ff6347" }]}>
                    Обязательное поле
                  </Text>
                ) : null}
              </View>

              <PersonalDataInput
                label={"Блюдо"}
                defaultData={dish}
                onChange={(value) => setDish(value)}
              />
            </View>

            <View style={{ marginTop: 29, flex: 1 }}>
              <View style={styles.blockLabelInput}>
                <Text style={[styles.title, fonts.text]}>
                  Заполните калорийность
                </Text>

                {!calories ? (
                  <Text
                    style={[
                      fonts.medium,
                      {
                        color: "#ff6347",
                        position: "absolute",
                        top: 25,
                        right: 0,
                      },
                    ]}
                  >
                    Обязательное поле
                  </Text>
                ) : null}
              </View>

              <NumberFormat
                value={calories}
                displayType={"text"}
                thousandSeparator={true}
                suffix={" кКал"}
                renderText={(value) => (
                  <PersonalDataInput
                    label={"Калории"}
                    inputType="numeric"
                    defaultData={calories || ""}
                    onChange={(value) => setCalories(value)}
                    value={value}
                  />
                )}
              />
            </View>

            <View style={{ marginTop: 29 }}>
              <Text style={[styles.title, fonts.text]}>Заполните жиры</Text>

              <PersonalDataInput
                label={"Жиры"}
                inputType="numeric"
                defaultData={fats ? fats.toString() : ""}
                onChange={(value) => setFats(value)}
              />
            </View>

            <View style={{ marginTop: 29 }}>
              <Text style={[styles.title, fonts.text]}>Заполните белки</Text>

              <PersonalDataInput
                label={"Белки"}
                inputType="numeric"
                defaultData={squirrels ? squirrels.toString() : ""}
                onChange={(value) => setSquirrels(value)}
              />
            </View>

            <View style={{ marginTop: 29 }}>
              <Text style={[styles.title, fonts.text]}>Заполните углеводы</Text>

              <PersonalDataInput
                label={"Углеводы"}
                inputType="numeric"
                defaultData={carbohydrates ? carbohydrates.toString() : ""}
                onChange={(value) => setCarbohydrates(value)}
              />
            </View>

            <View style={{ marginTop: 29 }}>
              <Text style={[styles.title, fonts.text]}>Заполните граммы</Text>

              <PersonalDataInput
                label={"Граммы"}
                inputType="numeric"
                defaultData={weight ? weight.toString() : ""}
                onChange={(value) => setWeight(value)}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 40,
    paddingBottom: 33,
    paddingHorizontal: 9,
  },
  title: {
    paddingHorizontal: 10,
    fontSize: 18,
    color: "#1E2022",
    marginBottom: 18,
  },
  container: {
    flex: 1,
    paddingTop: 40,
    alignItems: "center",
  },
  blockLabelInput: {
    position: "relative",
    overflow: "hidden",

    maxWidth: Dimensions.get("screen").width - 32,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
  },
});
