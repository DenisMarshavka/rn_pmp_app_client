import React, { Component } from 'react';
import { Text, View, ViewStyle, Animated, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import {
  fontSize18,
  fontSize20,
  fonts,
} from '../../../../../constants/theme';

export default class DropDownList extends Component {
  render() {
    const {items, style} = this.props;

    let eating = ['Завтрак', 'Обед', 'Ужин'],
        selectedEating = '',
        onShow = true,
        animatedValue = new Animated.Value(0),
        dish = '',
        calories = 0;

    const viewHeight = animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 75, 130],
    });
    return (
      <View style={[{flexDirection: 'column'}, style]}>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderBottomWidth: 1,
            borderBottomColor: 'rgba(0, 0, 0, 0.12)',
            height: 46,
          }}
          activeOpacity={1}
          onPress={() => {
            // showMenu();
          }}>
          <Text style={{fontSize: fontSize18, fontFamily: fonts.normal.fontFamily}}>
            {selectedEating}
          </Text>
          <Icon name="md-arrow-dropdown" size={fontSize20} color="#000000" type="ionicons" />
        </TouchableOpacity>
        <Animated.View
          style={{
            opacity: animatedValue,
            position: 'absolute',
            right: 0,
            left: 0,
            top: 36,
            height: viewHeight,
            backgroundColor: '#fafafa',
            zIndex: 50,
            elevation: 2,
            padding: 8,
          }}>
          {items.map(item => {
            return (
              <TouchableOpacity
                key={item}
                style={{paddingVertical: 8}}
                onPress={() => {
                  selectEating(item);
                  showMenu();
                }}>
                <Text
                  style={{
                    fontSize: fontSize18,
                    fontFamily: fonts.normal.fontFamily,
                  }}>
                  {item}
                </Text>
              </TouchableOpacity>
            );
          })}
        </Animated.View>
      </View>
    );
  }
}
