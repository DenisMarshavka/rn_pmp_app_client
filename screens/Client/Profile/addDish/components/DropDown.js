import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  Easing,
  TouchableOpacity,
} from "react-native";

import { colors } from "../../../../../constants/theme";

export const DropDown = ({
  title,
  heightTitle,
  children,
  parentCallback,
  value,
  isLoading = false,
  boldText = true,
}) => {
  const icons = {
    up: require("../../../../../assets/images/icons/arrow_up.png"),
    down: require("../../../../../assets/images/icons/arrow_down.png"),
  };

  const [animation] = useState(new Animated.Value(heightTitle));
  const [expanded, setExpanded] = useState(false);
  const [maxHeight, setMaxHeight] = useState(0);
  const [minHeight, setMinHeight] = useState(0);

  const toggle = () => {
    Animated.timing(animation, {
      toValue: expanded ? maxHeight : minHeight,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => {
      setExpanded(!expanded);
    });
  };

  const _setMaxHeight = (event) => {
    if (children && children.length && event.nativeEvent.layout.height) {
      setMaxHeight(event.nativeEvent.layout.height + 30);
    }
  };

  const _setMinHeight = (event) => {
    animation.setValue(event.nativeEvent.layout.height);
    setMinHeight(event.nativeEvent.layout.height);
  };

  const sendExpanded = () => {
    if (children && children.length) parentCallback(expanded);
  };

  useEffect(() => {
    sendExpanded();
  }, [expanded]);

  return (
    <View>
      <Text
        style={{
          marginBottom: 8,
          fontFamily: "PFDin500",
          fontWeight: "normal",
          fontSize: 13,
          color: colors.orange,
        }}
      >
        {title}
      </Text>

      <Animated.View
        style={{
          overflow: "hidden",
        }}
      >
        <View onLayout={(event) => (!isLoading ? _setMinHeight(event) : null)}>
          <TouchableOpacity
            activeOpacity={isLoading ? 1 : 0.8}
            onPress={() => {
              !isLoading && children && children.length ? toggle() : null;
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Text
                style={[
                  boldText ? { fontFamily: "PFDin500" } : {},
                  {
                    fontWeight: "normal",
                    fontSize: 18,
                    color: "#1E2022",
                  },
                ]}
              >
                {value}
              </Text>

              <Image
                style={{ width: 10, height: 5 }}
                source={expanded ? icons["down"] : icons["up"]}
              />
            </View>

            <View
              style={{
                marginTop: 5,
                borderBottomWidth: 1,
                borderBottomColor: !expanded
                  ? "rgba(0, 0, 0, 0.12)"
                  : colors.orange,
              }}
            />
          </TouchableOpacity>
        </View>

        {children && children.length ? (
          <Animated.View
            style={{
              ...styles.body,
              display: expanded ? "flex" : "none",
              opacity: expanded ? 1 : 0,
            }}
            onLayout={(event) => (!isLoading ? _setMaxHeight(event) : null)}
          >
            {children}
          </Animated.View>
        ) : null}
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({});
