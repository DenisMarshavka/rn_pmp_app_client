import {StyleProp, Text, View, ViewStyle} from 'react-native';
import React from 'react';
import {
  fontSize18,
  fonts,
} from '../../../../../constants/theme';

export const HeaderTitle = ({
  text,
  style,
}) => {
  return (
    <View style={style}>
      <Text style={{fontFamily: fonts.bold.fontFamily, fontSize: fontSize18}}>
        {text}
      </Text>
    </View>
  );
};
