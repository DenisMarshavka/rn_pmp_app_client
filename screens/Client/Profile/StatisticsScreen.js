import React from 'react'
import {
  ImageBackground,
  ScrollView,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import { Icon } from 'react-native-elements'
import Carousel from 'react-native-snap-carousel'
import { LinearGradient } from 'expo-linear-gradient'
import { PieChart } from 'react-native-svg-charts'

// import {NavigationProps} from '../../share/interfaces'
import { Header } from '../../../components'
import {
    fontSize14,
    fontSize16,
    fontSize18,
    fontSize28,
    fontSize34,
    WINDOW_HEIGHT,
    fonts,
} from '../../../constants/theme'
import {connect} from "react-redux";
import {getAttendanceStatistics} from "../../../actions/UserActions";
import {colors} from "../../../styles";

const DATA = [
  {
    title: 'Октябрь 2019',
    data: [
      'Персональная тренировка 55 минут',
      'СПА процедуры',
      'Посещение косметолога',
    ],
  },
];

const monthNames = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь',
];

const CustomTitle = ({
                         text,
                         background,
                     }) => {
    return (
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View
                style={{
                    height: 8,
                    width: 8,
                    borderRadius: 16,
                    backgroundColor: background,
                    marginRight: 8,
                }}
            />
            <Text
                style={{
                    fontFamily: fonts.normal.fontFamily,
                    fontSize: fontSize14,
                }}>
                {text}
            </Text>
        </View>
    );
};


//ToDo: Delete this screen

class StatisticsScreen extends React.Component {
  currIndex = 0;

  constructor(props) {
    super(props);

    this.state = {
      activeElement: 0,
      selectedSlice: {
        label: '',
        value: 0,
      },
      labelWidth: 0,
    };
  }

  componentDidMount() {
    this.currIndex = this._carousel.currentIndex;

    this.props.getAttendanceStatistics();
  }

  _renderItem = ({ item, index }) => (
      <View
        style={{
          backgroundColor: index === this.currIndex ? '#F46F22' : 'transparent',
          borderRadius: 30,
          paddingVertical: 8,
        }}>
        <Text
          style={{
            color: index === this.currIndex ? '#FAFAFA' : '#151C26',
            textAlign: 'center',
            fontFamily: fonts.normal.fontFamily,
            fontSize: fontSize14,
          }}>
          {item}
        </Text>
      </View>
    );

  render() {
    const { selectedSlice } = this.state;
    const {label} = selectedSlice;
    const keys1 = ['Пилатес', 'Силовая', 'Beauty'];
    const values = [50, 20, 40];
    const colors = ['#F46F22', '#151C26', '#0FD8D8'];

    const {
        attendanceStatisticsLoading = false,
        attendanceStatistics = [],
        attendanceStatisticsError = null,
    } = this.props;

    const data = keys1.map((key, index) => {
      return {
        key,
        value: values[index],
        svg: {fill: colors[index]},
        arc: {
          outerRadius: 70 + values[index] + '%',
          padAngle: label === key ? 0.1 : 0,
        },
        onPress: () =>
          this.setState({selectedSlice: {label: key, value: values[index]}}),
      };
    });

    return (
      <ImageBackground
        style={{
          width: '100%',
          height: '100%',
        }}
        source={require('../../../assets/images/background/staticAndHistory.png')}
      >
        <Header
          style={styles.header}
          marginTopHeader={WINDOW_HEIGHT / 16}
          headerLeft={
            <TouchableOpacity
              style={{paddingRight: 16}}
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <Icon
                name="chevron-small-left"
                size={fontSize34}
                color="#151C26"
                type="entypo"
              />
            </TouchableOpacity>
          }
          title={'Статистика и история посещений'}
        />

        <ScrollView>
          <View style={{flex: 1, alignItems: 'center'}}>
            <View style={{height: WINDOW_HEIGHT / 8}}>
              <Carousel
                shouldOptimizeUpdates={true}
                enableMomentum={true}
                activeSlideOffset={10}
                callbackOffsetMargin={20}
                ref={(c) => {
                  this._carousel = c;
                }}
                onBeforeSnapToItem={(currentIndex) => {
                  this.currIndex = currentIndex;
                  this.setState({});
                }}
                inactiveSlideOpacity={0.6}
                itemHeight={40}
                data={monthNames}
                firstItem={2}
                renderItem={this._renderItem}
                sliderWidth={400}
                itemWidth={100}
              />
            </View>

            <View style={{width: '100%'}}>
              <PieChart
                style={{height: WINDOW_HEIGHT / 6, width: '100%'}}
                outerRadius={'80%'}
                innerRadius={'25%'}
                data={data}
              />
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                width: '90%',
                marginVertical: 16,
              }}>
              {/*<CustomTitle text={'Пилатес (8)'} background={'#F46F22'} />*/}

              {/*<CustomTitle text={'Силовая (8)'} background={'#1D1C1F'} />*/}

              {/*<CustomTitle text={'Beauty (6)'} background={'#0FD8D8'} />*/}
            </View>

            <View style={styles.containerList}>
              <SectionList
                scrollEnabled={false}
                showsVerticalScrollIndicator={false}
                style={styles.list}
                sections={DATA}
                keyExtractor={(item, index) => item + index}
                renderItem={({item, index}) => (
                  <LinearGradient
                    key={index}
                    useAngle
                    angle={240}
                    locations={[0, 1]}
                    colors={['rgba(255, 255, 255, 0.5);', '#DADADA']}

                    style={styles.gradientListItems}>
                    <View style={{flexDirection: 'column', paddingLeft: 16}}>
                      <Text
                        style={{
                          fontSize: fontSize16,
                          fontFamily: fonts.normal.fontFamily,
                        }}>
                        {item}
                      </Text>
                      <Text style={{color: 'rgba(30, 32, 34, 0.54)'}}>
                        11.10.2019
                      </Text>
                    </View>
                    <Icon name="leaf" size={fontSize28} color="#0FD8D8" type="entypo" />
                  </LinearGradient>
                )}
                renderSectionHeader={({section: {title}}) => (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={{
                        fontFamily: fonts.bold.fontFamily,
                        fontSize: 18,
                        paddingVertical: 18,
                      }}>
                      {title}
                    </Text>
                  </View>
                )}
              />
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => ({
    attendanceStatisticsLoading: state.currentUser.attendanceStatisticsLoading,
    attendanceStatistics: state.currentUser.attendanceStatistics,
    attendanceStatisticsError: state.currentUser.attendanceStatisticsError,
});

export default connect(mapStateToProps, { getAttendanceStatistics })(StatisticsScreen);

const styles = StyleSheet.create({
  header: {
    // borderBottomWidth: 1,
    // borderBottomColor: 'rgba(0, 0, 0, 0.12)',
  },
  list: {
    marginBottom: WINDOW_HEIGHT / 11.5,
    backgroundColor: '#FAFAFA',
    width: '100%',
    paddingHorizontal: 16,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  containerList: {
      paddingHorizontal: 16,
      borderTopLeftRadius: 20,
      borderTopRightRadius: 20,
      shadowColor: colors.absoluteBlack,
      shadowOffset: {
          width: 0,
          height: 6,
      },
      shadowOpacity: 0.12,
      shadowRadius: 50,
      elevation: 6,
      height: 400,
      width: '100%',
      backgroundColor: '#FFF',
  },

  cCal: {
    paddingLeft: 16,
    color: '#0FD8D8',
    fontSize: fontSize18,
  },
  listItem: {
    paddingLeft: 16,
    fontSize: fontSize16,
    fontFamily: fonts.normal.fontFamily,
  },
  gradientListItems: {
    marginTop: 8,
    paddingVertical: 18,
    paddingHorizontal: 8,
    borderRadius: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  listEatingTimeHeader: {
    fontFamily: fonts.bold.fontFamily,
    fontSize: 18,
    paddingVertical: 18,
  },
});
