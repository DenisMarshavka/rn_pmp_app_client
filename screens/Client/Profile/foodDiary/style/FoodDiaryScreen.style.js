import {
    fontSize12,
    fontSize14,
    fontSize16,
    fontSize18,
    fontSize28,
    WINDOW_HEIGHT,
    fonts
  } from '../../../../../constants/theme';
import { StyleSheet } from 'react-native';

export const styles =  StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginTop: 40,
        paddingBottom: 33,
        paddingHorizontal: 9,
    },
    container: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'column',
      marginBottom: 75,
    },
    textCircleContainer: {
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    miniHeader: {
      fontSize: fontSize14,
      fontFamily: fonts.bold.fontFamily,
      color: '#F46F22',
      paddingTop: 18,
    },
    calories: {
      fontFamily: fonts.medium.fontFamily,
      fontSize: fontSize28,
      color: '#151C26',
    },
    cCal: {
      fontFamily: fonts.normal.fontFamily,
      fontSize: fontSize18,
      color: '#151C26',
    },
    goal: {
      fontFamily: fonts.normal.fontFamily,
      fontSize: fontSize12,
      color: 'rgba(21, 28, 38, 0.5)',
    },
    rowIconsContainer: {
      height: '10%',
      flexDirection: 'row',
      justifyContent: 'space-around',
      paddingHorizontal: '5%',
      marginTop: 45,
      marginBottom: 16,
    },
    iconTitleContainer: {
      justifyContent: 'center',
      flexDirection: 'column',
      alignItems: 'center',
    },
    iconImage: {
      width: fontSize28,
      height: fontSize28,
    },
    middleTitle: {
      fontSize: fontSize12,
      fontFamily: fonts.normal.fontFamily,
      textAlign: 'center',
      color: 'rgba(21, 28, 38, 0.38);',
      paddingTop: 8,
    },
    bottomTitle: {
      fontFamily: fonts.normal.fontFamily,
      textAlign: 'center',
      fontSize: fontSize16,
    },
    gradient: {
      marginTop: 8,
      paddingVertical: 18,
      paddingHorizontal: 8,
      borderRadius: 10,
      justifyContent: 'space-between',
      flexDirection: 'row',
    },
    listDish: {
      marginBottom: WINDOW_HEIGHT / 11.5,
      backgroundColor: '#FAFAFA',
      width: '100%',
      paddingHorizontal: 16,
      borderTopLeftRadius: 16,
      borderTopRightRadius: 16,
    },
    containerListFood: {
      backgroundColor: '#FAFAFA',
      paddingVertical: 20,
      paddingHorizontal: 16,
      width: '100%',
      flex: 1,
      borderRadius: 16,
    },
    listCal: {
      paddingLeft: 16,
      color: '#0FD8D8',
      fontSize: fontSize18,
    },
    listFoodItem: {
      paddingLeft: 16,
      fontSize: fontSize16,
      fontFamily: fonts.normal.fontFamily,
    },
    listEatingTimeHeader: {
      fontFamily: fonts.bold.fontFamily,
      fontSize: 18,
      paddingVertical: 18,
    },
});
