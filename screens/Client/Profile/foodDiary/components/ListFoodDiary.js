import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements";
import { EvilIcons } from "@expo/vector-icons";
import {
  sizes,
  fonts,
  colors,
  fontSize24,
} from "../../../../../constants/theme";
import { ListFoodDiaryItem } from "./ListFoodDiaryItem";
import SkeletonContent from "../../../../../components/loaders/SkeletonContent";

export const ListFoodDiary = ({
  data,
  title,
  emptyDataText,
  handlerDataFood,
  isLoading = false,
  isProgram = false,
  showDishes = false,
  summAll = 0,
  setShowDishes = () => {},
}) => {
  console.log(
    "FOOD ITEM",
    data,
    "showDishes",
    showDishes,
    "isProgram",
    isProgram
  );

  return (
    <View style={{ flex: 1 }}>
      {isProgram ? (
        <TouchableOpacity
          style={{ flex: 1, marginBottom: 20 }}
          activeOpacity={0.6}
          onPress={() =>
            showDishes ? setShowDishes(false) : setShowDishes(true)
          }
        >
          <ListFoodDiaryItem
            title={title}
            weight={`${summAll} кКал`}
            isProgram={isProgram}
          />

          {showDishes ? (
            <View style={{ position: "absolute", right: 25, top: 30 }}>
              <Icon
                name="chevron-small-up"
                type="entypo"
                color="#151C26"
                size={35}
              />
            </View>
          ) : (
            <View style={{ position: "absolute", right: 25, top: 30 }}>
              <Icon
                name="chevron-small-down"
                type="entypo"
                color="#151C26"
                size={35}
              />
            </View>
          )}
        </TouchableOpacity>
      ) : (
        <Text style={{ marginBottom: 20 }}>{title}</Text>
      )}

      {(isProgram && showDishes) || !isProgram ? (
        <View style={{ marginBottom: 20 }}>
          <SkeletonContent
            style={{ flex: 1 }}
            layout={[
              {
                marginTop: isLoading ? 30 : 0,
                width: "100%",
                marginHorizontal: 16,
                height: 60,
                marginBottom: 8,
                borderRadius: 10,
              },
            ]}
            isLoading={isLoading}
          >
            {data.length === 0 ? (
              <Text
                style={[
                  {
                    color: "#151C26",
                    opacity: 0.57,
                    width: "100%",
                    textAlign: "center",
                    marginTop: 20,
                  },
                  fonts.medium,
                  fonts.h3,
                ]}
              >
                {emptyDataText}
              </Text>
            ) : (
              data.map((item, index) => {
                const {
                  fats,
                  carbohydrates,
                  squirrels,
                  calories,
                  weight,
                } = item;

                return (
                  <TouchableOpacity
                    key={`item-${
                      item && item.id
                        ? item.id
                        : item && item.dish
                        ? item.dish
                        : item && item.createdAt
                        ? item.createdAt
                        : item.name
                    }-${Date.now()}-${index}`}
                    style={{
                      marginHorizontal: 16,
                      height: 60,
                      width: "100%",
                      display: showDishes ? "flex" : "none",
                    }}
                    activeOpacity={0.6}
                    onPress={() =>
                      handlerDataFood(
                        fats,
                        carbohydrates,
                        squirrels,
                        calories,
                        weight
                      )
                    }
                  >
                    <ListFoodDiaryItem
                      title={!isProgram ? item.dish : item.name}
                      weight={`${item.calories} кКал`}
                      isProgram={isProgram}
                    />
                  </TouchableOpacity>
                );
              })
            )}
          </SkeletonContent>
        </View>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({});
