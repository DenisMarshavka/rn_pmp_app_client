import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";
import { Icon } from "react-native-elements";
import {
  sizes,
  fonts,
  colors,
  fontSize24,
} from "../../../../../constants/theme";

export const ListFoodDiaryItem = ({ title, weight, icon, isProgram }) => {
  console.log("Food list item data", title, weight, icon, isProgram);

  return (
    <LinearGradient
      start={{ x: 1, y: 1 }}
      end={{ x: 0.01, y: 0.01 }}
      colors={["rgba(218, 218, 218, 0.8)", colors.gray]}
      style={[
        styles.gradient,
        { paddingBottom: isProgram ? 5 : 0, marginBottom: 8 },
      ]}
    >
      <View style={styles.container_line}>
        <Text
          style={[{ color: "#151C26", paddingTop: 18 }, fonts.text, fonts.h3]}
        >
          {title}
        </Text>

        <Text
          style={[
            { color: "#F46F22", fontSize: 18, paddingTop: 16 },
            fonts.text,
          ]}
        >
          {weight}
        </Text>
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  gradient: {
    flex: 1,
    borderRadius: 10,
  },
  container_line: {
    width: Dimensions.get("screen").width - 32,
    height: 64,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: sizes.margin,
    alignItems: "flex-start",
  },
});
