import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {
  fontSize12,
  fontSize16,
  fontSize28,
  fonts,
} from '../../../../../constants/theme';

export const SubstanceElement = ({
  imagePath,
  middleTitle,
  bottomTitle,
}) => {
  return (
    <View style={styles.iconTitleContainer}>
      <Image
        resizeMode={'contain'}
        style={styles.iconImage}
        source={imagePath}
      />
      <Text style={styles.middleTitle}>{middleTitle}</Text>
      <Text style={styles.bottomTitle}>{bottomTitle}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  iconTitleContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  iconImage: {
    width: fontSize28,
    height: fontSize28,
  },
  middleTitle: {
    fontSize: fontSize12,
    fontFamily: fonts.normal.fontFamily,
    textAlign: 'center',
    color: 'rgba(21, 28, 38, 0.38);',
    paddingTop: 8,
  },
  bottomTitle: {
    fontFamily: fonts.normal.fontFamily,
    textAlign: 'center',
    fontSize: fontSize16,
  },
});
