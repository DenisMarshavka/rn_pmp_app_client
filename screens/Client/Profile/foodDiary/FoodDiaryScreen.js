import React, { Component, useEffect, useState } from "react";
import moment from "moment";
import "moment/locale/ru";
import {
  ImageBackground,
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import {
  WINDOW_HEIGHT,
  WINDOW_WIDTH,
  fonts,
  sizes,
} from "../../../../constants/theme";

import { Icon } from "react-native-elements";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import Carousel from "react-native-snap-carousel";
import { SubstanceElement } from "./components/SubstanceElement";
import { DiaryFoodAPI } from "../../../../api/diary/food";
import { ListFoodDiary } from "./components/ListFoodDiary";
import { CustomBottomButton } from "../../../../components";
import { styles } from "./style/FoodDiaryScreen.style";
import Header from "../../../../components/Headers/Header";
import { Background2 } from "../../../../components/Background2";
import SkeletonContent from "../../../../components/loaders/SkeletonContent";

const iconImages = {
  prot: require("../../../../assets/icons/prot.png"),
  uglevod: require("../../../../assets/icons/uglevod.png"),
  fats: require("../../../../assets/icons/fats.png"),
};

export default class FoodDiaryScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      currentCarouselIndex: 15,
      activeElement: 0,
      currentCalories: 0,
      currentFats: 0,
      currentCarbohydrates: 0,
      currentSquirrels: 0,
      currentWeight: 0,
      allCalories: 0,
      allFats: 0,
      allCarbohydrates: 0,
      allSquirrels: 0,
      daysView: [],
      daysProp: [],
      activeStartDate: 0,
      breakfasts: [],
      dinners: [],
      obeds: [],
      poldniki: [],
      isReturnData: true,
    };
    this.getFoodsByDay = this.getFoodsByDay.bind(this);
  }

  async componentDidMount() {
    await this.getFoodsByDay(moment().format("YYYY-DD-MM"));

    const daysView = this.getCarouselDays().map((item) => item.dateView);
    const daysProp = this.getCarouselDays().map((item) => item.dateProp);

    const currentDayIndex = daysProp.findIndex(
      (day) => moment().format("YYYY-DD-MM") === day
    );

    console.log(
      "currentDayIndex",
      currentDayIndex,
      "daysProp[currentDayIndex]",
      daysProp[currentDayIndex]
    );

    if (currentDayIndex > -1) {
      this.setState({
        daysView,
        daysProp,
      });
    }
  }

  async getFoodsByDay(date, currentIndex = this.state.currentCarouselIndex) {
    try {
      const chuncksDate = date.split("-");
      const dateToQuesry =
        chuncksDate[0] +
        "-" +
        chuncksDate[chuncksDate.length - 1] +
        "-" +
        chuncksDate[1];
      console.log("datedate", dateToQuesry);

      const foodsByDate = await DiaryFoodAPI.getDiaryFoodsByDate(dateToQuesry);

      const {
        calories_summ = 0,
        squirrels_summ = 0,
        carbohydrates_summ = 0,
        fats_summ = 0,
      } = foodsByDate && foodsByDate.data ? foodsByDate.data : {};

      const { breakfasts, dinners, obeds, poldniki } = foodsByDate.data.foods;

      console.log(
        foodsByDate,
        "foodsByDatefoodsByDate",
        foodsByDate.data.foods,
        "foodsByDate.data.foods",
        carbohydrates_summ,
        "carbohydrates_summ"
      );

      this.setState({
        isLoading: false,
        isReturnData: true,
        currentCarouselIndex: currentIndex,
        breakfasts,
        dinners,
        obeds,
        poldniki,
        currentCalories: calories_summ,
        allCalories: calories_summ,
        allFats: fats_summ,
        allCarbohydrates: carbohydrates_summ,
        allSquirrels: squirrels_summ,
      });
    } catch (e) {
      console.log("Error DiaryFoodAPI.getDiaryFoodsByDate --- ", e);
    }
  }

  _renderItem = ({ item, index }) => {
    const { currentCarouselIndex } = this.state;

    return (
      <View
        style={{
          backgroundColor:
            index === currentCarouselIndex ? "#F46F22" : "transparent",
          borderRadius: 30,
          paddingVertical: 6,
          // paddingHorizontal: 10,
        }}
      >
        <Text
          style={[
            fonts.medium,
            {
              fontSize: 14,
              color: index === currentCarouselIndex ? "#FAFAFA" : "#151C26",
              textAlign: "center",
            },
          ]}
        >
          {item}
        </Text>
      </View>
    );
  };

  handlerDataFood = (fats, carbohydrates, squirrels, calories, weight) => {
    this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: false });

    this.setState({
      isReturnData: false,
      currentCalories: calories,
      currentFats: fats,
      currentCarbohydrates: carbohydrates,
      currentSquirrels: squirrels,
      currentWeight: weight,
    });
  };

  getCarouselDays = () => {
    const days = [];
    const yesterday = moment().subtract(1, "days").format("DD MMMM");
    const now = moment().subtract(0, "days").format("DD MMMM");
    const tomorrow = moment().subtract(-1, "days").format("DD MMMM");
    const dateStart = moment().subtract(15, "days");
    const dateEnd = moment().add(15, "days");

    while (dateEnd.diff(dateStart, "days") >= 0) {
      let dateView = dateStart.format("DD MMMM");

      if (dateView === yesterday) {
        dateView = "Вчера";
      }
      if (dateView === now) {
        dateView = "Сегодня";
      }
      if (dateView === tomorrow) {
        dateView = "Завтра";
      }

      const itemDate = {
        dateView,
        dateProp: dateStart.format("YYYY-DD-MM"),
      };

      days.push(itemDate);
      dateStart.add(1, "days");
    }

    return days;
  };

  render() {
    const {
      isLoading,
      daysProp,
      daysView,
      currentCarouselIndex,
      currentCalories,
      currentFats,
      currentCarbohydrates,
      currentSquirrels,
      currentWeight,
      allCalories,
      allFats,
      allCarbohydrates,
      allSquirrels,
      isReturnData,
    } = this.state;

    const issetFoodsData =
      this.state.breakfasts.length +
      this.state.obeds.length +
      this.state.poldniki.length +
      this.state.dinners.length;

    console.log("STATTTEEE", this.state);

    return (
      <>
        <Background2>
          <Header title={"Дневник питания"} />

          <ScrollView ref="_scrollView" showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
              <View style={{ position: "relative", height: WINDOW_HEIGHT / 8 }}>
                {daysView.length === 0 ? null : (
                  <Carousel
                    activeSlideAlignment={"center"}
                    // shouldOptimizeUpdates={true}
                    enableMomentum={true}
                    callbackOffsetMargin={0}
                    ref={this._carousel}
                    onBeforeSnapToItem={async (currentIndex) => {
                      this.setState({ isLoading: true });
                      console.log("daysProp", daysProp);

                      const day = daysProp[currentIndex];

                      await this.getFoodsByDay(day, currentIndex);
                    }}
                    inactiveSlideOpacity={0.6}
                    itemHeight={40}
                    firstItem={15}
                    data={daysView}
                    renderItem={this._renderItem}
                    sliderWidth={380}
                    itemWidth={100}
                  />
                )}
              </View>

              <View style={{ flex: 1 }}>
                {!this.state.isReturnData && (
                  <View style={{ position: "absolute", top: 0, right: 0 }}>
                    <TouchableOpacity
                      onPress={() => this.setState({ isReturnData: true })}
                      style={{ marginRight: 20 }}
                    >
                      <Icon
                        name="reply"
                        type="material-community"
                        color="#F46F22"
                      />

                      <Text
                        style={{ fontSize: 12, color: "#000", marginTop: 5 }}
                      >
                        Вернуть{"\n"} данные
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}

                <SkeletonContent
                  containerStyle={{
                    margin: 0,
                    marginLeft: "auto",
                    marginRight: "auto",
                  }}
                  layout={[
                    {
                      width: WINDOW_HEIGHT / 5,
                      height: WINDOW_HEIGHT / 5,
                      borderRadius: WINDOW_HEIGHT / 5,
                    },
                  ]}
                  isLoading={isLoading}
                >
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => {
                      !isLoading
                        ? this.setState({ currentCalories: allCalories })
                        : null;
                    }}
                  >
                    <AnimatedCircularProgress
                      rotation={130}
                      delay={500}
                      lineCap={"round"}
                      size={WINDOW_HEIGHT / 5}
                      width={10}
                      fill={
                        (isReturnData ? allCalories : currentCalories) !== 0
                          ? ((isReturnData ? allCalories : currentCalories) /
                              4000) *
                            100
                          : 0
                      }
                      tintColor="#F46F22"
                      onAnimationComplete={() =>
                        console.log("onAnimationComplete")
                      }
                      backgroundColor="rgba(244, 111, 34, 0.2)"
                    >
                      {() => (
                        <View style={styles.textCircleContainer}>
                          <Text style={styles.calories}>
                            {isReturnData ? allCalories : currentCalories}
                          </Text>

                          <Text style={styles.cCal}>кКал</Text>

                          <Text style={styles.goal}>Цель</Text>
                        </View>
                      )}
                    </AnimatedCircularProgress>
                  </TouchableOpacity>
                </SkeletonContent>

                <View
                  style={[styles.rowIconsContainer, { width: WINDOW_WIDTH }]}
                >
                  <SkeletonContent
                    layout={[{ width: 55, height: 55 }]}
                    isLoading={isLoading}
                  >
                    <SubstanceElement
                      imagePath={iconImages.prot}
                      middleTitle={"БЕЛКИ"}
                      bottomTitle={
                        isReturnData
                          ? `${allSquirrels} г`
                          : `${currentSquirrels}/${currentWeight} г`
                      }
                    />
                  </SkeletonContent>

                  <SkeletonContent
                    layout={[{ width: 55, height: 55 }]}
                    isLoading={isLoading}
                  >
                    <SubstanceElement
                      imagePath={iconImages.uglevod}
                      middleTitle={"УГЛЕВОДЫ"}
                      bottomTitle={
                        isReturnData
                          ? `${allCarbohydrates} г`
                          : `${currentCarbohydrates}/${currentWeight} г`
                      }
                    />
                  </SkeletonContent>

                  <SkeletonContent
                    layout={[{ width: 55, height: 55 }]}
                    isLoading={isLoading}
                  >
                    <SubstanceElement
                      imagePath={iconImages.fats}
                      middleTitle={"ЖИРЫ"}
                      bottomTitle={
                        isReturnData
                          ? `${allFats} г`
                          : `${currentFats}/${currentWeight} г`
                      }
                    />
                  </SkeletonContent>
                </View>
              </View>

              <View
                style={{
                  ...styles.containerListFood,
                  paddingHorizontal: 16,
                  height: Dimensions.get("screen").height / 2.5,
                }}
              >
                {issetFoodsData ? (
                  <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, paddingBottom: 115 }}
                    containerStyle={{ paddingBottom: 75 }}
                  >
                    {this.state.breakfasts && this.state.breakfasts.length ? (
                      <ListFoodDiary
                        isLoading={isLoading}
                        handlerDataFood={this.handlerDataFood}
                        data={this.state.breakfasts}
                        title="Завтрак"
                        showDishes={true}
                        isProgram={false}
                        emptyDataText="Блюда для завтрака не добавлены"
                      />
                    ) : null}

                    {this.state.obeds && this.state.obeds.length ? (
                      <ListFoodDiary
                        isLoading={isLoading}
                        handlerDataFood={this.handlerDataFood}
                        data={this.state.obeds}
                        title="Обед"
                        showDishes={true}
                        isProgram={false}
                        emptyDataText="Блюда для обеда не добавлены"
                      />
                    ) : null}

                    {this.state.poldniki && this.state.poldniki.length ? (
                      <ListFoodDiary
                        isLoading={isLoading}
                        handlerDataFood={this.handlerDataFood}
                        data={this.state.poldniki}
                        title="Полдник"
                        showDishes={true}
                        isProgram={false}
                        emptyDataText="Блюда для полдника не добавлены"
                      />
                    ) : null}

                    {this.state.dinners && this.state.dinners.length ? (
                      <ListFoodDiary
                        isLoading={isLoading}
                        handlerDataFood={this.handlerDataFood}
                        data={this.state.dinners}
                        title="Ужин"
                        showDishes={true}
                        isProgram={false}
                        emptyDataText="Блюда для ужина не добавлены"
                      />
                    ) : null}
                  </ScrollView>
                ) : (
                  <View
                    style={{
                      flex: 1,
                      minHeight: Dimensions.get("screen").height / 2.8,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Text style={{ width: "100%", textAlign: "center" }}>
                      Список пуст
                    </Text>
                  </View>
                )}
              </View>
            </View>
          </ScrollView>
        </Background2>

        <CustomBottomButton
          label={"Добавить блюдо"}
          onPress={() =>
            this.props.navigation.navigate("AddDish", {
              UpdateDishes: this.getFoodsByDay,
              daysProp,
              currentActiveIndex: this.state.currentCarouselIndex,
            })
          }
        />
      </>
    );
  }
}
