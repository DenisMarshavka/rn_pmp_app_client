import React, { useEffect, useState } from "react";
import { AsyncStorage } from "react-native";
import { connect } from "react-redux";

import { ProgramsAPI } from "../../../../api/programs";
import { getPrograms } from "../../../../actions/ProgramsActions";
import DefaultRowsList from "../../../DefaultRowsListScreen";

const MyProgramScreen = ({
  navigation,
  // loading = false,
  // programs = [],
  programMaxCount = 0,
  // requestFailed = false,
  getPrograms,
}) => {
  const [loading, setLoading] = useState(true);
  const [programs, setPrograms] = useState([]);
  const [requestFailed, setRequestFailed] = useState(false);

  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(15);
  const [orderData, seOrderData] = useState("desc");

  console.log("MY PROGRAMS DATA: ", programs, ", max Count: ", programMaxCount);

  useEffect(() => {
    setLoading(true);

    AsyncStorage.getItem("user")
      .then((user) => {
        if (user && JSON.parse(user) && JSON.parse(user).id !== false) {
          console.log(
            "PROGRAMS USER ID",
            JSON.parse(user).id,
            JSON.parse(user)
          );

          ProgramsAPI.getUserPrograms(JSON.parse(user).id)
            .then((programs) => {
              console.log("PROGRAMSSS", programs);

              if (programs) setPrograms(programs);

              setLoading(false);
            })
            .catch((e) => setRequestFailed(e));
        }
      })
      .catch((e) => setRequestFailed(e));

    // getPrograms(offset, limit, orderData);

    return () => {
      setLoading(true);
    };
  }, []);

  return (
    <DefaultRowsList
      title={"Мои программы"}
      isLoading={loading}
      dataList={programs}
      navigation={navigation}
      requestFailed={requestFailed}
      isProgramsList={true}
      isProgram={true}
    />
  );
};

const mapStateToProps = (state) => ({
  loading: state.ProgramsReducer.programsLoading,
  programs: state.ProgramsReducer.programsData,
  programMaxCount: state.ProgramsReducer.countItems,
  requestFailed: state.ProgramsReducer.programsError,
});

const mapDispatchToProps = {
  getPrograms,
};

export default connect(mapStateToProps, mapDispatchToProps)(MyProgramScreen);
