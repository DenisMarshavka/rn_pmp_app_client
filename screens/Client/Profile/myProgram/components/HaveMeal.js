import { LinearGradient } from 'expo-linear-gradient';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import { colors, fonts, sizes } from '../../../../../constants/theme';
import { Icon } from 'react-native-elements';
import { DishMeal } from './DishMeal';

export const HaveMeal = ({ eating, calories }) => {
    const [isOpen, setIsOpen] = useState(false);

    const dishs = [
        { id: 1, dish: 'Яичница с овощами', calories: 280},
        { id: 2, dish: 'Чай черный', calories: 0},
        { id: 3, dish: 'Яблоко', calories: 65}
    ];

    return (
        <LinearGradient
            start={{ x: 1.2, y: 1.2 }}
            end={{ x: 0.1, y: 0.1 }}
            colors={[colors.white, colors.gray]}
            style={{marginBottom: 11, borderRadius: 10}}
        >
            <TouchableOpacity activeOpacity={.8} onPress={() => setIsOpen(!isOpen)}>
                <View style={styles.container_line}>
                    <View style={{flex: 1, paddingRight:  10}}>
                        <Text style={[{ color: colors.text }, fonts.normal, fonts.h3]} numberOfLines={2}
                            ellipsizeMode="tail">
                            {eating}
                        </Text>
                    </View>

                    <View style={{marginTop: 5}}>
                        <Text style={[{ color: colors.orange }, fonts.normal, fonts.h3]}>
                            {calories} ккал
                        </Text>

                        <View>
                            <Icon name={isOpen ? 'chevron-small-up' : 'chevron-small-down'} type='entypo' color='#151C26' size={28}/>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>

            {
                isOpen &&

                <View style={{paddingHorizontal: sizes.margin, flex: 1}}>
                    <View style={{backgroundColor: colors.orange, height: 1}} />

                    <ScrollView style={{paddingBottom: 10, maxHeight: 450}}>
                        {
                            dishs.map((item, i) => (
                                <DishMeal showBorderBottom={i !== dishs.length - 1} key={`dish-${ item && item.id ? item.id : i }`} dish={item.dish} calories={item.calories} />
                            ))
                        }

                    </ScrollView>
                </View>
            }
        </LinearGradient>
    );
};

const styles = StyleSheet.create({
    container_line: {
        width: "100%",
        height: 64,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: sizes.margin,
        alignItems: 'center'
    }
});
