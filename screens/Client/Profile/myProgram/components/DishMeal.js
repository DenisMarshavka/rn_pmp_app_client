import { LinearGradient } from 'expo-linear-gradient';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { colors, fonts, sizes } from '../../../../../constants/theme';

export const DishMeal = ({ showBorderBottom = true, dish, calories }) => {
    const [isDescription, setIsDescription] = useState(false);

    return (
        <View>
            <TouchableOpacity onPress={() => setIsDescription(!isDescription)} activeOpacity={.8} style={{paddingVertical: 18, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                <Text style={[{ color: colors.title }, fonts.normal, fonts.h2]}>{dish}</Text>

                <Text style={[{ color: colors.title }, fonts.normal, fonts.h2]}>{calories} ккал</Text>
            </TouchableOpacity>

            {
                isDescription &&

                <View style={{paddingBottom: 10}}>
                    <Text style={[fonts.normal, fonts.description, {color: colors.title}]}>Шпинат</Text>

                    <Text style={[fonts.normal, fonts.description, {color: 'rgba(0, 0, 0, 0.54)', marginBottom: 10}]}>20 грамм</Text>

                    <Text style={[fonts.normal, fonts.description, {color: colors.title}]}>Стручковой фасоль</Text>

                    <Text style={[fonts.normal, fonts.description, {color: 'rgba(0, 0, 0, 0.54)', marginBottom: 10}]}>50 грамм</Text>

                    <Text style={[fonts.normal, fonts.description, {color: colors.title}]}>Помидор</Text>

                    <Text style={[fonts.normal, fonts.description, {color: 'rgba(0, 0, 0, 0.54)', marginBottom: 10}]}>1 шт</Text>

                    <Text style={[fonts.normal, fonts.description, {marginBottom: 10}]}>Соль и перец по вкусу</Text>

                    <Text style={[fonts.normal, fonts.description, {color: colors.title}]}>На сковороду выкладываем кусочки помидор и немного жарим их, затем кладем шпинат и стручковую фасоль. Готовим на маленьком огне, пока овощи не станут мягкими Затем разбиваем яйца, солим и перчим.</Text>
                </View>
            }

            <View style={{height: 1, backgroundColor: showBorderBottom ? 'rgba(0, 0, 0, 0.12)' : 'transparent'}} />
        </View>
    );
};

const styles = StyleSheet.create({
    container_line: {
        width: "100%",
        height: 64,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: sizes.margin,
        alignItems: 'center'
    }
});
