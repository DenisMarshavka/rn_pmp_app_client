import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Modal,
  View,
  FlatList,
  ImageBackground,
  ScrollView,
  Image,
  SafeAreaView,
  Animated,
  Easing,
  StatusBar,
  ActivityIndicator,
} from "react-native";
import { withNavigation } from "react-navigation";
import moment from "moment";
import YoutubePlayer from "react-native-youtube-iframe";

import { fonts, colors, sizes } from "../../../../constants/theme";
import { Icon } from "react-native-elements";
import Carousel from "react-native-snap-carousel";
import { connect } from "react-redux";
import {
  gerProgramById,
  // getFoodsByDay,
  getProgramSectionById,
  getProgramFoodsByDayId,
} from "../../../../actions/ProgramsActions";
import { ListFoodDiary } from "../foodDiary/components/ListFoodDiary";

const ProgramScreen = ({
  navigation,
  route,

  gerProgramById = () => {},
  programData = {},
  programSectionData = {},
  isLoading = true,

  getProgramSectionById = () => {},

  // getFoodsByDay = () => {},
  // foodDiaryData = {},
  // foodDiaryLoading = true,

  getProgramFoodsByDayId = () => {},
  recommendedDishesLoading = true,
  recommendedDishesData = {},
  recommendedDishesError = false,
}) => {
  const [isProgram, setIsProgram] = useState(true);
  const [showVideo, setShowVideo] = useState(false);
  const [videoId, setVideoId] = useState(null);

  const [showBreakfasts, setShowBreakfasts] = useState(false);
  const [showObeds, setShowObeds] = useState(false);
  const [showPoldniki, setShowPoldniki] = useState(false);
  const [showDinners, setShowDinners] = useState(false);
  const [loadingDiary, setLoadingDiary] = useState(true);

  const [animation] = useState(new Animated.Value(0));
  const [expanded, setExpanded] = useState(true);
  const [currentCarouselIndex, setCurrentCarouselIndex] = useState(0);
  const [days, setDays] = useState([]);
  const { programId = null } =
    route && route.params && route.params ? route.params : {};

  const [tabName, setTabName] = useState("");
  const [hideTabs, setHideTabs] = useState(true);
  const [sections, setSections] = useState([]);

  const [selectedFoodDayId, setSelectedFoodDayId] = useState(null);
  const data = recommendedDishesData;

  const {
    calories_summ: caloriesSumm = 0,
    squirrels_summ = 0,
    carbohydrates_summ = 0,
    fats_summ = 0,
    foods = {},
  } = recommendedDishesData;
  const { breakfasts, dinners, obeds, poldniki } = foods;
  const dishesByDayIdEmpty =
    !(breakfasts && Object.keys(breakfasts).length) &&
    !(dinners && Object.keys(dinners).length) &&
    !(obeds && Object.keys(obeds).length) &&
    !(poldniki && Object.keys(poldniki).length);

  const { title: programName = "" } = programData;

  useEffect(() => {
    gerProgramById(programId);

    // getFoodsByDay(moment().format("YYYY-MM-DD"));
  }, []);

  useEffect(() => {
    if (!isLoading && programData) {
      // console.log(
      //   "PROGRAM DAYS: ",
      //   programData.program.program_days,
      //   programData.program.program_days[0],
      //   "id: ",
      //   programData.program.program_days[0].id
      // );

      if (
        programData &&
        programData.program &&
        programData.program.program_days &&
        programData.program.program_days[0] &&
        programData.program.program_days[0].id !== false
      )
        getProgramFoodsByDayId(programData.program.program_days[0].id);
    }
  }, [isLoading, programData]);

  useEffect(() => {
    if (
      !isLoading &&
      programData &&
      programData.steps &&
      programData.steps.length
    ) {
      // console.log("dsd", programData.steps);
      setTabName(programData.steps[0].name);
      setSections(programData.steps[0].program_sections);
    }

    if (
      !isLoading &&
      programData &&
      programData.program &&
      programData.program.program_days &&
      programData.program.program_days.length
    ) {
      setDays(programData.program.program_days);
    }
  }, [programData, isLoading]);

  // console.log("programDataprogramData", programData, "days Data: ", days);

  const renderItem = ({ item, index }) => {
    return (
      <View
        key={`item-day-${index}`}
        style={{
          backgroundColor:
            index === currentCarouselIndex ? "#F46F22" : "transparent",
          borderRadius: 30,
          paddingVertical: 6,
          // paddingHorizontal: 10,
        }}
      >
        <Text
          style={[
            fonts.medium,
            {
              fontSize: 14,
              color: index === currentCarouselIndex ? "#FAFAFA" : "#151C26",
              textAlign: "center",
            },
          ]}
        >
          {item}
        </Text>
      </View>
    );
  };

  const toggle = (name, section) => {
    setTabName(name);
    hideTabs ? setHideTabs(false) : setHideTabs(true);

    if (tabName !== name) {
      setSections(section);
    }

    Animated.timing(animation, {
      toValue: expanded,
      duration: 1,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => {
      setExpanded(!expanded);
    });
  };

  const showVideoSection = (video_id, block) => {
    if (video_id) {
      setVideoId(video_id);
    }

    if (!block) {
      showVideo ? setShowVideo(false) : setShowVideo(true);
    }
  };

  const renderCarouselSetion = (sectionTitle = "", dataList = []) => (
    <View style={{ marginTop: 30 }}>
      {sectionTitle.trim() && (
        <Text
          style={[fonts.normal, fonts.h2, { paddingHorizontal: sizes.margin }]}
        >
          {sectionTitle}
        </Text>
      )}

      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={{ paddingHorizontal: sizes.margin, marginTop: 20 }}
      >
        {dataList.map((item, i) => (
          <TouchableOpacity
            key={`exercises-image-${item.id}`}
            onPress={() => showVideoSection(item.video_link, item.block)}
            style={{
              marginRight: 9,
              alignItems: "center",
            }}
          >
            <Image
              source={{ uri: item.preview_img }}
              style={{
                width: 105,
                height: 105,
                borderRadius: 7,
              }}
            />

            {!item.block ? null : (
              <View
                style={{
                  width: 105,
                  height: 105,
                  ...StyleSheet.absoluteFillObject,
                  backgroundColor: "rgba(22, 22, 22, 0.86)",
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 7,
                }}
              >
                <Image
                  source={require("../../../../assets/icons/block.png")}
                  style={{
                    width: 26,
                    height: 26,
                  }}
                />

                {item.start && (
                  <Text style={{ color: "#fff", paddingTop: 7, fontSize: 11 }}>
                    {item.start}
                  </Text>
                )}
              </View>
            )}

            <Text
              // numberOfLines={1}
              style={[
                fonts.normal,
                fonts.description,
                {
                  color: "rgba(30, 32, 34, 0.54)",
                  marginTop: 10,
                  width: "80%",
                  textAlign: "center",
                },
              ]}
            >
              {item.name}
            </Text>

            <Text
              style={[
                fonts.normal,
                fonts.description,
                { color: colors.title, marginTop: 6 },
              ]}
            >
              {item.time}
            </Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );

  const getAllSumCalories = (dishType = "") => {
    let summAll = 0;

    if (
      dishType &&
      dishType.trim() &&
      recommendedDishesData &&
      recommendedDishesData.foods &&
      recommendedDishesData.foods[dishType]
    ) {
      for (let dish of recommendedDishesData.foods[dishType]) {
        if (dish && dish.calories) summAll += dish.calories;
      }
    }

    return summAll;
  };

  return (
    <ImageBackground
      style={styles.imgBackground}
      source={require("../../../../assets/images/background/foodDiary.png")}
    >
      <StatusBar barStyle="light-content" />

      <SafeAreaView
        barStyle="dark-content"
        style={{ backgroundColor: colors.orange, color: "#fff" }}
      >
        <View
          style={{
            width: "100%",
            zIndex: 100,
          }}
        >
          <Animated.View
            style={{
              paddingTop: 15,
              paddingBottom: 20,
              backgroundColor: colors.orange,
              overflow: "hidden",
              maxHeight: expanded ? 50 : 500,
            }}
          >
            {isLoading ? (
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  paddingTop: 2.5,
                }}
              >
                <ActivityIndicator size={"small"} color={"#fff"} />
              </View>
            ) : programData.steps ? (
              programData.steps.map((item, i) => {
                const typesLen = programData.steps.length;

                return (
                  <View
                    key={item.id}
                    style={{
                      zIndex: 10,
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      marginTop: !i ? 0 : 15,
                    }}
                  >
                    {programData.steps.indexOf(item) === 0 ? (
                      <>
                        <TouchableOpacity
                          onPress={() =>
                            toggle(item.name, item.program_sections)
                          }
                        >
                          <Text
                            style={[
                              fonts.normal,
                              {
                                fontSize: 19,
                                color: "#FAFAFA",
                                textTransform: "uppercase",
                              },
                            ]}
                          >
                            {tabName}
                          </Text>
                        </TouchableOpacity>

                        {expanded && (
                          <TouchableOpacity
                            onPress={() => toggle(tabName, sections)}
                            style={{
                              position: "absolute",
                              right: "18%",
                            }}
                          >
                            <Icon
                              name="chevron-small-down"
                              type="entypo"
                              color="#FAFAFA"
                              size={35}
                            />
                          </TouchableOpacity>
                        )}
                      </>
                    ) : typesLen == i + 1 ? (
                      <View style={{ display: hideTabs ? "none" : "flex" }}>
                        <TouchableOpacity
                          onPress={() =>
                            toggle(item.name, item.program_sections)
                          }
                        >
                          <Text
                            style={[
                              fonts.normal,
                              {
                                fontSize: 19,
                                color: "#FAFAFA",
                                textTransform: "uppercase",
                                opacity: 0.5,
                              },
                            ]}
                          >
                            {item.name}
                          </Text>
                        </TouchableOpacity>

                        <View
                          style={{
                            position: "absolute",
                            right: -69,
                            top: -5,
                          }}
                        >
                          <TouchableOpacity
                            onPress={() => toggle(tabName, sections)}
                          >
                            <Icon
                              name="chevron-small-up"
                              type="entypo"
                              color="#FAFAFA"
                              size={35}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                    ) : (
                      <TouchableOpacity
                        onPress={() => toggle(item.name, item.program_sections)}
                      >
                        <Text
                          style={[
                            fonts.normal,
                            {
                              fontSize: 19,
                              color: "#FAFAFA",
                              textTransform: "uppercase",
                              opacity: 0.5,
                              display: hideTabs ? "none" : "flex",
                            },
                          ]}
                        >
                          {item.name}
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                );
              })
            ) : null}
          </Animated.View>
        </View>
      </SafeAreaView>

      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 75, marginTop: 40 }}
      >
        <View
          style={{
            marginTop: 0,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            style={{ marginRight: 15 }}
            onPress={() => navigation.goBack()}
          >
            <Icon
              name="chevron-small-left"
              type="entypo"
              color="#151C26"
              size={32}
            />
          </TouchableOpacity>

          <Text style={[fonts.normal, { fontSize: 16 }]}>
            {programData.program ? programData.program.title : "Программа"}
          </Text>

          <Text style={[fonts.normal, { fontSize: 16 }]}>{programName}</Text>
        </View>

        {isLoading ? (
          <View
            style={{
              marginVertical: 30,
              flex: 1,
              minHeight: 50,
              alignItems: "flex-start",
              justifyContent: "center",
              marginLeft: sizes.margin,
            }}
          >
            <ActivityIndicator size={"small"} color={colors.orange} />
          </View>
        ) : (
          <View
            style={{
              paddingHorizontal: sizes.margin,
              marginTop: 30,
              marginBottom: 30,
            }}
          >
            <Carousel
              shouldOptimizeUpdates={true}
              activeSlideAlignment={"start"}
              enableMomentum={true}
              activeSlideOffset={10}
              callbackOffsetMargin={0}
              onBeforeSnapToItem={async (currentIndex) => {
                const dayId =
                  days &&
                  days.length &&
                  days[currentIndex] &&
                  days[currentIndex].id !== false
                    ? days[currentIndex].id
                    : null;

                // await getFoodsByDay(day);
                if (dayId && !isNaN(dayId)) await getProgramFoodsByDayId(dayId);
                setCurrentCarouselIndex(currentIndex);
              }}
              inactiveSlideOpacity={0.6}
              // shouldOptimizeUpdates
              itemHeight={29}
              data={
                days && days.length
                  ? days.map((v, index) => `${index + 1} день`)
                  : []
              }
              firstItem={0}
              renderItem={renderItem}
              sliderWidth={380}
              itemWidth={77}
            />
          </View>
        )}

        {recommendedDishesLoading ? (
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              minHeight: 400,
            }}
          >
            <ActivityIndicator size={"large"} color={colors.orange} />
          </View>
        ) : (breakfasts || obeds || poldniki || dinners) &&
          !recommendedDishesError ? (
          <View style={styles.containerListFood}>
            <Text style={[fonts.normal, { fontSize: 16, marginBottom: 18 }]}>
              Программы питания
            </Text>

            <ListFoodDiary
              isLoading={isLoading}
              isProgram={isProgram}
              showDishes={showBreakfasts}
              setShowDishes={setShowBreakfasts}
              summAll={getAllSumCalories("breakfasts")}
              id={1}
              data={breakfasts}
              title="Завтрак"
              emptyDataText="Блюда для завтрака не добавлены"
            />

            <ListFoodDiary
              isLoading={isLoading}
              isProgram={isProgram}
              showDishes={showObeds}
              setShowDishes={setShowObeds}
              summAll={getAllSumCalories("obeds")}
              id={2}
              data={obeds}
              title="Обед"
              emptyDataText="Блюда для обеда не добавлены"
            />

            <ListFoodDiary
              isLoading={isLoading}
              isProgram={isProgram}
              showDishes={showPoldniki}
              setShowDishes={setShowPoldniki}
              id={3}
              data={poldniki}
              summAll={getAllSumCalories("poldniki")}
              title="Полдник"
              emptyDataText="Блюда для полдника не добавлены"
            />

            <ListFoodDiary
              isLoading={isLoading}
              isProgram={isProgram}
              showDishes={showDinners}
              setShowDishes={setShowDinners}
              id={4}
              data={dinners}
              summAll={getAllSumCalories("dinners")}
              title="Ужин"
              emptyDataText="Блюда для ужина не добавлены"
            />
          </View>
        ) : recommendedDishesError ? (
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              minHeight: 400,
            }}
          >
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Ошибка получения данных
            </Text>
          </View>
        ) : null}

        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            marginTop: 30,
          }}
        >
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              navigation.navigate("AddDish", {
                fromProgram: true,
                programName,
                // UpdateDishes: () =>
                //   getFoodsByDay(moment().format("YYYY-MM-DD")),
              })
            }
            style={{
              backgroundColor: colors.orange,
              paddingHorizontal: 25,
              paddingVertical: 8,
              borderRadius: 30,
            }}
          >
            <Text
              style={[fonts.normal, fonts.description, { color: colors.white }]}
            >
              Добавить данные в дневник
            </Text>
          </TouchableOpacity>
        </View>

        {sections.map((item) =>
          item.videos && item.videos.length
            ? renderCarouselSetion(item.name, item.videos)
            : null
        )}

        <Modal
          animationType="slide"
          transparent={true}
          visible={showVideo}
          onRequestClose={() => setShowVideo(false)}
        >
          <View style={styles.centeredView}>
            <TouchableOpacity
              onPress={() => showVideoSection()}
              style={styles.videoShadow}
            />

            <YoutubePlayer
              height={200}
              width={"100%"}
              videoId={videoId}
              play={true}
              onChangeState={(event) => console.log(event)}
              onPlaybackQualityChange={(q) => console.log(q)}
              volume={50}
              playbackRate={1}
              playerParams={{
                cc_lang_pref: "us",
                showClosedCaptions: true,
              }}
            />
          </View>
        </Modal>
      </ScrollView>
    </ImageBackground>
  );
};

const mapStateToProps = (state) => ({
  programData: state.ProgramsReducer.programData,
  programSectionData: state.ProgramsReducer.programSectionData,
  isLoading: state.ProgramsReducer.programLoading,

  // foodDiaryData: state.ProgramsReducer.foodDiaryData,
  // foodDiaryLoading: state.ProgramsReducer.diaryDataLoading,

  recommendedDishesLoading: state.ProgramsReducer.recommendedDishesLoading,
  recommendedDishesData: state.ProgramsReducer.recommendedDishesData,
  recommendedDishesError: state.ProgramsReducer.recommendedDishesError,
});

const mapDispatchToProps = {
  gerProgramById,
  // getFoodsByDay,
  getProgramSectionById,

  getProgramFoodsByDayId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withNavigation(ProgramScreen));

const styles = StyleSheet.create({
  imgBackground: {
    width: "100%",
    height: "100%",
  },
  containerListFood: {
    paddingLeft: 16,
    paddingRight: 16,
  },
  videoShadow: {
    opacity: 0.7,
    position: "absolute",
    width: "100%",
    height: "100%",
    top: 0,
    backgroundColor: "#000",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
