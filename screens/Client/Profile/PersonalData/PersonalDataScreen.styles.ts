import { StyleSheet, Dimensions } from "react-native";
import { fonts } from "../../../../constants/theme";
import { colors } from "react-native-elements";

const styles = StyleSheet.create({
  imageBackgroundStyle: {
    flex: 1,
  },
  imageStyle: {
    resizeMode: "stretch",
  },
  viewStyle: {
    flex: 1,
    justifyContent: "flex-end",
  },
  navBarStyle: {
    color: "#151C26",
    fontWeight: "bold",
    fontSize: 18,
  },
  containerStyle: {
    backgroundColor: "transparent",
    justifyContent: "space-around",
    height: 20,
    paddingRight: 25,
    position: "absolute",
    top: 0,
    left: 0,
    // marginBottom: 24
  },
  simpleText: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 13,
    lineHeight: 18,
    color: "#F46F22",
    paddingLeft: 10,
    fontWeight: "bold",
    marginTop: 15,
  },
  textAccept: {
    fontFamily: fonts.normal.fontFamily,
    fontSize: 13,
    color: "#000",
    opacity: 0.24,
    textAlign: "center",
    marginVertical: 17,
  },
});

export default styles;
