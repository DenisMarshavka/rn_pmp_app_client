import * as React from "react";
import { Component } from "react";
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  SafeAreaView,
} from "react-native";
import { Text } from "react-native-elements";
import { Formik } from "formik";
import * as Yup from "yup";
import moment from "moment";

import styles from "./PersonalDataScreen.styles";
import CheckBoxInput from "../../../../components/checkboxinput/CheckBoxInput";
import ButtonInput from "../../../../components/button/ButtonInput";
import { Background } from "../../../../components/Background";
import PersonalDataInput from "../../../../components/input/personal_data_input/PersonalDataInput";
import { Header, ToastView } from "../../../../components/index";
import { sizes } from "../../../../constants/theme";
import DatePickerInput from "../../../../components/datepickerinput/DatePickerInput.tsx";
import { UserAPI } from "../../../../api/user/index";
import { formatPhone } from "../../../../utils";

type IState = {
  date: Date;
  user: {
    login?: string;
    password?: string;
    lastname?: string;
    middlename?: string;
    firstname?: string;
    email?: string;
    female?: boolean;
  };
};

type ProfileProps = {
  navigation: any;
  route: any;
  currentUser: any;
};

export default class PersonalDataScreen extends Component<
  ProfileProps,
  IState
> {
  state = {
    date: new Date(),
    user: {
      login: "",
      password: "",
      lastname: "",
      middlename: "",
      firstname: "",
      email: "",
      female:
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.isFemale,
      showDatePicker: false,
    },
  };

  constructor(props: ProfileProps) {
    super(props);
  }

  setStartUserDate = async () => {
    const user = await UserAPI.getUserById(
      this.props.route.params.currentUser.id
    );

    console.log(
      "NNNEWWWW DATTTAA111",
      user,
      this.props.route.params.currentUser
    );

    return user;
  };

  componentDidMount() {
    if (
      this.props &&
      this.props.route.params &&
      this.props.route.params &&
      this.props.route.params.currentUser &&
      this.props.route.params.currentUser.id !== false
    ) {
      this.setStartUserDate().then((user) => {
        if (user) this.setState({ user });
      });
    }
  }

  render() {
    const {
      login,
      lastname,
      middlename,
      firstname,
      email,
      password,
    } = this.state.user;
    const { onUerDataUpdate = () => {}, currentUser = {} } =
      (this.props.route && this.props.route.params) || {};

    return (
      <KeyboardAvoidingView behavior="padding">
        <Formik
          enableReinitialize={true}
          initialValues={{
            login: formatPhone(login),
            lastname,
            middlename,
            firstname,
            email,
            password: "",
          }}
          validationSchema={Yup.object().shape({
            login: Yup.string()
              .required("Обязательное поле")
              .matches(
                /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{2}\)?)\s?-?\s?(\(?\d{2}\)?)?/,
                "Некорректный номер телефона"
              ),
            password: Yup.string().matches(
              /(?=.*[0-9])/,
              "Некорректный пароль"
            ),
            middlename: Yup.string().required("Обязательное поле"),
            firstname: Yup.string().required("Обязательное поле"),
            email: Yup.string().required("Обязательное поле"),
          })}
          onSubmit={async (values) => {
            try {
              const {
                login,
                lastname,
                middlename,
                firstname,
                email,
                password,
              } = values;

              const newUserData = {
                login: login.replace(/ /g, "").slice(0, 12),
                lastname,
                middlename,
                firstname,
                email,
                female: this.state.user.female,
                birthday: moment(this.state.date).format("YYYY-MM-DD"),
              };

              if (password && password.trim()) newUserData.password = password;

              const responseUpade = await UserAPI.update(newUserData);
              await onUerDataUpdate();

              console.log("responseUpaderesponseUpade", responseUpade);

              if (
                responseUpade &&
                responseUpade.status &&
                responseUpade.status === "success"
              ) {
                ToastView("Ваши данные сохранены");
                this.props.navigation.goBack();
              } else
                ToastView(
                  "Ошибка! Данные не сохранены! Произошла ошибка.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27."
                );
            } catch (e) {
              console.log("Error UserAPI.update --- ", e);
            }
          }}
        >
          {({ handleChange, handleSubmit, isValid, values }) => (
            <>
              <SafeAreaView>
                <Header
                  title={"Персональные данные"}
                  isSubmit={false}
                  style={{ paddingTop: 5, paddingLeft: 10 }}
                />
              </SafeAreaView>

              <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 75 }}
              >
                <View style={{ paddingHorizontal: sizes.margin }}>
                  <PersonalDataInput
                    value={values.middlename}
                    label={"Фамилия"}
                    onChange={handleChange("middlename")}
                    isRequired
                  />
                  <PersonalDataInput
                    value={values.firstname}
                    label={"Имя"}
                    onChange={handleChange("firstname")}
                    isRequired
                  />
                  <PersonalDataInput
                    value={values.lastname}
                    label={"Отчество"}
                    onChange={handleChange("lastname")}
                  />
                  <PersonalDataInput
                    value={values.email}
                    label={"Email"}
                    onChange={handleChange("email")}
                    isRequired
                  />
                  <PersonalDataInput
                    value={formatPhone(
                      values && values.login && !values.login.includes("+7")
                        ? "+7" + values.login
                        : values.login
                    )}
                    label={"Номер телефона"}
                    onChange={handleChange("login")}
                    isRequired
                    inputType="numeric"
                  />
                  <DatePickerInput
                    show={this.state.showDatePicker}
                    date={this.state.date}
                    label={"Дата рождения"}
                    setDate={(date) => this.setState({ date })}
                    isFocused={this.state.showDatePicker}
                    onPress={() =>
                      this.setState({
                        showDatePicker: !this.state.showDatePicker,
                      })
                    }
                    isRequired
                  />
                  <PersonalDataInput
                    label={"Новий пароль"}
                    onChange={handleChange("password")}
                    onlySecureText={false}
                    isSecure
                  />

                  <Text style={styles.simpleText}>Пол</Text>

                  <View
                    style={{
                      paddingHorizontal: sizes.margin / 2,
                      marginTop: 25,
                    }}
                  >
                    <CheckBoxInput
                      title={"Мужчина"}
                      style={{ marginLeft: 16 }}
                      checked={!this.state.user.female}
                      onPress={() =>
                        this.setState({
                          user: { ...this.state.user, female: false },
                        })
                      }
                    />

                    <View style={{ marginTop: 24 }}>
                      <CheckBoxInput
                        title={"Женщина"}
                        style={{ marginLeft: 16 }}
                        checked={this.state.user.female}
                        onPress={() =>
                          this.setState({
                            user: { ...this.state.user, female: true },
                          })
                        }
                      />
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    marginVertical: 20,
                    paddingHorizontal: 16,
                    paddingBottom: 35,
                  }}
                >
                  <Text style={styles.textAccept}>
                    Согласен(-на) с Политикой Конфиденциальности
                  </Text>

                  <ButtonInput
                    disabled={!isValid}
                    title={"Сохранить"}
                    onClick={handleSubmit}
                  />
                </View>
              </ScrollView>
            </>
          )}
        </Formik>
      </KeyboardAvoidingView>
    );
  }
}
