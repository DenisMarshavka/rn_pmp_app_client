import React, { Component, useState, useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  Dimensions,
} from "react-native";
import { Calendar, LocaleConfig } from "react-native-calendars";
import * as Animatable from "react-native-animatable";

import {
  fontSize16,
  fontSize28,
  fonts,
  colors,
  sizes,
} from "../../../constants/theme";
import { LinearGradient } from "expo-linear-gradient";
import { ListScheduleItem } from "../../../components/ListScheduleItem";
import { Icon } from "react-native-elements";
import { Background } from "../../../components/Background";
import Header from "../../../components/Headers/Header";
import moment from "moment";

import { connect } from "react-redux";
import { getSchedule, cleanSchedule } from "../../../actions/schedule";

LocaleConfig.locales["ru"] = {
  monthNames: [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь",
  ],
  monthNamesShort: [
    "Янв.",
    "Фев.",
    "Мрт.",
    "Апр.",
    "Май",
    "Ин.",
    "Ил.",
    "Авг.",
    "Снт.",
    "Окт.",
    "Нб.",
    "Дек.",
  ],
  dayNames: [
    "Понедельник",
    "Вторник",
    "Среда",
    "Четверг",
    "Пятница",
    "Суббота",
    "Воскресенье ",
  ],
  dayNamesShort: ["ВС", "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ"],
};
LocaleConfig.defaultLocale = "ru";

const ScheduleScreen = (props) => {
  const {
    loading = false,
    schedule = [],
    errorRequest = false,
    getSchedule,
  } = props;

  const dateCurrent = new Date();
  const [dateSelected, setDateSelected] = useState(null);

  useEffect(() => {
    return () => {
      cleanSchedule();
    };
  }, []);

  const loadData = (date) => {
    getSchedule(date.dateString);
  };

  const getSelectedDate = () => {
    if (dateSelected) {
      let key = Object.keys(dateSelected)[0];
      return moment(key ? new Date(key) : null).format("DD.MM.YYYY");
    }
  };

  console.log(loading, schedule, errorRequest, getSchedule);

  return (
    <Background>
      <Header title={"Мое расписание"} />

      <ScrollView
        style={{ flex: 1 }}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 50 }}
      >
        <View style={styles.container}>
          <LinearGradient
            useAngle
            angle={267}
            start={[0.8, 0.8]}
            end={[0, 0]}
            colors={["rgba(240, 240, 240, 1);", "#DADADA"]}
            style={styles.calendarGradient}
          >
            <Calendar
              style={styles.calendar}
              markedDates={dateSelected}
              renderArrow={(direction) => customIcons(direction)}
              theme={calendarTheme}
              onDayPress={(day) => {
                loadData(day);

                setDateSelected({
                  [day.dateString]: {
                    selected: true,
                  },
                });
                console.log("DAY PRESS - ", day);
              }}
              monthFormat={"MMMM yyyy"}
              hideExtraDays={true}
              firstDay={1}
            />
          </LinearGradient>

          <View>
            <View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginBottom: 18,
                  marginTop: 28,
                }}
              >
                <Text style={[fonts.normal, fonts.h2, { color: "#1E2022" }]}>
                  {dateSelected
                    ? `Планы на ${getSelectedDate()}`
                    : "Не выбрана дата"}
                </Text>
              </View>

              {!dateSelected ? null : loading ? (
                <View style={styles.fullFill}>
                  <ActivityIndicator color={colors.mainOrange} />
                </View>
              ) : !errorRequest && schedule && schedule.length ? (
                schedule.map((item, index) => {
                  const isLoading = item && item.isTest;
                  const Wrap = !isLoading ? Animatable.View : View;

                  console.log("item.time_start", item.time_start);

                  return (
                    <Wrap
                      duration={350}
                      delay={index < 5 ? index * 350 : 370}
                      animation="fadeInUp"
                      key={`item-${item.name}`}
                      style={{ flex: 1 }}
                    >
                      <TouchableOpacity activeOpacity={0.7} activeOpacity={1}>
                        <ListScheduleItem
                          title={item.name}
                          date={`${item.time_start} - ${item.time_end}`}
                          icon={item.train_type_id}
                        ></ListScheduleItem>
                      </TouchableOpacity>
                    </Wrap>
                  );
                })
              ) : errorRequest ? (
                <View style={styles.fullFill}>
                  <Text
                    style={{
                      width: "100%",
                      textAlign: "center",
                      color: "#000",
                    }}
                  >
                    Ошибка получения данных
                  </Text>
                </View>
              ) : !errorRequest && schedule && !schedule.length ? (
                <View style={styles.fullFill}>
                  <Text
                    style={{
                      width: "100%",
                      textAlign: "center",
                      color: "#000",
                    }}
                  >
                    Расписание пустое
                  </Text>
                </View>
              ) : null}
            </View>
          </View>
        </View>
      </ScrollView>

      <LinearGradient
        pointerEvents={"none"}
        colors={["rgba(255, 255, 255, 0)", "#FFF"]}
        style={styles.linearGradient}
      />
    </Background>
  );
};

const mapStateToProps = (state) => ({
  loading: state.ScheduleReducer.loading,
  schedule: state.ScheduleReducer.schedule,
  errorRequest: state.ScheduleReducer.error,
});

const mapDispatchToProps = {
  getSchedule,
};

export default connect(mapStateToProps, mapDispatchToProps)(ScheduleScreen);

const styles = StyleSheet.create({
  container: { flex: 1, marginBottom: 104, paddingHorizontal: 10 },
  calendarGradient: {
    //marginVertical: 24,
    width: "100%",
    borderRadius: 10,
    alignItems: "center",
  },
  calendar: {
    width: Dimensions.get("screen").width / 1.3,
    borderRadius: 10,
    backgroundColor: "transparent",
  },
  header: {
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0, 0, 0, 0.12)",
  },
  gradient: {
    marginTop: 8,
    paddingVertical: 18,
    paddingHorizontal: 8,
    borderRadius: 10,
    justifyContent: "space-between",
    flexDirection: "row",
  },
  linearGradient: {
    flex: 1,
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 321,
  },
  fullFill: {
    height: Dimensions.get("window").height / 4,
    alignItems: "center",
    justifyContent: "center",
  },
});

const calendarTheme = {
  calendarBackground: "transparent",
  textDayFontFamily: fonts.normal.fontFamily,
  textMonthFontFamily: fonts.bold.fontFamily,
  textDayHeaderFontFamily: fonts.bold.fontFamily,
  dayTextColor: "#151C26",
  todayTextColor: "#F46F22",
  textSectionTitleColor: "#151C26",
  monthTextColor: "#151C26",
  selectedDayBackgroundColor: "rgba(244, 111, 34, 0.12)",
  selectedDayTextColor: "#F46F22",
  textDayFontSize: fontSize16,
  "stylesheet.day.basic": {
    text: {
      marginTop: 6,
    },
  },
};

const customIcons = (direction) => {
  {
    if (direction === "right") {
      return (
        <Icon
          name="chevron-small-right"
          size={fontSize28}
          color="rgba(21, 28, 38, 0.5)"
          type="entypo"
        />
      );
    } else {
      return (
        <Icon
          name="chevron-small-left"
          size={fontSize28}
          color="rgba(21, 28, 38, 0.5)"
          type="entypo"
        />
      );
    }
  }
};
