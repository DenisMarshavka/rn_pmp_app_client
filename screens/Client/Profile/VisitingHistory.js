import React from "react";
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  SectionList,
  Dimensions,
} from "react-native";
import { Icon } from "react-native-elements/src/index";
import Carousel from "react-native-snap-carousel";
import { PieChart } from "react-native-svg-charts";
import { connect } from "react-redux";
import moment from "moment";
import { SafeAreaView } from "react-native-safe-area-context";

import { LinearGradient } from "expo-linear-gradient/build/index";
import { getAttendanceStatistics } from "../../../actions/UserActions";
import VisitingListItem from "../../../components/VisitingListItem";
import { Header, MeasurementDiaryListItem } from "../../../components";
import {
  colors,
  fonts,
  fontSize14,
  fontSize16,
  fontSize28,
  fontSize34,
  sizes,
  WINDOW_HEIGHT,
} from "../../../constants/theme";
import { Background } from "../../../components/Background";
import HeaderBack from "../../../components/Headers/BackHeader";
import { getSizeSkeletonLoaders } from "../../../utils";
import SkeletonLoader from "../../../components/loaders/SkeletonLoader.js";

const monthNames = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
];

const CustomTitle = ({ text, background }) => {
  return (
    <View style={{ flexDirection: "row", alignItems: "center" }}>
      <View
        style={{
          height: 8,
          width: 8,
          borderRadius: 16,
          backgroundColor: background,
          marginRight: 8,
        }}
      />
      <Text
        style={{
          fontFamily: fonts.normal.fontFamily,
          fontSize: fontSize14,
        }}
      >
        {text}
      </Text>
    </View>
  );
};

class VisitingHistory extends React.Component {
  constructor(props) {
    super(props);

    this.currentMonth = moment().format("M");
    this.currentYaer = moment().format("YYYY");

    this.state = {
      activeElement: 0,
      selectedSlice: {
        label: "",
        value: 0,
      },
      labelWidth: 0,
    };
  }

  async componentDidMount() {
    this.setState(
      {
        activeElement: new Date().getMonth(),
      },
      () => this.getCurrentDataBySelectedDate(new Date().getMonth())
    );
  }

  getCurrentDataBySelectedDate = (index = 0) => {
    const selectedMonthindex = Number(index) + 1;

    if (index !== false && !isNaN(+index)) {
      this.props.getAttendanceStatistics(
        // TODO: will change this to normnal
        `${this.currentYaer}.${
          index.toString().length < 2
            ? "0" + selectedMonthindex
            : selectedMonthindex
        }.01`
      );

      this.setState({ activeElement: index });
    }
  };

  async getFoodsByDay(date, currentIndex = 0) {
    try {
      const foodsByDate = await DiaryFoodAPI.getDiaryFoodsByDate(date);
      this.setState({
        isLoading: false,
        isReturnData: true,
        currentCarouselIndex: currentIndex,
        breakfasts,
        dinners,
        obeds,
        poldniki,
        currentCalories: calories_summ,
        allCalories: calories_summ,
        allFats: fats_summ,
        allCarbohydrates: carbohydrates_summ,
        allSquirrels: squirrels_summ,
      });
    } catch (e) {
      console.log("Error DiaryFoodAPI.getDiaryFoodsByDate --- ", e);
    }
  }

  _renderItem = ({ item, index }) => (
    <View
      style={{
        backgroundColor:
          index === this.state.activeElement ? "#F46F22" : "transparent",
        borderRadius: 30,
        paddingVertical: 8,
      }}
    >
      <Text
        style={{
          color: index === this.state.activeElement ? "#FAFAFA" : "#151C26",
          textAlign: "center",
          fontFamily: fonts.normal.fontFamily,
          fontSize: fontSize14,
        }}
      >
        {item}
      </Text>
    </View>
  );

  getTrainigTypeDataLengths = (data = {}) => {
    const keys = [];
    const values = [];

    if (data && typeof data === "object" && Object.keys(data).length) {
      Object.keys(data).map((name) => {
        if (data[name] && data[name].length) {
          keys.push(name);
          // console.log(
          //   "IIIIITTTTEEEM: ",
          //   name,
          //   "dfg",
          //   data[name],
          //   data[name]["train_type_id"]
          // );

          values.push(data[name].length);
        }
      });
    }

    return {
      keys,
      values,
    };
  };

  render() {
    const { selectedSlice } = this.state;
    const { label } = selectedSlice;
    const colors = ["#F46F22", "#151C26", "#0FD8D8"];

    const {
      attendanceStatisticsLoading = false,
      attendanceStatistics = {},
      attendanceStatisticsError = null,
    } = this.props;

    const keys = this.getTrainigTypeDataLengths(attendanceStatistics || {})
      .keys;

    const values = this.getTrainigTypeDataLengths(attendanceStatistics || {})
      .values;

    const data = keys.map((key, index) => {
      return {
        key,
        value: values[index],
        key: `pie-${index}`,
        svg: { fill: colors[index] },
        arc: {
          outerRadius: 70 + values[index] + "%",
          padAngle: label === key ? 0.1 : 0,
        },
        onPress: () =>
          this.setState({
            selectedSlice: { label: key, value: values[index] },
          }),
      };
    });

    console.log("DAAATA: ", attendanceStatistics);

    const existDataList =
      attendanceStatistics.Beauty_count +
      attendanceStatistics.Functional_count +
      attendanceStatistics.Pilates_count;

    return (
      <ImageBackground
        style={{
          width: "100%",
          height: "100%",
        }}
        source={require("../../../assets/images/background/staticAndHistory.png")}
      >
        <SafeAreaView>
          <Header
            marginTopHeader={WINDOW_HEIGHT / 14}
            headerLeft={
              <TouchableOpacity
                style={{ paddingRight: 16 }}
                onPress={() => {
                  this.props.navigation.goBack();
                }}
              >
                <Icon
                  name="chevron-small-left"
                  size={fontSize34}
                  color="#151C26"
                  type="entypo"
                />
              </TouchableOpacity>
            }
            title={"Статистика и история посещений"}
          />
        </SafeAreaView>

        <ScrollView>
          <View style={{ flex: 1, alignItems: "center" }}>
            <View
              style={{
                height: 50,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Carousel
                shouldOptimizeUpdates={true}
                enableMomentum={true}
                activeSlideOffset={10}
                callbackOffsetMargin={20}
                ref={(c) => {
                  this._carousel = c;
                }}
                onBeforeSnapToItem={async (index) => {
                  console.log(index);
                  this.getCurrentDataBySelectedDate(index);
                }}
                inactiveSlideOpacity={0.6}
                itemHeight={40}
                data={monthNames}
                firstItem={this.currentMonth - 1}
                renderItem={this._renderItem}
                sliderWidth={400}
                itemWidth={100}
              />
            </View>

            {!attendanceStatisticsError &&
            (data.length || attendanceStatisticsLoading) ? (
              <SkeletonLoader
                containerStyle={{
                  flex: 1,
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                }}
                layout={[{ width: 147, height: 147, borderRadius: 147 }]}
                isLoading={attendanceStatisticsLoading}
              >
                <PieChart
                  style={{
                    flex: 1,
                    height: WINDOW_HEIGHT / 3,
                    width: "100%",
                    alignSelf: "center",
                  }}
                  outerRadius={"80%"}
                  innerRadius={"25%"}
                  data={data}
                  startAngle={0}
                />
              </SkeletonLoader>
            ) : !attendanceStatisticsError &&
              !attendanceStatisticsLoading &&
              !data.length ? (
              <View
                style={{
                  flex: 1,
                  height: WINDOW_HEIGHT / 6,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text
                  style={{
                    flex: 1,
                    width: "100%",
                    textAlign: "center",
                  }}
                >
                  Нет данных для отрисовки
                </Text>
              </View>
            ) : !attendanceStatisticsLoading && attendanceStatisticsError ? (
              <View
                style={{
                  flex: 1,
                  hight: WINDOW_HEIGHT / 6,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text
                  style={{
                    width: "100%",
                    textAlign: "center",
                  }}
                >
                  Ошибка получения данны
                </Text>
              </View>
            ) : null}

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                width: "90%",
                marginVertical: 16,
              }}
            >
              {Object.keys(attendanceStatistics).map((name, index) => {
                if (typeof attendanceStatistics[name] === "object") {
                  return (
                    <CustomTitle
                      key={`${name}-${index}`}
                      text={`${
                        name + " (" + attendanceStatistics[name].length + ")"
                      }`}
                      background={colors[index]}
                    />
                  );
                }
              })}
            </View>

            <View style={styles.containerList}>
              <View style={styles.measurementsOverview}>
                <View
                  style={{
                    paddingTop: 20,
                  }}
                >
                  <Text style={styles.measurementMonth}>
                    {monthNames[this.state.activeElement] +
                      " " +
                      moment().format("YYYY")}
                  </Text>

                  <View style={{ marginTop: 18, borderRadius: 10 }}>
                    <ScrollView
                      contentContainerStyle={{ paddingBottom: 120 }}
                      showsVerticalScrollIndicator={false}
                    >
                      {!attendanceStatisticsError && existDataList ? (
                        Object.keys(attendanceStatistics).map((name) => {
                          if (typeof attendanceStatistics[name] === "object") {
                            return attendanceStatistics[
                              name
                            ].map((item, index) => (
                              <VisitingListItem
                                key={`state-${index}`}
                                loading={attendanceStatisticsLoading}
                                {...item}
                              />
                            ));
                          }
                        })
                      ) : attendanceStatisticsError ? (
                        <View
                          style={{
                            flex: 1,
                            minHeight: Dimensions.get("screen").height / 2.8,
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <Text style={{ width: "100%", textAlign: "center" }}>
                            Ошибка получения данних
                          </Text>
                        </View>
                      ) : !existDataList ? (
                        <View
                          style={{
                            flex: 1,
                            minHeight: Dimensions.get("screen").height / 2.8,
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <Text style={{ width: "100%", textAlign: "center" }}>
                            Список пуст
                          </Text>
                        </View>
                      ) : null}
                    </ScrollView>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => ({
  attendanceStatisticsLoading: state.currentUser.attendanceStatisticsLoading,
  attendanceStatistics: state.currentUser.attendanceStatistics,
  attendanceStatisticsError: state.currentUser.attendanceStatisticsError,
});

export default connect(mapStateToProps, { getAttendanceStatistics })(
  VisitingHistory
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  title: {
    color: colors.title,
    ...fonts.normal,
    ...fonts.title,
    paddingVertical: 16,
  },
  containerList: {
    color: "#000",
    paddingHorizontal: 16,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowColor: colors.absoluteBlack,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.12,
    shadowRadius: 50,
    elevation: 6,
    height: 400,
    width: "100%",
    backgroundColor: "#FFF",
  },
  measurementMonth: {
    position: "relative",

    fontFamily: fonts.medium.fontFamily,
    fontSize: 18,
    lineHeight: 22,
    color: colors.mainBlack,
    letterSpacing: 0.14,
  },
});
