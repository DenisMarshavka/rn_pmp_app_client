import React, { useEffect, useState } from "react";
import {
  ImageBackground,
  StyleSheet,
  View,
  Animated,
  Dimensions,
} from "react-native";

import Spin from "../components/spin/Spin";
import { fonts } from "../constants/theme";

const companyName = ["Practice", "Makes", "Perfect"];

const LoadingScreen = () => {
  const [textAnimations, setTextAnimations] = useState({});
  let textAnimationTime = null;
  let textElementsAnimationTime = null;

  let logoAnimate = new Animated.Value(0);

  useEffect(() => {
    setTextAnimations(textAnimationCreate());

    return () => {
      clearTimeout(textAnimationTime);
      clearTimeout(textElementsAnimationTime);

      setTextAnimations({});
    };
  }, []);

  useEffect(() => {
    if (Object.keys(textAnimations).length && companyName.length) {
      textElementsAnimationTime = setTimeout(() => {
        logoAnimate.setValue(0);

        Animated.timing(logoAnimate, {
          toValue: 1,
          duration: 500,
          useNativeDriver: true,
        }).start();
      }, companyName.length * 600);
    }
  }, [textAnimations]);

  const textAnimationCreate = () => {
    const newElements = {};

    if (companyName && companyName.length) {
      for (let text of companyName) {
        newElements[text] = new Animated.Value(1);
      }
    }

    return newElements;
  };

  const playElementTextAnimation = (
    textElementIndex = "",
    elementIndex = 0
  ) => {
    if (Object.keys(textAnimations).length && textElementIndex.trim()) {
      textAnimationTime = setTimeout(() => {
        Animated.timing(textAnimations[textElementIndex], {
          toValue: 0,
          duration: 550,
        }).start();
      }, elementIndex * 600);
    }
  };

  const centerOfCircle = Dimensions.get("screen").width / 5;

  const translateX = logoAnimate.interpolate({
    inputRange: [0, 1],
    outputRange: [
      25,
      Dimensions.get("screen").width / 2 - centerOfCircle / 0.65,
    ],
  });

  return (
    <ImageBackground
      style={styles.container}
      source={require("../assets/images/splash_loader.png")}
    >
      <View
        style={{
          width: 215,
          top: -7,
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Animated.View
          style={{
            position: "relative",
            transform: [
              {
                translateX,
              },
            ],
          }}
        >
          <Spin />
        </Animated.View>

        {companyName &&
        companyName.length &&
        Object.keys(textAnimations).length ? (
          <View style={{ flex: 1 }}>
            {companyName.map((item, i) => {
              playElementTextAnimation(item, i);

              return (
                <Animated.Text
                  key={`title-item-${item}`}
                  style={[
                    fonts.bold,
                    styles.titleCompany,
                    {
                      // marginTop: i !== 0 ? 1 : 0,
                      opacity: textAnimations[item],
                    },
                  ]}
                >
                  {item}
                </Animated.Text>
              );
            })}
          </View>
        ) : null}
      </View>
    </ImageBackground>
  );
};

export default LoadingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  titleCompany: {
    top: 5,
    marginLeft: 39,
    fontFamily: "CompanyRegul",
    letterSpacing: 1,

    color: "#000",
    textTransform: "uppercase",
    fontSize: Math.ceil(Dimensions.get("screen").width / 16),
  },
});
