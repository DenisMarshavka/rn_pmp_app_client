import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import Svg, { Path } from "react-native-svg";
import { connect } from "react-redux";

import colors from "../../styles/colors";
import { getAppInstruction } from "./../../actions/InstructionsActions";
import BoardingsContainer from "../../hoc/Auth/BoardingsContainer";

const arrowImage = (
  <Svg
    width="55"
    height="30"
    viewBox="0 0 55 30"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <Path
      d="M2.32138 23.6807C2.14501 24.0554 2.30586 24.5022 2.68065 24.6786L8.78817 27.5527C9.16296 27.7291 9.60976 27.5683 9.78614 27.1935C9.96251 26.8187 9.80166 26.3719 9.42687 26.1955L3.99796 23.6407L6.55274 18.2118C6.72911 17.837 6.56826 17.3902 6.19348 17.2139C5.81868 17.0375 5.37188 17.1983 5.19551 17.5731L2.32138 23.6807ZM52.746 5.29433L2.74596 23.2943L3.25404 24.7057L53.254 6.70567L52.746 5.29433Z"
      fill="#151C26"
    />
  </Svg>
);

// const boardingsMock = [
//   {
//     headText: "Lorem ipsum dolor sit amet, consectetur. \n Разделы:",
//     imgSrc: require("../../assets/images/boardings/board_1.png"),
//     arrowPosition: [190, 70],
//     transformStyles: {
//       transform: [{ rotate: "60deg" }],
//     },
//     arrowDescription: "Lorem ipsum dolor sit amet",
//     arrowDescriptionPosition: [240, 105],
//   },
//   {
//     headText: "Lorem ipsum dolor sit amet, consectetur. \n Разделы:",
//     imgSrc: require("../../assets/images/boardings/board_2.png"),
//     arrowPosition: [180, 135],
//     arrowDescription: "Lorem ipsum dolor sit amet ",
//     arrowDescriptionPosition: [245, 105],
//   },
//   {
//     headText: "Lorem ipsum dolor sit amet, consectetur. \n Разделы:",
//     imgSrc: require("../../assets/images/boardings/board_3.png"),
//     arrowPosition: [180, 80],
//     arrowDescription: "Lorem ipsum dolor sit amet ",
//     arrowDescriptionPosition: [240, 50],
//   },
//   {
//     headText: "Lorem ipsum dolor sit amet, consectetur. \n Разделы:",
//     imgSrc: require("../../assets/images/boardings/board_4.png"),
//     arrowPosition: [185, 145],
//     arrowDescription: "Lorem ipsum dolor sit amet ",
//     arrowDescriptionPosition: [245, 115],
//   },
//   {
//     headText: "Lorem ipsum dolor sit amet, consectetur. \n Разделы:",
//     imgSrc: require("../../assets/images/boardings/board_5.png"),
//     arrowPosition: [170, 200],
//     arrowDescription: "Lorem ipsum dolor sit amet ",
//     arrowDescriptionPosition: [230, 170],
//   },
//
//   {
//     headText: "Lorem ipsum dolor sit amet, consectetur. \n Разделы:",
//     imgSrc: require("../../assets/images/boardings/board_5.png"),
//     arrowPosition: [170, 225],
//     arrowDescription: "Lorem ipsum dolor sit amet ",
//     arrowDescriptionPosition: [230, 200],
//   },
//   {
//     headText: "Lorem ipsum dolor sit amet, consectetur. \n Разделы:",
//     imgSrc: require("../../assets/images/boardings/board_5.png"),
//     arrowPosition: [170, 250],
//     arrowDescription: "Lorem ipsum dolor sit amet ",
//     arrowDescriptionPosition: [230, 225],
//   },
//   {
//     headText: "Lorem ipsum dolor sit amet, consectetur. \n Разделы:",
//     imgSrc: require("../../assets/images/boardings/board_5.png"),
//     arrowPosition: [170, 280],
//     arrowDescription: "Lorem ipsum dolor sit amet ",
//     arrowDescriptionPosition: [230, 250],
//   },
//   {
//     headText: "Lorem ipsum dolor sit amet, consectetur. \n Разделы:",
//     imgSrc: require("../../assets/images/boardings/board_5.png"),
//     arrowPosition: [170, 305],
//     arrowDescription: "Lorem ipsum dolor sit amet ",
//     arrowDescriptionPosition: [230, 275],
//   },
//   {
//     headText: "Lorem ipsum dolor sit amet, consectetur. \n Разделы:",
//     imgSrc: require("../../assets/images/boardings/board_5.png"),
//     arrowPosition: [170, 335],
//     arrowDescription: "Lorem ipsum dolor sit amet ",
//     arrowDescriptionPosition: [230, 305],
//   },
// ];

const OnBoardings = ({
  navigation,
  route,

  getAppInstruction = () => {},
  appInstruction = [],
  appInstructionLoading = true,
  appInstructionFailed = null,
}) => {
  const [boardings, setBoargins] = useState([]);

  const generateBoardingsNormalData = (boardingsData = []) => {
    const newData = [];

    if (boardingsData && boardingsData.length) {
      for (let itemBoard of boardingsData) {
        if (
          itemBoard &&
          itemBoard.app_instructions_items &&
          itemBoard.app_instructions_items.length
        ) {
          for (let item of itemBoard.app_instructions_items) {
            if (item && item.json && Object.keys(item.json).length)
              newData.push(item.json);
          }
        }
      }
    }

    // console.log("New Boardings Data: ", newData);
    return newData;
  };

  const { moveEndScreen = "Home" } =
    (route &&
      typeof route === "object" &&
      typeof route.params === "object" &&
      route.params) ||
    {};

  useEffect(() => {
    getAppInstruction();
  }, []);

  useEffect(() => {
    if (!appInstructionLoading && appInstruction)
      setBoargins(generateBoardingsNormalData(appInstruction));
  }, [appInstructionLoading, appInstruction]);

  // console.log("App Instruction DATA: ", appInstruction);

  return (
    <View style={{ flex: 1, paddingBottom: 75 }}>
      {appInstructionLoading ? (
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            minHeight: Dimensions.get("screen").height / 1.2,
            paddingBottom: 23,
          }}
        >
          <ActivityIndicator size={"large"} color={colors.mainOrange} />
        </View>
      ) : !appInstructionFailed && boardings && boardings.length ? (
        <BoardingsContainer
          buttonStyle={{
            alignSelf: "center",
            width: "100%",
            maxWidth: "90%",
          }}
          boardings={boardings}
          colorDots={colors.mainGrey}
          colorDotsActive={colors.mainOrange}
          startIndexBoard={0}
          buttonText={"ДАЛЕЕ"}
          illustrationStyle={{ height: 450, width: 225 }}
          animateMoveDuration={350}
          cursor={true}
          cursorImage={arrowImage}
          skipText={"Или пропустить"}
          onEnd={() => navigation.navigate(moveEndScreen)}
        />
      ) : !appInstructionFailed && boardings && !boardings.length ? (
        <View
          style={{
            flex: 1,
            height: Dimensions.get("screen").height / 2.5,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text
            style={{
              width: "100%",
              textAlign: "center",
              color: "#000",
            }}
          >
            {
              "Cписок пуст,\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27."
            }
          </Text>
        </View>
      ) : (
        <View
          style={{
            flex: 1,
            height: Dimensions.get("screen").height / 1.2,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text
            style={{
              width: "100%",
              textAlign: "center",
              color: "#000",
            }}
          >
            {
              "Произошла ошибка.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27."
            }
          </Text>
        </View>
      )}

      {!appInstructionLoading &&
      ((!appInstructionFailed && boardings && !boardings.length) ||
        appInstructionFailed) ? (
        <TouchableOpacity onPress={() => navigation.navigate(moveEndScreen)}>
          <Text
            style={{
              marginTop: 17,

              color: colors.mainBlack,
              fontSize: 14,
              textDecorationLine: "underline",
              opacity: 0.55,

              alignSelf: "center",
            }}
          >
            Пропустить
          </Text>
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const mapStateToProps = (state) => ({
  appInstructionLoading: state.InstructionsReducer.appInstructionLoading,
  appInstruction: state.InstructionsReducer.appInstruction,
  appInstructionFailed: state.InstructionsReducer.appInstructionFailed,
});

export default connect(mapStateToProps, { getAppInstruction })(OnBoardings);
