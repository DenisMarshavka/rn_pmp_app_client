import React, { useState } from "react";
import { connect } from "react-redux";
import Constants from "expo-constants";
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Button,
  Text,
  View,
  AsyncStorage,
  Keyboard,
} from "react-native";

import { ToastView, SpinnerView } from "../../components";
const width = Math.round(Dimensions.get("window").width);
const height = Math.round(Dimensions.get("window").height);
//
import { UserAPI } from "../../api/user";
import { setCurrentUser } from "../../actions";

const RegAuthScreen = ({ navigation, scene, previous, setCurrentUser }) => {
  const [_isLoading, _setLoading] = useState(false);
  const [_showLogin, _setShowLogin] = useState(true);
  const [_confirmFocus, _setConfirmFocus] = useState(false);

  const handleSubmitRegForm = (values, resetForm) => {
    _setLoading(true);

    try {
      UserAPI.register(values).then((response) => {
        console.log("REGISTER - ", response);

        if (response.access_token !== undefined) {
          AsyncStorage.setItem("user", JSON.stringify(response)).then((res) => {
            setCurrentUser(response);

            ToastView("Благодарим Вас за регистрацию");

            _setLoading(false);
          });
        }
      });
    } catch (e) {
      console.log("UserAPI.register | ", e);

      ToastView(
        "К сожалению, произошла ошибка.\nОбратитесь в поддержку по телефону +7 495 797 94 27"
      );

      resetForm();
      _setLoading(false);
    }
  };

  // const handleSubmitLoginForm = async (values, resetForm) => {
  //   _setLoading(true);
  //
  //   try {
  //     UserAPI.login(values)
  //       .then(response => {
  //         console.log('LOGIN - ',response);
  //
  //         if(response.access_token !== undefined) {
  //           AsyncStorage.setItem('user', JSON.stringify(response)).then(res => {
  //             setCurrentUser(response);
  //
  //             ToastView('Добро пожаловать');
  //
  //             _setLoading(false);
  //           });
  //         }
  //       });
  //   } catch (e) {
  //     resetForm();
  //
  //     ToastView('Ошибка, попробуйте позже');
  //     _setLoading(false);
  //   }
  // };

  const handleTabsToggle = () => {
    Keyboard.dismiss();

    _setShowLogin((prevState) => !prevState);
  };

  return (
    <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
      <ImageBackground
        source={require("../../assets/images/splash_loading.png")}
        style={styles.ImageBackground}
      >
        <ScrollView
          style={[styles.scrollView, _confirmFocus && styles.paddingOnFocus]}
        >
          {_isLoading && <SpinnerView />}

          <View style={styles.main}>
            <View style={styles.selectsContainer}>
              <TouchableOpacity
                style={styles.selectBtn}
                activeOpacity={0.7}
                onPress={handleTabsToggle}
              >
                <Text
                  style={
                    _showLogin
                      ? styles.selectBtnTextActive
                      : styles.selectBtnText
                  }
                >
                  Авторизация
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.selectBtn}
                activeOpacity={0.7}
                onPress={handleTabsToggle}
              >
                <Text
                  style={
                    _showLogin
                      ? styles.selectBtnText
                      : styles.selectBtnTextActive
                  }
                >
                  Регистрация
                </Text>
              </TouchableOpacity>
            </View>

            {/*<View style={styles.textCenter}>
              { _showLogin
                ?
                  <LoginForm
                    onHandleSubmitLogForm={handleSubmitLoginForm}
                  />
                :
                  <RegisterForm
                    onHandleSubmitRegForm={handleSubmitRegForm}
                    setConfirmFocus={_setConfirmFocus}
                  />
              }
            </View>*/}
          </View>
        </ScrollView>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentUser: (user) => {
      dispatch(setCurrentUser(user));
    },
  };
};

export default connect(null, mapDispatchToProps)(RegAuthScreen);

const styles = StyleSheet.create({
  statusBar: {
    height: Constants.statusBarHeight,
  },
  container: {
    backgroundColor: "#009740",
    flex: 1,
  },
  ImageBackground: {
    width: "100%",
    height: "105%",
  },
  paddingOnFocus: {
    paddingTop: width / 7,
  },
  scrollView: {
    flex: 1,
    paddingTop: width / 3,
  },
  main: {
    flex: 1,
    zIndex: 1,
  },
  logoBlock: {
    flex: 3,
    justifyContent: "center",
    alignItems: "center",
  },
  formBlock: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  textCenter: {
    flex: 1,
    alignItems: "center",
  },
  load: {
    width: 42,
    height: 40,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  registerText: {
    fontSize: 10,
    color: "#fff",
    flex: 1,
  },
  registerTextWrap: {
    marginTop: 12,
    alignItems: "center",
    flex: 0.1,
  },
  selectsContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "flex-start",
    flexDirection: "row",
    marginBottom: 44,
  },
  selectBtn: {
    backgroundColor: "transparent",
    marginHorizontal: 8,
  },
  selectBtnText: {
    color: "#fff",
    fontSize: 20,
    fontFamily: "PFDin400",
    opacity: 0.6,
  },
  selectBtnTextActive: {
    color: "#fff",
    fontFamily: "PFDin400",
    fontSize: 20,
    paddingBottom: 4,
    borderBottomWidth: 1.5,
    borderBottomColor: "#fff",
  },
});
