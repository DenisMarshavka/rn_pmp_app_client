import React from "react";
import {
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import * as Animatable from "react-native-animatable";

import Header from "../components/Headers/Header";
import { Background } from "../components/Background";
import { RowsListItem } from "../components/RowsListItem";
import { getSizeSkeletonLoaders } from "../utils";
import { useRoute } from "@react-navigation/native";

// TODO: Replaced the list component to new optimizated list component
const DefaultRowsListScreen = ({
  itemToScreenNameMove = "Program",
  navigation,
  title = "",
  isHamburger = true,
  onHamburgerOpen = () => navigation.navigate("MainMenu"),
  isLoading = false,
  isProgram = false,
  dataList = [],
  basketIconStyle = {},
  headerTitleStyle = {},
  basketShow = false,
  requestFailed = false,
  isProgramsList = false,
}) => {
  const route = useRoute();

  const dataListRows =
    isLoading && !requestFailed
      ? getSizeSkeletonLoaders(
          100,
          Dimensions.get("window").width - 16,
          false,
          false
        )
      : !isLoading && dataList && dataList.length
      ? dataList
      : [];

  // console.log(
  //   "dataListdataList",
  //   dataList,
  //   "itemToScreenNameMove",
  //   itemToScreenNameMove
  // );

  const Wrap = !isLoading ? Animatable.View : View;

  return (
    <Background>
      <View style={{ paddingTop: 5 }}>
        <Header
          styleHeadTitle={headerTitleStyle}
          backTitle={true}
          title={title}
          basketIconStyle={basketIconStyle}
          basketShow={basketShow}
          routeName={route.name}
        />

        {!requestFailed && (dataListRows.length || isLoading) ? (
          <ScrollView
            contentContainerStyle={{ paddingBottom: 65 }}
            style={{ paddingTop: 20, paddingHorizontal: 16 }}
            showsVerticalScrollIndicator={false}
          >
            {dataList.map((item, index) => {
              // console.log("IIITEEEEM", item);

              if (item.program || !isProgram) {
                item.name =
                  isProgram && item.program && item.program.title
                    ? item.program.title
                    : item.title;

                return (
                  <Wrap
                    duration={350}
                    delay={index * 350}
                    animation="fadeInUp"
                    key={`touch-${item.id || Date.now() + index}`}
                  >
                    <TouchableOpacity
                      activeOpacity={0.7}
                      onPress={() => {
                        navigation.navigate(itemToScreenNameMove, {
                          programName: item.name || `Программа ${item.id}`,
                          programId:
                            item && item.program && item.program.id
                              ? item.program.id
                              : item && item.id
                              ? item.id
                              : 1,
                          item,
                        });
                      }}
                    >
                      <RowsListItem
                        isProgramsItem={isProgramsList}
                        key={`item-${item.id}`}
                        isLoading={isLoading}
                        title={item.name || `Программа ${item.id}`}
                        day={item.day}
                      />
                    </TouchableOpacity>
                  </Wrap>
                );
              }
            })}
          </ScrollView>
        ) : !requestFailed && !dataListRows.length ? (
          <View style={styles.fullFill}>
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Список пуст
            </Text>
          </View>
        ) : requestFailed ? (
          <View style={styles.fullFill}>
            <Text
              style={{
                width: "100%",
                textAlign: "center",
                color: "#000",
              }}
            >
              Ошибка получения данных
            </Text>
          </View>
        ) : null}
      </View>
    </Background>
  );
};

export default DefaultRowsListScreen;

const styles = StyleSheet.create({
  fullFill: {
    flex: 1,
    minHeight: Dimensions.get("window").height / 1.28,
    alignItems: "center",
    justifyContent: "center",
  },
});
