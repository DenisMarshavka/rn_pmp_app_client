import React from 'react';
import { Dimensions, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {Header} from "../components";
import {Background} from "../components/Background"
import {RowsListItem} from "../components/RowsListItem"
import {getSizeSkeletonLoaders} from "../utils";

const DefaultRowsListScreen = ({ itemToScreenNameMove = "Program", navigation, title = '', isHamburger = true, onHamburgerOpen = () => navigation.navigate("MainMenu"), isLoading = false, dataList = [], requestFailed = false, isProgramsList = false}) => {
    const dataListRows = isLoading && !requestFailed ? getSizeSkeletonLoaders() : !isLoading && dataList && dataList.length ? dataList : [];

    return (
        <Background>
            <View style={{paddingTop: 10}}>
                <Header
                    title={title}
                    onHamburgerOpen={onHamburgerOpen}
                    isHamburger={isHamburger}
                />

                {
                    !requestFailed ?

                        <ScrollView
                            contentContainerStyle={{paddingBottom: 65}}
                            style={{paddingTop: 20, paddingHorizontal: 16}}
                            showsVerticalScrollIndicator={false}
                        >
                            {
                                dataListRows.map(item => (
                                    <TouchableOpacity
                                        activeOpacity={0.7}
                                        onPress={() => {
                                            navigation.navigate(itemToScreenNameMove, {
                                                    programName: item.name || `Программа ${item.id}`,
                                                    id: item.id,
                                                    item,
                                                }
                                            )
                                        }}
                                        key={`touch-${item.id}`}
                                    >
                                        <RowsListItem
                                            isProgramsItem={isProgramsList}
                                            key={`item-${item.id}`}
                                            isLoading={isLoading}
                                            title={item.name || `Программа ${item.id}`}
                                            day={item.day}
                                        />
                                    </TouchableOpacity>
                                ))
                            }
                        </ScrollView> : !requestFailed && dataListRows && !dataListRows.length && !isLoading ?

                        <View style={styles.fullFill}>
                            <Text style={{
                                width: '100%',
                                textAlign: 'center',
                                color: '#000',
                            }}>Список пуст </Text>
                        </View> : requestFailed ?

                            <View style={styles.fullFill}>
                                <Text style={{
                                    width: '100%',
                                    textAlign: 'center',
                                    color: '#000',
                                }}>Ошибка получения данных</Text>
                            </View> :

                            null
                }
            </View>
        </Background>
    );
}

export default DefaultRowsListScreen;

const styles = StyleSheet.create({
    fullFill: {
        flex: 1,
        minHeight: Dimensions.get('window').height / 1.28,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
