import React from "react";
import { connect } from "react-redux";
import {
  AsyncStorage,
  View,
  Text,
  Easing,
  ImageBackground,
  ActivityIndicator,
  KeyboardAvoidingView,
  Image,
  TouchableOpacity,
  LayoutAnimation,
  Keyboard,
  ScrollView,
  TouchableWithoutFeedback,
  Dimensions,
  SafeAreaView,
} from "react-native";
import NumberFormat from "react-number-format";
import * as Animatable from "react-native-animatable";
import Animated from "react-native-reanimated";
import Constants from "expo-constants";

import Colors from "../constants/Colors";
import { ToastView } from "../components";
import { UserAPI } from "../api/user";
import { ApplicationsAPI } from "../api/applications";
import { setCurrentUser, getPushToken, setPushToken } from "../actions";
import { modeSmsCode } from "./Client/Confirm";
import {
  registerForPushNotifications,
  generatePushToken,
  formatPhone,
} from "../utils";
import CheckBoxInput from "../components/checkboxinput/CheckBoxInput";
import { Background } from "../components/Background";
import ButtonInput from "../components/button/ButtonInput";
import AuthInput from "../components/input/AuthInput.tsx";
import {
  colors,
  fonts,
  fontSize16,
  fontSize14,
  fontSize36,
} from "../constants/theme";

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      isLogin: false,
      phoneNumber: "",
      password: "",
      repass: "",
      isValidatePhoneNumber: true,
      isValidatePassword: true,
      isValidateRepass: true,
      isEqualityPassword: true,
      female: true,
      isFetching: false,
    };
  }

  checkPhoneNumber = (phone) => /^[0-9]{11}$/.test(phone.replace(/[^\d]/g, ""));

  checkPassword = (password) => /(.{6,})/.test(password);

  setUserData = (userData = {}, pmpToken = "", callBack = () => {}) => {
    console.log("USSSERRR DDDAAAAAAATA LOGIN SCREEN", userData);

    if (userData.token !== undefined && userData.user.id) {
      UserAPI.getUserById(userData.user.id).then((result) => {
        console.log("LOGIN DATA USER - ", result, "PMP TOKEN - ", pmpToken);

        if (pmpToken && pmpToken.trim()) result.token = pmpToken;

        AsyncStorage.setItem("user", JSON.stringify(result)).then((res) => {
          this.props.setCurrentUser(result);
          console.log("SETED DATA USER - ", result, result);

          callBack();
        });
      });
    } else
      ToastView(
        "Системная ошибка!\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27"
      );
  };

  spin() {
    this.spinValue.setValue(0);

    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: 500,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => this.spin());
  }

  async login() {
    const { phoneNumber, password } = this.state;
    const isCheckPhoneNumber = this.checkPhoneNumber(phoneNumber);
    const isCheckPassword = this.checkPassword(password);

    this.setState({
      isValidatePhoneNumber: isCheckPhoneNumber,
      isValidatePassword: isCheckPassword,
    });

    if (isCheckPhoneNumber && isCheckPassword)
      this.setState({ isFetching: true });

    // if (!isCheckPhoneNumber || !isCheckPassword) return null;
    // alert(`Phone: ${phoneNumber}, Password: ${password}`);
    // this.props.navigation.navigate('ConfirmSMS', {mode: Constants.LOGIN_CONFIRM, phone: phoneNumber});

    // console.log("Constants.deviceId", Constants.deviceId);

    let push_token = "";
    if (Constants.isDevice) push_token = await generatePushToken();
    console.log("LOGIN PUSH TOKEN: ", push_token);

    UserAPI.login({
      login: phoneNumber.replace(/ /g, ""),
      password,
      device_id: Constants.deviceId,
      push_token,
    })
      .then((response) => {
        // if (response) {
        // }
        console.log("LOGIN - ", response, response.data.user.id);

        this.setUserData(response.data, response.data.token, () => {
          this.setState({ isFetching: false });

          ToastView("Добро пожаловать");
        });
      })
      .catch((e) => {
        this.setState({ isFetching: false });
        console.log("errror LOGIN --- ", e);

        ToastView(
          "Данные введены неверно!\n Пожалуйста проверьте их, и Повторите попытку"
        );
      });
  }

  async register() {
    const { phoneNumber, password, repass } = this.state;
    const isCheckPhoneNumber = this.checkPhoneNumber(phoneNumber);
    const isCheckPassword = this.checkPassword(password);
    const isCheckRepass = this.checkPassword(repass);

    this.setState({
      isValidatePhoneNumber: isCheckPhoneNumber,
      isValidatePassword: isCheckPassword,
      isValidateRepass: isCheckRepass,
      isEqualityPassword: repass === password,
    });

    if (
      !isCheckPhoneNumber ||
      !isCheckPassword ||
      !isCheckRepass ||
      repass !== password
    ) {
      return null;
    } else this.setState({ isFetching: true });

    // alert(`Phone: ${phoneNumber}, Password: ${password}, Check: ${repass}`);
    // this.props.navigation.navigate('ConfirmSMS', {mode: Constants.REGISTER_CONFIRM, phone: phoneNumber});

    try {
      let push_token = "";
      push_token = await generatePushToken();
      console.warn("REGISTER PUSH TOKEN: ", push_token);

      // if (!!this.props.pushToken) {
      //   setPushToken(this.props.pushToken);
      // } else
      //   ToastView(
      //     "Ооо нет :( Случилось что-то непридвиденное,\n Обратитесь пожалуйста в техподдержку!"
      //   );

      // console.log("this.props.pushToken", this.props.pushToken);

      const response = await UserAPI.register({
        login: phoneNumber.replace(/ /g, ""),
        password,
        device_id: Constants.deviceId,
        push_token,
        female: this.state.female,
      });
      console.log("REGISTER - ", response);

      if (
        response &&
        response.msg &&
        response.status &&
        response.status === "error" &&
        response.msg === "Пользователь с этим логиной уже зарегистрирован"
      ) {
        this.setState({ isFetching: false });
        ToastView(response.msg);

        return;
      }

      if (response && response.data && response.data.token) {
        this.setUserData(response.data, response.data.token, () => {
          this.setState({ isFetching: false });
          ToastView("Благодарим Вас за регистрацию");

          this.props.navigation.navigate("OnBoardings");
        });
      }

      if (response.msg && response.status && response.status === "error") {
        this.setState({ isFetching: false });
        ToastView(response.msg);
      }
    } catch (e) {
      this.setState({ isFetching: false });
      console.log("UserAPI.register | ", e);

      ToastView(
        "К сожалению, произошла ошибка.\nОбратитесь в поддержку по телефону +7 495 797 94 27"
      );
    }
  }

  recoverPassword = async () => {
    let { phoneNumber } = this.state;
    const isValidatePhoneNumber = this.checkPhoneNumber(phoneNumber);

    if (isValidatePhoneNumber) {
      try {
        const responseExistingUser = await ApplicationsAPI.checkExistingUserByNumberPhone(
          phoneNumber.replace(/\s+/g, "")
        );

        console.log(
          "EXISTING",
          responseExistingUser,
          "phoneNumber",
          phoneNumber.replace(/\s+/g, "")
        );

        if (
          responseExistingUser &&
          responseExistingUser.status &&
          responseExistingUser.status === "success"
        ) {
          this.props.navigation.navigate(
            "ConfirmSmsCode" /* "ConfirmPassword"*/,
            {
              mode: "",
              phone: phoneNumber,
            }
          );
        } else
          ToastView("Пользователь с таким номером телефна не найден! :(", 2500);
      } catch (e) {
        console.log("ApplicationsAPI.checkExistingUserByNumberPhone | ", e);

        ToastView(
          "Ооо нет :( Случилось что-то непридвиденное.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27.",
          2000
        );
      }
    }

    this.setState({ isValidatePhoneNumber });
  };

  onPhoneNoChange = (newData) => {
    this.setState({ phoneNumber: newData });
  };

  onPasswordChange = (newData) => {
    this.setState({ password: newData });
  };

  onRepassChange = (newData) => {
    this.setState({ repass: newData });
  };

  inputChangedHandler = (values) => {
    this.setState({
      userInput: values,
    });
  };

  logoViewRef = (ref) => (this.logo = ref);
  formViewRef = (ref) => (this.form = ref);
  firsInputViewRef = (ref) => (this.firsInputViewRef = ref);

  showLogo = () => {
    this.logo.fadeInUp(500).then((endState) => {
      if (endState.finished) {
      } else {
      }
    });

    this.form.fadeInUpBig(800).then((endState) => {
      // console.log(endState)
      if (endState.finished) {
      }
    });
  };

  componentDidMount() {
    this.showLogo();

    console.log("Login Screen Navigation Data: ", this.props);
  }

  handleTabsToggle = (status) => {
    Keyboard.dismiss();
    Keyboard.dismiss();

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    this.setState({
      isLogin: status,
      phoneNumber: "",
      password: "",
      repass: "",
      isValidatePhoneNumber: true,
      isValidatePassword: true,
      isValidateRepass: true,
      isEqualityPassword: true,
    });
  };

  render() {
    const {
      isLogin,
      isValidatePhoneNumber,
      isValidatePassword,
      isValidateRepass,
      isEqualityPassword,
    } = this.state;

    return (
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={this.state.isLogin ? "padding" : "margin"}
      >
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <SafeAreaView style={{ flex: 1 }}>
            <ImageBackground
              style={{
                ...styles.container,
              }}
              source={require("../assets/images/splash_loader.png")}
            >
              <Animatable.View
                ref={this.logoViewRef}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  flex: 1,
                  minHeight: this.state.isLogin
                    ? "auto"
                    : Dimensions.get("screen").height / 5.5,
                }}
              >
                <Image
                  source={require("../assets/images/login_icon.png")}
                  style={{
                    maxWidth: 183,
                    maxHeight: 98,
                    display: this.state.isLogin ? "flex" : "none",
                  }}
                />
              </Animatable.View>

              <Animatable.View
                animation="fadeInUpBig"
                duration={800}
                ref={this.formViewRef}
                delay={310}
                style={styles.signForm}
              >
                <View style={styles.mode}>
                  <TouchableOpacity
                    style={[
                      isLogin === true
                        ? styles.modeSelection
                        : styles.modeDeselection,
                    ]}
                    onPress={() => this.handleTabsToggle(true)}
                  >
                    <Text
                      style={[
                        isLogin === true
                          ? styles.modeSelectionText
                          : styles.modeDeselectionText,
                      ]}
                    >
                      Авторизация
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[
                      isLogin !== true
                        ? styles.modeSelection
                        : styles.modeDeselection,
                    ]}
                    onPress={() => this.handleTabsToggle(false)}
                  >
                    <Text
                      style={[
                        isLogin !== true
                          ? styles.modeSelectionText
                          : styles.modeDeselectionText,
                      ]}
                    >
                      Регистрация
                    </Text>
                  </TouchableOpacity>
                </View>

                {this.state.isFetching ? (
                  <View
                    style={{
                      flex: 1,
                      justifyContent: "center",
                      alignItems: "center",
                      maxHeight: Dimensions.get("screen").height / 2.8,
                    }}
                  >
                    <ActivityIndicator size="large" color={colors.orange} />
                  </View>
                ) : isLogin === true ? (
                  <SafeAreaView>
                    <View>
                      <Animatable.View
                        duration={700}
                        delay={500}
                        animation="fadeInUp"
                        style={{ marginTop: 20 }}
                      >
                        <AuthInput
                          value={this.state.phoneNumber}
                          isValidate={isValidatePhoneNumber}
                          validateText="Введите корректный номер телефона"
                          inputType="numeric"
                          title="Введите, пожалуйста, номер телефона"
                          maxLength={16}
                          onChangeText={(phone) => {
                            if (phone && phone.trim()) {
                              this.setState({ isValidatePhoneNumber: true });

                              this.setState({
                                phoneNumber: formatPhone(phone),
                              });
                            } else
                              this.setState({ isValidatePhoneNumber: true });
                          }}
                        />
                      </Animatable.View>

                      <Animatable.View
                        duration={700}
                        delay={550}
                        animation="fadeInUp"
                        style={{ marginTop: 20 }}
                      >
                        <AuthInput
                          isValidate={isValidatePassword}
                          validateText={"Введите корректный пароль"}
                          onlySecureText
                          style={{ flex: 1 }}
                          title="Введите, пожалуйста, пароль"
                          onChangeText={(password) => {
                            this.onPasswordChange(password);
                          }}
                        >
                          <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => {
                              this.recoverPassword();
                            }}
                          >
                            <Text style={styles.forgetPassword}>
                              Забыли пароль?
                            </Text>
                          </TouchableOpacity>
                        </AuthInput>
                      </Animatable.View>

                      <Animatable.View
                        duration={700}
                        delay={350}
                        animation="fadeInUp"
                        style={{ marginTop: 30, marginBottom: 30 }}
                      >
                        <ButtonInput
                          title="Продолжить"
                          onClick={() => {
                            this.login();
                            Keyboard.dismiss();
                          }}
                        />
                      </Animatable.View>
                    </View>
                  </SafeAreaView>
                ) : (
                  <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 150 }}
                  >
                    <SafeAreaView>
                      <Animatable.View
                        duration={700}
                        delay={500}
                        animation="fadeInUp"
                        style={{ marginTop: 20 }}
                      >
                        <AuthInput
                          value={this.state.phoneNumber}
                          isValidate={isValidatePhoneNumber}
                          validateText="Введите корректный номер телефона"
                          inputType="numeric"
                          title="Введите, пожалуйста, номер телефона"
                          maxLength={16}
                          onChangeText={(phone) =>
                            this.setState({
                              phoneNumber: formatPhone(phone),
                            })
                          }
                        />
                      </Animatable.View>

                      <Animatable.View
                        duration={700}
                        delay={550}
                        animation="fadeInUp"
                        style={{ marginTop: 20 }}
                      >
                        <AuthInput
                          isValidate={isValidatePassword && isEqualityPassword}
                          validateText={
                            !isEqualityPassword
                              ? "Пароли должны совпадать"
                              : "Пароль должен быть формата: Qwerty123"
                          }
                          secure
                          style={{ flex: 1, paddingRight: 10 }}
                          title="Введите, пожалуйста, пароль"
                          onChangeText={(password) => {
                            this.onPasswordChange(password);
                          }}
                        ></AuthInput>
                      </Animatable.View>

                      <Animatable.View
                        duration={700}
                        delay={600}
                        animation="fadeInUp"
                        style={{ marginTop: 20 }}
                      >
                        <AuthInput
                          isValidate={isValidateRepass && isEqualityPassword}
                          validateText={
                            !isEqualityPassword
                              ? "Пароли должны совпадать"
                              : "Пароль должен быть формата: Qwerty123"
                          }
                          secure
                          style={{ flex: 1, paddingRight: 10 }}
                          title="Повторите, пожалуйста, пароль"
                          onChangeText={(password) => {
                            this.onRepassChange(password);
                          }}
                        ></AuthInput>
                      </Animatable.View>

                      <Animatable.View
                        duration={500}
                        delay={350}
                        animation="fadeInUp"
                        style={{ marginTop: 20 }}
                      >
                        <Text style={{ fontSize: 16 }}>Пол</Text>

                        <View style={{ marginTop: 25 }}>
                          <CheckBoxInput
                            title={"Мужчина"}
                            style={{ marginLeft: 16 }}
                            checked={!this.state.female}
                            onPress={() => this.setState({ female: false })}
                          />

                          <View style={{ marginTop: 24 }}>
                            <CheckBoxInput
                              title={"Женщина"}
                              style={{ marginLeft: 16 }}
                              checked={this.state.female}
                              onPress={() => this.setState({ female: true })}
                            />
                          </View>
                        </View>
                      </Animatable.View>

                      <View style={{ marginTop: 30, marginBottom: 30 }}>
                        <ButtonInput
                          title="Продолжить"
                          onClick={() => {
                            this.register();
                            Keyboard.dismiss();
                          }}
                        />
                      </View>
                    </SafeAreaView>
                  </ScrollView>
                )}
              </Animatable.View>
            </ImageBackground>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
}

// const mapStateToProps = (state) => {
//   return {
//     pushToken: state.currentUser.pushToken,
//   };
// };

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentUser: (user) => {
      dispatch(setCurrentUser(user));
    },
  };
};

export default connect(null, mapDispatchToProps)(LoginScreen);

const styles = {
  container: {
    flex: 1,
    alignItems: "center",
  },
  loading: {
    width: 97,
    height: 97,
  },
  signForm: {
    paddingTop: 27,
    paddingLeft: 19,
    paddingRight: 19,
    backgroundColor: "white",
    borderRadius: 10,
  },
  mode: {
    // margin: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 19,
  },
  modeSelection: {
    padding: 10,
    opacity: 1,
    width: "50%",
    borderRadius: 10,
    backgroundColor: colors.auth_gray,
  },
  modeSelectionText: {
    color: colors.orange,
    textAlign: "center",
    fontSize: fontSize16,
    fontFamily: fonts.medium.fontFamily,
  },
  modeDeselection: {
    // opacity: 0.3,
    padding: 10,
    width: "50%",
    borderRadius: 10,
  },
  modeDeselectionText: {
    textAlign: "center",
    fontSize: fontSize16,
    fontFamily: fonts.medium.fontFamily,
    color: colors.title,
    opacity: 0.3,
  },
  inputBack: {
    marginTop: 20,
    padding: 20,
    borderRadius: 10,
    backgroundColor: Colors.gray,
  },
  inputBack_diraction: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    flex: 1,
  },
  mobile: {
    fontSize: fontSize16,
    fontFamily: fonts.medium.fontFamily,
  },
  password: {
    // width: '60%',
    fontSize: fontSize16,
    fontFamily: fonts.medium.fontFamily,
  },
  forgetPassword: {
    alignSelf: "flex-end",
    color: "#0FD8D8",
    fontSize: fontSize14,
    fontFamily: fonts.medium.fontFamily,
  },
  passwordVisiblity: {},
  button: {},
};
