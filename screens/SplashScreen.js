import React from 'react';
import { Image, ImageBackground, StyleSheet } from 'react-native';

const SplashScreen = () => {
    return (
        <ImageBackground style={styles.container} source={require('../assets/images/splash_loader.png')}>
            <Image source={require('../assets/images/splashImage.png')} />
        </ImageBackground>
    )
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
  })

export default SplashScreen;