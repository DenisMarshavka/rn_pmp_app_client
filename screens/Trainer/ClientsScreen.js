import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const ClientsScreen = () => (
    <View style={styles.container}>
      <Text>Мой клиенты</Text>
    </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default ClientsScreen;
