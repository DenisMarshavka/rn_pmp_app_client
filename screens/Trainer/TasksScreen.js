import React from "react";
import moment from "moment";
import _ from "lodash";
import { Divider } from "react-native-elements";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { TextMedium14, TextMedium } from "../assets/ui/AppText";

const fakeProps = {
  date: moment("2019-12-21"),
  tasks: [
    {
      start: moment("2019-12-21 09:00"),
      end: moment("2019-12-21 11:00"),
      description: "Персональная тренировка",
    },
  ],
};

// console.log(fakeProps.tasks);

const isAncestry = (task, interval) => {
  const { start } = task;
  const intervalEnd = interval.clone().add(1, "h");
  return start.isBetween(interval, intervalEnd, null, "[)");
};

const buildData = (date) => {
  const result = [];
  for (let i = 0; i < 25; i += 1) {
    const current = date.clone().add(i, "h");
    result.push(current);
  }
  return result;
};

const processTask = (task, index, interval) => {
  const { start, end, description } = task;
  const duration = end.diff(start, "minutes");
  const height = (duration / 60) * 56;
  const offsetInMinutes = start.diff(interval, "minutes");
  const offset = ((offsetInMinutes + 30) / 60 + index) * 56;
  return isAncestry(task, interval) ? { offset, height, description } : null;
};

const renderTask = ({ offset, height, description }) => (
  <TouchableOpacity
    style={[styles.task, { height, top: offset }]}
    key={_.uniqueId()}
  >
    <View style={[styles.wrapper]}>
      <TextMedium>{description}</TextMedium>
    </View>
  </TouchableOpacity>
);

const TasksScreen = ({ navigation }) => {
  const { date, tasks } = fakeProps;
  const data = buildData(date);

  return (
    <View style={styles.container}>
      <FlatList
        style={{ width: "100%", position: "relative" }}
        data={data}
        renderItem={({ item, index }) => {
          const intervalTasks = tasks.filter((task) => isAncestry(task, item));
          const processedTasks = intervalTasks.map((task, index) =>
            processTask(task, index, item)
          );

          return (
            <View style={styles.item}>
              {processedTasks.map(renderTask)}

              <Text style={{ paddingLeft: 20 }}>{item.format("HH:mm")}</Text>

              <Divider style={styles.divider} />
            </View>
          );
        }}
        keyExtractor={() =>
          `task-item-${item.description}-${index}-${_.uniqueId()}`
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  item: {
    flexDirection: "row",
    alignItems: "center",
    height: 56,
  },
  divider: {
    width: "100%",
    marginLeft: 17,
    backgroundColor: "#cecdcd",
  },
  task: {
    position: "absolute",
    backgroundColor: "#f46f22",
    borderRadius: 8,
    right: 0,
    width: "80%",
    zIndex: 2,
  },
  wrapper: {
    width: "100%",
    height: "100%",
  },
});

export default TasksScreen;
