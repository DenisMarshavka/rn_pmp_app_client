import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const TimetableScreen = () => (
    <View style={styles.container}>
      <Text>Расписание</Text>
    </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default TimetableScreen;
