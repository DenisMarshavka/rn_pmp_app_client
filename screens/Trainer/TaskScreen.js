import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
import bg from "../assets/backgrounds/TaskScreenBg.png";
import { TextMedium, TextLight } from "../assets/ui/AppText";
import AppBackground from "../assets/ui/AppBackground";
import { AppButtonBack } from "../assets/ui/AppButtons";

const fakeProps = {
  name: "Lorem ipsum",
  date: "21.12.2019",
  begin: "15:00",
  end: "16:00",
  comment:
    "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet lorem ipsum. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet lorem ipsum. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet lorem ipsum. ipsum dolor sit amet lorem ipsum. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet lorem ipsum. ",
  executor: null,
  state: null,
};

export default function Task() {
  const {
    name,
    date,
    begin,
    end,
    comment,
    executor,
    state,
    navigation,
  } = fakeProps;

  const data = [
    ["Название", name, "value"],
    ["Дата", date, "value"],
    ["Время", `${begin}- ${end}`, "value"],
    ["Комментарий", comment, "comment"],
    ["Исполнитель", executor, "value"],
    ["Статус", state, "value"],
  ];

  return (
    <AppBackground source={bg} style={styles.container}>
      <AppButtonBack navigation={navigation} />

      <TextMedium style={styles.header}>Задача {name}</TextMedium>

      <FlatList
        data={data}
        renderItem={({ item }) => {
          const [title, value, type] = item;
          const isComment = type === "comment";

          return (
            value && (
              <View
                style={[styles.item, !isComment && { flexDirection: "row" }]}
              >
                <TextLight style={styles.title}>{title}</TextLight>

                <TextLight
                  style={[styles.value, isComment && { paddingLeft: 0 }]}
                >
                  {value}
                </TextLight>
              </View>
            )
          );
        }}
        keyExtractor={([title]) => title}
      />
    </AppBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    paddingHorizontal: 16,
  },
  header: {
    fontSize: 20,
    alignSelf: "center",
    marginBottom: 32,
  },
  title: {
    color: "#F46F22",
    flex: 2,
  },
  value: {
    paddingLeft: 10,
    textAlign: "left",
    flex: 5,
  },
  item: {
    paddingVertical: 16,
  },
});
