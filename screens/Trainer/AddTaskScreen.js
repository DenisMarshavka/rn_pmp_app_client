import React from 'react';
import { Text, View, ImageBackground } from 'react-native';
import styled from 'styled-components/native';
import SvgUri from 'react-native-svg-uri';
import bg from '../assets/backgrounds/AddTaskScreenBg.png';
import statistic from '../assets/icons/statistics.svg';
import AppBackground from '../assets/ui/AppBackground';

const TaskScreen = () => (
    <AppBackground source={bg}>
      <Header>
        <View></View>
        <HeaderDesc>Создать задачу</HeaderDesc>
      </Header>
    </AppBackground>
);

const Header = styled.View`
  width: 100%;
  height: 50px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
const HeaderDesc = styled.Text`
  font-size: 26px;
`;


export default TaskScreen;
