import React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  ImageBackground,
  Platform,
} from "react-native";
import { DATA } from "../data";
import { Post } from "../components/Post";

export const MainScreen = () => {
  return (
    <ImageBackground
      style={styles.image}
      source={require("../../assets/bg1.png")}
      resizeMode="stretch"
    >
      <View style={styles.container}>
        <FlatList
          data={DATA}
          keyExtractor={(post) => post.id.toString()}
          renderItem={({ item }) => <Post post={item} />}
        />
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    marginTop: Platform.OS === "ios" ? 88 : 50,
  },
  image: {
    width: "100%",
    height: "100%",
  },
});
