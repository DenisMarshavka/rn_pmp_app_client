import React, { Component } from "react";
import {
  View,
  TextInput,
  Text,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
} from "react-native";
import { Formik } from "formik";
import Icon from "react-native-vector-icons/AntDesign";

import { notificationManager } from "../services/NotificationManager";
import { THEME } from "../theme";

export default class PushScreen extends Component {
  constructor(props) {
    super(props);
    this.localNotify = null;
  }

  componentDidMount() {
    this.localNotify = notificationManager;
    this.localNotify.configure(
      this.onRegister,
      this.onNotification,
      this.onOpenNotification
    );
  }

  onRegister(token) {
    console.log("[Notification] Register: ", token);
  }

  onNotification(notify) {
    console.log("[Notification] onNotification : ", notify);
  }

  onOpenNotification(notify) {
    console.log("[Notification] onOpenNotification : ", notify);
    alert("Open Notification");
  }

  onPressCancelNotification = () => {
    this.localNotify.cancelAllLocalNotification();
  };

  onPressSendNotification = () => {
    const options = {
      soundName: "default",
      playSound: true,
      vibrate: true,
    };
    this.localNotify.showNotification(
      1,
      "App Notification",
      "local Notification",
      {}, //data
      options //options
    );
  };

  render() {
    return (
      <ImageBackground
        style={styles.image}
        source={require("../../assets/push-bg.png")}
      >
        <Formik
          initialValues={{ title: "", body: "" }}
          validate={(values) => {
            const errors = {};
            if (!values.title) {
              errors.title = "Обязательно";
            } else if (values.title.length < 4) {
              errors.title = "Введите больше 3 символов";
            }

            if (!values.body) {
              errors.body = "Обязательно";
            } else if (values.body.length < 11) {
              errors.body = "Введите больше 10 символов";
            }

            return errors;
          }}
          onSubmit={(values, actions) => {
            console.log("values", values);
            //this.onPressSendNotification();
            this.localNotify.showNotification(
              1,
              values.title,
              values.body,
              {}, //data
              {
                soundName: "default",
                playSound: true,
                vibrate: true,
              } //options
            );
            actions.resetForm();
          }}
        >
          {({
            values,
            errors,
            touched,
            handleBlur,
            handleChange,
            handleSubmit,
          }) => (
            <>
              <ScrollView>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                  <View style={styles.container}>
                    <View style={styles.header}>
                      <Text style={styles.label}>Кому: </Text>

                      <TextInput
                        style={styles.textInput}
                        placeholder="Введите имя или фамилию"
                        multiline
                        value={values.title}
                        onChangeText={handleChange("title")}
                        onBlur={handleBlur("title")}
                      />

                      <TouchableOpacity
                        onPress={() => console.log("press")}
                        style={styles.create}
                      >
                        <Icon size={18} name="plus" color="rgba(0,0,0,0.54)" />
                      </TouchableOpacity>
                    </View>

                    <View style={styles.line} />
                    {errors.title && touched.title ? (
                      <Text style={styles.errorText}>{errors.title}</Text>
                    ) : null}
                    <View style={styles.header}>
                      <Text style={styles.label}>Текст: </Text>

                      <TextInput
                        style={styles.textInput}
                        placeholder="Введите описание для пуш уведомления"
                        multiline
                        value={values.body}
                        onChangeText={handleChange("body")}
                        onBlur={handleBlur("body")}
                      />
                    </View>

                    {errors.body && touched.body ? (
                      <Text style={styles.errorText}>{errors.body}</Text>
                    ) : null}
                  </View>
                </TouchableWithoutFeedback>
              </ScrollView>

              <TouchableOpacity
                onPress={handleSubmit}
                style={styles.button}
                activeOpacity={0.8}
              >
                <Text style={styles.text}>Отправить</Text>
              </TouchableOpacity>
            </>
          )}
        </Formik>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 199,
    backgroundColor: "rgba(241, 241, 241, 0.5)",
    marginTop: Platform.OS === "ios" ? 88 : 50,
  },
  image: {
    width: "100%",
    height: "100%",
  },
  header: {
    padding: 16,
    position: "relative",
    flexDirection: "row",
  },
  textInput: {
    marginRight: "auto",
    overflow: "hidden",
    width: "80%",
  },
  label: {
    fontSize: 18,
    color: THEME.TEXT_COLOR,
    fontFamily: "PFDinTextCompPro-Bold",
    marginTop: 5,
    marginRight: 10,
    letterSpacing: -0.41,
  },
  create: {
    marginTop: 5,
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "rgba(0, 0, 0, 0.12)",
  },
  button: {
    backgroundColor: THEME.MAIN_COLOR,
    width: 343,
    height: 52,
    borderRadius: 10,
    shadowColor: THEME.TEXT_COLOR,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 1,
    marginLeft: "auto",
    marginRight: "auto",
    marginBottom: 31,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "#fff",
    fontFamily: "PFDinTextCompPro-Bold",
    textTransform: "uppercase",
    fontSize: 14,
    lineHeight: 57,
  },
  errorText: {
    fontFamily: "PFDinTextCompPro-Medium",
    color: THEME.DANGER_COLOR,
    marginLeft: 16,
    marginTop: 5,
    marginBottom: 5,
  },
});
