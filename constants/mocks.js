const studios = [
  {
    id: "1",
    image: require("../assets/images/studio1.png")
  },
  {
    id: "2",
    image: require("../assets/images/studio2.png")
  }
];

const exercises = [
  {
    id: "1",
    title: "Персональная тренировка 90 минут",
    price: "6700"
  },
  {
    id: "2",
    title: "Персональная тренировка 55 минут",
    price: "5300"
  },
  {
    id: "3",
    title: "Team-тренировка 55 минут (2 участника)",
    price: "3600"
  },
  {
    id: "4",
    title: "Пакет на 5 тренировок в течении 60 дней",
    price: "25 000"
  },
  {
    id: "5",
    title: "Пакет на 10 тренировок в течении 120 дней",
    price: "48 000"
  },
  {
    id: "6",
    title: "Пакет на 20 тренировок в течении 120 дней",
    price: "48 000"
  },
  {
    id: "7",
    title: "Team-тренировка 55 минут (2 участника)",
    price: "3600"
  },
  {
    id: "8",
    title: "Пакет на 5 тренировок в течении 60 дней",
    price: "25 000"
  },
  {
    id: "9",
    title: "Пакет на 20 тренировок в течении 120 дней",
    price: "48 000"
  }
];

const services = [
  {
    id: "1",
    title: "Персональная тренировка 90 минут",
    price: "6700",
    point: "7",
    img: require("../assets/images/fitness-studio1.png")
  },
  {
    id: "2",
    title: "Персональная тренировка 55 минут",
    price: "5300",
    point: "5",
    img: require("../assets/images/fitness-studio2.png")
  },
  {
    id: "3",
    title: "Team-тренировка 55 минут (2 участника)",
    price: "3600",
    point: "10",
    img: require("../assets/images/fitness-studio1.png")
  }
];

const specialists = [
  {
    id: "1",
    name: "Денис Сычёв",
    profile: "Владелец и основатель бренда",
    iconImg: require("../assets/images/icons/fitness-icon.png")
  },
  {
    id: "2",
    name: "Ксения Кулешова",
    profile: "Практикующий тренер по методу Пила...",
    iconImg: require("../assets/images/icons/fitness-icon.png")
  },
  {
    id: "3",
    name: "Юрий Гавриш",
    profile: "Фитнес-директор сети",
    iconImg: require("../assets/images/icons/fitness-icon.png")
  },
  {
    id: "4",
    name: "Татьяна Юркина",
    profile: "Сертифицированный инструктор Pilate..",
    iconImg: require("../assets/images/icons/fitness-icon.png")
  },
  {
    id: "5",
    name: "Мила Яковлева",
    profile: "CrossFit Level 1 Trainer Certificate",
    iconImg: require("../assets/images/icons/fitness-icon.png")
  }
];

const menuListData = [
  {
    id: 1,
    picture: require('../assets/images/gymPic.png'),
    name: 'Ягоды',
    category: 'Мороженое',
    price: '110',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
  {
    id: 2,
    picture: require('../assets/images/gymPic.png'),
    name: 'Фруктово-ягодный смузи',
    category: 'Напитки',
    price: '1000',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
  {
    id: 3,
    picture: require('../assets/images/gymPic.png'),
    name: 'Рис',
    category: 'Каши',
    price: '120',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt' +
      ' placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
  {
    id: 4,
    picture: require('../assets/images/gymPic.png'),
    name: 'Лимонад',
    category: 'Напитки',
    price: '70',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt' +
      ' placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
  {
    id: 5,
    picture: require('../assets/images/gymPic.png'),
    name: 'Кампот яблочный',
    category: 'Напитки',
    price: '1000',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt' +
      ' placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
  {
    id: 6,
    picture: require('../assets/images/gymPic.png'),
    name: 'Курица',
    category: 'Мясо',
    price: '121',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt' +
      ' placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
  {
    id: 7,
    picture: require('../assets/images/gymPic.png'),
    name: 'Жаркое',
    category: 'Мясо',
    price: '230',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt' +
      ' placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
  {
    id: 8,
    picture: require('../assets/images/gymPic.png'),
    name: 'Фруктово-ягодный смузи',
    category: 'Напитки',
    price: '1000',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt' +
      ' placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
  {
    id: 9,
    picture: require('../assets/images/gymPic.png'),
    name: 'Фруктово-ягодный смузи',
    category: 'Напитки',
    price: '1000',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt' +
      ' placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
  {
    id: 10,
    picture: require('../assets/images/gymPic.png'),
    name: 'Фруктово-ягодный смузи',
    category: 'Напитки',
    price: '1000',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt' +
      ' placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
  {
    id: 11,
    picture: require('../assets/images/gymPic.png'),
    name: 'Фруктово-ягодный смузи',
    category: 'Напитки',
    price: '1000',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt' +
      ' placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
  {
    id: 12,
    picture: require('../assets/images/gymPic.png'),
    name: 'Фруктово-ягодный смузи',
    category: 'Напитки',
    price: '1000',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt' +
      ' placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?',
    ingredients: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis dignissimos iste nesciunt ' +
      'placeat, sunt! Alias consectetur corporis distinctio dolorem doloremque explicabo, facilis ipsum natus odit omnis qui quidem ut?'
  },
];

export { studios, exercises, services, specialists, menuListData };
