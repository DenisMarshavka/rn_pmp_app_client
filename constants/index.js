import * as mocks from "./mocks";
import * as theme from "./theme";

export const BASE_URL = "https://back.pmp.aumagency.ru:3003";

export { theme, mocks };
