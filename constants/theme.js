import { Dimensions } from 'react-native';

export const WINDOW_WIDTH = Dimensions.get('window').width;
export const WINDOW_HEIGHT = Dimensions.get('window').height;
export const HEADER_HEIGHT = 50;
export const ROW_ICONS_HEIGHT = WINDOW_HEIGHT / 18;
export const fontSize12 = WINDOW_WIDTH / 34;
export const fontSize14 = WINDOW_WIDTH / 30;
export const fontSize16 = WINDOW_WIDTH / 26;
export const fontSize18 = WINDOW_WIDTH / 23;
export const fontSize20 = WINDOW_WIDTH / 21;
export const fontSize24 = WINDOW_WIDTH / 18;
export const fontSize28 = WINDOW_WIDTH / 15;
export const fontSize32 = WINDOW_WIDTH / 13.5;
export const fontSize34 = WINDOW_WIDTH / 13;
export const fontSize36 = WINDOW_WIDTH / 12;

const colors = {
  orange: "#F46F22",
  white: "#FFFFFF",
  black: "#FAFAFA",
  title: "#151C26",
  text: "#2A3138",

  gray: "#DADADA",
  auth_gray: 'rgba(21, 28, 38, 0.06)',

  swiper_active: "#0FD8D8",
  swiper_inactive: "#000000"
};

const sizes = {
  // global sizes
  base: 16,
  font: 14,
  radius: 10,
  margin: 16,
  opcity: 0.38,

  // font sizes
  h1: 34,
  h2: 18,
  h3: 16,
  navigation: 13,
  header: 16,
  text: 16,

  headerTitle: 28,
  headerSubTitle: 24,
  headerName: 18,
  description: 14
};

const fonts = {
  h1: {
    fontSize: sizes.h1
  },
  h2: {
    fontSize: sizes.h2
  },
  h3: {
    fontSize: sizes.h3
  },
  description: {
    fontSize: sizes.description
  },
  header: {
    fontSize: sizes.header
  },
  title: {
    fontSize: sizes.headerTitle
  },
  navigation: {
    fontSize: sizes.navigation
  },
  text: {
    fontSize: sizes.text
  },
  upper: {
    textTransform: "uppercase"
  },
  normal: {
    fontStyle: "normal",
    fontFamily: "PFDin400"
  },
  bold: {
    fontStyle: "normal",
    fontFamily: "PFDin700"
  },
  medium: {
    fontStyle: "normal",
    fontFamily: "PFDin500"
  },
  light: {
    fontFamily: "PFDin300"
  },
  thin: {
    fontFamily: "PFDin200"
  }
};

export { colors, sizes, fonts };
