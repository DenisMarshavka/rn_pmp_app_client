import API from "../../utils/APIConnector";

const Request = new API();

export class ServicesAPI {
  static getAllPurchasedTrainerServices = async (
    isOnline = false,
    typeName = "",
    offset = 0,
    limit = 15,
    order = "desc"
  ) =>
    await Request.GET(
      `/service/trainer-services/get-all/purchased/by-type?type=${typeName}&isOnline=${isOnline}&offset=${offset}&limit=${limit}&order=desc`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static getAllServiceTypes = async () =>
    await Request.GET(
      "/service/trainer-service-types/get-all",
      {},
      false,
      false,
      "pmp",
      true
    );

  static getServiceByType = async (typeName = "", offset = 0, limit = 10) =>
    await Request.GET(
      `/service/trainer-services/get-all/by-type?type=${typeName.trim()}&offset=${offset}$limit=${limit}`,
      {},
      false,
      false,
      "pmp",
      true
    );

  static getAllExpertServices = async (offset = 0, limit = 10) =>
    await Request.GET(
      `/service/expert-services/get-all?offset=${offset}&limit=${limit}`,
      {},
      false,
      false,
      "pmp",
      true
    );

  static getServicesByCompanyId = async (company_id) =>
    await Request.GET(`/services/${company_id}`);

  static getServicesByCompanyAndServiceId = async (company_id, service_id) =>
    await Request.GET(`/services/${company_id}/${service_id}`);

  // static getTrainersByServiceId = async (staffId, byExpert = false) => await Request.GET(`/${ byExpert ? 'expert' : 'trainer' }/get-by-id/${staffId}`, {}, false, false, 'pmp', true);
}
