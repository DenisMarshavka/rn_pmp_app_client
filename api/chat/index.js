import API from '../../utils/APIConnector';
import config from '../../utils/config';

const Request = new API();
const TOKEN = config.token;

export class ChatAPI {
  static sendMessage = async (props) => await Request.POST('/chat/send-message', {...props}, false, false, 'pmp');
  static getDialogMessages = async (props) => await Request.POST('/chat/get-dialog-messages', {...props}, false, false, 'pmp');
}
