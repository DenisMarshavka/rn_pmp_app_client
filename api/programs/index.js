import API from "../../utils/APIConnector";

const Request = new API();

export class ProgramsAPI {
  static getRecommendedDishes = async (dayId) =>
    await Request.GET(
      `/program/recommended-dishes/get-by/day-id/${dayId}`,
      {},
      false,
      false,
      "pmp"
    );

  static getPrograms = async (offset, limit, order) =>
    await Request.GET(
      `/program/section/get-all?offset=${offset}&limit=${limit}&order=${order}`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static getUserPrograms = async (id = 321) =>
    await Request.GET(
      `/program/info/by-user/${id}`,
      {},
      false,
      false,
      "pmp",
      true
    ).then((response) => response.data);

  static getProgramById = async (id) =>
    await Request.GET(
      `/program/get-single/${id}`,
      {},
      false,
      false,
      "pmp",
      true
    );

  static getStepInfoByProgramId = async (id) =>
    await Request.GET(
      `/program/section/get-by-id/${id}`,
      {},
      false,
      false,
      "pmp",
      true
    );

  static getProgramsByUserId = async (userId) =>
    await Request.GET(
      `/program/info/by-user/${userId}`,
      {},
      true,
      true,
      "pmp",
      true
    );
}
