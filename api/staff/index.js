import API from "../../utils/APIConnector";

const Request = new API();

export class StaffAPI {
  static getStaffs = async (
    isExpert = false,
    offset = 0,
    limit = 15,
    order = "desc"
  ) =>
    await Request.GET(
      `/${
        isExpert ? "expert" : "trainer"
      }/get-all?offset=${offset}&limit=${limit}&order=${order}`,
      {},
      false,
      false,
      "pmp",
      true
    );

  static getLessonsByTrainerId = async (
    trainerId,
    offset = 0,
    limit = 15,
    order = "desc"
  ) =>
    await Request.GET(
      `/trainer/lesson/get-all/by-trainer-id/${trainerId}/?offset=${offset}&limit=${limit}&order=${order}`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static getStaffSchedule = async (
    company_id,
    staff_id,
    start_date,
    end_date
  ) => {
    const url = `/schedule/${company_id}/${staff_id}/${start_date}/${end_date}`;

    return await Request.GET(url, {}, true, true, "yclients", true);
  };

  static getStaffByServiceId = async (staffId, byExpert = false) =>
    await Request.GET(
      `/${byExpert ? "expert" : "trainer"}/get-by-id/${staffId}`,
      {},
      false,
      false,
      "pmp",
      true
    );
}
