import API from '../../../utils/APIConnector';
const Request = new API();


export class WeightAPI {
  static getWeightsByDate = async (date) => await Request.GET(`/diary/weight/get-all/${date}`, {}, true, true, 'pmp', true);
  static addWeight = async (data) => await Request.POST(`/diary/weight/add`, {...data}, true, true, 'pmp', true)
};
