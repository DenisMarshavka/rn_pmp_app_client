import API from "../../../utils/APIConnector";
const Request = new API();

export class DiaryFoodAPI {
  static getDiaryFoodsByDate = async (date) =>
    await Request.GET(
      `/diary/food/get-all/${date}`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static addDish = async (data) =>
    await Request.POST(`/diary/food/add`, { ...data }, true, true, "pmp", true);

  static getDefaultFoods = async (offset, limit, order = "asc") =>
    await Request.GET(
      `/diary/food/get-dishes?offset=${offset}&limit=${limit}&order=${order}`,
      {},
      true,
      true,
      "pmp",
      true
    );
}
