import API from '../../../utils/APIConnector';
const Request = new API();


export class BodyAPI {
  static getBodiesByDate = async (date) => await Request.GET(`/diary/body/get-all/${date}`, {}, true, true, 'pmp', true);
  static addBody = async (data) => await Request.POST(`/diary/body/add`, {...data}, true, true, 'pmp', true)
};
