import API from "../../utils/APIConnector";
import config from "../../utils/config";

const Request = new API();
const TOKEN = config.token;

export class CertificatesAPI {
  static getAll = async () =>
    await Request.GET("/certificates/get-all", {}, false, false, "pmp", true);
  static getById = async (id) =>
    await Request.GET(
      `/certificates/get-by-id/${id}`,
      {},
      false,
      false,
      "pmp",
      true
    );

  static getNominalsByCertificateId = async (id) =>
    await Request.GET(`/certificates/get-by-id/${id}`, true, false);
}
