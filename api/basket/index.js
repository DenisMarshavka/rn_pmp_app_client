import API from "../../utils/APIConnector";

const Request = new API();

export class BasketAPI {
  static add = async (props) =>
    await Request.PUT("/baskets/add", { ...props }, true, true, "pmp", true);

  static getHistory = async (offset, limit, orderData) =>
    await Request.GET(
      `/orders?offset=${offset}&limit=${limit}&order=${orderData}&paid=true`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static remove = async (props) =>
    await Request.POST(
      "/baskets/remove/item",
      { ...props },
      true,
      true,
      "pmp",
      true
    );

  static getCurrentState = async (offset, limit, orderData, body = {}) =>
    await Request.GET(
      `/baskets?offset=${offset}&limit=${limit}&order=${orderData}`,
      { ...body },
      true,
      true,
      "pmp",
      true
    );
}
