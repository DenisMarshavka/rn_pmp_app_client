import API from "./../../../utils/APIConnector";

const Request = new API();

export class ReviewsAPI {
  static getAllReviews = async (userId, limit, offset, order = "desc") =>
    await Request.GET(
      `/reviews/get-all-for-current-user?offset=${offset}&limit=${limit}&order=${order}`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static getAllCompanyReviews = async (offset, limit, order = "desc") =>
    await Request.GET(
      `/reviews/company?offset=${offset}&limit=${limit}&order=${order}`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static createReview = async (props) =>
    await Request.POST("/review", { ...props }, false, true, "pmp", true);

  static deleteReview = async (reviewId) =>
    await Request.DELETE(`/review/${reviewId}`, {}, true, true, "pmp", true);

  static getCompanyReviews = async (offset = 0, limit = 15) =>
    await Request.GET(
      `/reviews/get-all?offset=${offset}&limit=${limit}`,
      {},
      true,
      true,
      "pmp",
      true
    );
}
