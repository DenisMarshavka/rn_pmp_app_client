import API from "../../utils/APIConnector";
import { AsyncStorage } from "react-native";
import axios from "axios/index";
import { getDataRequestStart } from "../../utils";

const Request = new API();
const BASE_URL = "https://back.pmp.aumagency.ru:3003";

function generateUrlInvitesAndProductsRequest(
  url = "",
  // isSpecial = false,
  // isNew = false,
  offset = 0,
  limit = 15
) {
  // if (isNew || isSpecial || limit) {
  let i = 0;
  let argsIndex = 0;
  const args = Array.from(arguments).slice(1, arguments.length);

  console.log("args", args);

  for (let item of args) {
    if (i !== args.length) {
      url += i !== 0 ? "&" : "?";

      // if (i === 0) url += `isSpecial=${isSpecial}`;
      // if (i === 1) url += `isNew=${isNew}`;
      if (i === 0) url += `offset=${offset}`;
      if (i === 1) url += `limit=${limit}`;

      i++;
    }

    argsIndex++;
  }
  // }

  console.log("Generated URL: ", url);

  return url;
}

export class StoreAPI {
  static getProductsCategories = async () =>
    await Request.GET(
      "/category-product/get-all",
      {},
      false,
      false,
      "pmp",
      true
    );

  static getNewUrlOrderPayment = async (id) =>
    Request.GET(
      `/orders/create-order-copy/by-id/${id}`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static getInvitesByParams = async (isNew = false, limit = 0) => {
    let url = generateUrlInvitesAndProductsRequest(
      "/product/get-all",
      // true,
      // isNew,
      limit
    );

    // return await Request.GET(url, {}, false, false, 'pmp', true);
    return getDataRequestStart(`${BASE_URL + url}`);
  };

  static getProductsByParams = async (
    isNew = false,
    offset = 0,
    limit = 15
  ) => {
    let url = generateUrlInvitesAndProductsRequest(
      "/product/get-all",
      // false,
      // isNew,
      offset,
      limit
    );

    return getDataRequestStart(`${BASE_URL + url}`);
  };

  static getTopProducts = async () =>
    await Request.GET(
      "/recommendations/get/current-top-products",
      {},
      false,
      false,
      "pmp",
      true
    );

  static createPreOrder = async () =>
    await Request.POST(
      "/orders/pre-orders/create",
      {},
      true,
      true,
      "pmp",
      true
    );
}
