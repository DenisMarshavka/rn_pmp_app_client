import API from "../../utils/APIConnector";
import config from "../../utils/config";

const Request = new API();

export class InstructionsAPI {
  static getAppInstruction = async (limit = 25, offset = 0, order = "desc") =>
    await Request.GET(
      `/app-instructions/instruction/get-all/?offset=${offset}&limit=${limit}&order=${order}`,
      {},
      false,
      false,
      "pmp",
      false
    );
}
