import axios from "axios";

const login = "veaceslav_c";
const password = "sb782841";
const message = "Введите код для восстановления";

export class SmsModule {
  static smsRequest = async (phone, smsCode) => {
    const body = {
      url: `https://smsc.ru/sys/send.php?login=${login}&psw=${password}&phones=${phone}&mes=${message} ${smsCode}`,
      headers: {},
      method: "GET",
    };
    return axios(body)
      .then((response) => {
        console.log("\n response ", response);
        return response.data;
      })
      .catch((error) => {
        console.log("====================================");
        console.log("axios.error", error);
        return { status: "error" };
      });
  };

  static generateRandomSmsCode() {
    return Math.random().toString(10).substring(2, 6);
  }
}
