import API from "../../utils/APIConnector";
import config from "../../utils/config";

const Request = new API();
const TOKEN = config.token;

export class SalonsAPI {
  static getSalons = async () => await Request.GET("/groups", {}, true, true);
}
