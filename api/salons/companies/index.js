import API from "../../utils/APIConnector";
import config from "../../utils/config";

const Request = new API();

export class CompaniesAPI {
  static getCompaniesByNetworkId = async (props) =>
    await Request.GET(`/companies?group_id=${props.group_id}`);
}
