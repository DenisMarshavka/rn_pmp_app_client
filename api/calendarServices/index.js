import API from "../../utils/APIConnector";
import config from "../../utils/config";

const Request = new API();

export class CalendarServicesAPI {
  static setNotificationApi = async (data) =>
    await Request.POST(`/push/add`, { ...data }, true, true, "pmp", true);
}
