import API from "../../utils/APIConnector";

const Request = new API();

export class TrainerAPI {
  static getAll = async () =>
    await Request.GET("/trainer/get-all", {}, false, false, "pmp", true);

  static getTrainerById = async (triunerId) =>
    await Request.GET(
      `/trainer/get-by-id/${triunerId}`,
      {},
      false,
      false,
      "pmp"
    );

  static getTrainersByServiceId = async (serviceId, isExpert = false) => {
    return await Request.GET(
      `/${!isExpert ? "trainers" : "experts"}/${
        !isExpert ? "by-trainer-service-id" : "by-expert-service-id"
      }/${serviceId}`,
      {},
      false,
      false,
      "pmp",
      true
    );
  };
}
