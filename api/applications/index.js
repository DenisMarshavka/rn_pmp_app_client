import API from "../../utils/APIConnector";
import config from "../../utils/config";

const Request = new API();
const TOKEN = config.token;

export class ApplicationsAPI {
  static getApplicationForDate = async (
    date,
    offset = 0,
    limit = 15,
    order = "desc"
  ) =>
    await Request.GET(
      `/applications/get-for-date/${date}?offset=${offset}&limit=${limit}&order=${order}`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static checkExistingUserByNumberPhone = async (phoneNumber) =>
    await Request.GET(`/user/is-existing-user/verify-by/${phoneNumber}`);

  static setError = async (props) =>
    await Request.POST("/logging", { ...props });

  static addApplication = async (props) =>
    await Request.POST(
      "/applications/add",
      { ...props },
      false,
      false,
      "pmp",
      true
    );
  //
  static generatePaymentLink = async (/*type = "", props*/) =>
    await Request.POST(
      `/orders`,
      {
        /*...props */
      },
      false,
      false,
      "pmp",
      true
    );

  static getFastCheckout = async (params) =>
    await Request.POST(
      "/orders/fast-check-out",
      { ...params },
      false,
      false,
      "pmp",
      true
    );

  // static generatePaymentLink = async (type = "", props) => {
  //   return await Request.POST(
  //     `/payment/${type}`,
  //     { ...props },
  //     false,
  //     false,
  //     "pmp",
  //     true
  //   );
  // };

  static getTrainingsBalance = async () =>
    await Request.GET("/applications/balance", {}, true, true, "pmp", true);

  static getTrainingsBalanceOnline = async (userId) =>
    await Request.GET(
      `/applications_online/balance/${userId}`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static getInfoToConstantlyClient = async () =>
    await Request.GET(
      "/applications/constantly-client",
      {},
      true,
      true,
      "pmp",
      true
    );

  // static getInfoToOnlineConstantlyClient = async (userId) =>
  //   await Request.GET(
  //     `/applications_online/constantly-client/${userId}`,
  //     {},
  //     true,
  //     true,
  //     "pmp",
  //     true
  //   );

  static getAboutUsInfo = async () =>
    await Request.GET("/about-us/get-latest", {}, true, true, "pmp", true);
}
