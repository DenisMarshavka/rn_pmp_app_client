import API from "../../utils/APIConnector";

const Request = new API();

export class StudiosAPI {
  static getStudiosByServiceId = async (
    serviceId,
    byExpert = false,
    offset,
    limit
  ) => {
    return await Request.GET(
      `/service/studios/${
        byExpert ? "by-expert-service-id" : "by-trainer-service-id"
      }/${serviceId}?offset=${offset}&limit=${limit}`,
      {},
      false,
      false,
      "pmp",
      true
    );
  };
}
