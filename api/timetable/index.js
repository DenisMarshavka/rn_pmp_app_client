import API from "../../utils/APIConnector";

const Request = new API();

export class TimetableAPI {
  static getTimetableByCompanyIdAndStaffId = async (
    company_id,
    staff_id,
    date
  ) => {
    const query = `/timetable/seances/${company_id}/${staff_id}/${date}`;

    return await Request.GET(query, {}, true, true, "yclients");
  };
}
