import { AsyncStorage } from "react-native";

import API from "../../utils/APIConnector";
import { getUserStorageData } from "../../utils";
import { BASE_URL } from "../../constants";
import config from "../../utils/config";
import axios from "axios";

const Request = new API();
const TOKEN = config.token;

export class UserAPI {
  static getUserDataPurchasedPrograms = async (userId) =>
    await Request.GET(
      `/program/info/by-user/${userId}`,
      {},
      false,
      false,
      "pmp",
      false
    );

  static register = async (props) =>
    await Request.POST("/register", { ...props }, false, false, "pmp");

  static updateUserAvatar = async (avatarImgForm) => {
    const userData = await getUserStorageData();
    let response = null;

    if (userData && userData.token && avatarImgForm) {
      return (response = await axios({
        method: "POST",
        data: avatarImgForm,
        url: BASE_URL + "/user/update-image",
        headers: {
          Authorization: "Bearer  " + userData.token,
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
        },
      })
        .then((response) => response.data)
        .catch((error) => error));
    }

    return response;
  };

  static getAllDialogsByUserId = async (
    userId = null,
    offset = 0,
    limit = 15,
    order = "desc"
  ) =>
    await Request.GET(
      `/chat/get-user-dialogs/${userId}?offset=${offset}&limit=${limit}&order=${order}`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static sendPushNotification = async (
    pushToken = "",
    title = "",
    body = "",
    moreParams = {},
    sound = "default"
  ) => {
    return await axios
      .post(
        "https://exp.host/--/api/v2/push/send",
        {
          to: pushToken,
          sound,
          title,
          body,
          ...moreParams,
        },
        {
          headers: {
            Accept: "application/json",
            "Accept-encoding": "gzip, deflate",
            "Content-Type": "application/json",
          },
        }
      )
      .then((result) => result.data);
  };

  static login = async (props) => {
    return await Request.POST(
      "/login",
      { ...props },
      false,
      false,
      "pmp",
      true
    );
  };

  static update = async (props) =>
    await Request.POST(`/user/update`, { ...props }, true, true, "pmp", true);

  static getUserById = async (userId) =>
    await Request.GET(`/user/${userId}`, {}, true, true, "pmp", true).then(
      (result) => result.data
    );

  static getUserByToken = async () =>
    await Request.GET(`/user`, {}, true, true, "pmp", true).then(
      (result) => result.data
    );

  static getNotifications = async (offset, limit) =>
    await Request.GET(
      `/push/get-all?limit=${limit}&offset=${offset}`,
      {},
      false,
      false,
      "pmp",
      true
    );

  static deleteNotifications = async (id = null) =>
    await Request.GET(`/push/delete/${id}`, {}, false, false, "pmp", true);

  static getAttendanceStatistics = async (date = "test") =>
    await Request.GET(
      `/applications/get-for-month/${date}`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static getCurrentPointsByUser = async () =>
    await Request.GET(
      "/user/points/get/for-current-user/",
      {},
      true,
      true,
      "pmp",
      true
    );

  static getPointsByUserId = async (userId = null) =>
    await Request.GET(
      `/user/points/get/by-id/${userId}`,
      {},
      true,
      true,
      "pmp",
      true
    );

  static getPasswordRecovery = async (props) =>
    await Request.POST(
      "/user/access-recovery",
      { ...props },
      true,
      true,
      "pmp",
      true
    );

  // static logout = async () => await Request.GET('/logout');
  //
  // static getProfile = async (props) => await Request.GET('/profile/get-profile', { ...props });
  //
  // static updateUser = async (props) => await Request.PUT('/user/update', { ...props });
}
