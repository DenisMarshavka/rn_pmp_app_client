import API from "../../utils/APIConnector";
import moment from "moment/moment";

const Request = new API();

export class NewsAPI {
  static getNews = async (
    startPeriod = "",
    endPeriod = "",
    offset = 0,
    limit = 15
  ) =>
    await Request.GET(
      `/new/get-all/${startPeriod}/${endPeriod}?offset=${offset}&limit=${limit}`,
      {},
      true,
      true,
      "pmp",
      true
    );
}
