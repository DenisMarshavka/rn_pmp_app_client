import { Notifications } from "expo";

import API from "../../utils/APIConnector";
import config from "../../utils/config";

const Request = new API();

export class MyNotificationAPI {
  static setNotificationApi = async (data) =>
    await Request.POST(`/push/add`, { ...data }, true, true, "pmp", true);

  static showNotificationNow = async (props) =>
    await Request.POST(
      "https://exp.host/--/api/v2/push/send",
      { ...props },
      false,
      false,
      "my"
    );
}
