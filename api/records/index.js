import API from "../../utils/APIConnector";

const Request = new API();

export class RecordsAPI {
  static createRecord = async ({ companyId, data }) =>
    await Request.POST(
      `/records/${companyId}`,
      { ...data },
      true,
      true,
      "yclients"
    );
}
