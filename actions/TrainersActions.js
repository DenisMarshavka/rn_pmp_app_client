import { TrainerAPI } from "../api/trainer";

export const GET_TRAINERS_REQUEST_START = "GET_TRAINERS_REQUEST_START";
export const GET_TRAINERS_REQUEST_SUCCESS = "GET_TRAINERS_REQUEST_SUCCESS";
export const GET_TRAINERS_REQUEST_FAILED = "GET_TRAINERS_REQUEST_FAILED";
export const CLEAR_TRAINERS_DATA = "CLEAR_TRAINERS_DATA";

export const GET_CURRENT_TRAINER_REQUEST_START =
  "GET_CURRENT_TRAINER_REQUEST_START";
export const GET_CURRENT_TRAINER_REQUEST_SUCCESS =
  "GET_CURRENT_TRAINER_REQUEST_SUCCESS";
export const GET_CURRENT_TRAINER_REQUEST_FAILED =
  "GET_CURRENT_TRAINER_REQUEST_FAILED";

const getTrainerInfoByIdRequestStart = () => ({
  type: GET_CURRENT_TRAINER_REQUEST_START,
});

export const getTrainerInfoById = (trainerId = NaN) => async (dispatch) => {
  if (trainerId && !isNaN(trainerId)) {
    try {
      dispatch(getTrainerInfoByIdRequestStart());

      const response = await TrainerAPI.getTrainerById(trainerId);

      if (
        response &&
        response.data &&
        response.status &&
        response.status === "success"
      )
        dispatch(getTrainerInfoByIdRequestSuccess(response.data));
      // console.log("Trainer REPONSE Data: ", response);
    } catch (e) {
      dispatch(getTrainerInfoByIdRequestFailed(e));

      console.log("Error TrainerAPI.getTrainerById --- ", e);
    }
  } else {
    dispatch(getTrainerInfoByIdRequestFailed("Error receied Data"));

    console.log(
      `Error: Received the bad property, when created request "Get Trainer Info By ID" to the API, "trainerId": `,
      trainerId
    );
  }
};

const getTrainerInfoByIdRequestSuccess = (payload) => ({
  type: GET_CURRENT_TRAINER_REQUEST_SUCCESS,
  payload,
});

const getTrainerInfoByIdRequestFailed = (payload) => ({
  type: GET_CURRENT_TRAINER_REQUEST_FAILED,
  payload,
});

const getTrainersByServiceIdRequestStart = () => ({
  type: GET_TRAINERS_REQUEST_START,
});

export const getTrainersByServiceId = (
  serviceId,
  byExpert = false,
  companyId = null
) => async (dispatch) => {
  const staffName = !byExpert ? "Trainers" : "Expert";

  try {
    if (serviceId && typeof serviceId === "number") {
      dispatch(getTrainersByServiceIdRequestStart());

      const response = await TrainerAPI.getTrainersByServiceId(
        serviceId,
        byExpert
      );

      console.log(
        `Received when created the request "Get ${staffName} By Service Id" to the API Data: `,
        response,
        ", Data: ",
        response.data
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data
      ) {
        dispatch(getTrainersByServiceIdRequestSuccess(response.data));
      } else dispatch(getTrainersByServiceIdRequestFailed("Empty"));
    } else {
      console.log(
        `Error: Received the bad property, when created request "Get ${staffName} By Service Id" to the API, "serviceId": `,
        serviceId
      );

      dispatch(getTrainersByServiceIdRequestFailed("Error"));
    }
  } catch (e) {
    console.log(
      `Error the request "Get ${staffName} By Service Id" to the API: `,
      e
    );

    dispatch(getTrainersByServiceIdRequestFailed(e));
  }
};

const getTrainersByServiceIdRequestSuccess = (payload) => ({
  type: GET_TRAINERS_REQUEST_SUCCESS,
  payload,
});

const getTrainersByServiceIdRequestFailed = (payload) => ({
  type: GET_TRAINERS_REQUEST_FAILED,
  payload,
});

export const clearTrainersData = () => ({
  type: CLEAR_TRAINERS_DATA,
});

const getAllTrainersStart = () => ({
  type: GET_TRAINERS_REQUEST_START,
});

export const getAllTrainers = () => async (dispatch) => {
  try {
    dispatch(getAllTrainersStart());

    const response = await TrainerAPI.getAll();

    console.log(
      `Received when created the request "Get All Trainers" to the API Data: `,
      response
    );

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data
    ) {
      dispatch(getAllTrainersSuccess(response.data.rows));
    } else dispatch(getAllTrainersFailed("Empty"));
  } catch (e) {
    console.log(`Error the request "Get All Trainers" to the API: `, e);

    dispatch(getAllTrainersFailed(e));
  }
};

const getAllTrainersSuccess = (payload) => ({
  type: GET_TRAINERS_REQUEST_SUCCESS,
  payload,
});

const getAllTrainersFailed = (payload) => ({
  type: GET_TRAINERS_REQUEST_FAILED,
  payload,
});
