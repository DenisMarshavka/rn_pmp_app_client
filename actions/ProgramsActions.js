import { ProgramsAPI } from "../api/programs";
import { DiaryFoodAPI } from "../api/diary/food";
import { generateQueryWithParams, getDataRequestStart } from "../utils";

export const GET_PROGRAM_BY_ID_START = "GET_PROGRAM_BY_ID_START";
export const GET_PROGRAM_BY_ID_SUCCESS = "GET_PROGRAM_BY_ID_SUCCESS";
export const GET_PROGRAM_BY_ID_FAILED = "GET_PROGRAM_BY_ID_FAILED";

export const GET_PROGRAMS_REQUEST_START = "GET_PROGRAMS_REQUEST_START";
export const GET_PROGRAMS_REQUEST_SUCCESS = "GET_PROGRAMS_REQUEST_SUCCESS";
export const GET_PROGRAMS_REQUEST_FAILED = "GET_PROGRAMS_REQUEST_FAILED";

export const GET_SPECIAL_PROGRAMS_START = "GET_SPECIAL_PROGRAMS_START";
export const GET_SPECIAL_PROGRAMS_SUCCESS = "GET_SPECIAL_PROGRAMS_SUCCESS";
export const GET_SPECIAL_PROGRAMS_FAILED = "GET_SPECIAL_PROGRAMS_FAILED";
export const CLEAR_SPECIAL_PROGRAMS_DATA = "CLEAR_SPECIAL_PROGRAMS_DATA";

export const GET_NEW_PROGRAMS_START = "GET_NEW_PROGRAMS_START";
export const GET_NEW_PROGRAMS_SUCCESS = "GET_NEW_PROGRAMS_SUCCESS";
export const GET_NEW_PROGRAMS_FAILED = "GET_NEW_PROGRAMS_FAILED";
export const SEND_REQUEST_PROGRAMS_DATA = "SEND_REQUEST_PROGRAMS_DATA";
export const CLEAR_NEW_PROGRAMS_DATA = "CLEAR_NEW_PROGRAMS_DATA";

export const GET_STEP_INFO_PROGRAM_START = "GET_STEP_INFO_PROGRAM_START";
export const GET_STEP_INFO_PROGRAM_SUCCESS = "GET_STEP_INFO_PROGRAM_SUCCESS";
export const GET_STEP_INFO_PROGRAM_FAILED = "GET_STEP_INFO_PROGRAM_FAILED";

export const GET_FOOD_DIARY_START = "GET_FOOD_DIARY_START ";
export const GET_FOOD_DIARY_SUCCESS = "GET_FOOD_DIARY_SUCCESS";
export const GET_FOOD_DIARY_FAILED = "GET_FOOD_DIARY_FAILED";

export const GET_PROGRAM_DISHED_START = "GET_PROGRAM_DISHED_START";
export const GET_PROGRAM_DISHED_SUCCESS = "GET_PROGRAM_DISHED_SUCCESS";
export const GET_PROGRAM_DISHED_FAILED = "GET_PROGRAM_DISHED_FAILED";

export const GET_PROGRAM_SECTIONS_BY_ID_START =
  "GET_PROGRAM_SECTIONS_BY_ID_START ";
export const GET_PROGRAM_SECTIONS_BY_ID_SUCCESS =
  "GET_PROGRAM_SECTIONS_BY_ID_SUCCESS";
export const GET_PROGRAM_SECTIONS_BY_ID_FAILED =
  "GET_PROGRAM_SECTIONS_BY_ID_FAILED";

const getProgramFoodsByDayIdStart = () => ({
  type: GET_PROGRAM_DISHED_START,
});

export const getProgramFoodsByDayId = (dayId) => async (dispatch) => {
  try {
    if (dayId && !isNaN(+dayId)) {
      dispatch(getDiaryFoodStart());

      const response = await ProgramsAPI.getRecommendedDishes(dayId);
      console.log(
        'Received when created the request "Get Program Foods By Day Id" to the API Data: ',
        response,
        ", data: ",
        response.data
      );

      if (response && response.status && response.status === "success") {
        dispatch(getProgramFoodsByDaySuccess(response.data));
      } else dispatch(getProgramFoodsByDayFailed("Empty"));
    } else {
      console.log(
        'Error: Received the bad property, when created the request "Get Program Foods By Day Id" to the API: "dayId" - ',
        dayId
      );
    }
  } catch (e) {
    console.log("Error ProgramsAPI.getRecommendedDishes --- ", e);
    dispatch(getProgramFoodsByDayFailed(e));
  }
};

const getProgramFoodsByDaySuccess = (payload) => ({
  type: GET_PROGRAM_DISHED_SUCCESS,
  payload,
});

const getProgramFoodsByDayFailed = (payload) => ({
  type: GET_PROGRAM_DISHED_FAILED,
  payload,
});

const getDiaryFoodStart = () => ({
  type: GET_FOOD_DIARY_START,
});

export const getFoodsByDay = (date) => async (dispatch) => {
  try {
    if (date) {
      dispatch(getDiaryFoodStart());
      const response = await DiaryFoodAPI.getDiaryFoodsByDate(date);

      // console.log('Received when created the request "Get Diary Food" to the API Data: ', response,'error:',  response.error, ',data: ', response.sections);

      if (response && response.status && response.status === "success") {
        dispatch(getDiaryFoodSuccess(response.data));
      } else dispatch(getDiaryFoodFailed("Empty"));
    } else {
      console.log(
        'Error: Received bad property, when created the request "Get Food Diary" to the API: "date" - ',
        date
      );
    }
  } catch (e) {
    console.log("Error DiaryFoodAPI.getDiaryFoodsByDate --- ", e);
    dispatch(getDiaryFoodFailed(e));
  }
};
const getDiaryFoodSuccess = (payload) => ({
  type: GET_FOOD_DIARY_SUCCESS,
  payload,
});

const getDiaryFoodFailed = (payload) => ({
  type: GET_FOOD_DIARY_FAILED,
  payload,
});

const getProgramByIdStart = () => ({
  type: GET_PROGRAM_BY_ID_START,
});

export const gerProgramById = (id) => async (dispatch) => {
  try {
    if (id && !isNaN(+id)) {
      dispatch(getProgramByIdStart());

      const response = await ProgramsAPI.getProgramById(id);

      console.log(
        'Received when created the request "Get Program By Id" to the API Data: ',
        response,
        ",data: ",
        response.sections
      );

      if (response && response.status && response.status === "success") {
        dispatch(getProgramByIdSuccess(response.data));
      } else dispatch(getProgramByIdFailed("Empty"));
    } else
      console.log(
        'Error: Received bad property, when created the request "Get Program By Id" to the API: "id" - ',
        id
      );
  } catch (e) {
    console.log('Error the request "Get Program By Id" to the API: ', e);

    dispatch(getProgramByIdFailed(e));
  }
};

const getProgramByIdSuccess = (payload) => ({
  type: GET_PROGRAM_BY_ID_SUCCESS,
  payload,
});

const getProgramByIdFailed = (payload) => ({
  type: GET_PROGRAM_BY_ID_FAILED,
  payload,
});

const getProgramSectionByIdStart = () => ({
  type: GET_PROGRAM_SECTIONS_BY_ID_START,
});
export const getProgramSectionById = (id) => async (dispatch) => {
  console.log("im here dsjd");
  try {
    if (id && !isNaN(+id)) {
      dispatch(getProgramSectionByIdStart());

      const response = await ProgramsAPI.getProgramSectionById(id);

      //console.log('Received when created the request "Get Program Section By Id" to the API Data: ', response,'error:',  response.error, ',data: ', response.sections);

      if (response && response.status && response.status === "success") {
        dispatch(getProgramSectionByIdSuccess(response.data));
      } else dispatch(getProgramSectionByIdFailed("Empty"));
    } else
      console.log(
        'Error: Received bad property, when created the request "Get Program Section By Id" to the API: "id" - ',
        id
      );
  } catch (e) {
    console.log(
      'Error the request "Get Program Section By Id" to the API: ',
      e
    );

    dispatch(getProgramSectionByIdFailed(e));
  }
};

const getProgramSectionByIdSuccess = (payload) => ({
  type: GET_PROGRAM_SECTIONS_BY_ID_SUCCESS,
  payload,
});

const getProgramSectionByIdFailed = (payload) => ({
  type: GET_PROGRAM_SECTIONS_BY_ID_FAILED,
  payload,
});

const getProgramsRequestStart = () => ({
  type: GET_PROGRAMS_REQUEST_START,
});

export const getPrograms = (
  offset = 0,
  limit = 15,
  orderDate = "desc"
) => async (dispatch) => {
  try {
    dispatch(getProgramsRequestStart());

    const response = await ProgramsAPI.getPrograms(offset, limit, orderDate);

    console.log(
      'Received when created the request "Get Programs" to the API Data: ',
      response,
      ",data: ",
      response.sections
    );

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.sections &&
      response.sections.rows &&
      response.sections.rows.length &&
      response.sections.count !== false
    ) {
      dispatch(
        getProgramsRequestSuccess(
          response.sections.rows,
          response.sections.count
        )
      );
    } else dispatch(getProgramsRequestFailed("Empty"));
  } catch (e) {
    console.log('Error the request "Get Programs" to the API: ', e);

    dispatch(getProgramsRequestFailed(e));
  }
};

const getProgramsRequestSuccess = (list = [], countItems = 0) => ({
  type: GET_PROGRAMS_REQUEST_SUCCESS,
  payload: {
    list,
    countItems,
  },
});

const getProgramsRequestFailed = (payload) => ({
  type: GET_PROGRAMS_REQUEST_FAILED,
  payload,
});

const getRequestToProgramsData = (
  isSpecial = false,
  isNew = false,
  limit = 20,
  offset = 0,
  getProgramsDataSuccess = () => {},
  getProgramsDataFailed = () => {},
  isUpdate = false
) => async (dispatch) => {
  let params = {
    special: isSpecial,
    new: isNew,
    limit,
    offset,
  };

  // if (isSpecial) params.special = isSpecial;
  // if (isNew) params.new = isNew;

  const url = generateQueryWithParams("/program/get-all", params);

  console.log("urlurlurl", url);

  const response = await getDataRequestStart(url);

  console.log("responseresponseresponseresponse555555555", response);

  console.log(
    `Received when created the request "Get${
      isSpecial ? " Special" : !isSpecial && isNew ? " New" : ""
    } Programs" to the API Data: `,
    response.programs.rows,
    response &&
      response.status &&
      response.status === "success" &&
      response.programs &&
      response.programs.rows
  );

  if (
    response &&
    response.status &&
    response.status === "success" &&
    response.programs &&
    response.programs.rows
  ) {
    dispatch(
      getProgramsDataSuccess(
        response.programs.rows,
        response.programs,
        isUpdate
      )
    );
  } else dispatch(getProgramsDataFailed("Error"));

  return {
    type: SEND_REQUEST_PROGRAMS_DATA,
  };
};

const getSpecialProgramsStart = (isUpdate = false) => ({
  type: GET_SPECIAL_PROGRAMS_START,
  payload: { isUpdate },
});

export const getSpecialPrograms = (
  isNew = false,
  limit = 20,
  offset = 0,
  isUpdate = false
) => (dispatch) => {
  try {
    dispatch(getSpecialProgramsStart(isUpdate));

    dispatch(
      getRequestToProgramsData(
        true,
        false,
        limit,
        offset,
        getSpecialProgramsSuccess,
        getSpecialProgramsFailed,
        isUpdate
      )
    );
  } catch (e) {
    console.log('Error the request "Get Special Programs" to the API: ', e);

    dispatch(getSpecialProgramsFailed(e));
  }
};

const getSpecialProgramsSuccess = (
  list = [],
  listMeta = {},
  isUpdate = false
) => ({
  type: GET_SPECIAL_PROGRAMS_SUCCESS,
  payload: {
    list,
    listMeta,
    isUpdate,
  },
});

export const clearSpecialProgramsData = () => ({
  type: CLEAR_SPECIAL_PROGRAMS_DATA,
});

const getSpecialProgramsFailed = (payload) => ({
  type: GET_SPECIAL_PROGRAMS_FAILED,
  payload,
});

const getNewProgramsStart = (isUpdate = false) => ({
  type: GET_NEW_PROGRAMS_START,
  payload: { isUpdate },
});

export const getNewPrograms = (
  isSpecial = false,
  limit = 20,
  offset = 0,
  isUpdate = false
) => (dispatch) => {
  try {
    dispatch(getNewProgramsStart(isUpdate));

    dispatch(
      getRequestToProgramsData(
        false,
        true,
        limit,
        offset,
        getNewProgramsSuccess,
        getNewProgramsFailed,
        isUpdate
      )
    );
  } catch (e) {
    console.log('Error the request "Get News Programs" to the API: ', e);

    dispatch(getNewProgramsFailed(e));
  }
};

const getNewProgramsSuccess = (list = [], listMeta = {}, isUpdate = false) => ({
  type: GET_NEW_PROGRAMS_SUCCESS,
  payload: {
    list,
    listMeta,
    isUpdate,
  },
});

const getNewProgramsFailed = (payload) => ({
  type: GET_NEW_PROGRAMS_FAILED,
  payload,
});

export const clearNewProgramsData = () => ({
  type: CLEAR_NEW_PROGRAMS_DATA,
});

const getProgramStepInfoStart = () => ({
  type: GET_STEP_INFO_PROGRAM_START,
});

export const getProgramStepInfo = (programId = null) => async (dispatch) => {
  try {
    if (programId !== false && !isNaN(+programId)) {
      dispatch(getProgramStepInfoStart());

      const response = await ProgramsAPI.getStepInfoByProgramId(programId);

      console.log(
        'Received when created the request "Get Step Info Program By Id" to the API Data: ',
        response,
        ",data: ",
        response.data
      );

      if (
        response &&
        response.data &&
        response.status &&
        response.status === "success"
      ) {
        dispatch(getProgramStepInfoSuccess(response.data));
      } else dispatch(getProgramStepInfoFailed("Error"));
    } else {
      console.log(
        'Error: Received bad property, when created the request "Get Step Info Program By Id" to the API: "programId" - ',
        programId
      );

      dispatch(getProgramStepInfoFailed("Error"));
    }
  } catch (e) {
    console.log(
      'Error the request "Get Step Info Program By Id" to the API: ',
      e
    );

    dispatch(getProgramStepInfoFailed(e));
  }
};

const getProgramStepInfoSuccess = (payload = {}) => ({
  type: GET_STEP_INFO_PROGRAM_SUCCESS,
  payload,
});

const getProgramStepInfoFailed = (payload = {}) => ({
  type: GET_STEP_INFO_PROGRAM_FAILED,
  payload,
});
