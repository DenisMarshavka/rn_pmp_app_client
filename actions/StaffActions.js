import { StaffAPI } from "../api/staff";
import { getCompanyIdAndStaffIdByTrainerId } from "./ServicesActions";

export const GET_STAFF_SCHEDULE_REQUEST_START =
  "GET_STAFF_SCHEDULE_REQUEST_START";
export const GET_STAFF_SCHEDULE_REQUEST_SUCCESS =
  "GET_STAFF_SCHEDULE_REQUEST_SUCCESS";
export const GET_STAFF_SCHEDULE_REQUEST_FAILED =
  "GET_STAFF_SCHEDULE_REQUEST_FAILED";

export const GET_STAFF_START = "GET_STAFF_START";
export const GET_STAFF_SUCCESS = "GET_STAFF_SUCCESS";
export const GET_STAFF_FAILED = "GET_STAFF_FAILED";
export const CLEAR_STAFF_DATA = "CLEAR_STAFF_DATA";

const getStaffStart = (isUpdate = false) => ({
  type: GET_STAFF_START,
  payload: { isUpdate },
});

const getStaffSuccess = (list = [], listMeta = {}, isUpdate = false) => ({
  type: GET_STAFF_SUCCESS,
  payload: { list, listMeta, isUpdate },
});

const getStaffFailed = (payload) => ({
  type: GET_STAFF_FAILED,
  payload,
});

const getStaffScheduleByTodayRequestStart = () => ({
  type: GET_STAFF_SCHEDULE_REQUEST_START,
});

export const getStaffScheduleByToday = (
  trainerId,
  yId,
  companyId,
  startDate,
  endDate,
  byExpert = false
) => async (dispatch, getState) => {
  try {
    dispatch(getStaffScheduleByTodayRequestStart());

    console.log("companyIdrrrrrr", companyId);

    await dispatch(getCompanyIdAndStaffIdByTrainerId(trainerId, byExpert));
    const {
      companyIdAndStaffIdData,
      companyIdAndStaffIdError = false,
    } = getState().ServicesReducer;

    console.log(
      "Result first request Data: ",
      companyIdAndStaffIdData,
      "receiveddddd",
      "yId",
      yId,
      trainerId,
      companyId,
      startDate,
      endDate,
      byExpert
    );

    if (companyId && trainerId && yId && startDate && endDate) {
      const response = await StaffAPI.getStaffSchedule(
        companyId,
        yId,
        startDate,
        endDate
      );

      console.log(
        'Received when created the request "Get Staff Schedule By Today" to the API Data: ',
        response,
        response[0],
        ", Data: ",
        response && response[0] && response[0].slots ? response[0].slots : null
      );

      if (response && response[0]) {
        dispatch(getStaffScheduleByTodayRequestSuccess(response[0]));
      } else dispatch(getStaffScheduleByTodayRequestFailed("Empty"));
    } else {
      console.log(
        'Error: Received bad properties, when created request "Get Staff Schedule By Today" to the API: "trainerId" - ',
        trainerId,
        ', "yId" - ',
        yId,
        ', "companyId" - ',
        companyId,
        startDate,
        ', "endDate" - ',
        endDate,
        ", error when created first actions: ",
        !!companyIdAndStaffIdError
      );

      dispatch(getStaffScheduleByTodayRequestFailed("Error"));
    }
  } catch (e) {
    console.log(
      'Error the request "Get Staff Schedule By Today" to the API: ',
      e
    );

    dispatch(getStaffScheduleByTodayRequestFailed(e));
  }
};

const getStaffScheduleByTodayRequestSuccess = (payload) => ({
  type: GET_STAFF_SCHEDULE_REQUEST_SUCCESS,
  payload,
});

const getStaffScheduleByTodayRequestFailed = (payload) => ({
  type: GET_STAFF_SCHEDULE_REQUEST_FAILED,
  payload,
});

export const getAllStaff = (
  isExpert = false,
  offset = 0,
  limit = 15,
  isUpdate = false
) => async (dispatch) => {
  try {
    dispatch(getStaffStart(isUpdate));

    const response = await StaffAPI.getStaffs(isExpert, offset, limit);

    // console.log('Received when created the request "Get All Staff" to the API Data: ', response, ', Data: ', response.data);

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data
    ) {
      dispatch(getStaffSuccess(response.data.rows, response.data, isUpdate));
    } else dispatch(getStaffFailed("Empty"));
  } catch (e) {
    console.log('Error the request "Get All Staff" to the API: ', e);

    dispatch(getStaffFailed(e));
  }
};

export const clearStaffData = () => ({
  type: CLEAR_STAFF_DATA,
});
