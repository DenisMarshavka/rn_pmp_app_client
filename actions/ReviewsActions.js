import { ReviewsAPI } from "../api/profile/reviews";
import moment from "moment";
// import axios from 'axios'
// import {AsyncStorage} from "react-native"

export const GET_ALL_REVIEWS_START = "GET_ALL_REVIEWS_START";
export const GET_ALL_REVIEWS_SUCCESS = "GET_ALL_REVIEWS_SUCCESS";
export const GET_ALL_REVIEWS_FAILED = "GET_ALL_REVIEWS_FAILED";

export const CREATE_REVIEW_START = "CREATE_REVIEW_START";
export const CREATE_REVIEW_SUCCESS = "CREATE_REVIEW_SUCCESS";
export const CREATE_REVIEW_FAILED = "CREATE_REVIEW_FAILED";

const CRETE_A_REVIEW = "CRETE_A_REVIEW";

export const GET_COMPANY_REVIEWS_START = "GET_COMPANY_REVIEWS_START";
export const GET_COMPANY_REVIEWS_MORE = "GET_COMPANY_REVIEWS_MORE";
export const GET_COMPANY_REVIEWS_SUCCESS = "GET_COMPANY_REVIEWS_SUCCESS";
export const GET_COMPANY_REVIEWS_FAILED = "GET_COMPANY_REVIEWS_FAILED";
export const CLEAN_COMPANY_REVIEWS = "CLEAN_COMPANY_REVIEWS";

// const BASE_URL = "http://vahtykov.ru:3003";

const getAllReviewsStart = () => ({
  type: GET_ALL_REVIEWS_START,
});

export const getAllReviews = (
  limit = 20,
  offset = 0,
  isRemove = false,
  reviewId = undefined
) => async (dispatch, getState) => {
  try {
    dispatch(getAllReviewsStart());

    const userId = getState().currentUser.currentUser.id;

    if (
      userId &&
      !isNaN(+userId) &&
      limit &&
      !isNaN(+limit) &&
      offset !== false &&
      !isNaN(+offset)
    ) {
      const response = await ReviewsAPI.getAllReviews(userId, limit, offset);

      console.log(
        'Received when created the request "Get All Reviews" to the API Data: ',
        response,
        ", Data: ",
        response.reviews
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.reviews
      ) {
        // let reviews = [...response.reviews];
        //
        // if (isRemove && reviewId !== false && !isNaN(+reviewId)) {
        //   const newData = [...response.reviews];
        //
        //   console.log("REsponse after dleted11111", newData, response.reviews);
        //
        //   if (response.reviews) {
        //     response.reviews.map((period) => {
        //       Object.keys(period).map((item, index) => {
        //         if (period[item] && period[item].data) {
        //           newData[item].data = period[item].data.filter(
        //             (review) =>
        //               review && review.id !== false && review.id !== reviewId
        //           );
        //         }
        //       });
        //     });
        //
        //     reviews = newData;
        //   }
        //
        //   console.log("REsponse after dleted", newData);
        // }

        dispatch(
          getAllReviewsSuccess(response.reviews, response.reviews.length)
        );
      } else dispatch(getAllReviewsFailed("Error"));
    } else {
      getAllReviewsFailed("Error");

      console.log(
        `Error: Received the bad properties, when created request "Get All Reviews" to the API: "userId" - `,
        userId,
        ', "limit" - ',
        limit,
        ', "offset" - ',
        offset
      );
    }
  } catch (e) {
    console.log('Error the request "Get All Reviews" to the API: ', e);

    dispatch(getAllReviewsFailed(e));
  }
};

const getAllReviewsSuccess = (list = [], countItems = 0) => ({
  type: GET_ALL_REVIEWS_SUCCESS,
  payload: {
    list,
    countItems,
  },
});

const getAllReviewsFailed = (payload) => ({
  type: GET_ALL_REVIEWS_FAILED,
  payload,
});

const createReviewStart = () => ({
  type: CREATE_REVIEW_START,
});

export const createReview = (
  text = "",
  creatingDate = "",
  trainerId = null,
  isReviewTrainer = true,
  ratingApp = 0
) => async (dispatch, getState) => {
  let response = {};

  try {
    dispatch(createReviewStart());

    const userId = getState().currentUser.currentUser.id;

    // console.log("UUUUUSSSERRRR_IIDDDDD", userId, {
    //   user_id: userId,
    //   text,
    //   date: moment().format("YYYY-MM-DD"),
    //   trainer_id: isReviewTrainer ? trainerId : null,
    //   points: isReviewTrainer ? null : ratingApp,
    // });

    if (text.trim() && (!isNaN(+trainerId) || !isReviewTrainer)) {
      const reviewData = {
        user_id: userId,
        text,
        date: moment(creatingDate).format("YYYY-MM-DD"),
        trainer_id: isReviewTrainer ? trainerId : null,
        points: isReviewTrainer ? null : ratingApp,
      };

      // console.log("reviewData", reviewData);

      response = await ReviewsAPI.createReview(reviewData);

      console.log(
        'Received when created the request "Create Review" to the API Data: ',
        response,
        ", Data: ",
        response.data
      );

      if (response && response.status && response.status === "success") {
        dispatch(getAllReviews(20, 0));

        dispatch(createReviewSuccess());
      } else dispatch(createReviewFailed("Error"));
    } else {
      createReviewFailed("Error");

      console.log(
        `Error: Received the bad properties, when created request "Create Review" to the API: "text" - `,
        text,
        ', "creatingDate" - ',
        creatingDate,
        ', "trainerId" - ',
        trainerId,
        ', "isReviewTrainer" - ',
        isReviewTrainer
      );
    }
  } catch (e) {
    console.log('Error the request "Create Review" to the API: ', e);

    dispatch(createReviewFailed(e));
  }

  return {
    type: CRETE_A_REVIEW,
    response,
  };
};

const createReviewSuccess = () => ({
  type: CREATE_REVIEW_SUCCESS,
});

const createReviewFailed = (payload) => ({
  type: CREATE_REVIEW_FAILED,
  payload,
});

export const deleteReview = (reviewId = null) => async (dispatch, getState) => {
  try {
    if (!isNaN(+reviewId)) {
      dispatch(getAllReviewsStart());

      const response = await ReviewsAPI.deleteReview(reviewId);

      console.log(
        'Received when created the request "Delete Review" to the API Data: ',
        response,
        ", Data: ",
        response.data
      );

      if (response && response.status && response.status === "success") {
        // const currentReviewsData = getState().ReviewsReducer.reviews;
        // const countTrainersReviewsItems = getState().ReviewsReducer
        //   .countTrainersReviewsItems;
        //
        // let newReviewsData = currentReviewsData;
        // if (currentReviewsData)
        //   newReviewsData = currentReviewsData.filter(
        //     (review) => review.id !== reviewId
        //   );

        dispatch(getAllReviews(20, 0, true, reviewId));
      } else dispatch(getAllReviewsFailed("Error"));
    } else {
      getAllReviewsFailed("Error");

      console.log(
        `Error: Received the bad property, when created request "Delete Review" to the API: "reviewId" - `,
        reviewId
      );
    }
  } catch (e) {
    console.log('Error the request "Delete Review" to the API: ', e);

    dispatch(getAllReviewsFailed(e));
  }
};

const getCompanyReviewsStart = () => ({
  type: GET_COMPANY_REVIEWS_START,
});

const getCompanyReviewsMore = () => ({
  type: GET_COMPANY_REVIEWS_MORE,
});

export const getCompanyReviews = (offset = 0, limit = 0) => async (
  dispatch
) => {
  try {
    if (offset === 0) {
      dispatch(getCompanyReviewsStart());
    } else {
      dispatch(getCompanyReviewsMore());
    }

    const response = await ReviewsAPI.getCompanyReviews(offset, limit);

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.rows
    ) {
      const meta = { count: response.count };

      dispatch(getCompanyReviewsSuccess({ data: response.rows, meta }));
    } else dispatch(getCompanyReviewsFailed("Error"));
  } catch (e) {
    dispatch(getCompanyReviewsFailed(e));
  }
};

const getCompanyReviewsSuccess = (payload) => ({
  type: GET_COMPANY_REVIEWS_SUCCESS,
  payload,
});

const getCompanyReviewsFailed = (payload) => ({
  type: GET_COMPANY_REVIEWS_FAILED,
  payload,
});

export const cleanCompanyReviews = () => ({ type: CLEAN_COMPANY_REVIEWS });
