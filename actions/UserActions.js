import { UserAPI } from "../api/user";

export const GET_USER_NOTIFICATIONS_START = "GET_USER_NOTIFICATIONS_START";
export const GET_USER_NOTIFICATIONS_SUCCESS = "GET_USER_NOTIFICATIONS_SUCCESS";
export const GET_USER_NOTIFICATIONS_FAILED = "GET_USER_NOTIFICATIONS_FAILED";
export const CLEAR_USER_NOTIFICATIONS = "CLEAR_USER_NOTIFICATIONS";

export const DELETE_USER_NOTIFICATIONS_SUCCESS =
  "DELETE_USER_NOTIFICATIONS_SUCCESS";

export const GET_ATTENDANCE_STATISTICS_START =
  "GET_ATTENDANCE_STATISTICS_START";
export const GET_ATTENDANCE_STATISTICS_SUCCESS =
  "GET_ATTENDANCE_STATISTICS_SUCCESS";
export const GET_ATTENDANCE_STATISTICS_FAILED =
  "GET_ATTENDANCE_STATISTICS_FAILED";

export const GET_CURRENT_POINTS_BY_USER_ID_START =
  "GET_CURRENT_POINTS_BY_USER_ID_START";
export const GET_CURRENT_POINTS_BY_USER_ID_SUCCESS =
  "GET_CURRENT_POINTS_BY_USER_ID_SUCCESS";
export const GET_CURRENT_POINTS_BY_USER_ID_FAILED =
  "GET_CURRENT_POINTS_BY_USER_ID_FAILED";

export const GET_POINTS_BY_USER_START = "GET_POINTS_BY_USER_START";
export const GET_POINTS_BY_USER_SUCCESS = "GET_POINTS_BY_USER_SUCCESS";
export const GET_POINTS_BY_USER_FAILED = "GET_POINTS_BY_USER_FAILED";

export const GET_DIALOGS_BY_USER_ID_START = "GET_DIALOGS_BY_USER_ID_START";
export const GET_DIALOGS_BY_USER_ID_SUCCESS = "GET_DIALOGS_BY_USER_ID_SUCCESS";
export const GET_DIALOGS_BY_USER_ID_FAILED = "GET_DIALOGS_BY_USER_ID_FAILED";

export const SET_COUNT_NOTIFITACIONS = "SET_COUNT_NOTIFITACIONS";
export const SET_COUNT_BASKET = "SET_COUNT_BASKET";

export const setNotificationAndBasketCount = (count = 0, isBaset = false) => ({
  type: isBaset ? SET_COUNT_BASKET : SET_COUNT_NOTIFITACIONS,
  payload: { count },
});

const getDialogsByUserIdStart = () => ({
  type: GET_DIALOGS_BY_USER_ID_START,
});

export const getDialogsByUserId = (
  userId = null,
  offset = 0,
  limit = 0,
  orderData = "desc"
) => async (dispatch) => {
  try {
    if (
      userId !== false &&
      !isNaN(+userId) &&
      typeof orderData === "string" &&
      orderData &&
      offset !== false &&
      !isNaN(+offset) &&
      limit !== false &&
      !isNaN(+limit)
    ) {
      dispatch(getDialogsByUserIdStart());

      const response = await UserAPI.getAllDialogsByUserId(
        userId,
        offset,
        limit,
        orderData
      );

      console.log(
        `Received when created the request "Get Dialogs By User Id" to the API Data: `,
        response,
        ", Data: ",
        response.data
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data &&
        response.data.count &&
        response.data.rown &&
        response.data.rown.length
      ) {
        dispatch(
          getDialogsByUserIdSuccess(response.data.rown, response.data.count)
        );
      } else dispatch(getDialogsByUserIdFailed("Empty"));
    } else {
      console.log(
        `Error: Received the bad properties, when created request "Get Dialogs By User Id"" to the API: "userId" - `,
        userId,
        ', "offset" - ',
        offset,
        ', "limit" - ',
        limit,
        ', "orderData" - ',
        orderData
      );
    }
  } catch (e) {
    console.log(`Error the request "Get Dialogs By User Id" to the API: `, e);

    dispatch(getDialogsByUserIdFailed(e));
  }
};

const getDialogsByUserIdSuccess = (list = [], countItems = 0) => ({
  type: GET_DIALOGS_BY_USER_ID_SUCCESS,
  payload: {
    list,
    countItems,
  },
});

const getDialogsByUserIdFailed = (payload) => ({
  type: GET_DIALOGS_BY_USER_ID_FAILED,
  payload,
});

const getUserNotificationsStart = (isMore = false) => ({
  type: GET_USER_NOTIFICATIONS_START,
  payload: { isMore },
});

export const getUserNotifications = (
  offset = 0,
  limit = 10,
  isMore = false
) => async (dispatch) => {
  try {
    dispatch(getUserNotificationsStart(isMore));

    const response = await UserAPI.getNotifications(offset, limit);

    console.log(
      `Received when created the request "Get User Notifications" to the API Data: `,
      response,
      ", Data: ",
      response.data
    );

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data
    ) {
      await dispatch(setNotificationAndBasketCount(response.data.count || 0));

      dispatch(
        getUserNotificationsSuccess(response.data.rows, response.data, isMore)
      );
    } else dispatch(getUserNotificationsFailed("Empty"));
  } catch (e) {
    console.log(`Error the request "Get User Notifications" to the API: `, e);

    dispatch(getUserNotificationsFailed(e));
  }
};

const getUserNotificationsSuccess = (
  list = [],
  listMeta = {},
  isMore = false
) => ({
  type: GET_USER_NOTIFICATIONS_SUCCESS,
  payload: {
    list,
    listMeta,
    isMore,
  },
});

const getUserNotificationsFailed = (payload) => ({
  type: GET_USER_NOTIFICATIONS_FAILED,
  payload,
});

export const clearUserNotifications = () => ({
  type: CLEAR_USER_NOTIFICATIONS,
});

export const deleteUserNotification = (id = null) => async (dispatch) => {
  try {
    if (id) {
      const response = await UserAPI.deleteNotifications(id);

      // console.log(`Received when created the request "Delete User Notifications" to the API Data: `, response);

      if (response && response.status && response.status === "success") {
        dispatch(getUserNotifications());
      }
    } else
      console.log(
        `Error: Received the bad property, when created request "Delete User Notifications" to the API: "id" - `,
        id
      );
  } catch (e) {
    console.log(
      `Error the request "Delete User Notifications" to the API: `,
      e
    );
  }
};

const getAttendanceStatisticsStart = () => ({
  type: GET_ATTENDANCE_STATISTICS_START,
});

export const getAttendanceStatistics = (date = "") => async (dispatch) => {
  try {
    if (date.trim()) {
      dispatch(getAttendanceStatisticsStart());

      // console.log("DATEEE RECEIVED", date);
      const response = await UserAPI.getAttendanceStatistics(date);

      console.log(
        `Received when created the request "Get Attendance Statistics" to the API Data: `,
        response,
        ", Data: ",
        response.data
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data
      ) {
        dispatch(getAttendanceStatisticsSuccess(response.data));
      } else dispatch(getAttendanceStatisticsFailed("Empty"));
    } else
      console.log(
        `Error: Received the bad property, when created request "Get Attendance Statistics" to the API: "date" - `,
        date
      );
  } catch (e) {
    console.log(
      `Error the request "Get Attendance Statistics" to the API: `,
      e
    );

    dispatch(getAttendanceStatisticsFailed(e));
  }
};

const getAttendanceStatisticsSuccess = (payload) => ({
  type: GET_ATTENDANCE_STATISTICS_SUCCESS,
  payload,
});

const getAttendanceStatisticsFailed = (payload) => ({
  type: GET_ATTENDANCE_STATISTICS_FAILED,
  payload,
});

const getCurrentPointsByUserIdStart = () => ({
  type: GET_CURRENT_POINTS_BY_USER_ID_START,
});

export const getCurrentPointsByUserId = (userId = null) => async (dispatch) => {
  try {
    if (userId && !isNaN(+userId)) {
      dispatch(getCurrentPointsByUserIdStart());

      const response = await UserAPI.getPointsByUserId(userId);

      // console.log(`Received when created the request "Get Current User Points" to the API Data: `, response, ', Data: ', response.data);

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data &&
        response.data.points !== false
      ) {
        dispatch(getCurrentPointsByUserIdSuccess(response.data.points));
      } else dispatch(getCurrentPointsByUserIdFailed("Empty"));
    } else
      console.log(
        `Error: Received the bad property, when created request "Get Current User Points" to the API: "userId" - `,
        userId
      );
  } catch (e) {
    console.log(`Error the request "Get Current User Points" to the API: `, e);

    dispatch(getCurrentPointsByUserIdFailed(e));
  }
};

const getCurrentPointsByUserIdSuccess = (payload) => ({
  type: GET_CURRENT_POINTS_BY_USER_ID_SUCCESS,
  payload,
});

const getCurrentPointsByUserIdFailed = (payload) => ({
  type: GET_CURRENT_POINTS_BY_USER_ID_FAILED,
  payload,
});

const getMaxPointsByUserStart = () => ({
  type: GET_POINTS_BY_USER_START,
});

export const getMaxPointsByUser = () => async (dispatch) => {
  try {
    dispatch(getMaxPointsByUserStart());

    const response = await UserAPI.getCurrentPointsByUser();

    // console.log(`Received when created the request "Get Max User Points" to the API Data: `, response, ', Data: ', response.data);

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data &&
      response.data.points !== false
    ) {
      dispatch(getMaxPointsByUserSuccess(response.data.points));
    } else dispatch(getMaxPointsByUserFailed("Empty"));
  } catch (e) {
    console.log(`Error the request "Get Max User Points" to the API: `, e);

    dispatch(getMaxPointsByUserFailed(e));
  }
};

const getMaxPointsByUserSuccess = (payload) => ({
  type: GET_POINTS_BY_USER_SUCCESS,
  payload,
});

const getMaxPointsByUserFailed = (payload) => ({
  type: GET_POINTS_BY_USER_FAILED,
  payload,
});
