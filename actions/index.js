import axios from "axios";

import { ApplicationsAPI } from "../api/applications";
import { UserAPI } from "../api/user";
import { getServicesTypes } from "./ServicesActions";
import { addBasketProductById } from "./BasketActions";
import { createReview } from "./ReviewsActions";
import { getUserStorageData } from "../utils";

export const SET_ERROR = "SET_ERROR";

// export const SET_RECEIVED_NOTIFICATION = "SET_RECEIVED_NOTIFICATION";

export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const GET_CURRENT_USER = "GET_CURRENT_USER";
export const GET_BOOKMARKS = "GET_BOOKMARKS";
export const SET_BOOKMARKS = "SET_BOOKMARKS";
export const GET_LATESTQUERIES = "GET_LATESTQUERIES";
export const SET_LATESTQUERIES = "SET_LATESTQUERIES";
export const GET_PUSHTOKEN = "GET_PUSHTOKEN";
export const SET_PUSHTOKEN = "SET_PUSHTOKEN";

export const GET_GENERATE_PAYMENT_LINK_START =
  "GET_GENERATE_PAYMENT_LINK_START";
export const GET_GENERATE_PAYMENT_LINK_SUCCESS =
  "GET_GENERATE_PAYMENT_LINK_SUCCESS";
export const GET_GENERATE_PAYMENT_LINK_FAILED =
  "GET_GENERATE_PAYMENT_LINK_FAILED";

export const GET_TRAININGS_BALANCE_START = "GET_TRAININGS_BALANCE_START";
export const GET_TRAININGS_BALANCE_SUCCESS = "GET_TRAININGS_BALANCE_SUCCESS";
export const GET_TRAININGS_BALANCE_FAILED = "GET_TRAININGS_BALANCE_FAILED";

// export const GET_TRAININGS_BALANCE_ONLINE_START =
//   "GET_TRAININGS_BALANCE_ONLINE_START";
// export const GET_TRAININGS_BALANCE_ONLINE_SUCCESS =
//   "GET_TRAININGS_BALANCE_ONLINE_SUCCESS";
// export const GET_TRAININGS_BALANCE_ONLINE_FAILED =
//   "GET_TRAININGS_BALANCE_ONLINE_FAILED";

export const GET_STATUS_TO_CONSTANTLY_START = "GET_STATUS_TO_CONSTANTLY_START";
export const GET_STATUS_TO_CONSTANTLY_SUCCESS =
  "GET_STATUS_TO_CONSTANTLY_SUCCESS";
export const GET_STATUS_TO_CONSTANTLY_FAILED =
  "GET_STATUS_TO_CONSTANTLY_FAILED";

export const GET_ABOUT_US_START = "GET_ABOUT_US_START";
export const GET_ABOUT_US_SUCCESS = "GET_ABOUT_US_SUCCESS";
export const GET_ABOUT_US_FAILED = "GET_ABOUT_US_FAILED";

export const SET_COMPANY_REVIEW_START = "SET_COMPANY_REVIEW_START";
export const SET_COMPANY_REVIEW_SUCCESS = "SET_COMPANY_REVIEW_SUCCESS";
export const SET_COMPANY_REVIEW_FAILED = "SET_COMPANY_REVIEW_FAILED";

// export const setNotificationOpened = (payload) => ({
//   type: SET_RECEIVED_NOTIFICATION,
//   payload,
// });

export const setError = (message = "") => {
  if (message && message.length) {
    ApplicationsAPI.setError({ message });
  } else
    console.log(
      'Error: Bad property, when created request "Set Error" to the Expo API: "message" - ',
      message
    );

  return {
    type: SET_ERROR,
  };
};

export const setCurrentUser = (user) => {
  return {
    type: SET_CURRENT_USER,
    payload: user,
  };
};

export const getCurrentUser = () => {
  return {
    type: GET_CURRENT_USER,
  };
};

export const getBookmarks = () => {
  return {
    type: GET_BOOKMARKS,
  };
};

export const setBookmarks = (bookmarks) => {
  return {
    type: SET_BOOKMARKS,
    payload: bookmarks,
  };
};

export const setLatestQueries = (user) => {
  return {
    type: SET_LATESTQUERIES,
    payload: user,
  };
};

export const getLatestQueries = () => {
  return {
    type: GET_LATESTQUERIES,
  };
};

const getTrainingsBalanceStart = () => ({
  type: GET_TRAININGS_BALANCE_START,
});

export const getTrainingsBalance = () => async (dispatch, getState) => {
  try {
    dispatch(getTrainingsBalanceStart());

    await dispatch(getServicesTypes());
    const serviceTypes = getState().ServicesReducer.serviceTypes;

    if (serviceTypes) {
      const response = await ApplicationsAPI.getTrainingsBalance();

      console.log(
        'Received when created the request "Get Trainings Balance" to the API Data: ',
        response
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data
      ) {
        dispatch(getTrainingsBalanceSuccess(response.data, serviceTypes));
      } else dispatch(getTrainingsBalanceFailed("Empty"));
    } else
      console.log(
        'Error: Bad property, when created request "Get Trainings Balance" to the Expo API: "servicesTypes" - ',
        serviceTypes
      );
  } catch (e) {
    console.log(
      'Error: When created request "Get Trainings Balance" to the Expo API: ',
      e
    );
  }
};

const getTrainingsBalanceSuccess = (trainingsBalanceList, serviceTypes) => ({
  type: GET_TRAININGS_BALANCE_SUCCESS,
  payload: {
    trainingsBalanceList,
    serviceTypes,
  },
});

const getTrainingsBalanceFailed = (payload) => ({
  type: GET_TRAININGS_BALANCE_FAILED,
  payload,
});

// const getTrainingsBalanceOnlineStart = () => ({
//   type: GET_TRAININGS_BALANCE_ONLINE_START,
// });

export const getTrainingsBalanceOnline = () => async (dispatch, getState) => {
  try {
    dispatch(getTrainingsBalanceOnlineStart());

    await dispatch(getServicesTypes());
    const serviceTypes = getState().ServicesReducer.serviceTypes;

    const userData = await getUserStorageData();

    console.log("ONLINE USER DATA", userData);

    if (
      serviceTypes &&
      userData &&
      Object.keys(userData).length &&
      userData.id !== false
    ) {
      const response = await ApplicationsAPI.getTrainingsBalanceOnline(
        +userData.id
      );

      // console.log(
      //   'Received when created the request "Get Online Trainings Balance" to the API Data: ',
      //   response
      // );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data
      ) {
        dispatch(getTrainingsBalanceOnlineSuccess(response.data, serviceTypes));
      } else dispatch(getTrainingsBalanceOnlineFailed("Empty"));
    } else
      console.log(
        'Error: Bad property, when created request "Get Online Trainings Balance" to the Expo API: "servicesTypes" - ',
        serviceTypes,
        ', "userData" - ',
        userData
      );
  } catch (e) {
    console.log(
      'Error: When created request "Get Online Trainings Balance" to the Expo API: ',
      e
    );
  }
};

// const getTrainingsBalanceOnlineSuccess = (
//   trainingsBalanceList,
//   serviceTypes
// ) => ({
//   type: GET_TRAININGS_BALANCE_ONLINE_SUCCESS,
//   payload: {
//     trainingsBalanceList,
//     serviceTypes,
//   },
// });

// const getTrainingsBalanceOnlineFailed = (payload) => ({
//   type: GET_TRAININGS_BALANCE_ONLINE_FAILED,
//   payload,
// });

const getStatusToConstantlyClientStart = () => ({
  type: GET_STATUS_TO_CONSTANTLY_START,
});

export const getStatusToConstantlyClient = () => async (dispatch) => {
  try {
    dispatch(getStatusToConstantlyClientStart());

    const response = await ApplicationsAPI.getInfoToConstantlyClient();

    // console.log('Received when created the request "Get Status To Constantly Client" to the API Data: ', response);

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data &&
      response.data.applicationsLeft !== false
    ) {
      dispatch(
        getStatusToConstantlyClientSuccess(
          response.data.applicationsLeft,
          response.data.maxValue
        )
      );
    } else dispatch(getStatusToConstantlyClientFailed("Empty"));
  } catch (e) {
    console.log(
      'Error: When created request "Get Status To Constantly Client" to the Expo API: ',
      e
    );
  }
};

const getStatusToConstantlyClientSuccess = (payload, maxValue) => ({
  type: GET_STATUS_TO_CONSTANTLY_SUCCESS,
  payload,
  maxValue,
});

const getStatusToConstantlyClientFailed = (payload) => ({
  type: GET_STATUS_TO_CONSTANTLY_FAILED,
  payload,
});

export const getPushToken = () => ({
  type: GET_PUSHTOKEN,
});

export const setPushToken = (token) => ({
  type: SET_PUSHTOKEN,
  payload: token,
});

export const getConfirmPushNotification = (title = "", body) => async (
  dispatch,
  getState
) => {
  try {
    const pushToken = await getState().currentUser.pushToken;

    if (pushToken && pushToken.trim() && title.trim() && body.trim()) {
      const result = await UserAPI.sendPushNotification(
        pushToken,
        title,
        body,
        {
          ios: {
            sound: true,
          },
          android: {
            sound: true,
            priority: "max",
            sticky: false,
            vibrate: true,
          },
        }
      );

      console.log("Push Token Generated!!! Response Data: ", result);
    } else
      console.log(
        'Error: Received bad properties, when created request "Get Confirm Push Notification" to the Expo API: "pushToken" - ',
        pushToken,
        ', "title" - ',
        title,
        ', "body" - ',
        body
      );
  } catch (e) {
    console.log(
      'Error: When created request "Get Confirm Push Notification" to the Expo API: ',
      e
    );
  }
};

const getGeneratePaymentLinkStart = () => ({
  type: GET_GENERATE_PAYMENT_LINK_START,
});

export const getGeneratePaymentLink = (type = "PRODUCT", params = {}) => async (
  dispatch,
  getState
) => {
  try {
    dispatch(getGeneratePaymentLinkStart());

    if (type && type.trim() && params && Object.keys(params).length) {
      let response = null;

      if (type !== "PROGRAM") {
        response = await ApplicationsAPI.generatePaymentLink();
      } else {
        const currentBasketState = getState().BasketReducer.basket;

        console.log("firstResponsefirstResponse", currentBasketState);

        if (currentBasketState && currentBasketState.length)
          response = await ApplicationsAPI.generatePaymentLink();
      }

      // console.log(
      //   'Received when created the request "Get Generate Payment Link" to the API Data: ',
      //   response
      // );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data
      ) {
        dispatch(getGeneratePaymentLinkSuccess(response.data));
      } else dispatch(getGeneratePaymentLinkFailed("Empty"));
    } else {
      console.log(
        'Error: Received bad properties, when created request "Get Generate Payment Link" to the API: "params" - ',
        params,
        ',  "type" - ',
        type
      );

      dispatch(getGeneratePaymentLinkFailed("Error"));
    }
  } catch (e) {
    console.log(
      'Error the request "Get Generate Payment Link" to the API: ',
      e
    );

    dispatch(getGeneratePaymentLinkFailed(e));
  }
};

const getGeneratePaymentLinkSuccess = (payload) => ({
  type: GET_GENERATE_PAYMENT_LINK_SUCCESS,
  payload: payload.url,
});

const getGeneratePaymentLinkFailed = (payload) => ({
  type: GET_GENERATE_PAYMENT_LINK_FAILED,
  payload,
});

const getAboutUsInfoStart = () => ({
  type: GET_ABOUT_US_START,
});

export const getAboutUsInfo = () => async (dispatch) => {
  try {
    dispatch(getAboutUsInfoStart());

    const response = await ApplicationsAPI.getAboutUsInfo();

    console.log(
      'Received when created the request "Get About Us Info" to the API Data: ',
      response
    );

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data
    ) {
      dispatch(getAboutUsInfoSuccess(response.data));
    } else dispatch(getAboutUsInfoFailed("Empty"));
  } catch (e) {
    console.log('Error the request "Get About Us Info" to the API: ', e);

    dispatch(getAboutUsInfoFailed(e));
  }
};

const getAboutUsInfoSuccess = (payload) => ({
  type: GET_ABOUT_US_SUCCESS,
  payload,
});

const getAboutUsInfoFailed = (payload) => ({
  type: GET_ABOUT_US_FAILED,
  payload,
});

export const getFastCheckout = (params = {}) => async (dispatch) => {
  try {
    if (params && Object.keys(params).length) {
      dispatch(getGeneratePaymentLinkStart());

      const response = await ApplicationsAPI.getFastCheckout(params);

      console.log(
        'Received when created the request "Get fast Checkout" to the API Data: ',
        response,
        ", Data: ",
        response.data,
        ", Data URL payment: ",
        response.data.url
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data &&
        response.data.url
      ) {
        dispatch(getGeneratePaymentLinkSuccess(response.data));
      } else dispatch(getGeneratePaymentLinkFailed("Error"));
    } else {
      console.log(
        'Error: Received the bad property, when created request "Get fast Checkout" to the API: "params" -- ',
        params
      );

      dispatch(getGeneratePaymentLinkFailed("Error"));
    }
  } catch (e) {
    console.log('Error the request "Get fast Checkout" to the API: ', e);

    dispatch(getGeneratePaymentLinkFailed(e));
  }
};

const setCompanyReviewStart = () => ({
  type: SET_COMPANY_REVIEW_START,
});

export const setCompanyReview = (reviewText = "", ratingCount = 0) => async (
  dispatch
) => {
  try {
    if (reviewText && reviewText.trim()) {
      dispatch(setCompanyReviewStart());

      const result = await dispatch(
        createReview(reviewText, new Date(), "", false, ratingCount)
      );

      console.log(
        'Received when created the request "Set Company Review" to the API Data: ',
        result
      );

      if (
        result &&
        result.response &&
        result.response.status &&
        result.response.status === "success"
      )
        dispatch(setCompanyReviewSuccess());
    } else
      console.log(
        'Error: Received the bad property, when created request "Set Company Review" to the API: "reviewText" -- ',
        reviewText
      );
  } catch (e) {
    console.log('Error the request "Set Company Review" to the API: ', e);

    dispatch(setCompanyReviewFailed());
  }
};

const setCompanyReviewSuccess = () => ({
  type: SET_COMPANY_REVIEW_SUCCESS,
});

const setCompanyReviewFailed = (payload) => ({
  type: SET_COMPANY_REVIEW_FAILED,
  payload,
});
