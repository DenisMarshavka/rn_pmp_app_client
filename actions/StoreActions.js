import { StoreAPI } from "../api/store";
import { getGeneratePaymentLink } from "./";

export const GET_INVITES_BY_PARAMS_START = "GET_INVITES_BY_PARAMS_START";
export const GET_INVITES_BY_PARAMS_SUCCESS = "GET_INVITES_BY_PARAMS_SUCCESS";
export const GET_INVITES_BY_PARAMS_FAILED = "GET_INVITES_BY_PARAMS_FAILED";

export const GET_PRODUCTS_CATEGORIES_START = "GET_PRODUCTS_CATEGORIES_START";
export const GET_PRODUCTS_CATEGORIES_SUCCESS =
  "GET_PRODUCTS_CATEGORIES_SUCCESS";
export const GET_PRODUCTS_CATEGORIES_FAILED = "GET_PRODUCTS_CATEGORIES_FAILED";

export const GET_PRODUCTS_BY_PARAMS_START = "GET_PRODUCTS_BY_PARAMS_START";
export const GET_PRODUCTS_BY_PARAMS_SUCCESS = "GET_PRODUCTS_BY_PARAMS_SUCCESS";
export const GET_PRODUCTS_BY_PARAMS_FAILED = "GET_PRODUCTS_BY_PARAMS_FAILED";
export const CLEAN_DATA_PRODUCTS_BY_PARAMS = "CLEAN_DATA_PRODUCTS_BY_PARAMS";

export const GET_TOP_PRODUCTS_START = "GET_TOP_PRODUCTS_START";
export const GET_TOP_PRODUCTS_SUCCESS = "GET_TOP_PRODUCTS_SUCCESS";
export const GET_TOP_PRODUCTS_FAILED = "GET_TOP_PRODUCTS_FAILED";

const getInvitesByParamsStart = () => ({
  type: GET_INVITES_BY_PARAMS_START,
});

export const getInvitesByParams = (isNew = false, limit = 0) => async (
  dispatch
) => {
  try {
    dispatch(getInvitesByParamsStart());

    const response = await StoreAPI.getInvitesByParams(isNew, limit);

    // console.log('Received when created the request "Get Invites By Params" to the API Data: ', response, ', Data: ', response.data);

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data
    ) {
      dispatch(getInvitesByParamsSuccess(response.data));
    } else dispatch(getInvitesByParamsFailed("Error"));
  } catch (e) {
    console.log('Error the request "Get Invites Params" to the API: ', e);

    dispatch(getInvitesByParamsFailed(e));
  }
};

const getInvitesByParamsSuccess = (payload) => ({
  type: GET_INVITES_BY_PARAMS_SUCCESS,
  payload,
});

const getInvitesByParamsFailed = (payload) => ({
  type: GET_INVITES_BY_PARAMS_FAILED,
  payload,
});

const getProductsCategoriesStart = () => ({
  type: GET_PRODUCTS_CATEGORIES_START,
});

export const getProductsCategories = () => async (dispatch) => {
  try {
    dispatch(getProductsCategoriesStart());

    const response = await StoreAPI.getProductsCategories();

    // console.log('Received when created the request "Get Products Categories" to the API Data: ', response, ', Data: ', response.data);

    if (response && response.status && response.status === "success") {
      dispatch(getProductsCategoriesSuccess(response.data));
    } else dispatch(getProductsCategoriesFailed("Error"));
  } catch (e) {
    console.log('Error the request "Get Products Categories" to the API: ', e);

    dispatch(getProductsCategoriesFailed(e));
  }
};

const getProductsCategoriesSuccess = (payload) => ({
  type: GET_PRODUCTS_CATEGORIES_SUCCESS,
  payload,
});

const getProductsCategoriesFailed = (payload) => ({
  type: GET_PRODUCTS_CATEGORIES_FAILED,
  payload,
});

const getProductsByParamsStart = (payload) => ({
  type: GET_PRODUCTS_BY_PARAMS_START,
  payload,
});

export const getProductsByParams = (
  isNew = false,
  offset = 0,
  limit = 15,
  isMore = false
) => async (dispatch) => {
  try {
    dispatch(getProductsByParamsStart(isMore));

    const response = await StoreAPI.getProductsByParams(isNew, offset, limit);

    console.log(
      'Received when created the request "Get Products By Params" to the API Data: ',
      response,
      ", Data: ",
      response.data
    );

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data &&
      response.data.rows
    ) {
      dispatch(
        getProductsByParamsSuccess(response.data.rows, response.data, isMore)
      );
    } else dispatch(getProductsByParamsFailed("Error"));
  } catch (e) {
    console.log('Error the request "Get Products Params" to the API: ', e);

    dispatch(getProductsByParamsFailed(e));
  }
};

const getProductsByParamsSuccess = (list, listMeta, isMore = false) => ({
  type: GET_PRODUCTS_BY_PARAMS_SUCCESS,
  payload: {
    list,
    listMeta,
    isMore,
  },
});

const getProductsByParamsFailed = (payload) => ({
  type: GET_PRODUCTS_BY_PARAMS_FAILED,
  payload,
});

export const cleanProductsByParams = () => ({
  type: CLEAN_DATA_PRODUCTS_BY_PARAMS,
});

const getTopProductsStart = () => ({
  type: GET_TOP_PRODUCTS_START,
});

export const getTopProducts = () => async (dispatch) => {
  try {
    dispatch(getTopProductsStart());

    const response = await StoreAPI.getTopProducts();

    console.log(
      'Received when created the request "Get Top Products" to the API Data: ',
      response,
      ", Data: ",
      response.data
    );

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data
    ) {
      dispatch(getTopProductsSuccess(response.data));
    } else dispatch(getTopProductsFailed("Error"));
  } catch (e) {
    console.log('Error the request "Get Top Product" to the API: ', e);

    dispatch(getTopProductsFailed(e));
  }
};

const getTopProductsSuccess = (payload) => ({
  type: GET_TOP_PRODUCTS_SUCCESS,
  payload,
});

const getTopProductsFailed = (payload) => ({
  type: GET_TOP_PRODUCTS_FAILED,
  payload,
});
