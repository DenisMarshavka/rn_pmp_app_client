import { InstructionsAPI } from "../api/instructions";

export const GET_APP_INSTRUCTION_START = "GET_APP_INSTRUCTION_START";
export const GET_APP_INSTRUCTION_SUCCESS = "GET_APP_INSTRUCTION_SUCCESS";
export const GET_APP_INSTRUCTION_FAILED = "GET_APP_INSTRUCTION_FAILED";

const getAppInstructionStart = () => ({
  type: GET_APP_INSTRUCTION_START,
});

export const getAppInstruction = (limit = 20, offset = 0) => async (
  dispatch
) => {
  try {
    if (limit && !isNaN(+limit)) {
      dispatch(getAppInstructionStart());

      const response = await InstructionsAPI.getAppInstruction(limit, offset);

      console.log(
        'Received when created the request "Get App Instruction" to the API Data: ',
        response,
        ", Data: ",
        response.data
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data &&
        response.data.rows
      ) {
        dispatch(getAppInstructionSuccess(response.data.rows));
      } else dispatch(getAppInstructionFailed("Empty"));
    } else
      console.log(
        'Error: Received the bad property, when created request "Get App Instruction" to the API: "limit" -- ',
        limit
      );
  } catch (e) {
    console.log('Error the request "Get App Instruction" to the API: ', e);

    dispatch(getAppInstructionFailed(e));
  }
};

const getAppInstructionSuccess = (payload) => ({
  type: GET_APP_INSTRUCTION_SUCCESS,
  payload,
});

const getAppInstructionFailed = (payload) => ({
  type: GET_APP_INSTRUCTION_FAILED,
  payload,
});
