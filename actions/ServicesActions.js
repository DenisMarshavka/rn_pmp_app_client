import { ServicesAPI } from "../api/services";
import axios from "axios";
import { AsyncStorage } from "react-native";
import { TrainerAPI } from "../api/trainer";

export const GET_SERVICES_TYPES_REQUEST_START =
  "GET_SERVICES_TYPES_REQUEST_START";
export const GET_SERVICES_TYPES_REQUEST_SUCCESS =
  "GET_SERVICES_TYPES_REQUEST_SUCCESS";
export const GET_SERVICES_TYPES_REQUEST_FAILED =
  "GET_SERVICES_TYPES_REQUEST_FAILED";

export const GET_SERVICES_BY_TYPE_QUERY_START =
  "GET_SERVICES_BY_TYPE_QUERY_START";
export const GET_SERVICES_BY_TYPE_REQUEST_SUCCESS =
  "GET_SERVICES_BY_TYPE_REQUEST_SUCCESS";
export const GET_SERVICES_BY_TYPE_REQUEST_FAILED =
  "GET_SERVICES_BY_TYPE_REQUEST_FAILED";

export const GET_COMPANY_ID_AND_STAFF_ID_START =
  "GET_COMPANY_ID_AND_STAFF_ID_START";
export const GET_COMPANY_ID_AND_STAFF_ID_SUCCESS =
  "GET_COMPANY_ID_AND_STAFF_ID_SUCCESS";
export const GET_COMPANY_ID_AND_STAFF_ID_FAILED =
  "GET_COMPANY_ID_AND_STAFF_ID_FAILED";

export const GET_EXPERT_SERVICES_START = "GET_EXPERT_SERVICES_START";
export const GET_EXPERT_SERVICES_SUCCESS = "GET_EXPERT_SERVICES_SUCCESS";
export const GET_EXPERT_SERVICES_FAILED = "GET_EXPERT_SERVICES_FAILED";

export const GET_SERVICES_DATA_RESET = "GET_SERVICES_DATA_RESET";

const BASE_URL = "https://back.pmp.aumagency.ru:3003";

export const getServicesDataReset = () => ({
  type: GET_SERVICES_DATA_RESET,
});

const getServicesTypesRequestStart = () => ({
  type: GET_SERVICES_TYPES_REQUEST_START,
});

export const getServicesTypes = () => async (dispatch) => {
  try {
    dispatch(getServicesTypesRequestStart());

    const response = await ServicesAPI.getAllServiceTypes();

    // console.log('Received when created the request "Get Services Types" to the API Data: ', response, ', Data: ', response.data);

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data
    ) {
      dispatch(getServicesTypesRequestSuccess(response.data));
    } else dispatch(getServicesTypesRequestFailed("Empty"));
  } catch (e) {
    console.log('Error the request "Get Services Types" to the API: ', e);

    dispatch(getServicesTypesRequestFailed(e));
  }
};

const getServicesTypesRequestSuccess = (payload) => ({
  type: GET_SERVICES_TYPES_REQUEST_SUCCESS,
  payload,
});

const getServicesTypesRequestFailed = (payload) => ({
  type: GET_SERVICES_TYPES_REQUEST_FAILED,
  payload,
});

const getServicesByTypeStart = () => ({
  type: GET_SERVICES_BY_TYPE_QUERY_START,
});

export const getServicesByTypeName = (
  typeName = "",
  offset = 0,
  limit = 15,
  isOnline = false,
  isPurchased = false,
  isAll = false
) => async (dispatch) => {
  try {
    dispatch(getServicesByTypeStart());

    let data = await AsyncStorage.getItem("user");
    const headers = { Authorization: `Bearer ${JSON.parse(data).token}` };
    let response = null;

    if (typeName.trim() && JSON.parse(data).token) {
      if (isAll) {
        response = await axios
          .get(
            `${BASE_URL}/service/trainer-services/get-all/by-type?type=${typeName.trim()}&limit=${limit}&offset=${offset}&isOnline=${isOnline}`,
            {
              headers,
            }
          )
          .then((result) => result.data);
      } else
        response = await ServicesAPI.getAllPurchasedTrainerServices(
          isOnline,
          typeName,
          offset,
          limit
        );

      console.log(
        `Received when created the request "Get ${
          !isAll ? "Purchased" : "All"
        } ${isOnline ? "Online" : ""} Services By Type ${
          typeName === "Pilates"
            ? "Pilates"
            : typeName === "Functional"
            ? "Functional"
            : "UNDEFINED Type"
        }" to the API Data:  `,
        response
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data
      ) {
        dispatch(getServicesByTypeSuccess(response.data.rows));
      } else dispatch(getServicesByTypeFailed("Empty"));
    } else
      console.log(
        'Error the Request: Received the bad properties "typeName", when created the request: "Get Services By Type"',
        typeName,
        ', "PMP Token" - ',
        JSON.parse(data).token
      );
  } catch (e) {
    console.log('Error the request "Get Services By Type" to the API: ', e);

    dispatch(getServicesByTypeFailed(e));
  }
};

const getServicesByTypeSuccess = (payload) => ({
  type: GET_SERVICES_BY_TYPE_REQUEST_SUCCESS,
  payload,
});

const getServicesByTypeFailed = (payload) => ({
  type: GET_SERVICES_BY_TYPE_REQUEST_FAILED,
  payload,
});

export const getCompanyIdAndStaffIdByTrainerId = (
  trainerId,
  byExpert = false
) => async (dispatch) => {
  try {
    if (trainerId && typeof +trainerId === "number") {
      const response = await TrainerAPI.getTrainersByServiceId(
        trainerId,
        byExpert
      );

      // console.log('Received when created the request "Get Staff Id And Company Id By Trainer Id" to the API Data: ', response, ', Data: ', response.data);

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data
      ) {
        dispatch(getCompanyIdAndStaffIdByTrainerIdSuccess(response.data));
      } else dispatch(getCompanyIdAndStaffIdByTrainerIdFailed("Empty"));
    } else {
      console.log(
        'Error: Received the bad property, when created request "Get Staff Id And Company Id By Trainer Id" to the API, "trainerId": ',
        trainerId
      );

      dispatch(getCompanyIdAndStaffIdByTrainerIdFailed("Error"));
    }
  } catch (e) {
    console.log(
      'Error the request "Get Staff Id And Company Id By Trainer Id" to the API: ',
      e
    );

    dispatch(getCompanyIdAndStaffIdByTrainerIdFailed(e));
  }

  return { type: GET_COMPANY_ID_AND_STAFF_ID_START };
};

const getCompanyIdAndStaffIdByTrainerIdSuccess = (payload) => ({
  type: GET_COMPANY_ID_AND_STAFF_ID_SUCCESS,
  payload,
});

const getCompanyIdAndStaffIdByTrainerIdFailed = (payload) => ({
  type: GET_COMPANY_ID_AND_STAFF_ID_FAILED,
  payload,
});

const getExpertServicesStart = () => ({
  type: GET_EXPERT_SERVICES_START,
});

export const getExpertServices = (offset = 0, limit = 10) => async (
  dispatch
) => {
  try {
    dispatch(getExpertServicesStart());

    const response = await ServicesAPI.getAllExpertServices(
      (offset = 0),
      (limit = 10)
    );

    // console.log('Received when created the request "Get All Expert Services" to the API Data: ', response);

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data
    ) {
      dispatch(getExpertServicesSuccess(response.data.rows));
    } else dispatch(getExpertServicesFailed("Empty"));
  } catch (e) {
    console.log('Error the request "Get All Expert Services" to the API: ', e);

    dispatch(getExpertServicesFailed(e));
  }
};

const getExpertServicesSuccess = (payload) => ({
  type: GET_EXPERT_SERVICES_SUCCESS,
  payload,
});

const getExpertServicesFailed = (payload) => ({
  type: GET_EXPERT_SERVICES_FAILED,
  payload,
});
