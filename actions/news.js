import { NewsAPI } from "../api/news";

export const GET_NEWS_REQUEST_ERROR = "GET_NEWS_REQUEST_ERROR";
export const GET_NEWS_REQUEST_START = "GET_NEWS_REQUEST_START";
export const GET_NEWS_REQUEST_SUCCESS = "GET_NEWS_REQUEST_SUCCESS";

const getNewsDataStart = () => ({
  type: GET_NEWS_REQUEST_START,
});

export const getNews = (
  startPeriod = "",
  endPeriod = "",
  offset = 0,
  limit = 15
) => async (dispatch) => {
  try {
    if (startPeriod.trim() && endPeriod.trim()) {
      dispatch(getNewsDataStart());

      const response = await NewsAPI.getNews(
        startPeriod,
        endPeriod,
        offset,
        limit
      );

      // console.log('Received Data from the request "Get News" to the API result: ', response, 'data: ', response.data.news);

      dispatch(getNewsSuccess(response.data.news.rows));
    } else
      console.log(
        'Error: Bad property, when created request "Get News" to the Expo API: "startPeriod" - ',
        startPeriod,
        ', "endPeriod" - ',
        endPeriod
      );
  } catch (e) {
    console.log("Error request to Get News: ", e);

    dispatch(getNewsError(e));
  }
};

const getNewsSuccess = (payload) => ({
  type: GET_NEWS_REQUEST_SUCCESS,
  payload,
});

const getNewsError = (payload) => ({
  type: GET_NEWS_REQUEST_ERROR,
  payload,
});
