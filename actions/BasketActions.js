import { BasketAPI } from "../api/basket";
import { setNotificationAndBasketCount } from "../actions/UserActions";

export const GET_BASKET_START = "GET_BASKET_START";
export const GET_BASKET_SUCCESS = "GET_BASKET_SUCCESS";
export const GET_BASKET_FAILED = "GET_BASKET_FAILED";
export const GET_CURRENT_BASKET_STATE = "GET_CURRENT_BASKET_STATE";
export const GET_BASKET_DATA_RESET = "GET_BASKET_DATA_RESET";
export const ADD_PRODUCT_BASKET = "ADD_PRODUCT_BASKET";

export const GET_HISTORY_PAYMENTS_START = "GET_HISTORY_PAYMENTS_START";
export const GET_HISTORY_PAYMENTS_SUCCESS = "GET_HISTORY_PAYMENTS_SUCCESS";
export const GET_HISTORY_PAYMENTS_FAILED = "GET_HISTORY_PAYMENTS_FAILED";
export const CLEAN_HISTORY_PAYMETS = "CLEAN_HISTORY_PAYMETS";

export const getBasketStateDataReset = () => ({
  type: GET_BASKET_DATA_RESET,
});

const getBasketStateStart = (isMore = false) => ({
  type: GET_BASKET_START,
  payload: { isMore },
});

export const getCurrentBasketState = (
  isUpdate = true,
  offset = 0,
  limit = 15,
  orderData = "desc",
  updateBasketList = false,
  isMore = false,
  onlyBeaty = false,
  onlyFood = false,
  onlySpecial = false,
  onlyNew = false,
  onlySport = false
) => async (dispatch) => {
  try {
    if (isUpdate || updateBasketList) dispatch(getBasketStateStart(isMore));

    const response = await BasketAPI.getCurrentState(offset, limit, orderData, {
      beaaty: onlyBeaty,
      food: onlyFood,
      special: onlySpecial,
      new: onlyNew,
      sport: onlySport,
    });

    console.log(
      'Received when created the request "Get Basket State" to the API Data: ',
      response,
      ", Data: ",
      response.data.data
    );

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data &&
      response.data.data
    ) {
      await dispatch(setNotificationAndBasketCount(response.data.count, true));

      await dispatch(
        getBasketStateSuccess(
          response.data.data,
          response.data.count,
          response.data,
          isUpdate || updateBasketList,
          isMore
        )
      );
    } else dispatch(getBasketStateFailed("Empty"));
  } catch (e) {
    console.log('Error the request "Get Basket State" to the API: ', e);

    dispatch(getBasketStateFailed(e));
  }

  return {
    type: GET_CURRENT_BASKET_STATE,
  };
};

const getBasketStateSuccess = (
  response = [],
  countItems = 0,
  listMeta = {},
  updateBasketList = true,
  isMore = false
) => {
  let basketState = [];
  let programsData = [];
  let certificatesData = [];
  let servicesData = [];
  // const paymentlink = paymentUrl && paymentUrl.trim() ? paymentUrl.trim() : "";

  // if (response && response.products) basketState = [...response.products];
  //
  // if (response && response.programs) {
  //   for (let program in response.programs) {
  //     response.programs[program].isPrograms = true;
  //
  //     programsData.push(response.programs[program]);
  //
  //     if (response.programs[program].count)
  //       allCounts += response.programs[program].count;
  //   }
  // }
  // basketState = [...basketState, ...programsData];

  //

  // const paymentlink = paymentUrl && paymentUrl.trim() ? paymentUrl.trim() : "";
  const setItemsBaset = (items = [], type = "") => {
    if (items && items.length && type) {
      for (let productItem in items) {
        items[productItem]["type"] = type;

        if (type && type.trim() && type === "trainer_service")
          items[productItem]["isSport"] = true;

        switch (type) {
          case "product":
            basketState = [...items];
            break;

          case "program":
            programsData = [...items];
            break;

          case "step":
            programsData = [...items];
            break;

          case "trainer_service":
            programsData = [...items];
            break;

          default:
            certificatesData = [...items];
        }
      }
    }
  };

  Object.keys(response).map((item, i) => {
    if (item && response[item] && response[item].length) {
      setItemsBaset(response[item], item);
    }
  });

  basketState = [...basketState, ...programsData, ...certificatesData];

  // console.log("sssssssssssssssss", basketState);
  //
  // console.log("BASKET: response --- ", {
  //   basketState,
  //   countItems,
  //   listMeta,
  //   updateBasketList,
  //   isMore,
  // });

  return {
    type: GET_BASKET_SUCCESS,
    payload: {
      list: basketState,
      countItems,
      listMeta,
      updateBasketList,
      isMore,
    },
  };
};

const getBasketStateFailed = (payload) => ({
  type: GET_BASKET_FAILED,
  payload,
});

export const addBasketProductById = (params = {}, isUpdate = false) => async (
  dispatch
) => {
  let response = null;

  try {
    // console.log("reCEIVED HWEN ADDDD: ", params);

    if (params && Object.keys(params).length) {
      response = await BasketAPI.add(params);

      console.log(
        'Received when created the request "Add Product To the Basket By Id" to the API Data: ',
        response,
        ", Data: ",
        response.data
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data
      ) {
        dispatch(getCurrentBasketState(isUpdate));
      } else dispatch(getBasketStateFailed("Empty"));
    } else
      console.log(
        'Error: Received the bad property, when created request "Add Product To the Basket By Id" to the API: "params" -- ',
        params
      );
    //     } catch (e) {
  } catch (e) {
    console.log(
      'Error the request "Add Product To the Basket By Id" to the API: ',
      e
    );

    dispatch(getBasketStateFailed(e));
  }

  return {
    type: ADD_PRODUCT_BASKET,
    response,
  };
};

const getHistoryStart = (isUpdate = false) => ({
  type: GET_HISTORY_PAYMENTS_START,
  payload: { isUpdate },
});

export const getHistoryState = (
  fromHome = false,
  offset = 0,
  limit = 15,
  orderData = "desc",
  isUpdate = false
) => async (dispatch) => {
  try {
    dispatch(getHistoryStart(isUpdate));

    const response = await BasketAPI.getHistory(offset, limit, orderData);

    console.log(
      'Received when created the request "Get History Payments" to the API Data: ',
      response
    );

    // if (response && response.data) {
    //   let historyState = [];
    //   let unicalsProduct = [];
    //   let summAllCounts = 0;
    //
    //   if (fromHome) {
    //     for (let productsList of response.data.rows) {
    //       console.log("productsList", productsList);
    //
    //       if (Object.keys(productsList).length) {
    //         historyState = [...historyState, productsList];
    //
    //         if (productsList.products.count) {
    //           summAllCounts = productsList.products.count;
    //         }
    //       }
    //     }
    //   } else historyState = response.data.rows;
    //
    //
    //
    //
    //
    //
    //+++++++++++
    if (response && response.data && response.data.rows) {
      //   let historyState = [];
      //   let unicalsProduct = [];
      //   let summAllCounts = 0;
      //
      //   if (fromHome) {
      //     for (let productsList of response.data.rows) {
      //       if (productsList.products.length)
      //         historyState = [...productsList.products];
      //
      //       if (productsList.programs.length) {
      //         historyState = [...historyState, ...productsList.programs];
      //       }
      //     }
      //   } else historyState = response.data.rows;

      dispatch(getHistorySuccess(response.data.rows, response.data, isUpdate));
    } else dispatch(getHistoryFailed("Empty"));
  } catch (e) {
    console.log('Error the request "Get History Payments" to the API: ', e);

    dispatch(getHistoryFailed(e));
  }
};

const getHistorySuccess = (list = [], listMeta = {}, isUpdate = false) => ({
  type: GET_HISTORY_PAYMENTS_SUCCESS,
  payload: {
    list,
    listMeta,
    isUpdate,
  },
});

const getHistoryFailed = (payload) => ({
  type: GET_HISTORY_PAYMENTS_FAILED,
  payload,
});

export const cleanHistoryState = () => ({
  type: CLEAN_HISTORY_PAYMETS,
});

export const removeProductItemFromBasket = (params = {}) => async (
  dispatch
) => {
  try {
    if (params && Object.keys(params).length) {
      const response = await BasketAPI.remove(params);

      console.log(
        'Received when created the request "Remove Product item from Basket" to the API Data: ',
        response,
        ", Data: ",
        response.data
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data
      ) {
        await dispatch(
          setNotificationAndBasketCount(response.data.count, true)
        );

        dispatch(getCurrentBasketState(false, 0, 15, "desc", false));
      } else dispatch(getBasketStateFailed("Empty"));
    } else {
      console.log(
        'Error: Received the bad property, when created request "Remove Product item from Basket" to the API: "params" -- ',
        params
      );
    }
  } catch (e) {
    console.log(
      'Error the request "Remove Product item from Basket" to the API: ',
      e
    );

    dispatch(getBasketStateFailed(e));
  }
};
