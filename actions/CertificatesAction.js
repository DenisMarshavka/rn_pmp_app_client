import { CertificatesAPI } from "../api/certificates";

export const GET_CERTIFICATE_START = "GET_CERTIFICATE_START";
export const GET_CERTIFICATE_SUCCESS = "GET_CERTIFICATE_SUCCESS";
export const GET_CERTIFICATE_FAILED = "GET_CERTIFICATE_FAILED";

export const GET_CERTIFICATES_START = "GET_CERTIFICATES_START";
export const GET_CERTIFICATES_SUCCESS = "GET_CERTIFICATES_SUCCESS";
export const GET_CERTIFICATES_FAILED = "GET_CERTIFICATES_FAILED";

export const GET_CERTIFICATES_DATA_RESET = "GET_CERTIFICATES_DATA_RESET";

export const getCertificatesDataReset = () => ({
  type: GET_CERTIFICATES_DATA_RESET,
});

const getCertificateStart = () => ({
  type: GET_CERTIFICATE_START,
});

export const getCertificateById = (id = null) => async (dispatch) => {
  try {
    if (id && !isNaN(+id)) {
      dispatch(getCertificateStart());

      const response = await CertificatesAPI.getById(id);

      console.log(
        'Received when created the request "Get Certificate By Id" to the API Data: ',
        response,
        ", Data: ",
        response.data
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.data
      ) {
        dispatch(getCertificateSuccess(response.data));
      } else dispatch(getCertificateFailed("Empty"));
    } else
      console.log(
        'Error: Received the bad property, when created request "Get Certificate By Id" to the API: "id" -- ',
        id
      );
  } catch (e) {
    console.log('Error the request "Get Certificate By Id" to the API: ', e);

    dispatch(getCertificateFailed(e));
  }
};

const getCertificateSuccess = (payload) => ({
  type: GET_CERTIFICATE_SUCCESS,
  payload,
});

const getCertificateFailed = (payload) => ({
  type: GET_CERTIFICATE_FAILED,
  payload,
});

const getCertificatesStart = () => ({
  type: GET_CERTIFICATES_START,
});

export const getCertificates = () => async (dispatch) => {
  try {
    dispatch(getCertificatesStart());

    const response = await CertificatesAPI.getAll();

    console.log(
      'Received when created the request "Get All Certificates" to the API Data: ',
      response,
      ", Data: ",
      response.data
    );

    if (
      response &&
      response.status &&
      response.status === "success" &&
      response.data &&
      response.data.rows &&
      response.data.count
    ) {
      dispatch(getCertificatesSuccess(response.data.rows, response.data.count));
    } else dispatch(getCertificatesFailed("Empty"));
  } catch (e) {
    console.log('Error the request "Get All Certificates" to the API: ', e);

    dispatch(getCertificatesFailed(e));
  }
};

const getCertificatesSuccess = (list = [], countItems = 0) => ({
  type: GET_CERTIFICATES_SUCCESS,
  payload: {
    list,
    countItems,
  },
});

const getCertificatesFailed = (payload) => ({
  type: GET_CERTIFICATES_FAILED,
  payload,
});
