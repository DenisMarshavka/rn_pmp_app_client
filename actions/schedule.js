import { ApplicationsAPI } from "../api/applications";
import axios from "axios/index";
import config from "../utils/config";

export const FETCH_SCHEDULE_START = "FETCH_SCHEDULE_START";
export const FETCH_SCHEDULE_SUCCESS = "FETCH_SCHEDULE_SUCCESS";
export const FETCH_SCHEDULE_ERROR = "FETCH_SCHEDULE_ERROR";
export const CLEAN_SCHEDULE = "CLEAN_SCHEDULE";

const getScheduleStart = () => ({
  type: FETCH_SCHEDULE_START,
});

export const getSchedule = (date) => async (dispatch) => {
  try {
    dispatch(getScheduleStart());

    // revert after back will be ready
    const response = await ApplicationsAPI.getApplicationForDate(date).then(
      (result) => result.data
    );

    console.log(
      'Received when created the request "Get Shedule State By Selected User Calendar Day" to the API Data: ',
      response
    );

    // const tempToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTU3LCJpYXQiOjE1OTQ1NTQ4MzB9.YVmpd5VJRHQio8JvzMMQHFPHAeHgwaKqRogioLc2ViU';
    // const headers = { 'Authorization': `Bearer ${tempToken}` };
    //
    // const response = await axios.get(`${config.pmpURL}/applications/get-for-date/${date}`, {
    //     headers
    // }).then(result => result.data);

    dispatch(getScheduleSuccess(response));
  } catch (e) {
    dispatch(getScheduleError(e));
  }
};

const getScheduleSuccess = (payload) => ({
  type: FETCH_SCHEDULE_SUCCESS,
  payload,
});

const getScheduleError = (payload) => ({
  type: FETCH_SCHEDULE_ERROR,
  payload,
});

export const cleanSchedule = () => async (dispatch) => {
  dispatch({ type: CLEAN_SCHEDULE });
};
