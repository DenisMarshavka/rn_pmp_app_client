import { StudiosAPI } from "../api/studios";

export const GET_STUDIOS_BY_SERVICE_REQUEST_START =
  "GET_STUDIOS_BY_SERVICE_REQUEST_START";
export const GET_STUDIOS_BY_SERVICE_REQUEST_SUCCESS =
  "GET_STUDIOS_BY_SERVICE_REQUEST_SUCCESS";
export const GET_STUDIOS_BY_SERVICE_REQUEST_FAILED =
  "GET_STUDIOS_BY_SERVICE_REQUEST_FAILED";
export const CLEAN_STUDIOS_BY_SERVICE = "CLEAN_STUDIOS_BY_SERVICE";

const getStudiosByServiceIdRequestStart = () => ({
  type: GET_STUDIOS_BY_SERVICE_REQUEST_START,
});

export const getStudiosByServiceId = (
  serviceId,
  byExpert = false,
  offset = 0,
  limit = 15
) => async (dispatch) => {
  try {
    console.log("REEESSSEEEIVED: ", serviceId, byExpert);

    if (serviceId && typeof +serviceId === "number") {
      dispatch(getStudiosByServiceIdRequestStart());

      const response = await StudiosAPI.getStudiosByServiceId(
        /*byExpert ? 5 : */ serviceId, //TODO
        byExpert,
        offset,
        limit
      );

      console.log(
        'Received when created the request "Get Studios By Service Id" to the API Data: ',
        response,
        "data: ",
        response.studios[0].rows
      );

      if (
        response &&
        response.status &&
        response.status === "success" &&
        response.studios &&
        response.studios[0] &&
        response.studios[0].rows
      ) {
        dispatch(
          getStudiosByServiceIdRequestSuccess(
            response.studios[0].rows,
            response.studios[0].count
          )
        );
      } else dispatch(getStudiosByServiceIdRequestFailed("Empty"));
    } else {
      console.log(
        'Error: Received the bad property, when created request "Get Studios By Service Id" to the API, "serviceId": ',
        serviceId
      );

      dispatch(getStudiosByServiceIdRequestFailed("Error"));
    }
  } catch (e) {
    dispatch(getStudiosByServiceIdRequestFailed("Error"));

    console.log(
      'Error the request "Get Studios By Service Id" to the API: ',
      e
    );

    dispatch(getStudiosByServiceIdRequestFailed(e));
  }
};

const getStudiosByServiceIdRequestSuccess = (list = [], countItems = 0) => ({
  type: GET_STUDIOS_BY_SERVICE_REQUEST_SUCCESS,
  payload: {
    list,
    countItems,
  },
});

const getStudiosByServiceIdRequestFailed = (payload) => ({
  type: GET_STUDIOS_BY_SERVICE_REQUEST_FAILED,
  payload,
});

export const cleanStudiosByServiceId = () => ({
  type: CLEAN_STUDIOS_BY_SERVICE,
});
