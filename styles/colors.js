export default {
  mainBlack: '#151C26',
  mainWhite: '#FFFFFF',
  mainOrange: '#F46F22',
  mainGrey: '#E0E1E2',
  mainGreen: '#0FD8D8',

  absoluteBlack: '#000000',
  
  darkGrey30: '#4D4D4D',
  darkGrey77: '#C4C4C4',
};
