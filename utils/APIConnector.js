import { AsyncStorage } from "react-native";
import axios from "axios";
import config from "../utils/config";

const SERVER_URL = config.serverURL;
const PMP_URL = config.pmpURL;
const TOKEN = config.token;
const USER_TOKEN = config.userToken;

const APIMethods = {
  PUT: "PUT",
  GET: "GET",
  POST: "POST",
  DELETE: "DELETE",
};

// TOKEN.then(r => {
//   console.log('TTTOOKKEEN', r.access_token);
// })

const request = async (
  route,
  method,
  params,
  token = true,
  user = false,
  type = "pmp",
  pmpToken = true
) => {
  console.log(
    `token ${token}`,
    `user ${user}`,
    `type ${type}`,
    `pmpToken ${pmpToken}`
  );
  const query = new URLSearchParams(params).toString();
  let url =
    type === "my" ? `${route}?${query}` : `${SERVER_URL}${route}?${query}`;
  let headers = {};

  if (type !== "my") {
    if (token && type === "yclients") {
      headers = { Authorization: `Bearer ${TOKEN}` };

      if (user) {
        headers = { Authorization: `Bearer ${TOKEN}, User ${USER_TOKEN}` };
      }
    }

    if (type === "pmp") {
      url = `${PMP_URL}${route}${query ? `?${query}` : ""}`;

      let data = await AsyncStorage.getItem("user").then((result) =>
        JSON.parse(result)
      );

      const pmpRequestsToken =
        data && data.token && data.token.trim()
          ? data.token
          : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTkzMjY5NDk0fQ.gVeBpOZ-9Wva6Xoy4m5mKlDq6ETtSGdFrbsAkUYPpQ8";

      console.log(
        "TOKENNNN",
        data && data.token ? data.token : "TOKKKEN is undefined!!!"
      );

      if (pmpRequestsToken) {
        headers = {
          Authorization: `Bearer ${pmpRequestsToken}`,
        };
      }
    }
  }

  let body = {
    url: url,
    headers: headers,
    method: APIMethods[method],
  };

  if (APIMethods[method] !== "GET") {
    body["data"] = params;
  }

  console.log("====================================");
  console.log("url", body);

  // return TOKEN.then(user => {
  // console.log('TTTOOKKEEN', user);

  return axios(body)
    .then((response) => {
      console.log("========++++++++++++++++++++=========");
      console.log("axios.success", response.data);
      return response.data;
    })
    .catch((error) => {
      console.log("==========-----------------============");
      console.log("axios.error", error);

      return { status: "error", error };
    });
  // })
};

class API {
  url = "";
  constructor(url = "") {
    this.url = url;
  }

  GET = (internal = "", params, token, userToken, type, pmpToken) =>
    request(
      this.url + internal,
      APIMethods.GET,
      params,
      token,
      userToken,
      type,
      pmpToken
    );

  POST = (internal = "", params, token, userToken, type, pmpToken) =>
    request(
      this.url + internal,
      APIMethods.POST,
      params,
      token,
      userToken,
      type,
      pmpToken
    );

  PUT = (internal = "", params, token, userToken, type, pmpToken) =>
    request(
      this.url + internal,
      APIMethods.PUT,
      params,
      token,
      userToken,
      type,
      pmpToken
    );

  DELETE = (internal = "", params, token, userToken, type, pmpToken) =>
    request(
      this.url + internal,
      APIMethods.DELETE,
      params,
      token,
      userToken,
      type,
      pmpToken
    );
}

export default API;
