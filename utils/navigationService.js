import { NavigationActions } from "react-navigation";

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  if (_navigator && _navigator.navigate) {
    _navigator.navigate(routeName, { ...params });
  } else console.log("Error Navigation!!!");
}

// add other navigation functions that you need and export them
export default {
  navigate,
  setTopLevelNavigator,
};
