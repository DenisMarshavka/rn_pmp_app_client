import { AppLoading, Notifications, emitNotification } from "expo";

import { getUserStorageData } from "../../utils";

// Notifications.setNotificationHandler({
//   handleNotification: async () => ({
//     shouldShowAlert: true,
//     shouldPlaySound: false,
//     shouldSetBadge: false,
//   }),
// });

export async function allowsNotificationsAsync() {
  const settings = await Notifications.getPermissionsAsync();

  return (
    settings.granted ||
    settings.ios.status === Notifications.IosAuthorizationStatus.PROVISIONAL
  );
}

export async function requestPermissionsAsync() {
  return await Notifications.requestPermissionsAsync({
    ios: {
      allowAlert: true,
      allowBadge: true,
      allowSound: true,
      allowAnnouncements: true,
    },
  });
}

export function presentNotification(content = "", trigger = null) {
  if (contnet && Object.keys(content).length) {
    Notifications.setNotificationHandler({
      handleNotification: async () => {
        return {
          shouldShowAlert: true,
          shouldPlaySound: true,
          shouldSetBadge: true,
        };
      },
    });

    Notifications.scheduleNotificationAsync({ content, trigger });
  }
}

async function sendPushNotification() {
  const userData = await getUserStorageData();

  if (userData && Object.keys(userData) && Object.keys(userData).length) {
    await Notifications.scheduleNotificationAsync({
      content: {
        title: "You've got mail! 📬",
        body: "Here is the notification body",
        data: { data: "goes here" },
      },
      trigger: { seconds: 2 },
    });
  }
}
