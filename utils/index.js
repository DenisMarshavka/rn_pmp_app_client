import React from "react";
import { Dimensions, Alert, AsyncStorage, Linking } from "react-native";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import { Notifications } from "expo";
import axios from "axios";
import { View, Text } from "react-native";

import NavigationService from "./../utils/navigationService";

const SERVER_URL = config.serverURL;
const PMP_URL = config.pmpURL;

// import NotificationItem from "../screens/Client/Notifications/NotificationItem"
import config from "../utils/config";
import configureStore from "./../reducers";

const store = configureStore();

export const handleNotificationOpen = async (
  type = "",
  data = {},
  isJsonData = false,
  eventAfterOpen = () => {}
) => {
  const userData = await getUserStorageData();
  const receivedData =
    isJsonData && data && data.body
      ? JSON.parse(data.body)
      : data && data.body && Object.keys(data.body).length
      ? data.body
      : data;

  console.log("DAAAATATTAA: ", type, data);

  if (
    /*type &&
    typeof type === "string" &&
    type.trim() &&*/

    data &&
    receivedData &&
    Object.keys(receivedData).length
  ) {
    switch (type) {
      case "NEW_MESSAGES":
        NavigationService.navigate("ChatDialog", {
          trainer: {
            trainer_id: receivedData.id,
            img: receivedData.avatar,
            ...receivedData,
          },
          currentUser: { ...userData },
        });

        break;

      default:
        return false;
    }

    eventAfterOpen();
  }
};

export const formattingPrice = (n = 0, currency = "RUB") => {
  let newFormat = "";

  if (n)
    newFormat = new Intl.NumberFormat("ru", {
      style: "currency",
      currency,
    }).format(n);

  if (newFormat && newFormat.trim()) {
    newFormat = newFormat.slice(0, newFormat.toString().length - 5);
  } else newFormat = "0";

  return newFormat;
};

export const isNotEndContentList = ({
  layoutMeasurement,
  contentOffset,
  contentSize,
}) => {
  const paddingBottomTop = 110;

  return (
    contentSize.height - layoutMeasurement.height - paddingBottomTop <=
    contentOffset.y
  );
};

export const getBlackListProductsToCountChanging = async () =>
  await AsyncStorage.getItem("disableSliderForProducts").then((res) =>
    JSON.parse(res)
  );

export const addToBlackListProductCountChanging = async (id = NaN) => {
  let newList = [];
  const existingList = await getBlackListProductsToCountChanging();

  if (
    !isNaN(id) &&
    existingList &&
    existingList.products &&
    existingList.products.length &&
    !existingList.products.includes(id)
  ) {
    if (
      existingList &&
      Object.keys(existingList).length &&
      existingList.products &&
      existingList.products.length
    ) {
      newList = existingList.products;

      newList.push(id);
    } else newList.push(id);
  } else newList.push(id);

  await AsyncStorage.setItem(
    "disableSliderForProducts",
    JSON.stringify({ products: newList })
  );
};

export const getSizeSkeletonLoaders = (
  offsetTop = 0,
  sizeItem = 0,
  isTabCarousel = false,
  isEasyData = true,
  itemBody = {}
) => {
  let orientation = isTabCarousel ? "width" : "height";

  let resultArray = [];

  if (sizeItem) {
    const sizeScreen = !isTabCarousel
      ? Dimensions.get("screen")[orientation] - offsetTop
      : Dimensions.get("screen")[orientation];
    const sizeOrientations = sizeScreen / sizeItem;
    let countItems = Math.ceil(sizeOrientations);

    for (let item = 0; item <= countItems; item++) {
      resultArray.push(
        isEasyData
          ? { id: item, type: `type=${item}`, ...itemBody, isTest: true }
          : { ...itemBody, isTest: true, id: item }
      );
    }
  }

  return resultArray;
};

export const showAlert = (
  title = "",
  message = "",
  buttons = [],
  options = {}
) => {
  try {
    if (title.trim() && message.trim() && buttons.length) {
      Alert.alert(title, message, [...buttons], {
        cancelable: false,
        ...options,
      });
    }
  } catch (e) {
    console.log("Error showed a Alert: ", e);
  }
};

export const askPermissionNotifications = async () => {
  const userData = await getUserStorageData();

  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;

  if (existingStatus !== "granted") {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }
  if (finalStatus !== "granted") {
    alert("Failed to get push token for push notification!");

    return;
  }

  console.log("YYYYEEEESSS NITIFY");
  if (
    userData &&
    Object.keys(userData).length &&
    (userData.notifications === undefined || userData.notifications === null)
  )
    await AsyncStorage.mergeItem(
      "user",
      JSON.stringify({
        notifications:
          finalStatus === "granted" && existingStatus === "granted",
      })
    );
};

export const generatePushToken = async () => {
  let experienceId = undefined;

  await askPermissionNotifications();

  if (!Constants.manifest) {
    // Absence of the manifest means we're in bare workflow
    experienceId = "@c.amazon/pmpApp";
  }
  const expoPushToken = await Notifications.getExpoPushTokenAsync({
    experienceId,
  });

  console.log("FIRST Token - ", expoPushToken);
  return expoPushToken;
};

export const registerPushNotificationsAsync = async () => {
  console.log("IS DEVICE - ", Constants.isDevice);

  await askPermissionNotifications();
  return await generatePushToken();
};

export const clearUserDataAndAccountExit = async (
  onSetCurrentUser = () => {}
) => {
  const blackListChangingCountProducts = await getBlackListProductsToCountChanging();

  await AsyncStorage.removeItem("user");
  await AsyncStorage.clear();

  if (blackListChangingCountProducts)
    await AsyncStorage.setItem(
      "disableSliderForProducts",
      JSON.stringify(blackListChangingCountProducts)
    );

  store.dispatch(onSetCurrentUser(null));
};

export const validURL = (str) => {
  var pattern = new RegExp(
    "^(https?:\\/\\/)?" +
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" +
      "((\\d{1,3}\\.){3}\\d{1,3}))" +
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" +
      "(\\?[;&a-z\\d%_.~+=-]*)?" +
      "(\\#[-a-z\\d_]*)?$",
    "i"
  );

  return !!pattern.test(str);
};

export const generateQueryWithParams = (
  url = "",
  params = {},
  type = "pmp"
) => {
  let requestUrl = "";
  let queryParams = "?";

  if (type === "pmp") {
    requestUrl += PMP_URL;
  } else if (type === "yClient") requestUrl += SERVER_URL;

  requestUrl += url;

  if (url.trim() && Object.keys(params).length) {
    let currentIndex = 0;

    for (let field in params) {
      queryParams +=
        currentIndex === 0
          ? `${field}=${params[field]}`
          : `&${field}=${params[field]}`;
      currentIndex++;
    }

    requestUrl += queryParams;
  }

  return requestUrl;
};

export const getUserStorageData = async (dataName = "user") => {
  let userData = await AsyncStorage.getItem(dataName);

  if (
    userData &&
    JSON.parse(userData) &&
    Object.keys(JSON.parse(userData)).length
  ) {
    userData = JSON.parse(userData);
  }

  return userData;
};

export const getDataRequestStart = async (
  url = "",
  params = {},
  method = "get"
) => {
  let data = await AsyncStorage.getItem("user");
  const headers = { Authorization: `Bearer ${JSON.parse(data).token}` };

  console.log("HEEEEEEEEEEEEEEEE", data, "HHHEEEEEEE222222", headers);

  let response = "";

  if (JSON.parse(data).token && url.trim()) {
    if (method === "get") {
      response = await axios[method](`${url}`, {
        headers,
      }).then((result) => {
        console.log("resultresultresultresultresult", result);
        return result.data;
      });
    } else if (method === "post") {
      response = await axios[method](
        `${url}`,
        { ...params },
        {
          headers,
        }
      ).then((result) => {
        console.log("resultresultresultresultresult222222", result);
        return result.data;
      });
    }
  }

  console.log("resultresultresultresultresult 2222222333333333", response);

  return response;
};

export const checkAsyncStorageUserByData = async (
  paramNameCheck = "",
  setCheckingProgress = () => {},
  setCheckedData = () => {},
  getGeneratePaymentList = () => {},
  fromFunctionalComponent = true,
  isPayment = true
) => {
  if (paramNameCheck.trim()) {
    const getCurrentBasketState = async () =>
      await AsyncStorage.getItem("user");

    if (fromFunctionalComponent) setCheckingProgress(false);

    return new Promise((resolve, reject) => {
      try {
        getCurrentBasketState().then((userData) => {
          const currentBasketState =
            JSON.parse(userData)[paramNameCheck] &&
            JSON.parse(userData)[paramNameCheck].length
              ? JSON.parse(userData)[paramNameCheck]
              : [];

          if (currentBasketState.length) {
            if (fromFunctionalComponent) {
              setCheckedData([...new Set(currentBasketState)]);
            } else resolve(currentBasketState);

            if (fromFunctionalComponent) setCheckingProgress(true);

            if (currentBasketState.length && isPayment)
              getGeneratePaymentList(currentBasketState);
          } else {
            if (fromFunctionalComponent) setCheckingProgress(true);

            resolve([]);
          }
        });
      } catch (e) {
        reject(e);
      }
    });
  } else
    throw new Error(
      'The property "paramNameCheck" is not valid, "paramNameCheck" -- ',
      paramNameCheck
    );
};

export const padding = (a, b, c, d) => ({
  paddingTop: a,
  paddingRight: b ? b : a,
  paddingBottom: c ? c : a,
  paddingLeft: d ? d : b ? b : a,
});

export const handleLinkOpen = async (
  link = "",
  withMyCallBack = false,
  callBack = () => {}
) => {
  const supported = await Linking.canOpenURL(link);

  if (supported && link.trim()) {
    if (withMyCallBack) {
      callBack();
    } else await Linking.openURL(link);
  } else
    showAlert(
      `Ошибка`,
      "Не удаль открыть страницу :( Произошла ошибка.\nПожалуйста, обратитесь в поддержку по телефону +7 495 797 94 27",
      [
        {
          title: "Ок",
          onPress: () => {},
          style: "error",
        },
      ]
    );
};

export const formatPhone = (phone = "") => {
  if (phone && typeof phone === "string" && phone.length <= 16) {
    phone = phone.replace("+7", "").replace(/[^\d]/g, "");

    if (phone.length >= 4 && phone.length <= 6) {
      return phone.replace(/(\d{3})(\d)/, "+7 $1 $2");
    }

    if (phone.length >= 7 && phone.length <= 8) {
      console.log(phone);
      return phone.replace(/(\d{3})(\d{3})(\d)/, "+7 $1 $2 $3");
    }

    if (phone.length >= 8 && phone.length <= 10) {
      return phone.replace(/(\d{3})(\d{3})(\d{2})(\d)/, "+7 $1 $2 $3 $4");
    }

    console.log("phone", phone);

    return phone[0] === "7" ? "+" + phone : "+7 " + phone;
  }

  return phone.slice(0, 16);
};

export const renderCountNotification = (length = 0, style = {}) => (
  <View
    style={{
      position: "absolute",
      zIndex: 2,
      top: 0,
      right: -5,
      height: 20,
      width: 20,
      borderRadius: 20,
      justifyContent: "center",
      alignItems: "center",
      borderWidth: 3,
      borderColor: "#EBE9E8",
      backgroundColor: "#E02323",
      ...style,
    }}
  >
    <Text
      style={{
        color: "#fff",
        fontSize:
          length < 100 && length < 10 ? 8 : length < 100 && length > 10 ? 6 : 5,
        fontWeight: "bold",
        textAlign: "center",
      }}
    >
      {length}
    </Text>
  </View>
);
