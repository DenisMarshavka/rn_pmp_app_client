import React from "react";
import { connect } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
// import { createAppContainer } from "react-navigation";
// import NavigationService from "./../utils/navigationService";

import {
  TestScreen,
  RegAuthScreen,
  LoadingScreen,
  HomeScreen,
  LoginScreen,
  MainMenuScreen,
  NewsScreen,
  ProfileScreen,
  ArticleScreen,
  AboutScreen,
  ArticleDetailsScreen,
  BasketScreen,
  ItemBasketProductScreen,
  MenuListScreen,
  PaymentScreen,
  HistoryScreen,
  ChatScreen,
  CreateReview,
  ReviewsScreen,
  NotificationScreen,
  Points,
  DiaryResultScreen,
  AddWeightMeasurementScreen,
  ChatDialogScreen,
  AddBodyMeasurementScreen,
  ListServiceScreen,
  ListProductScreen,
  ItemServiceScreen,
  ItemSpecialistScreen,
  BeautyStudioScreen,
  FitnessScreen,
  FitnessStudioScreen,
  StatisticsScreen,
  ScheduleScreen,
  FoodDiaryScreen,
  AddDishScreen,
  CertificateScreen,
  SelectServiceDateScreen,
  SubmitServiceScreen,
  PersonalDataScreen,
  ConfirmPhoneScreen,
  ConfirmSmsCodeScreen,
  ConfirmPasswordScreen,
  SettingsScreen,
  MapScreen,
  MyProgramScreen,
  CertificatesScreen,
  ProgramScreen,
  DetoxProgramScreen,
  ProgramsScreen,
  NewProducts,
  Coaches,
  Specialists,
  EmployeeCard,
  ListServiceSpecialists,
  StudioSelection,
  Program,
  VisitingHistory,
  SupportScreen,
  PrivacyScreen,
} from "../screens";
import OnBoardings from "../screens/Auth/OnBoardings";
import NavigationService from "./../utils/navigationService";

const Stack = createStackNavigator();

const config = {
  animation: "spring",
  config: {
    stiffness: 1000,
    damping: 500,
    mass: 3,
    overshootClamping: false,
  },
};

function AppNavigator(props) {
  const auth = props.auth;
  const load = props.load;
  const currentUser = props.currentUser;

  console.log("NAVIGATOR USER", currentUser);

  return (
    <NavigationContainer
      ref={(ref) => NavigationService.setTopLevelNavigator(ref)}
    >
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          gestureEnabled: true,
        }}
      >
        {!load /* true*/ ? (
          <Stack.Screen
            name="Loading"
            component={LoadingScreen}
            options={{ headerShown: false, headerLeft: null }}
          />
        ) : currentUser !== null ? (
          <>
            <Stack.Screen
              name="OnBoardings"
              component={OnBoardings}
              options={{
                headerShown: false,
                gestureEnabled: false,
                headerLeft: null,
              }}
            />

            <Stack.Screen
              name="Home"
              component={HomeScreen}
              options={{
                headerShown: false,
                headerLeft: null,
                visible: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
                ...TransitionPresets.SlideFromRightIOS,
              }}
            />
            <Stack.Screen
              name="MainMenu"
              component={MainMenuScreen}
              options={{
                headerShown: false,
                headerLeft: null,
                gestureEnabled: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="News"
              component={NewsScreen}
              options={{
                headerShown: false,
                headerLeft: null,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="Profile"
              component={ProfileScreen}
              options={{
                headerShown: false,
                headerLeft: null,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="Article"
              component={ArticleScreen}
              options={{
                headerShown: false,
                headerLeft: null,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="ArticleDetails"
              component={ArticleDetailsScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="About"
              component={AboutScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="Basket"
              component={BasketScreen}
              options={{
                headerShown: false,
                gestureEnabled: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="HistoryOrders"
              component={HistoryScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="Payment"
              component={PaymentScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="History"
              component={HistoryScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="ItemBasketProduct"
              component={ItemBasketProductScreen}
              options={{
                gesturesEnabled: false,
                swipeEnabled: false,
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="MenuList"
              component={MenuListScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />

            <Stack.Screen
              name="Chat"
              component={ChatScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="CreateReview"
              component={CreateReview}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="Reviews"
              component={ReviewsScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="Notification"
              component={NotificationScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />

            <Stack.Screen
              name="Points"
              component={Points}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />

            <Stack.Screen
              name="DiaryResult"
              component={DiaryResultScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />

            <Stack.Screen
              name="VisitingHistory"
              component={VisitingHistory}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />

            <Stack.Screen
              name="AddWeightMeasurement"
              component={AddWeightMeasurementScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
                gestureEnabled: false,
              }}
            />

            <Stack.Screen
              name="ChatDialog"
              component={ChatDialogScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />

            <Stack.Screen
              name="AddBodyMeasurement"
              component={AddBodyMeasurementScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />

            <Stack.Screen
              name="ListServices"
              component={ListServiceScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="ListProducts"
              component={ListProductScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="ItemService"
              component={ItemServiceScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="ItemSpecialist"
              component={ItemSpecialistScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="ProgramScreen"
              component={Program}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="DetoxProgramScreen"
              component={DetoxProgramScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="BeautyStudio"
              component={BeautyStudioScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="FitnessStudio"
              component={FitnessStudioScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="Statistics"
              component={StatisticsScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="Schedule"
              component={ScheduleScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="FoodDiary"
              component={FoodDiaryScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="AddDish"
              component={AddDishScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="Certificate"
              component={CertificateScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="SelectServiceDate"
              component={SelectServiceDateScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="SubmitService"
              component={SubmitServiceScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />
            <Stack.Screen
              name="ListServiceSpecialists"
              component={ListServiceSpecialists}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />

            <Stack.Screen
              name="StudioSelection"
              component={StudioSelection}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />

            <Stack.Screen
              name="PersonalData"
              component={PersonalDataScreen}
              options={{
                headerShown: false,
                transitionSpec: {
                  open: config,
                  close: config,
                },
              }}
            />

            <Stack.Screen
              name="Settings"
              component={SettingsScreen}
              options={{
                headerShown: false,
                headerLeft: null,
              }}
            />

            <Stack.Screen
              name="Support"
              component={SupportScreen}
              options={{
                headerShown: false,
                headerLeft: null,
              }}
            />

            <Stack.Screen
              name="Privacy"
              component={PrivacyScreen}
              options={{
                headerShown: false,
                headerLeft: null,
              }}
            />

            <Stack.Screen
              name="Map"
              component={MapScreen}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="MyProgram"
              component={MyProgramScreen}
              options={{
                headerShown: false,
              }}
            />

            <Stack.Screen
              name="CertificatesList"
              component={CertificatesScreen}
              options={{
                headerShown: false,
              }}
            />

            <Stack.Screen
              name="Program"
              component={ProgramScreen}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="ProgramsScreen"
              component={ProgramsScreen}
              options={{
                headerShown: false,
              }}
            />
            {/* <Stack.Screen*/}
            {/*  name="NewProducts"*/}
            {/*  component={NewProducts}*/}
            {/*  options={{*/}
            {/*    // header: (props) => <Header {...props} />,*/}
            {/*    headerShown: false,*/}
            {/*  }}*/}
            {/*/>*/}

            <Stack.Screen
              name="Specialists"
              component={Specialists}
              options={{
                // header: (props) => <Header {...props} />,
                headerShown: false,
              }}
            />

            {/*<Stack.Screen*/}
            {/*  name="Coaches"*/}
            {/*  component={Coaches}*/}
            {/*  options={{*/}
            {/*    // header: (props) => <Header {...props} />,*/}
            {/*    headerShown: false,*/}
            {/*  }}*/}
            {/*/>*/}
            <Stack.Screen
              name="EmployeeCard"
              component={EmployeeCard}
              options={{
                // header: (props) => <Header {...props} />,
                headerShown: false,
              }}
            />
          </>
        ) : (
          <>
            <Stack.Screen
              name="Login"
              component={LoginScreen}
              options={{
                headerShown: false,
                headerLeft: null,
              }}
            />
            <Stack.Screen
              name="RegAuth"
              component={RegAuthScreen}
              options={{
                headerShown: false,
                headerLeft: null,
              }}
            />
            <Stack.Screen
              name="ConfirmPhone"
              component={ConfirmPhoneScreen}
              options={{
                headerShown: false,
                headerLeft: null,
              }}
            />
            <Stack.Screen
              name="ConfirmSmsCode"
              component={ConfirmSmsCodeScreen}
              options={{
                headerShown: false,
                headerLeft: null,
              }}
            />
            <Stack.Screen
              name="ConfirmPassword"
              component={ConfirmPasswordScreen}
              options={{
                headerShown: false,
                headerLeft: null,
              }}
            />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

// const AppContainer = createAppContainer(Stack);

const mapStateToProps = (state) => {
  return {
    currentUser: state.currentUser.currentUser,
  };
};

export default connect(mapStateToProps, null)(AppNavigator);

// export default createAppContainer(
//   createSwitchNavigator({
//     // You could add another route here for authentication.
//     // Read more at https://reactnavigation.org/docs/en/auth-flow.html
//     Main: MainTabNavigator,
//     // Drawer: DrawerNavigator,
//   })
// );
