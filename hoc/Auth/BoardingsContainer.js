import React, { useState, useRef } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  SafeAreaView,
  ScrollView,
  Dimensions,
  Animated,
  Easing,
} from "react-native";

import ButtonInput from "../../components/button/ButtonInput";
import colors from "../../styles/colors";
import { validURL } from "../../utils";

const BoardingsContainer = (props) => {
  const {
    headTextStyle = {},
    buttonStyle = {},
    arrowDescriptionTextStyle = {},
    illustrationStyle = {},

    animateMoveDuration = 200,
    buttonText = "",
    colorDots = "#fff",
    colorDotsActive = "#000",
    backgroundColor = "#fff",
    startIndexBoard = 0,
    boardings = [],
    cursor = false,
    cursorImage = "",
    onEnd = () => {},
    skipText = "",
  } = props;

  const [_boardIndexActive, _setBoardIndexActive] = useState(startIndexBoard);
  const [_upproveMoveBoard, _setUpproveMoveBoard] = useState(true);
  const currentBoard = boardings[_boardIndexActive];

  const wrapBoardMoveValue = useRef(new Animated.Value(0)).current;

  const renderBoardContent = () => (
    <View style={{ flex: 1, position: "relative", zIndex: 5 }}>
      {currentBoard && currentBoard.imgSrc && (
        <Image
          source={
            currentBoard &&
            currentBoard.imgSrc &&
            currentBoard.imgSrc.trim() &&
            validURL(currentBoard.imgSrc)
              ? { uri: currentBoard.imgSrc }
              : require("../../assets/images/boardings/board_1.png")
          }
          style={[styles.illustration, { ...illustrationStyle }]}
        />
      )}

      {cursor &&
        cursorImage &&
        currentBoard &&
        currentBoard.arrowPosition &&
        currentBoard.arrowPosition[0] !== false &&
        currentBoard.arrowPosition[1] !== false &&
        currentBoard.arrowDescriptionPosition &&
        currentBoard.arrowDescriptionPosition[0] !== false &&
        currentBoard.arrowDescriptionPosition[1] !== false && (
          <>
            <View
              style={[
                currentBoard.transformStyles &&
                currentBoard.transformStyles.transform &&
                currentBoard.transformStyles.transform.length
                  ? { ...currentBoard.transformStyles }
                  : {},

                {
                  position: "absolute",
                  left: currentBoard.arrowPosition[0],
                  top: currentBoard.arrowPosition[1],
                },
              ]}
            >
              {cursorImage}
            </View>

            <Text
              style={[
                styles.arrowDescriptionText,
                {
                  left: currentBoard.arrowDescriptionPosition[0],
                  top: currentBoard.arrowDescriptionPosition[1],
                },
                { ...arrowDescriptionTextStyle },
              ]}
            >
              {currentBoard.arrowDescription}
            </Text>
          </>
        )}
    </View>
  );

  const renderBoardingsDots = () => {
    const dots = boardings.map((_, i) => (
      <TouchableOpacity key={i} onPress={() => onBoardMove(i, true)}>
        <View
          style={[
            styles.dots,

            {
              backgroundColor:
                i === +_boardIndexActive ? colorDotsActive : colorDots,
            },
            { marginRight: i + 1 !== boardings.length ? 10 : 0 },
          ]}
        />
      </TouchableOpacity>
    ));

    return <View style={styles.parentDots}>{dots}</View>;
  };

  const onBoardMove = (moveToBoardIndex = false, isTouchFromDot = false) => {
    if (boardings[_boardIndexActive + 1] || isTouchFromDot) {
      if (_upproveMoveBoard) {
        _setUpproveMoveBoard(false);
        changeBackdropAnimateDisplaying();

        setTimeout(() => {
          _setBoardIndexActive(
            moveToBoardIndex === false
              ? _boardIndexActive + 1
              : moveToBoardIndex
          );

          changeBackdropAnimateDisplaying(0);
          _setUpproveMoveBoard(true);
        }, 550);
      }
    } else onEnd();
  };

  const changeBackdropAnimateDisplaying = (valueToAnimate = 1) => {
    Animated.timing(wrapBoardMoveValue, {
      easing: Easing.linear,
      toValue: valueToAnimate,
      duration: animateMoveDuration,
      useNativeDriver: true,
    }).start();
  };

  return (
    <SafeAreaView style={{ backgroundColor: backgroundColor }}>
      <View style={[styles.container, { backgroundColor: backgroundColor }]}>
        <ScrollView
          style={styles.scrollWrapper}
          showsVerticalScrollIndicator={false}
        >
          <Text style={[styles.headText, { ...headTextStyle }]}>
            {currentBoard && currentBoard.headText
              ? currentBoard && currentBoard.headText
              : ""}
          </Text>

          <View
            style={[
              styles.contentBox,
              {
                minHeight:
                  illustrationStyle && illustrationStyle.height
                    ? illustrationStyle.height
                    : 300,
              },
            ]}
          >
            <Animated.View
              style={[
                styles.backdropAnimate,
                { backgroundColor, opacity: wrapBoardMoveValue },
              ]}
            />

            {renderBoardContent()}
          </View>

          <View style={styles.navigation}>
            <View style={{ width: "100%" }}>
              <ButtonInput
                title={buttonText || "NEXT"}
                style={buttonStyle}
                onClick={onBoardMove}
              />
            </View>

            {skipText && (
              <TouchableOpacity onPress={onEnd}>
                <Text style={styles.skipButton}>{skipText}</Text>
              </TouchableOpacity>
            )}

            {renderBoardingsDots()}
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default BoardingsContainer;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    height: "100%",
    minHeight: Dimensions.get("window").height,
  },
  scrollWrapper: {
    position: "relative",
    paddingTop: 32,
    paddingBottom: 36,

    width: "100%",
    height: "100%",
  },
  headText: {
    textAlign: "center",
    lineHeight: 28,
    fontSize: 18,

    alignSelf: "center",
  },
  contentBox: {
    position: "relative",
    flex: 1,
    height: "100%",
    width: "100%",
    marginTop: 34,
  },
  backdropAnimate: {
    position: "absolute",
    top: 0,
    left: 0,
    zIndex: 10,
    height: "100%",
    width: "100%",
  },
  illustration: {
    resizeMode: "cover",
  },
  arrowDescriptionText: {
    position: "absolute",
    top: 0,
    right: 0,

    maxWidth: 90,
    flexWrap: "wrap",
    flexShrink: 1,

    textAlign: "center",
    fontSize: 14,
    lineHeight: 17,
  },
  navigation: {
    paddingBottom: 90,
    marginTop: 29,

    alignItems: "center",
  },
  skipButton: {
    marginTop: 17,

    color: colors.mainBlack,
    fontSize: 14,
    textDecorationLine: "underline",
    opacity: 0.55,

    alignSelf: "center",
  },
  parentDots: {
    marginTop: 31,
    height: 8,

    flexDirection: "row",
    justifyContent: "center",
  },
  dots: {
    width: 8,
    height: 8,

    borderRadius: 8,
  },
});
