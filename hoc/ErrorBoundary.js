import React from "react";
// import { Text, View } from "react-native";
import * as Sentry from "sentry-expo";
// import { Button } from "react-native-elements";
// import Expo from "expo";

import { ApplicationsAPI } from "../api/applications";
// import styles from "./../components/button/ButtonInput.styles";

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);

    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    console.log(error, "errorInfo", error);

    if (
      !toString(error).includes(
        "Warning: Please report: Excessive number of pending callbacks"
      ) &&
      !toString(error.TypeError).includes(
        "Warning: Please report: Excessive number of pending callbacks"
      )
    ) {
      ApplicationsAPI.setError({
        message:
          error && error.TypeError
            ? toString(error.TypeError)
            : toString(error),
      });

      Sentry.captureException(error, errorInfo);
    }
  }

  render() {
    return <>{this.props.children || null}</>;
  }
}

export default ErrorBoundary;
