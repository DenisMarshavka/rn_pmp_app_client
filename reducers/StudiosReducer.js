import {
  GET_STUDIOS_BY_SERVICE_REQUEST_FAILED,
  GET_STUDIOS_BY_SERVICE_REQUEST_START,
  GET_STUDIOS_BY_SERVICE_REQUEST_SUCCESS,
  CLEAN_STUDIOS_BY_SERVICE,
} from "../actions/StudiosActions";

const INITIAL_STATE = {
  loading: true,
  studios: [],
  count: 0,
  error: null,
};

const StudiosReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_STUDIOS_BY_SERVICE_REQUEST_START:
      return {
        ...state,
        loading: true,
        error: null,
        studios: [],
        count: 0,
      };

    case GET_STUDIOS_BY_SERVICE_REQUEST_SUCCESS:
      return {
        ...state,
        loading: false,
        studios: [...action.payload.list],
        count: action.payload.countItems,
      };

    case GET_STUDIOS_BY_SERVICE_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case CLEAN_STUDIOS_BY_SERVICE:
      return {
        ...state,
        loading: true,
        error: null,
        studios: [],
      };

    default:
      return state;
  }
};

export default StudiosReducer;
