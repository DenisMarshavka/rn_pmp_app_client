import {
  GET_BASKET_FAILED,
  GET_BASKET_START,
  GET_BASKET_SUCCESS,
  GET_BASKET_DATA_RESET,
  GET_HISTORY_PAYMENTS_FAILED,
  GET_HISTORY_PAYMENTS_START,
  GET_HISTORY_PAYMENTS_SUCCESS,
  CLEAN_HISTORY_PAYMETS,
  // GET_HISTORY_PAYMENTS_PRODUCTS_START,
  // GET_HISTORY_PAYMENTS_PRODUCTS_SUCCESS,
  // GET_HISTORY_PAYMENTS_PRODUCTS_FAILED,
} from "../actions/BasketActions";

const INITIAL_STATE = {
  basketLoading: true,
  basket: [],
  listProductsId: [],
  priceAll: 0,
  basketListMeta: {},
  moreBasketListDataLoading: false,
  countBasketItems: 0,
  basketFailed: null,

  historyLoading: true,
  history: [],
  moreHistoryListDataLoading: false,
  historyListMeta: {},
  historyFailed: null,

  // historyProductsLoading: false,
  // historyProducts: [],
  // historyProductsFailed: null,
};

const BasketReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_BASKET_DATA_RESET:
      return {
        ...state,
        basketLoading: true,
        basketListMeta: {},
        moreBasketListDataLoading: false,
        basketFailed: null,
        listProductsId: [],
        priceAll: 0,
      };

    case GET_BASKET_START:
      return {
        ...state,
        basketLoading: !action.payload.isMore,
        basketFailed: null,
        countBasketItems: 0,
        moreBasketListDataLoading: action.payload.isMore,
      };

    case GET_BASKET_SUCCESS:
      const basket = action.payload.isMore
        ? [...state.basket, ...action.payload.list]
        : [...action.payload.list];

      console.log(
        "BASKET LIST META: ",
        action.payload.listMeta,
        "PRODUCTS IDDDSSS: ",
        action.payload.listMeta.list_products_id
      );

      const newBasketSate = {
        ...state,
        basket,
        countBasketItems: action.payload.countItems,
        moreBasketListDataLoading: false,
        basketListMeta: { ...action.payload.listMeta },
        listProductsId: [...action.payload.listMeta.list_products_id],
        priceAll: action.payload.listMeta.priceAll,
      };

      if (action.payload.updateBasketList) newBasketSate.basketLoading = false;

      return {
        ...newBasketSate,
      };

    case GET_BASKET_FAILED:
      return {
        ...state,
        basketLoading: false,
        moreBasketListDataLoading: false,
        basketFailed: action.payload,
        basketListMeta: {},
        listProductsId: [],
        priceAll: 0,
      };

    case GET_HISTORY_PAYMENTS_START:
      return {
        ...state,
        historyLoading: !action.payload.isUpdate,
        moreHistoryListDataLoading: action.payload.isUpdate,
        historyFailed: null,
      };

    case GET_HISTORY_PAYMENTS_SUCCESS:
      const history = action.payload.isUpdate
        ? [...state.history, ...action.payload.list]
        : [...action.payload.list];

      return {
        ...state,
        historyLoading: false,
        moreHistoryListDataLoading: false,
        history,
        historyListMeta: { ...action.payload.listMeta },
      };

    case GET_HISTORY_PAYMENTS_FAILED:
      return {
        ...state,
        historyLoading: false,
        moreHistoryListDataLoading: false,
        historyListMeta: {},
        historyFailed: action.payload,
      };

    case CLEAN_HISTORY_PAYMETS:
      return {
        ...state,
        historyLoading: false,
        history: [],
        moreHistoryListDataLoading: false,
        historyListMeta: {},
        historyFailed: null,
      };

    // case GET_HISTORY_PAYMENTS_PRODUCTS_START:
    //   return {
    //     ...state,
    //     historyProductsLoading: true,
    //     historyProducts: [],
    //     historyProductsFailed: null,
    //   };
    //
    // case GET_HISTORY_PAYMENTS_PRODUCTS_SUCCESS:
    //   return {
    //     ...state,
    //     historyProductsLoading: false,
    //     historyProducts: { ...action.payload },
    //   };
    //
    // case GET_HISTORY_PAYMENTS_PRODUCTS_FAILED:
    //   return {
    //     ...state,
    //     historyProductsLoading: false,
    //     historyProductsFailed: action.payload,
    //   };

    default:
      return state;
  }
};

export default BasketReducer;
