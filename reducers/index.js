import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import CurrentUserReducer from "./CurrentUserReducer";
import SalonsReducer from "./SalonsReducer";
import MainReducer from "./MainReducer";
import { NewsReducer } from "./NewsReducer";
import ProgramsReducer from "./ProgramsReducer";
import ServicesReducer from "./ServicesReducer";
import { ScheduleReducer } from "./ScheduleReducer";
import StudiosReducer from "./StudiosReducer";
import TrainersReducer from "./TrainersReducer";
import StaffReducer from "./StaffReducer";
import StoreReducer from "./StoreReducer";
import ReviewsReducer from "./ReviewsReducer";
import CertificatesReducer from "./CertificatesReducer";
import BasketReducer from "./BasketReducer";
import InstructionsReducer from "./InstructionsReducer";

const rootReducer = combineReducers({
  currentUser: CurrentUserReducer,
  salonsReducer: SalonsReducer,
  mainReducer: MainReducer,
  ProgramsReducer,
  ServicesReducer,
  NewsReducer,
  ScheduleReducer,
  StudiosReducer,
  TrainersReducer,
  StaffReducer,
  StoreReducer,
  ReviewsReducer,
  CertificatesReducer,
  BasketReducer,
  InstructionsReducer,
});

const configureStore = () => {
  return createStore(rootReducer, applyMiddleware(thunk));
};

export default configureStore;
