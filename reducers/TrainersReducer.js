import {
  GET_TRAINERS_REQUEST_FAILED,
  GET_TRAINERS_REQUEST_START,
  GET_TRAINERS_REQUEST_SUCCESS,
  GET_CURRENT_TRAINER_REQUEST_START,
  GET_CURRENT_TRAINER_REQUEST_SUCCESS,
  GET_CURRENT_TRAINER_REQUEST_FAILED,
} from "../actions/TrainersActions";

const INITIAL_STATE = {
  loading: false,
  trainers: [],
  error: null,

  currentTrainerLoading: true,
  currentTrainer: {},
  currentTrainerError: null,
};

const TrainersReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_CURRENT_TRAINER_REQUEST_START:
      return {
        ...state,
        currentTrainerLoading: true,
        currentTrainerError: null,
      };

    case GET_CURRENT_TRAINER_REQUEST_SUCCESS:
      return {
        ...state,
        currentTrainerLoading: false,
        currentTrainer: { ...action.payload },
        currentTrainerError: null,
      };

    case GET_CURRENT_TRAINER_REQUEST_FAILED:
      return {
        ...state,
        currentTrainerLoading: false,
        currentTrainer: {},
        currentTrainerError: action.payload,
      };

    case GET_TRAINERS_REQUEST_START:
      return {
        ...state,
        loading: true,
        trainers: [],
        error: null,
      };

    case GET_TRAINERS_REQUEST_SUCCESS:
      return {
        ...state,
        loading: false,
        trainers: [...action.payload],
      };

    case GET_TRAINERS_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    default:
      return state;
  }
};

export default TrainersReducer;
