import {
  GET_NEW_PROGRAMS_FAILED,
  GET_NEW_PROGRAMS_START,
  GET_NEW_PROGRAMS_SUCCESS,
  CLEAR_NEW_PROGRAMS_DATA,
  GET_PROGRAM_BY_ID_FAILED,
  GET_PROGRAM_BY_ID_START,
  GET_PROGRAM_BY_ID_SUCCESS,
  GET_PROGRAMS_REQUEST_FAILED,
  GET_PROGRAMS_REQUEST_START,
  GET_PROGRAMS_REQUEST_SUCCESS,
  GET_SPECIAL_PROGRAMS_FAILED,
  GET_SPECIAL_PROGRAMS_START,
  GET_SPECIAL_PROGRAMS_SUCCESS,
  CLEAR_SPECIAL_PROGRAMS_DATA,
  GET_STEP_INFO_PROGRAM_START,
  GET_STEP_INFO_PROGRAM_SUCCESS,
  GET_STEP_INFO_PROGRAM_FAILED,
  GET_PROGRAM_SECTIONS_BY_ID_START,
  GET_PROGRAM_SECTIONS_BY_ID_SUCCESS,
  GET_PROGRAM_SECTIONS_BY_ID_FAILED,
  GET_FOOD_DIARY_START,
  GET_FOOD_DIARY_SUCCESS,
  GET_FOOD_DIARY_FAILED,
  GET_PROGRAM_DISHED_START,
  GET_PROGRAM_DISHED_SUCCESS,
  GET_PROGRAM_DISHED_FAILED,
} from "../actions/ProgramsActions";

const INITIAL_STATE = {
  programLoading: false,
  programData: {},
  programError: null,

  programSectionLoading: false,
  programSectionData: {},
  programSectionError: null,

  programsLoading: false,
  programsData: [],
  programsError: null,

  programStepInfoLoading: false,
  programStepInfoData: {},
  programStepInfoError: null,

  specialProgramsLoading: true,
  specialProgramsData: [],
  specialProgramsError: null,
  specialProgramsListMeta: {},
  moreSpecialProgramsLoading: false,

  newProgramsLoading: true,
  newProgramsData: [],
  newProgramsError: null,
  newProgramsDataListMeta: {},
  moreNewProgramsLoading: false,

  recommendedDishesLoading: true,
  recommendedDishesData: {},
  recommendedDishesError: null,

  diaryDataLoading: false,
  foodDiaryData: [],
  diaryDataError: null,
};

const ProgramsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_PROGRAM_BY_ID_START:
      return {
        ...state,
        programLoading: true,
        programData: [],
        programError: null,
      };

    case GET_PROGRAM_BY_ID_SUCCESS:
      return {
        ...state,
        programLoading: false,
        programData: { ...action.payload },
      };

    case GET_PROGRAM_BY_ID_FAILED:
      return {
        ...state,
        programLoading: false,
        programError: action.payload,
      };

    case GET_PROGRAM_SECTIONS_BY_ID_START:
      return {
        ...state,
        programSectionLoading: true,
        programSectionData: [],
        programSectionError: null,
      };

    case GET_PROGRAM_SECTIONS_BY_ID_SUCCESS:
      return {
        ...state,
        programSectionLoading: false,
        programSectionData: { ...action.payload },
      };

    case GET_PROGRAM_SECTIONS_BY_ID_FAILED:
      return {
        ...state,
        programSectionLoading: false,
        programSectionError: action.payload,
      };

    case GET_PROGRAMS_REQUEST_START:
      return {
        ...state,
        programsLoading: true,
      };

    case GET_PROGRAMS_REQUEST_SUCCESS:
      return {
        ...state,
        programsLoading: false,
        programsData: [...action.payload.list],
        countItems: action.payload.countItems,
      };

    case GET_PROGRAMS_REQUEST_FAILED:
      return {
        ...state,
        programsLoading: false,
        programsError: action.payload,
      };

    case GET_STEP_INFO_PROGRAM_START:
      return {
        ...state,
        programStepInfoLoading: true,
        programStepInfoError: null,
      };

    case GET_STEP_INFO_PROGRAM_SUCCESS:
      return {
        ...state,
        programStepInfoLoading: false,
        programStepInfoData: { ...action.payload },
        countItems: action.payload.countItems,
      };

    case GET_STEP_INFO_PROGRAM_FAILED:
      return {
        ...state,
        programStepInfoLoading: false,
        programStepInfoError: action.payload,
      };

    case GET_SPECIAL_PROGRAMS_START:
      return {
        ...state,
        specialProgramsLoading: !action.payload.isUpdate,
        moreSpecialProgramsLoading: action.payload.isUpdate,
      };

    case GET_SPECIAL_PROGRAMS_SUCCESS:
      const specialProgramsData = action.payload.isUpdate
        ? [...state.specialProgramsData, ...action.payload.list]
        : [...action.payload.list];

      return {
        ...state,
        specialProgramsLoading: false,
        specialProgramsData,
        specialProgramsListMeta: { ...action.payload.listMeta },
        moreSpecialProgramsLoading: false,
      };

    case CLEAR_SPECIAL_PROGRAMS_DATA:
      return {
        ...state,
        specialProgramsLoading: false,
        specialProgramsError: null,
        specialProgramsData: [],
        specialProgramsListMeta: {},
        moreSpecialProgramsLoading: false,
      };

    case GET_SPECIAL_PROGRAMS_FAILED:
      return {
        ...state,
        specialProgramsLoading: false,
        specialProgramsError: action.payload,
        moreSpecialProgramsLoading: false,
      };

    case GET_NEW_PROGRAMS_START:
      return {
        ...state,
        newProgramsLoading: !action.payload.isUpdate,
        moreNewProgramsLoading: action.payload.isUpdate,
      };

    case GET_NEW_PROGRAMS_SUCCESS:
      const newProgramsData = action.payload.isUpdate
        ? [...state.newProgramsData, ...action.payload.list]
        : [...action.payload.list];

      return {
        ...state,
        newProgramsLoading: false,
        newProgramsData,
        newProgramsDataListMeta: { ...action.payload.listMeta },
        moreNewProgramsLoading: false,
      };

    case CLEAR_NEW_PROGRAMS_DATA:
      return {
        ...state,
        newProgramsData: [],
        newProgramsDataListMeta: {},
        newProgramsLoading: false,
        moreNewProgramsLoading: false,
        newProgramsError: null,
      };

    case GET_NEW_PROGRAMS_FAILED:
      return {
        ...state,
        newProgramsLoading: false,
        newProgramsError: action.payload,
        newProgramsDataListMeta: {},
        moreNewProgramsLoading: false,
      };

    case GET_FOOD_DIARY_START:
      return {
        ...state,
        diaryDataLoading: true,
        foodDiaryData: [],
        diaryDataError: null,
      };

    case GET_FOOD_DIARY_SUCCESS:
      return {
        ...state,
        diaryDataLoading: false,
        foodDiaryData: action.payload,
      };

    case GET_FOOD_DIARY_FAILED:
      return {
        ...state,
        diaryDataLoading: false,
        diaryDataError: action.payload,
      };

    case GET_PROGRAM_DISHED_START:
      return {
        ...state,
        recommendedDishesLoading: true,
        recommendedDishesData: {},
        recommendedDishesError: null,
      };

    case GET_PROGRAM_DISHED_SUCCESS:
      return {
        ...state,
        recommendedDishesLoading: false,
        recommendedDishesData: { ...action.payload },
      };

    case GET_PROGRAM_DISHED_FAILED:
      return {
        ...state,
        recommendedDishesLoading: false,
        recommendedDishesError: action.payload,
      };

    default:
      return state;
  }
};

export default ProgramsReducer;
