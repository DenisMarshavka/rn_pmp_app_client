import {
  GET_STAFF_FAILED,
  GET_STAFF_SCHEDULE_REQUEST_FAILED,
  GET_STAFF_SCHEDULE_REQUEST_START,
  GET_STAFF_SCHEDULE_REQUEST_SUCCESS,
  GET_STAFF_START,
  GET_STAFF_SUCCESS,
  CLEAR_STAFF_DATA,
} from "../actions/StaffActions";

const INITIAL_STATE = {
  staffLoading: false,
  staff: [],
  moreStaffloading: false,
  staffListMeta: {},
  staffError: null,

  scheduleLoading: false,
  schedule: {},
  scheduleError: null,
};

const StaffReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_STAFF_START:
      return {
        ...state,
        staffLoading: !action.payload.isUpdate,
        moreStaffloading: action.payload.isUpdate,
        staffError: null,
      };

    case GET_STAFF_SUCCESS:
      const staff = action.payload.isUpdate
        ? [...state.staff, ...action.payload.list]
        : [...action.payload.list];

      return {
        ...state,
        staffLoading: false,
        staff,
        staffListMeta: { ...action.payload.listMeta },
        moreStaffloading: false,
      };

    case GET_STAFF_FAILED:
      return {
        ...state,
        staffLoading: false,
        staffError: action.payload,
        staffListMeta: {},
        moreStaffloading: false,
      };

    case CLEAR_STAFF_DATA:
      return {
        ...state,
        staffLoading: false,
        staff: [],
        staffError: null,
        staffListMeta: {},
        moreStaffloading: false,
      };

    case GET_STAFF_SCHEDULE_REQUEST_START:
      return {
        ...state,
        scheduleLoading: true,
        scheduleError: null,
      };

    case GET_STAFF_SCHEDULE_REQUEST_SUCCESS:
      return {
        ...state,
        scheduleLoading: false,
        schedule: { ...action.payload },
      };

    case GET_STAFF_SCHEDULE_REQUEST_FAILED:
      return {
        ...state,
        scheduleLoading: false,
        scheduleError: action.payload,
      };

    default:
      return state;
  }
};

export default StaffReducer;
