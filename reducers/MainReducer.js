import {
  GET_GENERATE_PAYMENT_LINK_FAILED,
  GET_GENERATE_PAYMENT_LINK_START,
  GET_GENERATE_PAYMENT_LINK_SUCCESS,
  GET_STATUS_TO_CONSTANTLY_FAILED,
  GET_STATUS_TO_CONSTANTLY_START,
  GET_STATUS_TO_CONSTANTLY_SUCCESS,
  GET_TRAININGS_BALANCE_FAILED,
  GET_TRAININGS_BALANCE_START,
  GET_TRAININGS_BALANCE_SUCCESS,
  GET_ABOUT_US_START,
  GET_ABOUT_US_SUCCESS,
  GET_ABOUT_US_FAILED,
  SET_COMPANY_REVIEW_START,
  SET_COMPANY_REVIEW_SUCCESS,
  SET_COMPANY_REVIEW_FAILED,
  // GET_TRAININGS_BALANCE_ONLINE_START,
  // GET_TRAININGS_BALANCE_ONLINE_SUCCESS,
  // GET_TRAININGS_BALANCE_ONLINE_FAILED,
} from "../actions";

const generateBalanceData = (
  payloadCurrentBalance = {},
  isOnlineBalance = false
) => {
  let trainingsBalanceList = [];
  let serviceTypes = [];
  let trainingsBalanceData = [];

  if (
    payloadCurrentBalance &&
    Object.keys(payloadCurrentBalance).length &&
    payloadCurrentBalance.trainingsBalanceList &&
    payloadCurrentBalance.serviceTypes
  ) {
    trainingsBalanceList = [...payloadCurrentBalance.trainingsBalanceList];
    serviceTypes = [...payloadCurrentBalance.serviceTypes];

    trainingsBalanceData = trainingsBalanceList.map((item, i) => {
      const indexType = serviceTypes.findIndex(
        (type) => type.id === item.trainerServiceTypeId
      );

      if (indexType >= 0) {
        item.title =
          serviceTypes &&
          serviceTypes[indexType] &&
          serviceTypes[indexType].type
            ? serviceTypes[indexType].type
            : "";

        if (isOnlineBalance) item.title += " Online";

        return item;
      } else return { id: Date.now() };
    });
  }

  return trainingsBalanceData.reverse();
};

const UPDATE_TAB = "UPDATE-TAB";

const INITIAL_STATE = {
  activeTab: 0,

  paymentLinkLoading: false,
  paymentLink: "",
  paymentLinkError: null,

  constantlyStatusLoading: true,
  constantlyStatus: [],
  maxValue: 0,
  constantlyStatusFailed: null,

  trainingsBalanceLoading: true,
  trainingsBalanceData: [],
  trainingsBalanceFailed: null,

  // trainingsBalanceOnlineLoading: false,
  // trainingsBalanceOnlineData: [],
  // trainingsBalanceOnlineFailed: null,

  notificationsLoading: false,
  notifications: 0,
  notificationsFailed: null,

  aboutUsLoading: false,
  aboutUsInfo: {},
  aboutUsError: null,

  companyReviewLoading: false,
  companyReviewError: null,
  companyReviewSuccess: false,
};

const MainReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_TAB:
      return {
        ...state,
        activeTab: action.data,
      };

    // case GET_TRAININGS_BALANCE_ONLINE_START:
    //   return {
    //     ...state,
    //     trainingsBalanceOnlineLoading: true,
    //     trainingsBalanceOnlineFailed: null,
    //   };
    //
    // case GET_TRAININGS_BALANCE_ONLINE_SUCCESS:
    //   return {
    //     ...state,
    //     trainingsBalanceOnlineLoading: false,
    //     trainingsBalanceOnlineData: generateBalanceData(action.payload, true),
    //   };
    //
    // case GET_TRAININGS_BALANCE_ONLINE_FAILED:
    //   return {
    //     ...state,
    //     trainingsBalanceOnlineLoading: false,
    //     trainingsBalanceOnlineFailed: action.payload,
    //   };

    case GET_TRAININGS_BALANCE_START:
      return {
        ...state,
        trainingsBalanceLoading: true,
        trainingsBalanceFailed: null,
      };

    case GET_TRAININGS_BALANCE_SUCCESS:
      // const trainingsBalanceList = [...action.payload.trainingsBalanceList];
      // const serviceTypes = [...action.payload.serviceTypes];
      //
      // const trainingsBalanceData = trainingsBalanceList.map((item, i) => {
      //   const indexType = serviceTypes.findIndex(
      //     (type) => type.id === item.trainerServiceTypeId
      //   );
      //
      //   if (indexType >= 0) {
      //     item.title =
      //       serviceTypes &&
      //       serviceTypes[indexType] &&
      //       serviceTypes[indexType].type
      //         ? serviceTypes[indexType].type
      //         : "";
      //
      //     return item;
      //   } else return { id: Date.now() };
      // });

      return {
        ...state,
        trainingsBalanceLoading: false,
        trainingsBalanceData: generateBalanceData(action.payload),
      };

    case GET_TRAININGS_BALANCE_FAILED:
      return {
        ...state,
        trainingsBalanceLoading: false,
        trainingsBalanceFailed: action.payload,
      };

    case GET_STATUS_TO_CONSTANTLY_START:
      return {
        ...state,
        constantlyStatusLoading: true,
        constantlyStatusFailed: null,
      };

    case GET_STATUS_TO_CONSTANTLY_SUCCESS:
      return {
        ...state,
        constantlyStatusLoading: false,
        constantlyStatus: action.payload,
        maxValue: action.maxValue,
      };

    case GET_STATUS_TO_CONSTANTLY_FAILED:
      return {
        ...state,
        constantlyStatusLoading: false,
        constantlyStatusFailed: action.payload,
      };

    case GET_GENERATE_PAYMENT_LINK_START:
      return {
        ...state,
        paymentLinkLoading: true,
        paymentLink: "",
        paymentLinkError: null,
      };

    case GET_GENERATE_PAYMENT_LINK_SUCCESS:
      return {
        ...state,
        paymentLinkLoading: false,
        paymentLink: action.payload,
      };

    case GET_GENERATE_PAYMENT_LINK_FAILED:
      return {
        ...state,
        paymentLinkLoading: false,
        paymentLinkError: action.payload,
      };

    case GET_ABOUT_US_START:
      return {
        ...state,
        aboutUsLoading: true,
        aboutUsError: null,
      };

    case GET_ABOUT_US_SUCCESS:
      return {
        ...state,
        aboutUsLoading: false,
        aboutUsInfo: { ...action.payload },
      };

    case GET_ABOUT_US_FAILED:
      return {
        ...state,
        aboutUsLoading: false,
        aboutUsError: action.payload,
      };

    case SET_COMPANY_REVIEW_START:
      return {
        ...state,
        companyReviewError: false,
        companyReviewLoading: true,
        companyReviewSuccess: false,
      };

    case SET_COMPANY_REVIEW_SUCCESS:
      return {
        ...state,
        companyReviewError: false,
        companyReviewLoading: false,
        companyReviewSuccess: true,
      };

    case SET_COMPANY_REVIEW_FAILED:
      return {
        ...state,
        companyReviewError: action.payload,
        companyReviewLoading: false,
        companyReviewSuccess: false,
      };

    default:
      return state;
  }
};

export const updateTab = (data) => ({ type: UPDATE_TAB, data });

export default MainReducer;
