import { FETCH_SCHEDULE_START, FETCH_SCHEDULE_SUCCESS, FETCH_SCHEDULE_ERROR } from "../actions/schedule";

const initialState = {
    loading: false,
    schedule: [],
    error: null,
};

export const ScheduleReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SCHEDULE_START:
            return {
                ...state,
                loading: true,
            };

        case FETCH_SCHEDULE_SUCCESS:
            return {
                ...state,
                loading: false,
                schedule: [...action.payload],
                error: null,
            };

        case FETCH_SCHEDULE_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload,
            };

        default:
            return state;
    }
};
