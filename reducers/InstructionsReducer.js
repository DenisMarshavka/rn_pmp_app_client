import {
  GET_APP_INSTRUCTION_START,
  GET_APP_INSTRUCTION_SUCCESS,
  GET_APP_INSTRUCTION_FAILED,
} from "../actions/InstructionsActions";

const INITIAL_STATE = {
  appInstruction: [],
  appInstructionLoading: true,
  appInstructionFailed: null,
};

const InstructionsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_APP_INSTRUCTION_START:
      return {
        ...state,
        appInstructionLoading: true,
        appInstructionFailed: null,
      };

    case GET_APP_INSTRUCTION_SUCCESS:
      return {
        ...state,
        appInstruction: [...action.payload],
        appInstructionLoading: false,
      };

    case GET_APP_INSTRUCTION_FAILED:
      return {
        ...state,
        appInstructionLoading: false,
        appInstructionFailed: action.payload,
      };

    default:
      return state;
  }
};

export default InstructionsReducer;
