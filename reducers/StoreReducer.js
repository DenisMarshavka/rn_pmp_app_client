import {
  GET_INVITES_BY_PARAMS_FAILED,
  GET_INVITES_BY_PARAMS_START,
  GET_INVITES_BY_PARAMS_SUCCESS,
  GET_PRODUCTS_BY_PARAMS_FAILED,
  GET_PRODUCTS_BY_PARAMS_START,
  GET_PRODUCTS_BY_PARAMS_SUCCESS,
  CLEAN_DATA_PRODUCTS_BY_PARAMS,
  GET_PRODUCTS_CATEGORIES_FAILED,
  GET_PRODUCTS_CATEGORIES_START,
  GET_PRODUCTS_CATEGORIES_SUCCESS,
  GET_TOP_PRODUCTS_START,
  GET_TOP_PRODUCTS_SUCCESS,
  GET_TOP_PRODUCTS_FAILED,
} from "../actions/StoreActions";

export const GET_FAST_CHECKOUT_SUCCESS = "GET_FAST_CHECKOUT_SUCCESS";
export const GET_FAST_CHECKOUT_FAILED = "GET_FAST_CHECKOUT_FAILED";

const INITIAL_STATE = {
  productsCategoriesLoading: true,
  productsCategories: [],
  productsCategoriesError: null,
  productsListMeta: {},
  moreProductsLoading: false,

  topProductsLoading: true,
  topProductsError: null,
  topProducts: [],

  invitesLoading: false,
  invites: [],
  invitesError: null,

  productsLoading: false,
  products: [],
  productsError: null,

  topProductsLoading: true,
  topProductsError: null,
  topProducts: [],
};

const StoreReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_PRODUCTS_CATEGORIES_START:
      return {
        ...state,
        productsCategoriesLoading: true,
        productsCategories: [],
        productsCategoriesError: null,
      };

    case GET_PRODUCTS_CATEGORIES_SUCCESS:
      return {
        ...state,
        productsCategoriesLoading: false,
        productsCategories: [...action.payload],
      };

    case GET_PRODUCTS_CATEGORIES_FAILED:
      return {
        ...state,
        productsCategoriesLoading: false,
        productsCategoriesError: action.payload,
      };

    case GET_INVITES_BY_PARAMS_START:
      return {
        ...state,
        invitesLoading: true,
        invitesError: null,
        invites: [],
      };

    case GET_INVITES_BY_PARAMS_SUCCESS:
      return {
        ...state,
        invitesLoading: false,
        invites: [...action.payload],
      };

    case GET_INVITES_BY_PARAMS_FAILED:
      return {
        ...state,
        invitesLoading: false,
        invitesError: action.payload,
      };

    case GET_TOP_PRODUCTS_START:
      return {
        ...state,
        topProductsLoading: true,
        topProductsError: null,
        topProducts: [],
      };

    case GET_TOP_PRODUCTS_SUCCESS:
      return {
        ...state,
        topProductsLoading: false,
        topProducts: [...action.payload],
      };

    case GET_TOP_PRODUCTS_FAILED:
      return {
        ...state,
        topProductsLoading: false,
        topProductsError: action.payload,
      };

    case GET_PRODUCTS_BY_PARAMS_START:
      return {
        ...state,
        productsLoading: !action.payload,
        productsError: null,
        moreProductsLoading: action.payload,
      };

    case GET_PRODUCTS_BY_PARAMS_SUCCESS:
      const products = action.payload.isMore
        ? [...state.products, ...action.payload.list]
        : [...action.payload.list];

      return {
        ...state,
        productsLoading: false,
        moreProductsLoading: false,
        products,
        productsListMeta: { ...action.payload.listMeta },
      };

    case GET_PRODUCTS_BY_PARAMS_FAILED:
      return {
        ...state,
        productsLoading: false,
        productsError: action.payload,
        moreProductsLoading: false,
      };

    case CLEAN_DATA_PRODUCTS_BY_PARAMS:
      return {
        ...state,
        productsLoading: false,
        productsError: null,
        products: [],
        productsListMeta: {},
        moreProductsLoading: false,
      };

    default:
      return state;
  }
};

export default StoreReducer;
