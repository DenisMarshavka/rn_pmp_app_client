import {
  CREATE_REVIEW_FAILED,
  CREATE_REVIEW_START,
  CREATE_REVIEW_SUCCESS,
  GET_ALL_REVIEWS_FAILED,
  GET_ALL_REVIEWS_START,
  GET_ALL_REVIEWS_SUCCESS,
  GET_COMPANY_REVIEWS_START,
  GET_COMPANY_REVIEWS_MORE,
  GET_COMPANY_REVIEWS_SUCCESS,
  GET_COMPANY_REVIEWS_FAILED,
  CLEAN_COMPANY_REVIEWS,
} from "../actions/ReviewsActions";

const INITIAL_STATE = {
  reviewsLoading: false,
  reviews: [],
  countTrainersReviewsItems: 0,

  companyReviewsLoading: false,
  companyReviews: [],
  companyReviewsError: null,
  companyReviewsMoreLoading: false,
  companyReviewsMeta: null,

  createReviewLoading: false,
  createReviewsError: null,
};

const ServicesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CREATE_REVIEW_START:
      return {
        ...state,
        createReviewLoading: true,
        createReviewsError: null,
      };

    case CREATE_REVIEW_SUCCESS:
      return {
        ...state,
        createReviewLoading: false,
      };

    case CREATE_REVIEW_FAILED:
      return {
        ...state,
        createReviewLoading: false,
        createReviewError: action.payload,
      };

    case GET_ALL_REVIEWS_START:
      return {
        ...state,
        reviewsLoading: true,
        countTrainersReviewsItems: 0,
        reviewsError: null,
      };

    case GET_ALL_REVIEWS_SUCCESS:
      return {
        ...state,
        reviewsLoading: false,
        reviews: [...action.payload.list],
        countTrainersReviewsItems: action.payload.countItems,
      };

    case GET_ALL_REVIEWS_FAILED:
      return {
        ...state,
        reviewsLoading: false,
        reviewsError: action.payload,
      };

    case GET_COMPANY_REVIEWS_START:
      return {
        ...state,
        companyReviewsLoading: true,
        companyReviewsError: null,
      };

    case GET_COMPANY_REVIEWS_MORE:
      return {
        ...state,
        companyReviewsMoreLoading: true,
      };

    case GET_COMPANY_REVIEWS_SUCCESS:
      return {
        ...state,
        companyReviewsLoading: false,
        companyReviews: [...state.companyReviews, ...action.payload.data],
        companyReviewsMeta: action.payload.meta,
        companyReviewsMoreLoading: false,
      };

    case GET_COMPANY_REVIEWS_FAILED:
      return {
        ...state,
        companyReviewsLoading: false,
        companyReviewsMoreLoading: false,
        companyReviewsError: action.payload,
      };

    case CLEAN_COMPANY_REVIEWS:
      return {
        ...state,
        companyReviews: [],
        companyReviewsMeta: null,
      };

    default:
      return state;
  }
};

export default ServicesReducer;
