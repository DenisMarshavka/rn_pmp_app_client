import {
  SET_CURRENT_USER,
  GET_CURRENT_USER,
  GET_BOOKMARKS,
  SET_BOOKMARKS,
  GET_PUSHTOKEN,
  SET_PUSHTOKEN,
  // SET_RECEIVED_NOTIFICATION,
} from "../actions";
import {
  DELETE_USER_NOTIFICATIONS_SUCCESS,
  GET_ATTENDANCE_STATISTICS_FAILED,
  GET_ATTENDANCE_STATISTICS_START,
  GET_ATTENDANCE_STATISTICS_SUCCESS,
  GET_CURRENT_POINTS_BY_USER_ID_FAILED,
  GET_CURRENT_POINTS_BY_USER_ID_START,
  GET_CURRENT_POINTS_BY_USER_ID_SUCCESS,
  GET_POINTS_BY_USER_FAILED,
  GET_POINTS_BY_USER_START,
  GET_POINTS_BY_USER_SUCCESS,
  GET_USER_NOTIFICATIONS_FAILED,
  GET_USER_NOTIFICATIONS_START,
  GET_USER_NOTIFICATIONS_SUCCESS,
  CLEAR_USER_NOTIFICATIONS,
  GET_DIALOGS_BY_USER_ID_START,
  GET_DIALOGS_BY_USER_ID_SUCCESS,
  GET_DIALOGS_BY_USER_ID_FAILED,
  SET_COUNT_NOTIFITACIONS,
  SET_COUNT_BASKET,
} from "../actions/UserActions";

const INITIAL_STATE = {
  currentUser: null,
  pushToken: null,
  bookmarks: null,

  notificationsLoading: false,
  notificationsMoreLoading: false,
  notifications: [],
  notificationsError: null,
  notificationsListMeta: {},

  attendanceStatisticsLoading: true,
  attendanceStatistics: [],
  attendanceStatisticsError: null,

  currentPointsLoading: false,
  currentPoints: 0,
  currentPointsFailed: null,

  maxPointsLoading: false,
  maxPoints: 0,
  maxPointsFailed: null,

  dialogsLoading: false,
  dialogs: [],
  dialogsCountItems: 0,
  dialogsError: null,

  notificationsCount: 0,
  basketCount: 0,

  // existingNotificationOpened: {},
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    // case SET_RECEIVED_NOTIFICATION:
    //   return {
    //     ...state,
    //     existingNotificationOpened: { ...action.payload },
    //   };

    case SET_COUNT_NOTIFITACIONS:
      return {
        ...state,
        notificationsCount: action.payload.count,
      };

    case SET_COUNT_BASKET:
      return {
        ...state,
        basketCount: action.payload.count,
      };

    case GET_DIALOGS_BY_USER_ID_START:
      return {
        ...state,
        dialogsLoading: true,
        dialogs: [],
        dialogsCountItems: 0,
        dialogsError: null,
      };

    case GET_DIALOGS_BY_USER_ID_SUCCESS:
      return {
        ...state,
        dialogsLoading: false,
        dialogs: [...action.payload.list],
        dialogsCountItems: action.payload.countItems,
      };

    case GET_DIALOGS_BY_USER_ID_FAILED:
      return {
        ...state,
        dialogsLoading: false,
        dialogsError: action.payload,
      };

    case SET_CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload,
      };

    case SET_PUSHTOKEN:
      return {
        ...state,
        pushToken: action.payload,
      };

    case GET_PUSHTOKEN:
      return {
        currentUser: state.currentUser,
        pushToken: state.pushToken,
      };

    case GET_BOOKMARKS:
      return state;

    case SET_BOOKMARKS:
      return {
        ...state,
        bookmarks: action.payload,
      };

    case GET_CURRENT_USER:
      return state;

    case GET_CURRENT_POINTS_BY_USER_ID_START:
      return {
        ...state,
        currentPointsLoading: true,
        currentPointsFailed: null,
      };

    case GET_CURRENT_POINTS_BY_USER_ID_SUCCESS:
      return {
        ...state,
        currentPointsLoading: false,
        currentPoints: action.payload,
      };

    case GET_CURRENT_POINTS_BY_USER_ID_FAILED:
      return {
        ...state,
        currentPointsLoading: false,
        currentPointsFailed: action.payload,
      };

    case GET_ATTENDANCE_STATISTICS_START:
      return {
        ...state,
        attendanceStatisticsLoading: true,
        attendanceStatistics: [],
        attendanceStatisticsError: null,
      };

    case GET_ATTENDANCE_STATISTICS_SUCCESS:
      return {
        ...state,
        attendanceStatisticsLoading: false,
        attendanceStatistics: { ...action.payload },
      };

    case GET_ATTENDANCE_STATISTICS_FAILED:
      return {
        ...state,
        attendanceStatisticsLoading: false,
        attendanceStatisticsError: action.payload,
      };

    case DELETE_USER_NOTIFICATIONS_SUCCESS:
      return state;

    case GET_USER_NOTIFICATIONS_START:
      return {
        ...state,
        notificationsLoading: !action.payload.isMore,
        notificationsMoreLoading: action.payload.isMore,
        notificationsError: null,
      };

    case GET_USER_NOTIFICATIONS_SUCCESS:
      const notifications = action.payload.isMore
        ? [...state.notifications, ...action.payload.list]
        : [...action.payload.list];

      return {
        ...state,
        notificationsLoading: false,
        notificationsMoreLoading: false,
        notifications,
        notificationsListMeta: { ...action.payload.listMeta },
      };

    case CLEAR_USER_NOTIFICATIONS:
      return {
        ...state,
        notificationsLoading: false,
        notifications: [],
        notificationsError: null,
        notificationsListMeta: {},
        notificationsMoreLoading: false,
      };

    case GET_USER_NOTIFICATIONS_FAILED:
      return {
        ...state,
        notificationsLoading: false,
        notificationsError: action.payload,
        notificationsListMeta: {},
        notificationsMoreLoading: false,
      };

    case GET_POINTS_BY_USER_START:
      return {
        ...state,
        maxPointsLoading: true,
        maxPointsFailed: null,
      };

    case GET_POINTS_BY_USER_SUCCESS:
      return {
        ...state,
        maxPointsLoading: false,
        maxPoints: action.payload,
      };

    case GET_POINTS_BY_USER_FAILED:
      return {
        ...state,
        maxPointsLoading: false,
        maxPointsFailed: action.payload,
      };

    default:
      return state;
  }
};

export default userReducer;
