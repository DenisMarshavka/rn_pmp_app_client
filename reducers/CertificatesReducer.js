import {
  GET_CERTIFICATE_FAILED,
  GET_CERTIFICATE_START,
  GET_CERTIFICATE_SUCCESS,
  GET_CERTIFICATES_DATA_RESET,
  GET_CERTIFICATES_FAILED,
  GET_CERTIFICATES_START,
  GET_CERTIFICATES_SUCCESS,
} from "../actions/CertificatesAction";

const INITIAL_STATE = {
  certificatesLoading: false,
  certificates: [],
  countItems: 0,
  certificatesFailed: null,

  certificateLoading: false,
  certificate: {},
  certificateFailed: null,
};

const CertificatesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_CERTIFICATES_DATA_RESET:
      return {
        ...state,
        certificatesLoading: false,
        certificates: [],
        certificatesFailed: null,

        certificateLoading: false,
        certificate: [],
        certificateFailed: null,
      };

    case GET_CERTIFICATE_START:
      return {
        ...state,
        certificateLoading: true,
        certificate: [],
        certificateFailed: null,
      };

    case GET_CERTIFICATE_SUCCESS:
      return {
        ...state,
        certificateLoading: false,
        certificate: { ...action.payload },
      };

    case GET_CERTIFICATE_FAILED:
      return {
        ...state,
        certificateLoading: false,
        certificateFailed: action.payload,
      };

    case GET_CERTIFICATES_START:
      return {
        ...state,
        certificatesLoading: true,
        certificates: [],
        certificateFailed: null,
        countItems: 0,
      };

    case GET_CERTIFICATES_SUCCESS:
      return {
        ...state,
        certificatesLoading: false,
        certificates: [...action.payload.list],
        countItems: action.payload.countItems,
      };

    case GET_CERTIFICATES_FAILED:
      return {
        ...state,
        certificatesLoading: false,
        certificatesFailed: action.payload,
      };
    default:
      return state;
  }
};

export default CertificatesReducer;
