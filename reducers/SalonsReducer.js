const UPDATE_SALONS = 'UPDATE-SALONS';
const UPDATE_ACTIVE_SALONS = 'UPDATE-ACTIVE-SALONS';
  
  const INITIAL_STATE = {
    activeSalon: 0,
    salons: []
  };
  
  const SalonsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case UPDATE_SALONS:
        return {
          ...state,
          salons: action.data
        }
        case UPDATE_ACTIVE_SALONS:
        return {
          ...state,
          activeSalon: action.id
        }
      default: return state
    }
  };

  export const updateSalons = (data) => ({type: UPDATE_SALONS, data});
  export const updateActiveSalon = (id) => ({type: UPDATE_ACTIVE_SALONS, id});
  
  export default SalonsReducer;
  