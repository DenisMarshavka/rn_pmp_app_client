import { GET_NEWS_REQUEST_START, GET_NEWS_REQUEST_ERROR, GET_NEWS_REQUEST_SUCCESS } from "../actions/news";

const initialState = {
    loading: false,
    news: [],
    error: null,
};

export const NewsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_NEWS_REQUEST_START:
            return {
                ...state,
                loading: true,
            };

        case GET_NEWS_REQUEST_SUCCESS:
            return {
                ...state,
                loading: false,
                news: [...action.payload],
                error: null,
            };

        case GET_NEWS_REQUEST_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload,
            };

        default:
            return state;
    }
};
