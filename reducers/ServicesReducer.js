import {
  GET_COMPANY_ID_AND_STAFF_ID_FAILED,
  GET_COMPANY_ID_AND_STAFF_ID_SUCCESS,
  GET_SERVICES_BY_TYPE_QUERY_START,
  GET_SERVICES_BY_TYPE_REQUEST_FAILED,
  GET_SERVICES_BY_TYPE_REQUEST_SUCCESS,
  CLEAN_SERVICES_BY_TYPE,
  GET_SERVICES_TYPES_REQUEST_FAILED,
  GET_SERVICES_TYPES_REQUEST_START,
  GET_SERVICES_TYPES_REQUEST_SUCCESS,
  GET_EXPERT_SERVICES_START,
  GET_EXPERT_SERVICES_SUCCESS,
  GET_EXPERT_SERVICES_FAILED,
  GET_SERVICES_DATA_RESET,
} from "../actions/ServicesActions";

const INITIAL_STATE = {
  serviceTypesLoading: false,
  serviceTypes: [],
  serviceTypesError: null,

  serviceLoading: false,
  services: [],
  serviceError: null,

  companyIdAndStaffIdLoading: true,
  companyIdAndStaffIdData: {},
  companyIdAndStaffIdError: null,

  expertServicesLoading: true,
  expertServices: [],
  expertServicesError: null,
};

const ServicesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_SERVICES_DATA_RESET:
      return {
        ...state,
        services: [],
        serviceTypes: [],
        expertServices: [],
        serviceLoading: true,
        serviceTypesLoading: true,
        expertServicesLoading: true,
      };

    case GET_SERVICES_TYPES_REQUEST_START:
      return {
        ...state,
        serviceTypesLoading: true,
        serviceLoading: true,
        serviceTypesError: null,
      };

    case GET_SERVICES_TYPES_REQUEST_SUCCESS:
      return {
        ...state,
        serviceTypesLoading: false,
        serviceTypes: [...action.payload],
      };

    case GET_SERVICES_TYPES_REQUEST_FAILED:
      return {
        ...state,
        serviceTypesLoading: false,
        serviceTypesError: action.payload,
      };

    case GET_SERVICES_BY_TYPE_QUERY_START:
      return {
        ...state,
        serviceLoading: true,
        services: [],
        serviceError: null,
      };

    case GET_SERVICES_BY_TYPE_REQUEST_SUCCESS:
      return {
        ...state,
        serviceLoading: false,
        services: [...action.payload],
      };

    case GET_SERVICES_BY_TYPE_REQUEST_FAILED:
      return {
        ...state,
        serviceLoading: false,
        serviceError: action.payload,
      };

    case GET_COMPANY_ID_AND_STAFF_ID_SUCCESS:
      return {
        ...state,
        companyIdAndStaffIdLoading: false,
        companyIdAndStaffIdData: { ...action.payload },
        companyIdAndStaffIdError: null,
      };

    case GET_COMPANY_ID_AND_STAFF_ID_FAILED:
      return {
        ...state,
        companyIdAndStaffIdLoading: false,
        companyIdAndStaffIdError: action.payload,
      };

    case GET_EXPERT_SERVICES_START:
      return {
        ...state,
        expertServicesLoading: true,
        expertServicesError: null,
      };

    case GET_EXPERT_SERVICES_SUCCESS:
      return {
        ...state,
        expertServicesLoading: false,
        expertServices: [...action.payload],
        expertServicesError: null,
      };

    case GET_EXPERT_SERVICES_FAILED:
      return {
        ...state,
        expertServicesLoading: false,
        expertServicesError: action.payload,
      };

    default:
      return state;
  }
};

export default ServicesReducer;
